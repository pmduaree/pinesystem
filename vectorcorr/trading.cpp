#include <iostream>

#include <unordered_map>
#include <chrono>
#include "asset.hpp"
#include <math.h>
#include "common.hpp"
#include "vectorCorr.hpp"
#include "seriesCol.hpp"
#include "prog_opts.hpp"
int main(int argc, char** argv)
{
    prog_opts parser;
    parser.add_option("-n", OptionType::Float, "n_std", 0.9);
    parser.add_option("-m", OptionType::Float, "m_std", 0.75);
    parser.add_option("-b", OptionType::Integer, "binSize", 3);
    parser.add_option("-k", OptionType::Integer, "VARmodel_k", 1);
    parser.add_option("-q", OptionType::Integer, "freq", 10);
    parser.add_option<std::string>("--from", OptionType::String, "date_from", "");
    parser.add_option<std::string>("--to", OptionType::String, "date_to", "");
    

    parser.parse_args(argc,argv);
    double n_std=parser.option<double>("n_std");
    double m_std=parser.option<double>("m_std");
    size_t binSize=parser.option<size_t>("binSize");
    size_t VARmodel_k=parser.option<size_t>("VARmodel_k");
    int q=parser.option<int>("freq");
    std::string date_from = parser.option<>("date_from");
    std::string date_to = parser.option<>("date_to");

    
    auto conn = std::make_shared<sql_connection>();
    auto last_2month = std::chrono::hours(24 * 30 * 2);
    //auto last_2month = std::chrono::hours(24 * 15 );

    
    
    
    
    
    tiempo::datetime date_2, date_1;
    if (date_to.empty())
        date_2.from_string("now");
    else
        date_2.from_string(date_to);

    if (date_from.empty())
        date_1 = date_2 - std::chrono::hours(24 * 5);
    else 
        date_1.from_string(date_from);
    
std::cout << "dates 1 = " << date_1.to_string() << " 2= " << date_2.to_string()<<std::endl;    
    
    std::vector<std::string> names={{"AZN","PFE","DRGS","LLY"}};
    std::vector<TixSeries> ticks;
    for(const auto& asset_name : names)
    {
        auto asset = std::make_shared<Asset>(conn, asset_name);
        asset->init(last_2month);
        auto tix = asset->get_ticks();
        while(tix.rbegin() != tix.rend() && tix.rbegin()->first > date_1) tix.erase(tix.rbegin()->first);
        ticks.emplace_back(tix);
    }
   

    if (ticks.empty()) {
        std::cerr << "Somethning wrong, no assets.\n";
        return 0;
    }
    //std::cout<<ticks.size()<<std::endl;
    tix_series_collection ts;
    for(const auto& tt: ticks) {
        ts.push_back(tt);
    }
    ts.align();
    ts.generateSampleTimes();
    
    std::cout << "ts.size: " << ts.size() << ", num samples: " << ts.numSamples() << std::endl;
    std::cout << "We have everything, ts.size: " << ts.size() << ", num samples: " << ts.numSamples()<<std::endl;
    //prep data
    assets assetsData(ts, names);
    ///console outputs
    prn(assetsData.titles,"InputAssets");
    prn(assetsData.size(),"Input_size");
    prn(binSize);
    ///do everything
    collection<portfolio,assets> population(binSize,assetsData,VARmodel_k);
   //console outputs
    std::cout<<std::endl;
    const auto& BestM = population.members[0];
        double stdd=BestM.stdDev;
            double MB=BestM.mean;
    prn(population.size(),"popSize");
    prn(population.members[0].assetsNames,"bestCombination");
    prn(population.members[0].dataIndices,"bestCombination indices");
    prn(population.members[0].R,"traceTestStat");
    prn(population.members[0].Rconf,"ConfidenceLevel");
    prn(BestM.w ,"weights");
    prn(stdd,"std of portfolio series");
    prn(MB ,"Mean value of portfolio series");
    
    std::string exit= (boost::format("binsize=%u,meanVP=%f,bestComb=")% binSize % MB ).str();
    
    for (unsigned int i =0; i!=binSize ; i++ ){
        exit+= population.members[0].assetsNames[i];
        
        if ( i < binSize -1   )
            exit += "-";
    }
    
    exit +="bestComIndx=";
    for (unsigned int i =0; i!=binSize ; i++ ){
        exit+= std::to_string(population.members[0].dataIndices[i]);
        
        if ( i < binSize -1   )
            exit += "-";
    }
    
    
    std::cout << "e= " << exit << std::endl;
   
   std::unordered_map<int, std::shared_ptr<Asset> > all_assets;
   
   for(int unsigned i=0;i<BestM.assetsNames.size();++i){
       auto asset=std::make_shared<Asset>(conn,BestM.assetsNames[i]);
       auto how_far_back_from_now = std::chrono::hours(24 * 60);
       asset->init(how_far_back_from_now);
       all_assets[i]=asset;
   }

   auto calc_PPP = [&all_assets,MB,stdd,&BestM](const tiempo::datetime& date) -> std::tuple<double,double>
   {
       double Port=0.0;
       for (auto& x: all_assets){
           const auto  &tix=x.second->get_ticks();
           auto it=tix.rbegin();
           while (it != tix.rend() && it->first > date) ++it;
           if (it == tix.rend()) { 
               std::cerr << "NO DATA... Quitting." << std::endl;
               return std::make_tuple(0.0, -1.0);
           }
           Port+=BestM.w[x.first] * it->second.p;
       }
       double prob = 0.5 - 0.5 * erf((Port-MB)/(stdd * sqrt(2)));
       return std::make_tuple(Port, prob);
   };
    std::cout << boost::format("%19s %8s %8s %8s %6s %7s  %7s %7s")
                        %"date @ time" % "mean" % "std dev" % "cur_p" % "p_buy" % "status" % "diff" % "profit" << std::endl;
    
    
    
    double n = std::min(n_std,m_std);
    double m = std::max(n_std,m_std);
    if(n == m) throw::runtime_error("wrong::m=n");
    auto date = date_1;
    int position = 0;
    std::string status=" open";
    double diff=0.0;
    double profit = 0.0;
    double total_profit=0.0;
    date_2 = date_1 + std::chrono::hours(5);
    
    while(date < date_2) {
        double P_cur, Pbuy_cur;
        std::tie(P_cur, Pbuy_cur) = calc_PPP(date);
        if (Pbuy_cur < 0 || Pbuy_cur > 1) {
            std::cerr << "sth wrong" << std::endl;
            // thing about what to do. maybe skip
            date += std::chrono::minutes(q);
            continue;
        }
        if (position == 0)//current no position
        { if(Pbuy_cur > m){
        position+=1;
        status=" long";
        diff-=P_cur;
        profit=0.0;
        }//go long
         else if(Pbuy_cur < (1.0-m)) {
         position-=1;
         status="short";
         diff+=P_cur;
         profit=0.0;
         }//go short
         else {
         status=" wait";
         profit=0.0;
         }
         }
        else if(position == 1)//current long
        {
        if(Pbuy_cur<n) {
        position-=1;
        status="close";
        profit=diff+P_cur;
        diff=0.0;
        }// close
        else{
        status=" hold";
        profit=0.0;
        }
        }
         else if (position ==-1)//current short
        {
        if(Pbuy_cur>(1.0-n)){
        position+=1;
        status="close";
        profit= diff-P_cur;
        diff=0.0;
        }//close
        else {
        status=" hold";
        profit=0.0;
        }
        }
     std::cout << boost::format("%19s %8.4f %8.4f %8.4f %6.4f %7s  %7.3f %7.3f")
          %date.to_string() % MB % stdd % P_cur % Pbuy_cur % status % diff % profit << std::endl;
       //std::cout<<P_cur<<std::endl;

        date += std::chrono::minutes(q);
        total_profit+=profit/MB;
    }
  
    std::cout << boost::format("Total mean profit: %7.3f%%")%(100*total_profit) << std::endl;


    return 0;

}




