/*
 //
 //  Multiple_Signaller.cpp
 //
 //
 //  Created by David de la Rosa on 7/25/14.
 //  // Kalman filer by Juan Castillo
 //
Kalman filter Data Filler. 


using the previous chunk of data try to infer the 
values of the Assets that are missing

made for a fixed interval 


*/

#include <armadillo>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include "../trunk/SQL/sql_connection.hpp"
#include "../trunk/Tiempo/tiempo.hpp"
#include "../trunk/Utils/prog_opts.hpp"
#include "../trunk/KalmanFiller.hpp"
#include <boost/format.hpp>


void to_Log(arma::mat &m )
{
    
    int N = m.n_rows;
    int M = m.n_cols;
    
    for (int i = 0  ;  i != N  ; i ++)
        for (int  j = 0 ; j!=  M ; j++  )
        {
            double value  = m(i,j);
            if(value!=0)
                m(i,j)  = log(value);
            else
                m(i,j)=0.0;
        }
}
void to_Exp(arma::mat &m )
{
    int N = m.n_rows;
    int M = m.n_cols;
    for (int i = 0  ;  i != N  ; i ++)
        for (int  j = 0 ; j!=  M ; j++  )
        {
            double value  = m(i,j);
            if(value>0)
                m(i,j)  = exp(value);
            else
                m(i,j)=0.0;
        }
}
void print_mat(arma::mat &m ,tiempo::datetime date_back )
{
    
    int N = m.n_rows;
    int M = m.n_cols;
    
    for (int i = 0  ;  i != N  ; i ++){
        tiempo::datetime Now = date_back + std::chrono::minutes(i );
        std::cout << (boost::format("[%d] ")%  Now.to_string()   ).str() ;
        for (int  j = 0 ; j!=  M ; j++  )
            std::cout << (boost::format("%0.2f \t" ) % m(i,j) ).str() ;
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void print_mat_exp(arma::mat &m,tiempo::datetime date_back )
{

    int N = m.n_rows;
    int M = m.n_cols;
    
    for (int i = 0  ;  i != N  ; i ++){
        tiempo::datetime Now = date_back + std::chrono::minutes(i );
        std::cout << (boost::format("[%d] ")%  Now.to_string()   ).str() ;
        for (int  j = 0 ; j!=  M ; j++  )
            std::cout << (boost::format("%0.2f \t" ) % exp(m(i,j) ) ).str() ;
        std::cout << std::endl;
    }std::cout << std::endl;
}

class KalmanFiller
{
private:
    std::shared_ptr<sql_connection> conn;
    int TimeWindow;
    int portfolio_id;
    tiempo::datetime date_back;
    std::vector<int> Assets_id;

    void Load_info()
    {   Assets_id.clear();
        conn->lock();
        std::shared_ptr<sql::Connection> con = conn->connection();
        std::string query = (boost::format("SELECT instrument_id FROM `Main`.`Portfolio` WHERE `portfolio_id`=%d ORDER BY instrument_id") 
                                % portfolio_id ).str();
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        while(res->next()) {
            
            Assets_id.push_back(  res->getInt("instrument_id")  );
        }
        conn->unlock();
    }
    
    double Fetch_data (int Minutes_Fwd, int asset_id  )
    {
        
        conn->lock();
        tiempo::datetime date_fetch = date_back + std::chrono::minutes(Minutes_Fwd);
        std::shared_ptr<sql::Connection> con = conn->connection();
        std::string query = (boost::format(" \
                        SELECT close FROM \
                        (\
                            SELECT date, close FROM Main.Candles \
                                WHERE date = '%s'\
                                AND instrument_id = %d\
                                UNION \
                                SELECT date, close FROM Main.CandlesKalman \
                                WHERE date = '%s' \
                                AND instrument_id = %d \
                        ) AS X" )
                                            % date_fetch.to_string("%Y-%m-%d %H:%M:00")
                                            % asset_id
                                            % date_fetch.to_string("%Y-%m-%d %H:%M:00")
                                            % asset_id
                                            ).str();
        
        //std::cout << query << std::endl;
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        if (res->rowsCount() > 0){
            res->first();
            conn->unlock();
            double f = res->getDouble("close");
            std::cout << (boost::format("[%s] form %d instrument = fetch %f") % date_fetch.to_string () %asset_id % f ).str() << std::endl;
            return f;
        }
        conn->unlock();
        std::cout << (boost::format("[%s] %d instrument = fetch NONE") % date_fetch.to_string ()  %asset_id ).str() << std::endl;
        return 0;
    }
    
    void Fill_Data( arma::mat &m )
    {
       for (int i = 0  ;  i != TimeWindow ; i ++)
           for (int unsigned j = 0 ; j!= Assets_id.size(); j++  )
                m(i,j) = Fetch_data(i,Assets_id[j]);
    }
    
    
    void Push_Data (int Minutes_Fwd, int asset_id , double Value  )
    {
        conn->lock();
        tiempo::datetime date_push = date_back +  std::chrono::minutes(Minutes_Fwd);
        
        std::cout << (boost::format("[%s] push value = %f on instrument %d")% date_push.to_string() % Value % asset_id ).str() <<std::endl;
        
        std::shared_ptr<sql::Connection> con = conn->connection();
        std::string query = (boost::format("INSERT INTO `Main`.`CandlesKalman` (`instrument_id`, `date`,`close` ) \
                                           VALUES( %d , '%s',%0.2f   ) \
                                           ON DUPLICATE KEY UPDATE `close` = %0.2f , `instrument_id` = %d , `date` = '%s' " )
                             % asset_id
                             % date_push.to_string("%Y-%m-%d %H:%M:%S")
                             % Value
                             % Value
                             % asset_id
                             % date_push.to_string("%Y-%m-%d %H:%M:%S")
                             ).str();
        
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        //std::cout << query << std::endl;
        
        stmt->execute(query);
        conn->unlock();
    }
    
    void Push_SyntheticCandles( arma::mat &m )
    {
        for (int i = TimeWindow - 120  ;  i != TimeWindow ; i ++)
            for (int unsigned j = 0 ; j!= Assets_id.size(); j++  )
                 Push_Data(i,Assets_id[j], m(i,j) );
        
    }
    
    
    
public:
    KalmanFiller(std::shared_ptr<sql_connection>  con__, int _id,int window_,std::string date_  )
    :conn(con__),TimeWindow(window_),portfolio_id(_id),date_back( tiempo::datetime(date_.c_str())  )
    { Load_info();
    };
    ~KalmanFiller(){};

    void run()
    {
        arma::mat Data(TimeWindow,Assets_id.size());
        Fill_Data(Data);


        //reducir la matriz
        //sobreescribir vector assets_id con el código de Juan
        //w es el vector con los ids de los assets que se usaron en kalman

        int ta = Data.n_rows;
        int n = Data.n_cols;

        arma::umat nocero   = (Data != 0);          //Si hay elemento pone cero               
        arma::umat w=arma::find(sum(nocero) >= (n*0.5));   //vector con los indices de columnas
        int d = w.n_rows;                      //con suficientes datos

        arma::mat inr(ta,d); inr.zeros();
        for (int i=0;i<ta;i++){             //recorre el índice de los renglones
            for(int j=0;j<d;j++){             //índices de columnas con datos
                inr(i,j) = Data(i,w(j));          //suficientes.
            }
        }

        Data = inr;

        std::vector<int> assets_aux;
        assets_aux.clear();
        for(unsigned int i = 0; i < w.n_rows; i++)
            assets_aux.push_back(Assets_id[w(i)]); 
        Assets_id = assets_aux;
        print_mat(Data,date_back);
        //to_Log(Data);
        //print_mat(Data,date_now);
        
        // Do Kalman
        kalmanfilter kalman;
        arma::mat Kal_Filler =  kalman(Data).xsmo;
        //print_mat(Kal_Filler,date_now);
        //to_Exp(Kal_Filler);

        //Ampliar la matriz

        print_mat(Kal_Filler,date_back);
        //push to DB
        Push_SyntheticCandles(Kal_Filler);
    }

};

int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<int>("-p", "--potfolio", OptionType::Integer, "portfolio_id", 0);
    opts.add_option<int>        ("-w"    ,"--TimeWindow", OptionType::Integer, "TimeWindow", 0); //in minutes
    opts.add_option<std::string>("--from", OptionType::String , "date_from", "");
    
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help"))
	{
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    int portfolio_id    = opts.option<int>("portfolio_id");
std::cout << portfolio_id << std::endl;
    int TimeWindow      = opts.option<int>("TimeWindow");
    std::string date_from   = opts.option<>("date_from");
    
    /*
 *
 *
 *Juan para que solo corra una sola vez hay que usar prodccion igual a false
* para que corra infinidad de veces true
 * */

    
    auto conn = std::make_shared<sql_connection>();
    tiempo::datetime date_;
	if (date_from=="")
	{
	  date_ = tiempo::now()- std::chrono::minutes(TimeWindow+1);

	}else
	   {
	      date_ = tiempo::datetime(date_from) - std::chrono::minutes(TimeWindow+1);
	   }


    std::cout << "runing Kalman from " << date_.to_string("%Y-%m-%d %H:%M:00") << " and window " << TimeWindow << std::endl;
    KalmanFiller Kalman(conn,portfolio_id,TimeWindow,date_.to_string("%Y-%m-%d %H:%M:00"));
    Kalman.run();
    //std::this_thread::sleep_for(std::chrono::seconds(10) );


    return 0 ;
}
