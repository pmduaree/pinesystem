#include <iostream>
#include "Vector_Optimizer.hpp"
#include "SQL/sql_connection.hpp"
#include "Utils/prog_opts.hpp"

int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<int>("-s", "--strategy", OptionType::Integer, "strategy_id", 0);
    opts.add_option<std::string>("--from", OptionType::String, "date_from", "");
    opts.add_option<std::string>("--to", OptionType::String, "date_to", "");
    opts.add_option<int>("-M", "--Money", OptionType::Float, "InvestingMoney", 100000);
        opts.add_option<int>("-r", "--resolution", OptionType::Integer, "resolution", 1);
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help"))
	{
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string date_from   = opts.option<>("date_from");
    std::string date_to     = opts.option<>("date_to");
    double InvestingMoney   = opts.option<double>("InvestingMoney");
    int Resolution          = opts.option<int>("resolution");
    int strategy_id         = opts.option<int>("strategy_id");
    
    auto conn  = std::make_shared<sql_connection>();
    std::cout << "Vector Optimization Process, Strategy " << strategy_id <<
    " from=" << date_from <<
    " to="   << date_to <<
    " Money=" << InvestingMoney <<
    " Resolution=" << Resolution << std::endl;
    
    Vector_Optimizer Optimizier(conn,strategy_id);
    std::cout<<  Optimizier.to_string() << std::endl;
    Optimizier.Run(date_from,date_to,InvestingMoney,Resolution);
}
