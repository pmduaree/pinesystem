#include <iostream>

#include "Pseudo_Trader.hpp"
#include "../trunk/Utils/prog_opts.hpp"
int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<int>("-s", "--strategy", OptionType::Integer, "strategy_id", 0);
    opts.add_option<int>("-M", "--Money", OptionType::Integer, "money", 10000);
    opts.add_option<std::string>("--from", OptionType::String, "date_from", "");
    opts.add_option<std::string>("--to", OptionType::String, "date_to", "");
    opts.add_option<std::string>("--c" ,OptionType::String,"CalculateOnTheFly","yes");
    opts.add_option<std::string>("--DB",OptionType::String,"StoreInDB","no");
    opts.add_option<std::string>("--input",OptionType::String,"InputType","close");

    
    if ( ! opts.parse_args(argc, argv) || opts.option<bool>("help"))

	{
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string date_from   = opts.option<>("date_from");
    std::string date_to     = opts.option<>("date_to");
    std::string  CalculateOnTheFlyStr  = opts.option<>("CalculateOnTheFly");
    std::string  StoreDBStr            = opts.option<>("StoreInDB");
    std::string input       = opts.option<>("InputType");
    
    
    bool CalculateOnTheFly = true;
    if (CalculateOnTheFlyStr == "yes")
        CalculateOnTheFly = true;
    
    if (CalculateOnTheFlyStr == "no")
        CalculateOnTheFly = false;
    
    bool StoreDB= true;
    if (StoreDBStr == "yes")
        StoreDB = true;
    
    if (StoreDBStr == "no")
        StoreDB = false;
    
    auto conn       = std::make_shared<sql_connection>();
    int strategy_id = opts.option<int>("strategy_id");
    int money       = opts.option<int>("money");
    std::cout << "Pseudo Trading Calculating on the fly "
                << CalculateOnTheFlyStr << " " << CalculateOnTheFly <<
                " from=" << date_from <<
                " to="   << date_to <<
                " push to DB " << StoreDBStr << "  " << StoreDB  << std::endl;
    
    
     Pseudo_Trader _Trader(conn,strategy_id);
    _Trader.SetTypeInput(input);

    _Trader.Set_Push_IntoDB(StoreDB);
    _Trader.Set_LiveTradeStatus(false);
    _Trader.run(tiempo::datetime(date_from), tiempo::datetime(date_to),money, CalculateOnTheFly);
    //_Trader.PrintToFile(std::string("TestPseudoTrader.csv"));
}
