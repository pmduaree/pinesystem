#include <iostream>
#include "SingleAssetManager.hpp"

int main ()
{
    auto Conn = std::make_shared<sql_connection>();
    
    auto AssetManager = std::make_shared<SingleAssetManager>(Conn,1);
    
    
    tiempo::datetime date = tiempo::now()-std::chrono::hours(90);
    
    AssetManager->Long_position(date,2);
    std::cout << AssetManager->toString() << std::endl;
    
    AssetManager->Long_position(date,3);
    std::cout << AssetManager->toString() << std::endl;
    
    AssetManager->Long_position(date+std::chrono::hours(1),4);
    std::cout << AssetManager->toString() << std::endl;
    
    AssetManager->Short_position(date+std::chrono::hours(2),5);
    std::cout << AssetManager->toString() << std::endl;
    
    AssetManager->Short_position(date+std::chrono::hours(3),6);
    std::cout << AssetManager->toString() << std::endl;
    
    AssetManager->Long_position(date+std::chrono::hours(4),7);
    std::cout << AssetManager->toString() << std::endl;

}
