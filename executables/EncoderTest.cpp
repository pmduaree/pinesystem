#include <iostream>
#include "sql_connection.hpp"
#include "Encoder.hpp"
#include "tiempo.hpp"
#include "prog_opts.hpp"


int main(int argc, char** argv)
{
    
    prog_opts opts;
    opts.add_option<int>("-S", "--trader", OptionType::Integer, "signal", 0);
    opts.add_option<std::string>("--Action", OptionType::String, "action", "UP");
    opts.add_option<std::string>("--date", OptionType::String, "date_", "");
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string date_from = opts.option<>("date_");
    std::string Action    = opts.option<>("action");
    int Signal = opts.option<int>("signal");
    
    std::string comment ="ACTION="; comment+= Action; comment+=",comment=bla" ;
    tiempo::datetime date2(date_from.c_str());
    std::cout << "date=" << date2.to_string() << " Signal=" << Signal << " Action=" << Action << "\nComment=" << comment << std::endl;
    auto conn = std::make_shared<sql_connection>();
    
    
    auto MyEnconder = std::make_shared<Encoder>(conn,2);
    std::vector<std::tuple<int,int>> S = MyEnconder->Encode_Singal(date2,10000,std::make_tuple(Signal,comment));
    
    for (auto s:S )
        std::cout << "Exit Assetid=" << std::get<0>(s) << " Positions=" <<std::get<1>(s)<< std::endl;
}




