#include "../trunk/Strategy/strategy.hpp"
#include "../trunk/Strategy/strategy_engine.hpp"
#include "../trunk/Strategy/strategy_engines.hpp"
#include "../trunk/Assets/asset.hpp"
#include "../trunk/Utils/prog_opts.hpp"
#include "../trunk/SignalSeries.hpp"

int main(int argc, char** argv)
{
    
    prog_opts opts;
    opts.add_option<int>("-s", "--strategy", OptionType::Integer, "strategy", 0);
    opts.add_option<int>("-p", "--portfolio", OptionType::Integer, "portfolio", 0);
    opts.add_option<std::string>("-d", "--date", OptionType::String, "date", "now");
    
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }

    int StrategyID  = opts.option<int>("strategy");
    int PortfolioID = opts.option<int>("portfolio");
    std::string date_str = opts.option<>("date");
    tiempo::datetime  to;
    //tiempo::duration from;
    if (date_str== "")
        to = tiempo::now();
    else
        to = tiempo::datetime(date_str);
    
    //from = to - tiempo::duration( 24*5);
    
    auto conn = std::make_shared<sql_connection>();
    
    std::cout << "Going to create an Signaller.  ID= " << StrategyID << " date="
                    << date_str <<  " Portfolio=" << PortfolioID <<   std::endl;
    
    auto __Strat         = std::make_shared<Strategy> (conn,StrategyID);
    std::cout << (boost::format("ID[%d]   Type= %s  Param=%s Freq=%d")
                  % __Strat->get_id()
                  % __Strat->get_type()
                  % __Strat->get_param()
                  % __Strat->get_freq() ).str() << std::endl;
    
     auto asset = std::make_shared<MAsset>(conn, PortfolioID);
    
     //std::cout << "Got the asset: " << asset->get_name() << std::endl;
     //asset->init(tiempo::duration( 24*5),to);
    
     //std::cout << "last price: " << asset->get_last_price() << std::endl;
     //std::cout << "last date: "  << asset->get_last_date() << std::endl;
     
     //auto strategy = std::make_shared<Strategy>(conn, strategy_name, strategy_param, freq);
     auto strategy = std::make_shared<Strategy>(conn,__Strat->get_id() );
     std::cout << "Strategy ID: " << strategy->get_id() << std::endl;
     
     // what's the strategy then?
     tiempo::datetime wanted_date(date_str);
     std::cout << "Wanted date: " << wanted_date << std::endl;
     
     auto on_date = offset_date(wanted_date, __Strat->get_freq() , 0);
     std::cout << "Regularized date: " << on_date << std::endl;
    
    
    auto ids = asset->get_ids();
    
    for(unsigned int i =0; i!= ids.size(); i++)
    {
        auto V = asset->_Assets[i]->fetchValue(on_date);
        std::cout << (boost::format("ID [%d] %s -> Price [%f | %f | %f | %f ]\n")% ids[i] %  on_date.to_string() % std::get<0>(V) %  std::get<1>(V) % std::get<2>(V) % std::get<3>(V)   ).str();
    
    }
    
    /* std::tuple<int,double,std::string> res = strategy->calculate_signal(asset, on_date);
    
     std::cout << (boost::format("Signal Flag %d \n   Prob=[%f] \n    Comment=[%s]")
                    % std::get<0>(res)
                    % std::get<1>(res)
                    % std::get<2>(res) ).str()  << std::endl;
*/
    return 0;
}
