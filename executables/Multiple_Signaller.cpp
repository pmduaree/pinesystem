//
//  Multiple_Signaller.cpp
//  
//
//  Created by David de la Rosa on 6/19/14.
//
//



/*
 Signaller for multple asset trading
 a portfolio of assets it's a set of assets that willl be 
 traded at once by a strategy
 signaller only ll launch the strategies and 
 store the signals on the DataBase
 */

#include "../trunk/Tiempo/tiempo.hpp"
#include "../trunk/Strategy/strategy.hpp"
#include "../trunk/Assets/masset.hpp"

#define     __TYPE    0
#define     __PROB    1
#define     __COMM    2

#define _MULTIPLE_ASSET_TYPE 0
#define _SINGLE_ASSET_TYPE 1


class Multiple_Signaller
{
    std::shared_ptr<sql_connection> conn;
    std::vector<std::tuple<int,std::string,std::string,int,int,int,int,bool> >  StrategiesPortfolio;
public:
    Multiple_Signaller() : conn(std::make_shared<sql_connection>()){};
    ~Multiple_Signaller(){};
    
void run()
    {
        std::cout << "Load Signals from ..." ;
        load_signals_list();
        tiempo::datetime Now(tiempo::now().to_string("%Y-%m-%d %H:%M:00"));
        
        std::cout << "   Signals Loaded!." << std::endl;
        for (auto x :StrategiesPortfolio)
        {
           /* std::cout << "          Runing Signal "
            << " id=" << std::get<0>(x)//
            << " type=" << std::get<1>(x)
            << " param=" << std::get<2>(x)
            << " Hrs_FarBack=" << std::get<3>(x)
            << " freq=" << std::get<4>(x)
            << " portofolio=" << std::get<5>(x) << std::endl;
            */
            
            std::tuple<int,double,std::string> res =
                Run_Signal(std::get<0>(x),std::get<1>(x),std::get<2>(x),
                           std::get<3>(x),std::get<4>(x),std::get<5>(x),
                           std::get<6>(x),std::get<7>(x),Now);
            Process_signal(x,res,Now);
        }
    };

private:
void Push_Signal(std::tuple<int,std::string,std::string,int,int,int,int,bool> Portfolio,
                 std::tuple<int,double,std::string> signal, tiempo::datetime Time )
    {
        int Strategy_id     =   std::get<0>(Portfolio);
        int portfolio_id    =   std::get<5>(Portfolio);
        std::string timestamp = Time.to_string("%Y-%m-%d %H:%M:%S");
        double type         =   std::get<__PROB>(signal);
        double Probability  =   std::get<__PROB>(signal);
        std::string Comment =   std::get<__COMM>(signal);
        
        std::string query =
        (boost::format("SELECT id FROM `Main`.`RawSignals` WHERE \
                       `strategy_id`= %d AND `portfolio_id` = %d AND `date` = '%s' ")
         % Strategy_id % portfolio_id % timestamp  ).str();
        //std::cout <<  query << std::endl;
        conn->lock();
        auto _con = conn->connection();
        std::unique_ptr<sql::Statement> stmt(_con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        if (res->rowsCount() >0)
            {
                res->first();
                int ID = res->getInt("id");
                std::cout << "updating row ID " << ID << std::endl;
                query =(boost::format("UPDATE `Main`.`RawSignals` SET  `Live`= 1,   \
                                      `SignalType`=%d ,`Ppos`= %0.3f, `Rpar` = '%s'  WHERE `id`= %d;")
                        % type % Probability % Comment %ID  ).str();
                
                stmt.reset(_con->createStatement());
                stmt->execute(query);
                conn->unlock();
                return;
            }
        query =(boost::format("INSERT INTO `Main`.`RawSignals` (`strategy_id`, `portfolio_id`, `date`, `Live`,`SignalType`, `Ppos`, `Rpar`) \
                              VALUES (%d , %d, '%s',1,%d, %0.3f ,'%s');")
                % Strategy_id % portfolio_id % timestamp % type %Probability % Comment ).str();
        std::cout << "Inserting new row \n"  << std::endl;
        stmt.reset(_con->createStatement());
        stmt->execute(query);
        conn->unlock();
    }
    
void Process_signal(std::tuple<int,std::string,std::string,int,int,int,int,bool> Portfolio,
                    std::tuple<int,double,std::string> signal,tiempo::datetime Time )
    {
       //filter signals
       if(  std::get<__TYPE>(signal) == EMIT_WAIT_TRADE_SIGNAL )
                return;
        Push_Signal(Portfolio,signal,Time);
        return;
    }
    
void load_signals_list()
    {
        StrategiesPortfolio.clear();
        conn->lock();
        std::shared_ptr<sql::Connection> _con = conn->connection();
        std::string query =
        (boost::format("SELECT `S`.`id` as id, `S`.`portfolio_id`, `S`.`freq`,  `ST`.`assetType`, \
           `SE`.`CurrentParam`,`ST`.`Name`,`ST`.`Hrs_FarBack` ,`S`.`OnTrade` as TradeStatus \
           FROM `Main`.`Strategies` S \
           INNER JOIN  `Main`.`SignalerEngines` SE ON \
           `SE`.`id` = `S`.`SignalerEngine_id` \
           INNER JOIN `Main`.`SignalerType` ST ON \
           `ST`.id = `SE`.`Type_id`\
            WHERE `disabled`=0 AND LiveTracking = 1") ).str();
        std::unique_ptr<sql::Statement> stmt(_con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        
        while(res->next()) {
            int id_strat        = res->getInt("id");
            std::string type    = res->getString("Name");
            std::string param   = res->getString("CurrentParam");
            int Hrs_FarBack     = res->getInt("Hrs_FarBack");
            int freq            = res->getInt("freq");
            int portfolio_id    = res->getInt("portfolio_id");
            int TypeAsset       = res->getInt("assetType");
            bool TradeStatus    = (res->getInt("TradeStatus") ==  1 )?true:false;
            StrategiesPortfolio.push_back(
                std::make_tuple(
                  id_strat,
                  type,
                  param,
                  Hrs_FarBack,
                  freq,
                  portfolio_id,
                  TypeAsset,
                  TradeStatus
                                ) );
        }
        conn->unlock();
    };
std::tuple<int,double,std::string>
    Run_Signal(int id_strat,std::string type,std::string strategy_param, int Hrs_FarBack,
               int freq,int portfolio_id, int TypeAsset, bool TradeStatus, tiempo::datetime Time)
    {
        
        if (TypeAsset==  _SINGLE_ASSET_TYPE)
        {
            std::cout << "Going to create an asset." << std::endl;
            std::shared_ptr<Asset > Single_asset = std::make_shared<Asset>(conn, portfolio_id);
            Single_asset->init(std::chrono::hours(Hrs_FarBack) );
            
            auto strategy = std::make_shared<Strategy>(conn, id_strat);
            
            std::cout << (boost::format("<result>\
                                        Strategy ID: %d\n\
                                        Calculating on Date: %s\n\
                                        Hrs far Back : %d \n\
                                        Strategy Type: %s\n\
                                        Parameters : %s -  %s \n\
                                        Over portfolio ID: %d\n\
                                        </result>")
                          % strategy->get_id()
                          % Time.to_string()
                          % Hrs_FarBack
                          % type
                          % strategy_param
                          % strategy->get_param()
                          % portfolio_id
                          ).str() << std::endl;
            
            std::tuple<int,double,std::string> res = strategy->calculate_signal(Single_asset, Time,TradeStatus);
            std::cout << "\t\t\t\t\t<result>Type of Signal = "<< std::get<__TYPE>(res) << " Probability for the signal: " << std::get<__PROB>(res) << std::endl;
            std::cout << "\t\t\t\t\tFull Result: " << std::get<__COMM>(res)  <<  "</result>" << std::endl;
            return res;
        }
        
        
        if (TypeAsset==  _MULTIPLE_ASSET_TYPE)
        {
            std::cout << "Going to create an asset." << std::endl;
            std::shared_ptr<MAsset > Multiple_asset = std::make_shared<MAsset>(conn, portfolio_id);
            Multiple_asset->init(std::chrono::hours(Hrs_FarBack) );
            
            auto strategy = std::make_shared<Strategy>(conn, id_strat);
            
            std::cout << (boost::format("<result>\
                                        Strategy ID: %d\n\
                                        Calculating on Date: %s\n\
                                        Hrs far Back : %d \n\
                                        Strategy Type: %s\n\
                                        Parameters : %s -  %s \n\
                                        Over portfolio ID: %d\n\
                                        </result>")
                                        % strategy->get_id()
                                        % Time.to_string()
                                        % Hrs_FarBack
                                        % type
                                        % strategy_param
                                        % strategy->get_param()
                                        % portfolio_id
                          ).str() << std::endl;
            
            std::tuple<int,double,std::string> res = strategy->calculate_signal(Multiple_asset, Time,TradeStatus);
            std::cout << "\t\t\t\t\t<result>Type of Signal = "<< std::get<__TYPE>(res) << " Probability for the signal: " << std::get<__PROB>(res) << std::endl;
            std::cout << "\t\t\t\t\tFull Result: " << std::get<__COMM>(res)  <<  "</result>" << std::endl;
                return res;
            
        }
        
        return  std::make_tuple(0,0.0,"Error");
    };
};
int main ()
{
     Multiple_Signaller Signaller;
    std::cout << "Multiple Signaller, Strategies with Portfolio of assets " << std::endl;
    
int i =0;
   while (true) {
        Signaller.run();
         std::this_thread::sleep_for(std::chrono::seconds(5) );
        std::cout <<  std::endl << std::endl << std::endl;
  // i++;
    }
  //  Signaller.run();
    
    return 0;

}
