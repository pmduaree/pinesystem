#include <iostream>
#include <chrono>

#include <boost/format.hpp>

#include "sql_connection.hpp"
#include "prog_opts.hpp"
#include "asset.hpp"
#include "asset_pool.hpp"
#include "strategy_portfolio.hpp"
#include "historic_trader.hpp"
#include "trader_parameters.hpp"

class QuasiTrader
{
    std::shared_ptr<sql_connection> conn;
    AssetPool assetpool;
  public:
    QuasiTrader(std::shared_ptr<sql_connection> conn_)
        : conn(conn_), assetpool(conn)
    {}
    
    std::tuple<int,int,int> extract_parameters(int trader_id)
    {
        int strategyportfolio_id = 0, traderpar_id = 0, asset_id=0;
        conn->lock();
        auto con = conn->connection();
        std::string query = (boost::format("SELECT * FROM `Main`.`Trading` WHERE `id`=%d") % trader_id).str();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        if (res->rowsCount() > 0) {
            res->first();
            asset_id = res->getInt("asset_id");
            strategyportfolio_id = res->getInt("strategyportfolio_id");
            traderpar_id = res->getInt("traderpar_id");
        }
        conn->unlock();

        return std::make_tuple(asset_id, strategyportfolio_id, traderpar_id);
    }
    
    void operator()(int asset_id, int stratportfolio_id, int traderpar_id,
                    const tiempo::datetime& date_from, const tiempo::datetime& date_to)
    {
        if (!asset_id) return;
        
        assetpool.add_asset(asset_id);
        auto asset = assetpool.get_asset(asset_id);
        asset->init(std::chrono::hours(24 * 30) );
        auto sigmaker = std::make_shared<StrategyPortfolio>(conn, asset, stratportfolio_id);
        
        #if 0
        // make some signal.
        auto now = tiempo::now();
        auto ss = (*sigmaker)(now);
        std::cout << " signal: " << signal_to_string(std::get<0>(ss)) << std::endl;
        std::cout << "signal date: " << std::get<1>(ss) << std::endl;
        #endif
        
        HistoricTrader htrader(asset);
        
        TraderParameters trp_x(conn);
        TraderParams trp;
        if (!trp_x.load(traderpar_id, trp)) {
            std::cout << "Could not load trader parameters (id="<<traderpar_id<<").\n";
            return;
        }

        std::cout << "Trading parameters: " << trp.to_string() << std::endl;
        
        //std::cout << "Calling the history guy " << std::endl;
        double money0 = 100000;
        tiempo::duration unit_step(sigmaker->get_freq() * 60);
        std::vector<trade_slice> RV;
        htrader(sigmaker, trp, date_from, date_to, unit_step, money0, RV);
        //std::cout << "GOT TV: " << RV.size() << std::endl;
        
        std::cout << "<results>\n";
        for(const auto& rv : RV) {
            std::cout << boost::format("%s %7s %10.2f %11.2f")
                % rv.date.to_string("%Y-%m-%d %H:%M:%S") % order_to_string(rv.order) % asset->get_price(rv.date) 
                % rv.eff_money << std::endl;
        }
        std::cout << "</results>\n";
        auto alpha = calculate_alpha(RV);
        std::cout << boost::format("<alpha>%.5f</alpha>")%std::get<2>(alpha)<<std::endl;
    }
};



int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<int>("-x", "--trader", OptionType::Integer, "trader_id", 0);
    opts.add_option<int>("-i", "--asset", OptionType::Integer, "asset_id", 0);
    opts.add_option<int>("-p", "--portfolio", OptionType::Integer, "portfolio_id", 0);
    opts.add_option<int>("-t", "--tradpar", OptionType::Integer, "traderpar_id", 0);
    opts.add_option<std::string>("--from", OptionType::String, "date_from", "");
    opts.add_option<std::string>("--to", OptionType::String, "date_to", "");
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string date_from = opts.option<>("date_from");
    std::string date_to = opts.option<>("date_to");
    
    auto conn = std::make_shared<sql_connection>();
    QuasiTrader qtrader(conn);
    
    tiempo::datetime date_2, date_1;
    if (date_to.empty())
        date_2.from_string("now");
    else
        date_2.from_string(date_to);

    if (date_from.empty())
        date_1 = date_2 - std::chrono::hours(24 * 5);
    else 
        date_1.from_string(date_from);

    int trader_id = opts.option<int>("trader_id");
    int opt_asset = opts.option<int>("asset_id");
    int opt_portfolio = opts.option<int>("portfolio_id");
    int opt_traderpar = opts.option<int>("traderpar_id");

    int asset_id = 0, traderpar_id = 0, portfolio_id = 0;
    if (trader_id) {
        // then extract from teh trader id.
        std::tie(asset_id, portfolio_id, traderpar_id) = qtrader.extract_parameters(trader_id);
    }
    if (opt_asset) asset_id = opt_asset;
    if (opt_portfolio) portfolio_id = opt_portfolio;
    if (opt_traderpar) traderpar_id = opt_traderpar;

    qtrader(asset_id, portfolio_id, traderpar_id, date_1, date_2);
    
    return 0;
}

