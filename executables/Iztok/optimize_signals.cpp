#include <iostream>
#include <atomic>
#include "asset.hpp"
#include "strategy.hpp"
#include "strategy_engines.hpp"
#include "strategy_portfolio.hpp"
#include "signal_measure.hpp"

#include "stringparpars.hpp"
#include "prog_opts.hpp"

#include "signal_optimizer.hpp"
#include "mc_optimizer.hpp"

class Worker
{
    std::shared_ptr<sql_connection> conn;
  public:
    Worker(std::shared_ptr<sql_connection> conn_)
        : conn(std::move(conn_))
    {
    }
    
    void optimize(int optparam_id, std::string task_info, std::atomic<bool>& d_)
    {
        //-------------------------------------------------------------------
        // extract the parameters from the database and update the DB
        //-------------------------------------------------------------------
        StringParamParser ppr(task_info);
        int asset_id = ppr.get<int>("asset", 0);
        int freq = ppr.get<int>("freq", 0);
        int offset = ppr.get<int>("offset", 0);
        int num_strategies = ppr.get<int>("numstrat", 0);
        std::string str_d_1 = ppr.get<std::string>("from", "");
        std::string str_d_2 = ppr.get<std::string>("to", "");
        size_t num_trials = ppr.get<int>("numtrials", 10000);
        
        num_trials = std::max<size_t>(num_trials, 100);
        
        if (true) {
            // set the timestamp and the asset, signalling that the task has been taken.
            conn->lock();
            auto con = conn->connection();
            std::shared_ptr<sql::Statement> stmt(con->createStatement());
            auto q = (boost::format("UPDATE `Main`.`SignalOptimization` SET `asset_id`=%d , `timestamp`=\'%s\' WHERE `id`=%d")% asset_id % tiempo::now().to_string("%Y-%m-%d %H:%M:%S") % optparam_id ).str();
            stmt->execute(q);
            conn->unlock();
        }
        //-------------------------------------------------------------------
        // the optimization starts here
        //-------------------------------------------------------------------
        std::cout << "Number of trials: " << num_trials << std::endl;
        auto date_2 = offset_date(tiempo::datetime(str_d_2), freq, offset);
        auto date_1 = offset_date(tiempo::datetime(str_d_1), freq, offset);
        auto asset = std::make_shared<Asset>(conn, asset_id);
        auto how_far_back = tiempo::now() - date_1;
        how_far_back += std::chrono::hours(300);
        asset->init(how_far_back);
        std::cout << "The asset has been initialized. " << std::endl;
        SignalOptimizer sigopt(conn, asset, freq, offset, date_1, date_2);
        
        std::cout << "Optimizing over the portfolio of strategies..." << std::endl;
#if  0
        size_t conv_check = num_trials / 5;
        std::cout << "Convergence check: " << conv_check << std::endl;
        typedef sigOptim<SignalOptimizer> tOptim;
        tOptim seed(sigopt,sigopt.size(), num_strategies);
        MCMC localOptim(50,10,0.15); //numbers of steps, number of coolings, starting range
        std::vector<tOptim> Pfront;
        optimizationEngine oPt;
        oPt.generatePopulation(localOptim,Pfront,seed,num_trials,conv_check,false);  //localOptim Class,fr
        std::cout << "P:\n" << Pfront << std::endl;
        // in this case we only have one element in the Pfront.
        auto best_Wj = Pfront.front().getParams();
        assert(false );
#else
        MCOpt mcopt(sigopt.size(), num_strategies);
        auto best_Wj = mcopt(sigopt, num_trials);
#endif
        time_series<signal_type> signals;
        sigopt.make_signals(best_Wj, signals);
        tiempo::duration dfreq(60 * freq);
        for(const auto& s : signals) {
            auto date = s.first;
            auto siq = s.second;
            auto date1 = date + dfreq;
            auto c = make_candle(asset->get_ticks(), date, dfreq);
            auto pavg = asset->get_ticks().get_mean_price(date, date1);
            std::cout << boost::format("[%s] %s : [o=%.3f,h=%.3f,l=%.3f,c=%.3f,avg=%.3f]")  
                         % date.to_string() % signal_to_string(siq) % c.p_o % c.p_h % c.p_l % c.p_c % pavg << std::endl;
        }
        //-------------------------------------------------------------------
        // save the new strategy portfolio to the database
        //-------------------------------------------------------------------
        sigopt.save(optparam_id, best_Wj);
        std::cout << boost::format("Parameters [id=%d] finished.") % optparam_id << std::endl;
        d_ = true;
    }
};


class Main
{
    std::shared_ptr<sql_connection> conn;
    std::map<int, std::thread> threads;
    std::map<int, std::atomic<bool> > done;
    Worker worker;
  public:
    Main()
        : conn(std::make_shared<sql_connection>()), worker(conn)
    {
        // clear unfinished tasks.
        conn->lock();
        auto con = conn->connection();
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute("UPDATE `Main`.`SignalOptimization` SET `timestamp` = NULL WHERE `portfolio_id` IS NULL");
        conn->unlock();
    }
    
    ~Main()
    {
        for(auto& t : threads) t.second.join();
    }
    
    void run()
    {
        std::string q = "SELECT id, info FROM `Main`.`SignalOptimization` \
                         WHERE `portfolio_id` IS NULL AND `timestamp` IS NULL";

        conn->lock();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
        std::vector<std::tuple<int,std::string> > Z;
        while(res->next()) {
            int id = res->getInt("id");
            std::string task_info = res->getString("info");
            Z.push_back(std::make_tuple(id, task_info));
        }
        conn->unlock();
        
        for(const auto& z : Z) {
            int optparam_id = std::get<0>(z);
            std::string task = std::get<1>(z);
            auto& d_ = done[optparam_id];
            d_ = false;
            std::cout << "*** Optimization of optparam_id = " << optparam_id << std::endl;
            worker.optimize(optparam_id, task, d_);
        }
    }
};



int main(int argc, char** argv)
{
    Main engine;
    
    auto output_time = std::chrono::steady_clock::now();
    while(true) {
        engine.run();
        std::this_thread::sleep_for(std::chrono::seconds(5) );
        
        auto time_now = std::chrono::steady_clock::now();
        if (time_now > output_time + std::chrono::minutes(5) ) {
            output_time = time_now;
            std::cout << boost::format("[%s] PING[optimize_signals]") % tiempo::now().to_string() << std::endl;
        }

    }
    
    return 0;
}

