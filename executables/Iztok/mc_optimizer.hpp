#ifndef mc_optimizer_hpp
#define mc_optimizer_hpp

#include <iostream>
#include <random>
#include <vector>
#include <unordered_map>
#include <chrono>

#include <boost/format.hpp>

class MCOpt
{
    size_t n, m;
    std::default_random_engine gen;
  public:
    MCOpt(size_t dim, size_t m_)
        : n(dim), m(m_)
    {
    }
    
    void make_random_W(std::vector<double>& Wj)
    {
        std::uniform_real_distribution<> unigen(0,1);
        std::unordered_map<int, double> ZZ;
        double wsum = 0.0;
        while(ZZ.size() < m) {
            int strategy_id = static_cast<int>(unigen(gen) * n);
            if (ZZ.count(strategy_id)) continue;
            ZZ[strategy_id] = unigen(gen);
            wsum += ZZ[strategy_id];
        }
        Wj.resize(n);
        std::fill(Wj.begin(), Wj.end(), 0.0);
        for(auto& z : ZZ) 
            Wj[z.first] = z.second / wsum;
    }
    
    template<class Module>
    std::vector<double> operator()(Module calc_measure, size_t n_trials)
    {
        std::map<double, std::vector<double> > QQ;
        auto output_time = std::chrono::steady_clock::now();
        for(size_t i=0;i<n_trials;++i) {
            std::vector<double> Wj;
            make_random_W(Wj);
            //std::cout <<" trying for Wj: " << Wj << std::endl;
            double measure = calc_measure(Wj);
            QQ[measure] = Wj;
            while(QQ.size() > 10) QQ.erase(QQ.begin());
            auto clock_now = std::chrono::steady_clock::now();
            if (clock_now - output_time > std::chrono::minutes(1)) {
                output_time = clock_now;
                std::cout << boost::format("[%3d%%] measure: %.8f") % static_cast<size_t>( (i+1.0)*100.0/n_trials) 
                                                          % QQ.rbegin()->first << std::endl;
            }
            if (QQ.rbegin()->first == measure) {
                std::cout << boost::format("[%3d%%] measure: %.8f") % static_cast<size_t>( (i+1.0)*100.0/n_trials) 
                                                          % measure << std::endl;
            }
        }
        auto W_best = QQ.rbegin()->second;
        return W_best;
    }
};

#endif
