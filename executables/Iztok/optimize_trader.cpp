#include <iostream>
#include <chrono>

#include <random>

#include <boost/format.hpp>
#include "sql_connection.hpp"
#include "asset.hpp"
#include "asset_pool.hpp"
#include "strategy_portfolio.hpp"
#include "prog_opts.hpp"
#include "historic_trader.hpp"
#include "trader_parameters.hpp"

class TraderOptimizer
{
    std::shared_ptr<sql_connection> conn;
    std::shared_ptr<Asset> asset;
    std::shared_ptr<StrategyPortfolio> sigmaker;
    std::shared_ptr<HistoricTrader> htrader;
    std::default_random_engine gen;
    tiempo::datetime date_from, date_to;
    std::mutex output_mutex;
  public:
    TraderOptimizer(std::shared_ptr<sql_connection> conn_, int asset_id, int portfolio_id, 
                    const tiempo::datetime& date1, const tiempo::datetime& date2)
        : conn(conn_), date_from(date1), date_to(date2)
    {
        std::cout << "Constructing the trade optimizer" << std::endl;
        gen.seed(time(NULL));
        asset = std::make_shared<Asset>(conn, asset_id);
        std::cout << "Initializing..." << std::endl;
        auto time_difference = std::chrono::hours(24 * 40);
        std::cout << "Initializing1..." << std::endl;
        auto my_duration = tiempo::duration(time_difference);
        std::cout << "Initializing2: " << my_duration.to_string() << std::endl;
        asset->init(my_duration);//std::chrono::hours(24 * 40) );
        std::cout << "Making the Strategy Portfolio..." << std::endl;
        sigmaker = std::make_shared<StrategyPortfolio>(conn, asset, portfolio_id);
        assert( sigmaker->get_id() == portfolio_id );
        htrader = std::make_shared<HistoricTrader>(asset);
        std::cout << "Constructing the trade optimizer .. DONE" << std::endl;
    }
    
    void make_random(TraderParams& trp)
    {
        std::uniform_real_distribution<> unireal(0,1);
        std::bernoulli_distribution fair_coin;
        trp.HP = unireal(gen) < 0.4 ? 1 : 0;
        trp.shorttime = 1 + static_cast<int>(8 * unireal(gen));
        trp.trendshort = 1 + static_cast<int>(8 * unireal(gen));
        trp.trendlong = trp.trendshort + 1 + static_cast<int>(8*unireal(gen));
        trp.profthres = 0.0001 + unireal(gen) * (0.003  - 0.0001);
        trp.maxloss =  0.001 + unireal(gen) * 0.006;
        trp.proftgt = 0.002 + unireal(gen) * 0.008;
        trp.closehold = fair_coin(gen) ? 0 : static_cast<int>(4 + unireal(gen) * 2.2);
    }
    
    void run_many(size_t how_many, std::tuple<double,TraderParams>& best)
    {
        std::map<double, TraderParams> results;
        auto printout_time = std::chrono::steady_clock::now();
        for(size_t i=0;i<how_many;++i) {
            TraderParams trp;
            double measure;
            std::tie(measure, trp) = run_one();
            auto trp_str = trp.to_string();
            //std::cout << boost::format("[%8d] %17.10f: %s") % i %  measure % trp_str << std::endl;
            results[measure] = trp;
            auto time_now = std::chrono::steady_clock::now();
            if (time_now > printout_time + std::chrono::seconds(10) ) {
                output_mutex.lock();
                auto best_measure = results.rbegin()->first;
                auto best_param = results.rbegin()->second.to_string();
                std::cout << boost::format("[%8d] %17.10f: %s") % i %  best_measure % best_param << std::endl;
                output_mutex.unlock();
                printout_time = std::chrono::steady_clock::now();
            }
        }
        auto best_measure = results.rbegin()->first;
        auto best_trparam = results.rbegin()->second;
        best = std::make_tuple(best_measure, best_trparam);
    }
    
    TraderParams run_optimization(size_t ntrials)
    {
        std::cout << boost::format("[%s] Trader Optimization started.")%tiempo::now().to_string() << std::endl;
        size_t nthreads = std::thread::hardware_concurrency();
        size_t max_allowed_threads = 32;
        //size_t max_allowed_threads = 1;
        nthreads = std::min<size_t>(max_allowed_threads, nthreads);
        //std::cout << boost::format("[%s] run_optimization started on %d threads.")
        //            % tiempo::now().to_string() % nthreads << std::endl;
        size_t tasks_per_thread = ntrials / nthreads;
        std::unordered_map<size_t, std::tuple<double,TraderParams> > R;
        std::unordered_map<size_t, std::thread> threads;
        for(size_t tt=0;tt<nthreads;++tt) {
            auto& r = R[tt];
            threads.emplace(tt, std::thread(&TraderOptimizer::run_many, this, tasks_per_thread, std::ref(r) ) );
        }
        std::map<double, TraderParams> results;
        for(auto& t : threads) {
            t.second.join();
            auto r = R[t.first];
            results[std::get<0>(r)] = std::get<1>(r);
        }
        auto best_measure = results.rbegin()->first;
        auto best_trparam = results.rbegin()->second;
        std::cout << "The best trading parameters:\n" << best_trparam.to_string() << std::endl;
        std::cout << "Measure: " << best_measure << std::endl;
        return std::move(best_trparam);
    }
    
    void save(int optp_id, const TraderParams& trparam)
    {
        int asset_id = asset->get_id();
        TraderParameters trp_x(conn);
        int traderpar_id = trp_x.save(trparam);
        std::cout << "traderpar_id obtained: " << traderpar_id << std::endl;
        //int traderpar_id = trparam.save(conn);
        int portfolio_id = sigmaker->get_id();

        std::string timestamp = tiempo::now().to_string("%Y-%m-%d %H:%M:%S");
        std::string date1_str = date_from.to_string("%Y-%m-%d %H:%M:%S");
        std::string date2_str = date_to.to_string("%Y-%m-%d %H:%M:%S");
        
        // store it to the database.
        conn->lock();
        
        std::string q = (boost::format(
            "UPDATE `Main`.`TraderOptimization` SET \
            `asset_id`=%d, `traderpar_id`=%d, `strategyportfolio_id`=%d,`timestamp`=\'%s\',\
            `date_from`=\'%s\',`date_to`=\'%s\' WHERE `id`=%d"
               ) % asset_id % traderpar_id % portfolio_id % timestamp % date1_str % date2_str % optp_id ).str();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute(q);
        conn->unlock();
    }
    
  private:
    std::tuple<double,double,double> run_trading(const TraderParams& trp)
    {
        if (!sigmaker || !asset) return std::make_tuple(0.0,0.0,0.0);
        double money0 = 100000;
        tiempo::duration unit_step(sigmaker->get_freq() * 60);
        std::vector<trade_slice> RV;
        (*htrader)(sigmaker, trp, date_from, date_to, unit_step, money0, RV);
        auto alpha = calculate_alpha(RV);
        return alpha;
    }
    
    TraderParams make_random_parameters()
    {
        std::uniform_real_distribution<> unireal(0,1);
        std::bernoulli_distribution fair_coin;
        TraderParams trp;
        trp.HP = unireal(gen) < 0.4 ? 1 : 0;
        trp.shorttime = 1 + static_cast<int>(8 * unireal(gen));
        trp.trendshort = 1 + static_cast<int>(8 * unireal(gen));
        trp.trendlong = trp.trendshort + 1 + static_cast<int>(8*unireal(gen));
        trp.profthres = 0.0001 + unireal(gen) * (0.003  - 0.0001);
        trp.maxloss =  0.001 + unireal(gen) * 0.006;
        trp.proftgt = 0.002 + unireal(gen) * 0.008;
        trp.closehold = fair_coin(gen) ? 0 : static_cast<int>(4 + unireal(gen) * 2.2);
        return trp;
    }
    
    std::tuple<double, TraderParams> run_one()
    {
        TraderParams trp = make_random_parameters();
        auto alpha = run_trading(trp);
        double measure = std::get<2>(alpha);
        return std::make_tuple(measure, std::move(trp) );
    }
};

class Main
{
    std::shared_ptr<sql_connection> conn;
  public:
    Main()
        : conn(std::make_shared<sql_connection>())
    {
    }
    
    void run()
    {
        std::string q = "SELECT id, note FROM `Main`.`TraderOptimization` \
                         WHERE `traderpar_id` IS NULL";
        conn->lock();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
        std::vector<std::tuple<int,std::string> > Z;
        while(res->next()) {
            int id = res->getInt("id");
            std::string task_info = res->getString("note");
            Z.push_back(std::make_tuple(id, task_info));
        }
        conn->unlock();
        for(const auto& z : Z) {
            int optp_id = std::get<0>(z);
            std::string task = std::get<1>(z);
            std::cout << boost::format("Submitting optimization task %d [%s]")
                            % optp_id % task << std::endl;
            optimize(optp_id, task);
        }
    }

    void optimize(int optp_id, std::string task)
    {
        std::cout << "Optimization for optp_id = " << optp_id << std::endl;
        StringParamParser ppr(task);
        int asset_id = ppr.get<int>("asset",0);
        int portfolio_id = ppr.get<int>("portfolio", 0);
        std::string date_from = ppr.get<std::string>("from","");
        std::string date_to = ppr.get<std::string>("to","");
        size_t ntrials = ppr.get<int>("ntrials", 10000);
        if (true) {
            std::cout << "Updating the Trader OPtimization sset" << std::endl;
            conn->lock();
            auto con = conn->connection();
            std::shared_ptr<sql::Statement> stmt(con->createStatement());
            auto timestamp = tiempo::now().to_string("%Y-%m-%d %H:%M:%S");
            auto q = (boost::format("UPDATE `Main`.`TraderOptimization` SET `asset_id`=%d, `timestamp`=\'%s\' WHERE `id`=%d")
                % asset_id % timestamp % optp_id).str();
            stmt->execute(q);
            conn->unlock();
            std::cout << "Updated..." << std::endl;
        }
        tiempo::datetime date_2(date_to);
        tiempo::datetime date_1(date_from);
        TraderOptimizer toptimizer(conn, asset_id, portfolio_id, date_1, date_2);
        std::cout << "toptimizer constructed " << std::endl;
        auto best_params = toptimizer.run_optimization(ntrials);
        toptimizer.save(optp_id, best_params);
    }
};


int main(int argc, char** argv)
{
    Main main;
    while(true) {
        main.run();
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    return 0;
}

