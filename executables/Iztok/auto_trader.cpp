#define AUTOEXECUTION
#include <iostream>
#include <atomic>
#include <thread>

#include "asset.hpp"
#include "asset_pool.hpp"
#include "signals.hpp"
#include "strategy_portfolio.hpp"
#include "trader_comm.hpp"
#include "trader_parameters.hpp"
#include "trader_request.hpp"



class AssetExecutor
{
    int liveasset_id;
    std::atomic<bool> brun;
    std::shared_ptr<Asset> asset;
    std::shared_ptr<StrategyPortfolio> strats;
    std::shared_ptr<TraderParams> trp;
    std::shared_ptr<TraderComm> comm;
    tiempo::datetime last_date;

  
  public:
    AssetExecutor(int liveassetid, std::shared_ptr<Asset> asset_, std::shared_ptr<StrategyPortfolio> strats_,
                  std::shared_ptr<TraderParams> trp_,
                  std::shared_ptr<TraderComm> comm_)
        : liveasset_id(liveassetid), brun(false), asset(asset_), strats(strats_), trp(trp_), 
          comm(comm_), stop_request(false), sleeping(false)
         
    {
        std::cout << boost::format("[%s] Asset executor for asset %s construted.") % tiempo::now().to_string() % asset->get_name()<<std::endl;
    }
    
    ~AssetExecutor()
    {
        std::cout << boost::format("[%s] Asset executor for asset %s destructed.") % tiempo::now().to_string() % asset->get_name()<<std::endl;
        sleeping = false;
    }
    
    std::shared_ptr<StrategyPortfolio> get_strategy() { return strats; }
    std::shared_ptr<TraderParams> get_trparam() { return trp; }
    std::shared_ptr<Asset> get_asset() { return asset; }
    
    void set_strategy(std::shared_ptr<StrategyPortfolio> strats_) { strats = strats_; }
    void set_trparam(std::shared_ptr<TraderParams> trp_) { trp = trp_; }
    
    void run();
    void stop() { stop_request = true; }
    bool is_sleeping() const { return sleeping; }
    bool is_running() const { return brun; }
  
  private:
    bool stop_request;
    bool sleeping;
    void init();
    void do_trading();
    void stop_trading();
};


void AssetExecutor::run()
{
    stop_request = false;
    init();
    brun = true;
    //std::cout << "Running asset executor for asset " << asset->get_name() << std::endl;
    while(brun) {
        do_trading();
        sleeping = true;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000) );
        sleeping = false;
        //std::cout << "Asset " << asset->get_name() << " still running." << std::endl;
        if (stop_request) {
            stop_trading();
            break;
        }
    }
    stop_request = false;
    brun = false;
}

void AssetExecutor::init()
{
}


void AssetExecutor::stop_trading()
{
    auto now = tiempo::now();
    TradePosition position;
    if (!comm->get_position(liveasset_id, position)) {
        position.quantity = 0;
        position.price = 0;
        position.date = now;
    }

    if (!position.quantity) return;
    
    // we do have a position, close it gracefully.
    asset->update();
    int asset_id = asset->get_id();
    auto P_current = asset->get_price(now);
    
    double profit = (P_current - position.price)*position.quantity;
    std::string dbg_msg = (
        boost::format("[%s] Close order (induced), asset=%d @ price=%.3f, profit=%.3f") % 
                       now.to_string() % asset_id % P_current % profit ).str();
    std::cout << dbg_msg << std::endl;
    comm->exit_trade(liveasset_id, now, P_current);

}



void AssetExecutor::do_trading()
{
    //std::cout << boost::format("[%s] Trading...") % tiempo::now().to_string() << std::endl;
    int n_lots = 1;
    if (comm->is_processing(liveasset_id)) {
        std::cout << "Is processing...." << std::endl;
        // try again later.:
        return ;
    }
    asset->update();
    auto now = tiempo::now();
    auto P_current = asset->get_price(now);

    int asset_id = asset->get_id();

    int freq = strats->get_freq();
    tiempo::duration step_freq = std::chrono::minutes(freq);

    TradePosition position;
    if (!comm->get_position(liveasset_id, position)) {
        position.quantity = 0;
        position.price = 0;
        position.date = now;
    }
        
    signal_type sig = signal_type::None;

    tiempo::datetime db_process_date;
    bool any_processed = comm->get_last_signal(liveasset_id, db_process_date);
    
    auto on_date = strats->get_regularized_date(now);
    
    tiempo::datetime sig_date;
    if (!any_processed || db_process_date < on_date) {
        std::cout << boost::format("Calculating a signal because expected signal date [%s] > previous procesed date [%s].")
                            % on_date.to_string() % db_process_date.to_string() << std::endl;
        // calculate a new signal now.
        std::tie(sig, sig_date) = (*strats)(on_date);
        std::cout << boost::format("Got signal [%s]: %s") % asset->get_name() % signal_to_string(sig) << std::endl;

        auto signal_age = now - sig_date;
        if (signal_age > std::chrono::seconds(step_freq.seconds() / 6) ) {
            std::cout << boost::format("Signal [%s]: %s invalidated beacuse it is too old.") % asset->get_name() % signal_to_string(sig) << std::endl;
            comm->update_last_signal(liveasset_id, sig_date);
            // invalidate the system altogether.
            // assume like we did process it.
            sig = signal_type::None;
        }
    }
    
    if (sig == signal_type::None && position.quantity == 0) {
        std::cout << boost::format("[%s] No signal.")%tiempo::now().to_string() << std::endl;
        // when do we expect the next signal?
        auto next_date = on_date + step_freq;
        now = tiempo::now();
        if (next_date > now + std::chrono::minutes(2) ) {
            /*
            auto waiting_time = next_date - now - std::chrono::minutes(1);
            if (waiting_time > std::chrono::minutes(3)) waiting_time = std::chrono::minutes(3);
            std::cout << boost::format("Sleeping for %s until %d.")
                        % tiempo::duration(waiting_time).to_string() % tiempo::datetime(now + waiting_time).to_string() << std::endl;
            sleeping = true;
            std::this_thread::sleep_for(waiting_time);
            sleeping = false;
            */
        }
        return;
    }
    
    TradeRequest tradereq(asset);
    trad_order order = tradereq.get_order(now, sig, *trp, step_freq, 
                                          position.quantity, position.date,
                                          position.price, P_current);
    
    std::cout << boost::format("[%s] Signal: %s, Order: %s") % tiempo::now().to_string() % signal_to_string(sig) % order_to_string(order) << std::endl;
    
    if (order == trad_order::Postpone) return;
    
    if (on_date > db_process_date && order != trad_order::CloseAndReset) {
        comm->update_last_signal(liveasset_id, on_date);
    }
    
    if (order == trad_order::Wait) {
        return;
    } else if (order == trad_order::Buy || order == trad_order::Sell) {
        int sign = (order == trad_order::Buy ? 1 : -1);
        std::string dbg_msg = (boost::format("[%s] Order %s [asset=%d] based on sig[%s] @ price=%.3f") %
                                now.to_string() % order_to_string(order) % 
                                liveasset_id % sig_date.to_string() % P_current ).str();
        comm->enter_trade(liveasset_id, asset_id, sign * n_lots, P_current, on_date);
        std::cout << dbg_msg << std::endl;
    } else if (order == trad_order::Close || order == trad_order::CloseAndReset) {
        double profit = (P_current - position.price)*position.quantity;
        std::string dbg_msg = (boost::format("[%s] Close order, asset=%d @ price=%.3f, profit=%.3f") % now.to_string() % asset_id 
                                               % P_current % profit ).str();
        std::cout << dbg_msg << std::endl;
        comm->exit_trade(liveasset_id, on_date, P_current);

        if (order == trad_order::CloseAndReset) {
            comm->update_last_signal(liveasset_id, db_process_date - std::chrono::minutes(1) );
        }
    } else if (order == trad_order::Hold) {
        return;
    } else {
        std::cout << "WEIRD ORDER" << std::endl;
        //std::cout << "Hold order, asset="<<asset_id<<std::endl;
    }

}


class DistributedExecutor
{
    std::shared_ptr<sql_connection> conn;
    AssetPool assetpool;
    std::map<int,std::thread> threads;
    std::map<int,std::shared_ptr<AssetExecutor> > executors;
    std::shared_ptr<TraderComm> comm;

  public:
    DistributedExecutor()
        : conn(std::make_shared<sql_connection>()),
          assetpool(conn), comm(std::make_shared<TraderComm>(conn) )
    {
    }
    
    void get_traders( std::map<int, std::tuple<int,int,int> >& Z)
    {
        Z.clear();
        conn->lock();
        auto con = conn->connection();
        std::string query = "SELECT * FROM `Main`.`Trading` WHERE `disabled`=0";
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        while(res->next()) {
            int trader_id = res->getInt("id");
            int asset_id = res->getInt("asset_id");
            int portfolio_id = res->getInt("strategyportfolio_id");
            int trparam_id = res->getInt("traderpar_id");
            Z[trader_id] = std::make_tuple(asset_id, portfolio_id, trparam_id);
        }
        conn->unlock();
    }
    
    void run()
    {
        std::cout << boost::format("[%s] PING[Distributed Executor]") %
                            tiempo::now().to_string() << std::endl;
        std::map<int, std::tuple<int,int,int> > Z;
        get_traders(Z);
        for(const auto& z : Z) {
            int trader_id = z.first;
            int asset_id = std::get<0>(z.second);
            int strategyportfolio_id = std::get<1>(z.second);
            int trparam_id = std::get<2>(z.second);
            
            // is this one running already?
            auto it_exec = executors.find(trader_id);
            if (it_exec != executors.end() ) {
                auto& exec = it_exec->second;
                auto asset = exec->get_asset();
                if (asset->get_id() != asset_id) {
                    std::cout << "WE CANNOT CHANGE THE ASSET!!!!";
                    exec->stop();
                    continue;
                }
                auto sigmaker = exec->get_strategy();
                if (sigmaker->get_id() != strategyportfolio_id) {
                    // make a new one.
                    sigmaker = std::make_shared<StrategyPortfolio>(conn, asset, strategyportfolio_id);
                    while(!exec->is_sleeping()) std::this_thread::sleep_for(std::chrono::milliseconds(100) );
                    std::cout << "Replacing the strategy portfolio in trader " << trader_id << std::endl;
                    exec->set_strategy(std::move(sigmaker));
                }
                TraderParameters trp_x(conn);
                std::shared_ptr<TraderParams> trp = trp_x.load(trparam_id);
                if ( *(exec->get_trparam()) != (*trp) ) {
                    while(!exec->is_sleeping()) std::this_thread::sleep_for(std::chrono::milliseconds(100) );
                    std::cout << "Replacing the trading parametres in trader " << trader_id << std::endl;
                    exec->set_trparam(std::move(trp) );
                }
                // we have updated the trader successfully..
                continue;
            }
            
            assetpool.add_asset(asset_id);
            auto asset = assetpool.get_asset(asset_id);
            asset->init(std::chrono::hours(24 * 30) );
            std::shared_ptr<StrategyPortfolio> sigmaker = std::make_shared<StrategyPortfolio>(conn, asset, strategyportfolio_id);
            TraderParameters trp_x(conn);
            std::shared_ptr<TraderParams> trp = trp_x.load(trparam_id);
            auto assetexec = std::make_shared<AssetExecutor>(trader_id, asset, std::move(sigmaker), std::move(trp), comm);
            executors.emplace(std::make_pair(trader_id, assetexec));
            // spawn a new thread.
            #if 1
            std::thread t(&AssetExecutor::run, executors[trader_id]);//, &communicator, &sqlconn, assetid);
            std::swap(t, threads[trader_id]);
            if (t.joinable()) t.join();
            #endif
        }

        // let's go through the running ones and then check if one of them is disabled now.

        for(auto& exec : executors) {
            int trader_id = exec.first;
            if (!Z.count(trader_id)) {
                // stop it.
                std::cout << boost::format("[%s] Sending a STOP signal to the trader %d.") % 
                                    tiempo::now().to_string() % trader_id << std::endl;
                exec.second->stop();
            }
        }
        
        std::this_thread::sleep_for(std::chrono::milliseconds(100) );
        // now check all those that might have been finished.
        auto it = executors.begin();
        while (it != executors.end()) {
            int trader_id = it->first;
            if (it->second->is_running()) {
                //std::cout << "Executor for asset " << assetid << " is still running." << std::endl;
                ++it;
                continue;
            }
            std::cout << boost::format("[%s] Killing executor for the trader [id=%d].") % 
                                tiempo::now().to_string() % trader_id << std::endl;
            auto it_t = threads.find(trader_id);
            it_t->second.join();
            threads.erase(it_t);
            
            auto it_to_del = it;
            it->second.reset();
            ++it;
            executors.erase(it_to_del);
        }

    }
};





int main()
{
    // ProfitPreserver::debug_level  = 1;
    std::cout << boost::format("[%s] Autoexecution started.") % tiempo::now().to_string()  << std::endl;
    DistributedExecutor process;
    std::cout << "DistributedExecutor constructed." << std::endl;
    while(true) {
        process.run();
        std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    }
    return 0;

    
    return 0;
}
