#include <cmath>
#include "asset.hpp"
#include "strategy.hpp"
#include "prog_opts.hpp"

#include <boost/lexical_cast.hpp>

int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<std::string>("-a", "--asset", OptionType::String, "asset", "QOQ4");
    opts.add_option<std::string>("-s", "--strategy", OptionType::String, "strategy", "");
    opts.add_option<std::string>("-p", "--param", OptionType::String, "param", "");
    opts.add_option<int>("-q", "--period", OptionType::Integer, "freq", 30);
    opts.add_option<std::string>("--from", OptionType::String, "date_from", "");
    opts.add_option<std::string>("--to", OptionType::String, "date_to", "");
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string asset_parameter = opts.option<>("asset");
    std::string strategy_name = opts.option<>("strategy");
    std::string strategy_param = opts.option<>("param");
    int freq = opts.option<int>("freq");
    std::string date_from = opts.option<>("date_from");
    std::string date_to = opts.option<>("date_to");

    if (strategy_name.empty() || strategy_param.empty())  {
        std::cout << "INVALID PARAMETER.\n";
        std::cout << opts.help() << std::endl;
        return 0;
    }

    auto conn = std::make_shared<sql_connection>();
    std::cout << "Going to create an asset." << std::endl;
    std::shared_ptr<Asset> asset;

    try {
        int asset_id = boost::lexical_cast<int>(asset_parameter);
        asset = std::make_shared<Asset>(conn, asset_id);
    } catch( ... ) {
        std::string asset_name = asset_parameter;
        asset = std::make_shared<Asset>(conn, asset_name);
    }
    std::cout << "Got the asset: " << asset->get_name() << std::endl;


    tiempo::datetime date_1(date_from);
    tiempo::datetime date_2(date_to);

    auto how_far_back = (tiempo::now() - date_1) + 300 * std::chrono::minutes(freq);

    asset->init(how_far_back);//std::chrono::hours(24*30) );
    std::cout << "Last price: " << asset->get_last_price() << std::endl;
    std::cout << "Last date: " << asset->get_last_date() << std::endl;
    
    int offset = 0;

    auto strategy = std::make_shared<Strategy>(conn, strategy_name, strategy_param, freq);
    std::cout << "Indicator ID: " << strategy->get_id() << std::endl;

    date_1 = offset_date(date_1, freq, offset);
    date_2 = offset_date(date_2, freq, offset);

    const auto& ticks = asset->get_ticks();
    tiempo::duration dur = std::chrono::minutes(freq);

    std::cout << boost::format("%19s %8s %6s %7s %6s %8s %8s")%
        "date @ time" % "price" % "Pbuy" % "sig" % "DIFF" % "return" % "profit" << std::endl;
    double total_profit = 0.0;

    size_t sig_cnt = 0;
    size_t counter = 0;
//std::cout << "1" << std::endl;
    for(auto date = date_1 ; date <= date_2; date += dur) {
        if (!asset->get_exchange().is_market_open(date)) {
            continue;
        }

//std::cout << "2" << std::endl;

        std::tuple<double,std::string> res = strategy->calculate_signal(asset, date);
        // the actual return at this time.
        auto candle = make_candle(ticks, date, dur);
        if (!candle.v) {
            continue;
        }
//std::cout << "3" << std::endl;
        double p_buy = std::get<0>(res);
        auto siq = signal_from_buy_probability(p_buy);


//std::cout << "4" << std::endl;
        double cur_price = ticks.get_price(date);
        double mean_price = ticks.get_mean_price(date, date + dur);
        double mean_gain = mean_price - candle.p_o;
        assert( candle.p_o != 0.0);
        double mean_return = mean_gain / fabs(mean_price);
        int sig = siq == signal_type::Buy ? 1 : (siq == signal_type::Sell ? -1 : 0);
        double profit = mean_return * sig;
        total_profit += profit;
//std::cout << "5" << std::endl;
        std::cout << boost::format("%19s %8.3f %6.2f %7s %6.3f %7.3f%% %7.3f%%")
            %date.to_string() % cur_price % p_buy % signal_to_string(siq) % mean_gain % (100.0 * mean_return) % (100.0*profit) << std::endl;
        sig_cnt += (sig != 0 ? 1 : 0);
        ++counter;
    }

    std::cout << boost::format("Total mean profit: %7.3f%%")% (100.0*total_profit) << std::endl;
    std::cout << boost::format("Nonzero signal rate: %.2f%%") % (sig_cnt*100./counter)<<std::endl;
    
    return 0;
}
