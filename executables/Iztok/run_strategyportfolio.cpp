#include <cmath>
#include "asset.hpp"
#include "strategy_portfolio.hpp"
#include "prog_opts.hpp"

#include <boost/lexical_cast.hpp>

int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option<std::string>("-a", "--asset", OptionType::String, "asset", "QOQ4");
    opts.add_option<int>("-s", "--strategyportolio", OptionType::Integer, "strategyportfolio", 0);
    opts.add_option<std::string>("--from", OptionType::String, "date_from", "");
    opts.add_option<std::string>("--to", OptionType::String, "date_to", "");
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }
    
    std::string asset_parameter = opts.option<>("asset");
    int strategyportfolio_id = opts.option<int>("strategyportfolio");
    std::string date_from = opts.option<>("date_from");
    std::string date_to = opts.option<>("date_to");

    auto conn = std::make_shared<sql_connection>();
    std::cout << "Going to create an asset." << std::endl;
    std::shared_ptr<Asset> asset;

    try {
        int asset_id = boost::lexical_cast<int>(asset_parameter);
        asset = std::make_shared<Asset>(conn, asset_id);
    } catch( ... ) {
        std::string asset_name = asset_parameter;
        asset = std::make_shared<Asset>(conn, asset_name);
    }
    std::cout << "Got the asset: " << asset->get_name() << std::endl;
    
    tiempo::datetime date_1(date_from);
    tiempo::datetime date_2(date_to);


    auto portfolio = std::make_shared<StrategyPortfolio>(conn, asset, strategyportfolio_id);
    int freq = portfolio->get_freq();
    int offset = portfolio->get_offset();


    auto how_far_back = (tiempo::now() - date_1) + 300 * std::chrono::minutes(freq);

    asset->init(how_far_back);//std::chrono::hours(24*30) );
    std::cout << "Last price: " << asset->get_last_price() << std::endl;
    std::cout << "Last date: " << asset->get_last_date() << std::endl;
    
    
    date_1 = offset_date(date_1, freq, offset);
    date_2 = offset_date(date_2, freq, offset);
    
    const auto& ticks = asset->get_ticks();
    tiempo::duration dur = std::chrono::minutes(freq);

    double total_profit = 0.0;
    size_t sig_cnt = 0;
    size_t counter = 0;
    for(auto date = date_1 ; date <= date_2; date += dur) {
        if (!asset->get_exchange().is_market_open(date)) {
            continue;
        }
        if (!asset->get_exchange().is_in_trading_day(date)) {
            continue;
        }
        signal_type siq;
        tiempo::datetime on_date;
        std::tie(siq,on_date) = (*portfolio)(date);
        
        // the actual return at this time.
        auto candle = make_candle(ticks, date, dur);
        if (!candle.v) {
            continue;
        }
        double cur_price = ticks.get_price(date);
        double mean_price = ticks.get_mean_price(date, date + dur);
        double mean_gain = mean_price - candle.p_o;
        assert( candle.p_o != 0.0);
        double mean_return = mean_gain / fabs(mean_price);
        int sig = siq == signal_type::Buy ? 1 : (siq == signal_type::Sell ? -1 : 0);
        double profit = mean_return * sig;
        total_profit += profit;
        std::cout << boost::format("%19s %8.3f %7s %6.3f %7.3f%% %7.3f%%")
            %date.to_string() % cur_price % signal_to_string(siq) % mean_gain % (100.0 * mean_return) % (100.0*profit) << std::endl;
        sig_cnt += (sig != 0 ? 1 : 0);
        ++counter;
    }

    std::cout << boost::format("Total mean profit: %7.3f%%")% (100.0 * total_profit) << std::endl;
    std::cout << boost::format("Nonzero signal rate: %.2f%%") % (sig_cnt*100./counter)<<std::endl;
    
    return 0;
}
