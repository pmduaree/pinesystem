#include <iostream>

#include "asset.hpp"

int main()
{
    auto conn = std::make_shared<sql_connection>();
    auto asset = std::make_shared<Asset>(conn, "Astrazeneca");
    asset->init(std::chrono::hours(24 * 20) );

    auto ticks = asset->get_ticks();
    std::cout << "WE HAVE TICKS FROM : " << ticks.begin()->first.to_string() <<
                " TO " << ticks.rbegin()->first.to_string() << std::endl;


    auto my_date = offset_date(ticks.rbegin()->first, 60, 0);
    time_series<CandleType> candles;
    make_candles(ticks, 60, my_date, 30, candles);
    std::cout << "We have candles from: " << candles.begin()->first << " to " << candles.rbegin()->first << std::endl;
    return 0;
}
