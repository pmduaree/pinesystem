#include <iostream>

#include "Pseudo_Trader.hpp"
#include "../trunk/Utils/prog_opts.hpp"

class LiveQT
{
private:
    std::shared_ptr<sql_connection> conn;
    std::vector<std::tuple<int,double > >  StrategiesPortfolio;
public:
    LiveQT():conn(std::make_shared<sql_connection>())
    {
        load_Portfolio_list();
    };
    ~LiveQT(){};
    
private:
    void load_Portfolio_list()
    {
        StrategiesPortfolio.clear();
        conn->lock();
        std::shared_ptr<sql::Connection> _con = conn->connection();
        std::string query =
        (boost::format("SELECT * FROM `Main`.`Strategies`  \
                       WHERE `LiveTracking` = 1 AND `disabled` = 0 ") ).str();
        std::unique_ptr<sql::Statement> stmt(_con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        
        while(res->next()) {
            int id_strat  = res->getInt("id");
            double money     = res->getDouble("InvestingMoney");
            
            StrategiesPortfolio.push_back( std::make_tuple(
                id_strat,
                money
                ) );
        }
        conn->unlock();
    }
public:
    void run()
    {
        for (auto x:StrategiesPortfolio){
            int ID       = std::get<0>(x);
            double money = std::get<1>(x);
            
            Pseudo_Trader _Trader(conn, ID  );
            _Trader.Set_Push_IntoDB(true);
            _Trader.Set_LiveTradeStatus(true);
            _Trader.RunLive(tiempo::now(),money);
            
        }
    }
};
int main(int argc, char** argv)
{
    std::cout << "Live QT calculating " ;
    
    LiveQT Live;
    
    Live.run();
    
    while (true)
    {
        LiveQT Live;
        
        Live.run();
        std::this_thread::sleep_for(std::chrono::seconds(5) );
        std::cout <<  std::endl << std::endl << std::endl;
    }
	
}
