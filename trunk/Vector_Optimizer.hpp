//
//  Vector_Optimizer.h
//  
//
//  Created by David de la Rosa on 7/7/14.
//
//

#ifndef ____Vector_Optimizer__
#define ____Vector_Optimizer__

#include <iostream>
#include "Pseudo_Trader.hpp"

#define Name_Col    0
#define Min_Col     1
#define Max_Col     2
#define Step_Col    3
#define Range_Col   4

#define _OPTIMIZATION_PROCESS_STARTED 1
#define _OPTIMIZATION_PROCESS_ENDED 2

class Vector_Optimizer
{
public:
    
     Vector_Optimizer(std::shared_ptr<sql_connection> __conn,int __StrategyID);
    ~Vector_Optimizer(){};
    std::string to_string();
    void Run (tiempo::datetime from, tiempo::datetime to , double money,int Scale );
    
private:
    
    
    
    std::tuple<double,int,double>
           SingleRun(std::string Param,int freq);
    void   SetUpOptmization();
    
    void push_Param_toDB(std::string ,int freq);
    void Populate_DB(int scale);
    void Load_Parameters();
    void get_OptipmizationID();
    void Push_SolutionToDB(std::tuple<double,int,double>  , int );
    void Change_Order_Status(int);
    
    std::tuple<int,std::string,int>
          fetch_Params();
    
    void MiniRun();
    bool SamplesNeedToRunn();
    
    int No_double_Param(std::string, int );
    
    double RandomDouble(double,double);
    int    RandomInt   (int,int );
    unsigned long int Loop_Size(unsigned long int );

    std::shared_ptr<sql_connection> conn;
    int Optimization_Scale;
    int N_int;
    int N_doub;
    int OptimizationID;
    int Strategy_ID;
    double InvestingMoney;
    int SignalerEngine_ID;
    int EncoderEngine_ID;
    tiempo::datetime from_date;
    tiempo::datetime to_date;
    
    std::vector<std::tuple<std::string , int,int,int,int>> Int_Variables_Param;
    std::vector<std::tuple<std::string , double,double,double,double>> Doub_Variables_Param;
};

#endif /* defined(____Vector_Optimizer__) */
