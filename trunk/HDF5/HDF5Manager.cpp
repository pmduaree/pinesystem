#include "HDF5Manager.hpp"

HDF5Manager::HDF5Manager(std::string filename)
{
	std::string f = (boost::format("%s/%s" % PATH % filename)).str();
	//opens the file
	if(openFile(f)) //if the file doesn't exist, create it
	{
		createFile(f); //create file
		openFile(f); //opens it again
	}
	createDataset();
}
void HDF5Manager::createFile(std::string filename)
{
	H5Fcreate(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT, H5P_DEFAULT);	
}
bool HDF5Manager::openFile(std::string filename)
{
	try
    {
		// Turn off the auto-printing when failure occurs so that we can
		// handle the errors appropriately
		Exception::dontPrint();
		H5std_string f(filename);
		// Open an existing file and dataset.
		file = new std::shared_ptr<H5File>(f, H5F_ACC_RDWR);

    }  // end of try block

    // catch failure caused by the H5File operations
    catch(FileIException error)
    {
		error.printError();
		return false;
    }
    return true;

}
void HDF5Manager::createType(int ncols)
{
	type = new std::shared_ptr<CompType>(ncols);
	hid_t strtype = H5Tcopy(H5T_C_S1);                     /* Datatype ID */
	H5Tset_size(strtype, 23);
	type->insertMember( "Date", 0, strype);
	for(int i = 1; i < ncols + 1; i++)
		type->insertMember( "Data", i, PredType::NATIVE_DOUBLE);

}
void HDF5Manager::appendData(std::string date, std::vector<double> data)
{
	const hsize_t ncols = data.size() + 1;
	createType(ncols);
	DataSet dataset*;
	try
	{
		dataset = file->openDataSet(DATASET_NAME); //open the dataset
	}
	catch( DataSetIException error )
	{
		//no dataset in the file (just created)
		hsize_t dim[2] = {0, ncols};
		DataSpace dataspace(2, dim);
		dataset = file->createDataSet(DATASET_NAME, type, dataspace);
	}


	//extend the dataset by 1 
	DataSpace dataspace = dataset->getSpace();
	hsize_t dims_out[2];
	int ndims = dataspace.getSimpleExtentDims( dims_out, NULL);
	dims_out[0] = dims_out[0] + 1;
	dataset->extend(dims_out);

	//select the subspace of the array to write (the one we extended earlier)
	hsize_t dims_hs = {1, dims_out[1]}
	hsize_t offset = {dims_out[0] - 1 ,0};
	dataspace.selectHyperslab(H5S_SELECT_SET, dims, offset);

	//define new space
	DataSpace mspace(2, dims_hs);

	//prepare data to write in the file
	switch(ncols)
	{
		case 4: //tick
			tick t[1];
			t[0].date = date;
			t[0].b = data[0];
			t[0].a = data[1];
			t[0].m = data[2];
			dataset->write(t, type);

			break;
		case 6: //candles
			candle c[1];
			c[0].date = date;
			c[0].o = data[0];
			c[0].h = data[1];
			c[0].l = data[2];
			c[0].c = data[3];
			c[0].v = data[4];

			dataset->write(c, type);
			break;

		default: //don't do anything
			break;
	}
	delete dataset;

}
void HDF5Manager::appendData(tiempo::datetime date, std::vector<double> data)
{
	appendData(date.to_string(), data);
}
void HDF5Manager::HDF5Manager::close()
{
	status = H5Fclose(file_id);
}