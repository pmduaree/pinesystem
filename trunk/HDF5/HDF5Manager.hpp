#include "hdf5.h"
#include "boost/format.hpp"
#include "Tiempo/tiempo.hpp"


const H5std_string	DATASET_NAME("dset");

typedef struct candle {
	std::string date;
	double o, h, l, c, v;
};
typedef struct tick{
	std::string date;
	double b, a, m;
};

class HDF5Manager
{
private:
	std::shared_ptr<H5File> file;
	std::shared_ptr<CompType> type;

public:
	HDF5Manager(std::string);
	void close();
	void appendData(tiempo::datetime date, std::vector<double> data);
	void appendData(std::string date, std::vector<double> data);
private:
	bool openFile(std:string);
	void createFile(std::string);
	void createType(int ncols);
}