#ifndef hmm_strategy_engine
#define hmm_strategy_engine

#include <iostream>
#include <vector>
#include <boost/format.hpp>
#include "../Utils/stringparpars.hpp"

#include "strategy_engine.hpp"
#include "hmm/hmm_indicator.hpp"

class Strategy_hmm : public StrategyEngine_Single
{
  public:
    Strategy_hmm() : StrategyEngine_Single() {}
    virtual ~Strategy_hmm() {}
    
    virtual std::tuple<int,double,std::string>
    calculate_signal(std::shared_ptr<Asset> asset, const std::string& param, int freq,
                     const tiempo::datetime_type& date,bool TradeStatus)
    {
        std::cout << "Strategy HMM Single asset calculate signal "<<  std::endl;
        // compose the data
        tiempo::duration dfreq(freq * 60);
        const auto& tix = asset->get_ticks();
        CandleSeries candles;
        tiempo::datetime_type date_x = date - dfreq;
        make_candles(tix, freq, date_x, 50, candles);
        if (candles.size() < 48) {
            std::cerr << boost::format("Insufficient data for HMM [%d;%s], on date %s")
                        % freq % param % date.to_string() << std::endl;
            return err_result;
        }
        //std::cout << "Candles: " << candles.size() << " items" << std::endl;
        time_series<double> P;
        for(const auto& c : candles)
            P[c.first + dfreq] = c.second.p_c;
        std::cout << std::endl;
        auto Px = make_log_returns(P);
        std::vector<double> O_train;
        for(const auto& p : Px) 
            O_train.push_back(p.second);
        
        HMM_indicator hmm_predict;
        auto rv = hmm_predict.calculate(param, O_train, TradeStatus);
        if (std::get<1>(rv) < 0) {
            return err_result;
        }
        return rv;
    }
};


#endif

