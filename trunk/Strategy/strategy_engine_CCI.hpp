#ifndef CCI_strategy_engine
#define CCI_strategy_engine

#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <numeric>

#include "strategy_engine.hpp"

#include "../Utils/stringparpars.hpp"

class Strategy_CCI : public StrategyEngine
{
  public:
    Strategy_CCI() : StrategyEngine() {}
    virtual ~Strategy_CCI() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        StringParamParser paramparser(param);
        //------------------------------------
        //StringParamParser params(param);
        size_t nperiods = paramparser.get<int>("nperiods", 20);
        size_t thres = paramparser.get<double>("threshold", 1.0);

        size_t nstat = std::max<size_t>(80, 5*nperiods);
        tiempo::duration dfreq(freq * 60);
        const auto& tix = asset->get_ticks();
        //std::cout <<" Number of ticks: " << tix.size() << " from " << tix.begin()->first << " to " << tix.rbegin()->first << std::endl;
        price_series candles;
        tiempo::datetime_type date_x = date - dfreq;
        int n_ticks = nstat + nperiods + 5;
        make_candles(tix, freq, date_x, n_ticks, candles);
        
        auto it = candles.rbegin();
        while(it != candles.rend() && it->first > date_x) ++it;
        if (it == candles.rend() ) {
            return std::make_tuple(0.5, "n/a");
        }

        if (candles.size() < 3* nperiods + 1) {
            std::cerr << "NOT ENOUGH DATA FOR CCI\n";
            return std::make_tuple(0.5, "n/a");
        }
        
        auto typical_price = [](const CandleType& c) { return (c.p_l + c.p_h + c.p_c)/3.0; };
        
        std::vector<double> TC;
        TC.reserve(nstat+2);
        
        while(it != candles.rend() ) {
            double tc = typical_price(it->second);
            TC.push_back(tc);
            ++it;
        }
        //std::cout << "GOT TC." << std::endl;
        std::vector<double> DIF;
        DIF.reserve(TC.size() - nperiods);
        for(size_t i=0;i<TC.size()-nperiods;++i) {
            double sma = std::accumulate(TC.begin()+i, TC.begin()+i+nperiods, 0.0, 
                                         [](double a,double b){return a+b;}) / nperiods;
            double dval = TC[i] - sma;
            DIF.push_back(dval);
        }
        //std::cout << "GOT DIF." << std::endl;

        std::vector<double> MDIF;
        DIF.reserve(DIF.size() - nperiods);
        for(size_t i=0;i<DIF.size()-nperiods;++i) {
            double mean_dif = std::accumulate(DIF.begin()+i,DIF.begin()+i+nperiods,0.0, 
                                              [](double xp,double x) { return xp+fabs(x); })/nperiods;
            MDIF.push_back(mean_dif);
        }
        //std::cout << "GOT MDIF." << std::endl;

        std::vector<double> CCI;
        CCI.resize(MDIF.size());
        std::transform(MDIF.begin(), MDIF.end(), DIF.begin(), CCI.begin(), 
                       [](double m,double v) { return v / m;});
        //std::cout << "CCI:\n" << CCI << std::endl;
        // what's the average mean value?
        double std_cci = sqrt(std::accumulate(CCI.begin(),CCI.end(),0.0, [](double xp,double x) ->double { return xp+x*x; })/DIF.size());
        //std::cout << "std_cci: " << std_cci << std::endl;
        for(auto& c : CCI) c *= 1.0 /  std_cci;
        //std::cout << "CCI AFTER:\n" << CCI << std::endl;
        // now just take the first two.
        double cci_0 = CCI[0];
        double cci_1 = CCI[1];
        double p = 0.5;
        if (cci_1 < -thres && cci_0 > -thres) {
            // buy signal.
            p = 0.95;
        } else if (cci_1 > thres && cci_0 < thres) {
            // sell signal
            p = 0.05;
        } else if (cci_1 < -thres && cci_0 < -thres && cci_0 > cci_1) {
            // almost of buy signal.
            p = 0.90;
        } else if (cci_1 > thres && cci_0 > thres && cci_0 < cci_1) {
            // almost sell signal
            p = 0.10;
        } else if (cci_1 < -thres && cci_0 < -thres) {
            // weak buy
            p = 0.75;
        } else if (cci_0 > thres && cci_1 > thres)  {
            // weak sell
            p = 0.25;
        } else if (cci_0 < -thres) {
            // weaker buy
            p = 0.65;
        } else if (cci_0 > thres) {
            // weaker sell
            p = 0.35;
        }
        StringParamParser resp;
        resp.put("cci0", cci_0);
        resp.put("cci1", cci_1);
        resp.put("stdc", std_cci);
        auto r = resp.to_string();
        return std::make_tuple(p, r);
    }
 
};


#endif

