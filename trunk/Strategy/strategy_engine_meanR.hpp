//
//  File.h
//  MeanR.hpp
//
//  Created by David de la Rosa on 5/19/14.
//  Copyright (c) 2014 ___VixTrend___. All rights reserved.
//

#ifndef _MeanReversion_strategy_engine
#define _MeanReversion_strategy_engine

#include <iostream>
#include <vector>

#include "strategy_engine.hpp"
#include "Pair/MeanReversion.hpp"
#include "../Tiempo/ticks.hpp"


class Strategy_MeanReversion : public StrategyEngine_Multiple
{
    public:
    Strategy_MeanReversion() : StrategyEngine_Multiple(){}
    virtual  ~Strategy_MeanReversion() {}
    
    virtual std::tuple<int,double,std::string>
    calculate_signal(std::shared_ptr<MAsset> asset, const std::string& param, int freq, const tiempo::datetime_type& date,bool TradeStatus)
    {
    
   // auto MultipleAsset = asset;
        
     std::cout << "Strategy Mean Reversion calculate signal "<<  std::endl;
     asset->Merge_Assets();
	StringParamParser MParam(param);
    
        std::cout << "MParam: " << MParam.to_string() <<
                    " last date " <<  asset->get_last_date() <<
                    " Calculating on date" <<  date.to_string() << std::endl;
  
         auto tix = asset->get_ticks();
    
        std::vector<double> prices;
        tiempo::datetime_type Fetch;
        int length = 5*MParam.get<int>("M",300) ;
       
         tiempo::datetime_type LastDate =  tix.rbegin()->first;
         tiempo::datetime_type OlderDate = tix.begin()->first;
        
        std::cout << "Last=" << LastDate.to_string() << " Older=" << OlderDate.to_string()<< std::endl;
        for(Fetch = LastDate ; Fetch  >= OlderDate ; )
        { //  std::cout << "push date " << Fetch << " value=" << tix.get_price(Fetch) << std::endl;
            try {
                prices.push_back( tix.get_price(Fetch)   );
                Fetch -= std::chrono::minutes(freq);
            } catch (...) {
                std::cout << "Not enought values, just " << prices.size() << " fetched on date "<< Fetch.to_string() << std::endl;
                // return std::make_tuple(0.0,"ACTION=WAIT,comment=NotEnoughData");
            }
        }
        
        //prices contructed backwards
        std::vector<double> correct_price;
        
        for(auto it= prices.crbegin() ; it!=prices.crend() ; it++  )
            correct_price.push_back( *it);
        
        if ((int)correct_price.size() <  length*0.8 )
        {
            std::cout << "Not enought values, just " << prices.size() << " fetched"<< std::endl;
            return err_result;;
        }
        
        std::cout << "Constructing Spread with "<< correct_price.size() << " data points"<< std::endl;
        
        MeanReversion Spread(MParam, correct_price );
        std::cout << "About to calculate Signals" << std::endl;
        double Enter=  Spread.FetchEnter(correct_price);
        double Exit =  Spread.FetchExit(correct_price);
        double Response=(double)0.0;
        int    Emited_signal= EMIT_WAIT_TRADE_SIGNAL;
        std::cout << "signal calculated Enter=" <<Enter << " Exit=" << Exit<< std::endl;
        
        std::string comment("ACTION=WAIT,comment=DoNothing");
        if ( Enter != 0  )
        {   Response=(double) 1.0;
            Emited_signal = EMIT_ENTER_TRADE_SIGNAL;
            
            if (Enter == -1)
              comment = "ACTION=DOWN,comment=EnterTrade";
            
            if (Enter == 1)
                comment = "ACTION=UP,comment=EnterTrade";

        }
        if (Exit != 0 )
        {   Response=(double) -1.0;
            Emited_signal = EMIT_EXIT_TRADE_SIGNAL;
            comment = "ACTION=EXIT,comment=ExitTrade";
        }
        
/*        if (Exit != 0  && Enter != 0  )
        {   Response=(double) -1.0;
            comment = "ACTION=EXIT,comment=ExitTrade";
        }
        
  */
      //  Spread.PrintToFile("MeanR_"+date.to_string()+".txt",correct_price);
        
        
       // std::cout<< "About to exit" << std::endl;
       /* std::tuple<int,int,int> Parsed= tiempo::parse_time(date.to_string("%Y-%m-%d %H:%M:%S"));
        
        if (std::get<1>(Parsed) == 0 )// buy
        { Response = 1.0; comment="ACTION=DOWN,comment=EnterTrade";}
        
        if (std::get<1>(Parsed) == 20 )// SELL
        { Response = 1.0; comment="ACTION=UP,comment=EnterTrade";}
        
        if (std::get<1>(Parsed) == 40 )// SELL
        { Response = -1.0; comment="ACTION=EXIT,comment=ExitTrade";}
        
        */
        std::cout << "exit MeanR" <<std::endl;
        return std::make_tuple(Emited_signal,Response,comment);
        
    }
    
};
#endif /* defined(__MultiAsset__File__) */
