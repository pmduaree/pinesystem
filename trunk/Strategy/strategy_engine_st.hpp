#ifndef st_strategy_engine
#define st_strategy_engine

#include <iostream>
#include <vector>

#include "../Utils/linalg.hpp"
#include "strategy_engine.hpp"

class Strategy_st : public StrategyEngine
{
  public:
    Strategy_st() : StrategyEngine() {}
    virtual ~Strategy_st() {}

    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        //std::cout << "StrategysignalST start" << std::endl;
        // parse the parameters.
        StringParamParser params(param);
        int w = params.get<int>("w", 0);
        if (w<=0) return err_result;
        const auto& tix = asset->get_ticks();
        auto date_w = date - tiempo::duration(w * 60 * freq);
        time_series<double> X;
        auto it = tix.rbegin();
        while(it != tix.rend() && it->first > date) ++it;
        while(it != tix.rend() && it->first >= date_w) {
            X[it->first] = it->second.p;
            ++it;
        }

        if (X.size() < 2 ) {
            return std::make_tuple(0.5, "alpha=n/a");
        }
        
        auto date0 = X.begin()->first;
        auto p0 = X.begin()->second;
        
        std::vector<double> tt,xx;
        for(const auto& z : X) {
            double t_dif = tiempo::duration(z.first - date0).seconds() / 3600.;
            double pft = (z.second - p0) / fabs(p0);
            tt.push_back(t_dif);
            xx.push_back(pft);
        }
        double c, alpha;
        std::tie(c,alpha) = linear_fit(tt, xx);
        
        double ppos = 0.5 + atan(1e4 * alpha) / M_PI;
        std::string r = (boost::format("alpha=%.9e")%  alpha).str();
        //std::cout << "StrategysignalST stop" << std::endl;
        return std::make_tuple(ppos, r);
    }
};


#endif


