#ifndef __strategy__
#define __strategy__

#include "../Tiempo/tiempo.hpp"
//#include "../signals.hpp"
#include "../SQL/sql_connection.hpp"
#include "../Assets/vasset.hpp"
#include "strategy_engine.hpp"
#include "strategy_engines.hpp"
#define MULTIPLE_ASSET_STRAT      0
#define SINGLE_ASSET_STRAT        1


class Strategy
{
  protected:
    int strategy_id;
    std::string type;
    std::string param;
    int freq;
    int TypeofVasset;
    
    std::shared_ptr<sql_connection> conn;
  public:
    Strategy(std::shared_ptr<sql_connection> conn_, 
             const std::string& type_, const std::string& param_, int freq_,int Assettype_);
    Strategy(std::shared_ptr<sql_connection> conn_, int id_);
    void Set_Params(std::string Params_){param = Params_;}
    
    ~Strategy();
    void save();
    
    int get_freq() const { return freq; }
    int get_id() const { return strategy_id; }
    const std::string& get_type() const { return type; }
    const std::string& get_param() const { return param; }
    tiempo::duration get_period() const { return tiempo::duration(freq * 60); }
    
    std::tuple<int,double,std::string> calculate_signal(std::shared_ptr<Asset> asset,
                                                    const tiempo::datetime& date, bool TradeStatus);
    
    std::tuple<int,double,std::string> calculate_signal(std::shared_ptr<MAsset> asset,
                                                        const tiempo::datetime& date, bool TradeStatus);
    std::string get_info() const;
    
  private:
    std::shared_ptr<StrategyEngine_Single>   _engine_S;
    std::shared_ptr<StrategyEngine_Multiple> _engine_M;
};

#endif
