//
//  MeanReversion.hpp
//  
//
//  Created by David de la Rosa on 5/24/14.
//
//

#ifndef _MeanReversion_hpp
#define _MeanReversion_hpp

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "../../Utils/stringparpars.hpp"

#define UP_MeanR 1
#define DOWN_MeanR -1
#define WAIT_MeanR 0
#define EXIT_MeanR 1


/////////////////////////////////////////////////////////
int Crossing_Signals(std::vector<double> A,std::vector<double>B,int day)
{
    
    
    double Day=(double)day;
    double P1= A[day-1];
	double P2= A[day];
	double E1= B[day-1];
	double E2= B[day];
	
	double x1 = day-1;
	double x2 = day;
	double x3 = x1;
	double x4 = x2;
	
	double y1 = P1;
	double y2 = P2;
	double y3 = E1;
	double y4 = E2;
	
	double X_ = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/
    ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    
	//double Y_ = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/
	//		((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    
    if ( Day-1.0 < X_ && X_ < Day  )
        return 1;
    else
        return 0;
    
}



/////////////////////////////////////////////////////////
std::vector<double> EMA(int unsigned N, std::vector<double> in )
{
    double K = (2.0/((double)N+1.0));
    
    int unsigned size=  in.size();
    std::vector<double> out;
    
    double SMA=(double)0.0;
    
    if (size >N )

        for(int unsigned i = 0 ; i<N ; i++)
        {
            SMA += (double)in[i];
            out.push_back(0.0);
        }
        else
            for(int unsigned i = 0 ; i<N ; i++)
                out.push_back(0.0);
            
    // out[0]=SMA;
    
    SMA =(double) SMA / (double)N ;
    //  out[1]=SMA;
    //  out[2]=K;
    out[N-1]=SMA;
    //out.push_back(SMA);
    
    
    for(int unsigned i =N ; i<in.size();i++)
        out.push_back ((double)  K*in[i]  + (1.0-K)*out[i-1] );
    
    // std::cout << " EMA ok" << std::endl;
    return out;
    
}
/////////////////////////////////////////////////////////
//Construct Mean Reversion Spread with the data
class MeanReversion
{
private:
    std::vector<double> Psigma;
    std::vector<double> Nsigma;
    std::vector<double> Mean;
    int unsigned N;
    int unsigned M;
    double Factor;
public:
    MeanReversion(StringParamParser Param, std::vector<double> Data)
    {
        N= Param.get<int>("N",100);
        M=Param.get<int>("M",300);
        Factor=Param.get<double>("Sigma",2.2);
        
        Mean = EMA(N,Data);
        
       //for (int i =0 ;i!=Mean.size();i++) std::cout << "Mean["<<i<<"]=" << Mean[i] << std::endl;
       
        for(auto it=Mean.begin();it!=Mean.end();it++){
            Psigma.push_back( *it );
            Nsigma.push_back( *it );
        }
        
        if(Data.size()>N && Data.size()> M ){
            int S;
            S = (N>M)? N : M;
            for(int unsigned i =S ;i!=Mean.size();i++){
                
                double mean2=(double )0.0;
                
                for (int unsigned ii= 0 ; ii < M ; ii++)
                    mean2 += Data[i-ii];
                
                mean2= (double )mean2/M;
                
                double sigma = (double) 0.0;
                for (int unsigned ii =0  ; ii!= M ; ii++){
                    sigma += (double)pow(Data[i-ii]-mean2,2.0);
                   // std::cout <<"i="<<i<<" ii="<<ii<< " P=" << pow(Data[i-ii]-mean2,2.0) << " D="<< Data[i-ii]<< " m="<< mean2 <<std::endl;
                }
                try {
                    sigma = sigma/((double)M-1);
                    
                } catch(...){
                    sigma = 1;
                    // std::cout <<"i="<<i << " sigma="<< sigma << " mean="<< mean2 << std::endl;
                    throw  std::runtime_error("division by zero , On generation MeanR Spread");
                }
                
                sigma = (double)sqrt(sigma);
                
                Psigma[i]+= (double) Factor*sigma;
                Nsigma[i]-= (double) Factor*sigma;
                
                
              //  std::cout << std::endl<< "["<<i <<"] mean=" << mean2 << " sigma=" << sigma << std::endl <<
              //  "P=" << Psigma[i] << " D=" << Data[i] << " N=" << Nsigma[i] << std::endl;
                
            }
        }
    }
    ~MeanReversion(){};
    
    
    double FetchEnter(std::vector<double> Data)
    {
        
      /*  int i= Data.size()-1;
        
         std::cout << std::endl<< "["<<i <<"]" << "P=" << Psigma[i] << " D=" << Data[i] << " Mean="<< Mean[i]<<" N=" << Nsigma[i] << std::endl;
        
        i--;
        std::cout << std::endl<< "["<<i <<"]" << "P=" << Psigma[i] << " D=" << Data[i] << " Mean="<< Mean[i]<<" N=" << Nsigma[i] << std::endl;
        */
        int End= Data.size()-1;
        
            if ( Crossing_Signals(Psigma,Data,End ) == 1 )
                if( Data[End] < Psigma[End]  )
                    return UP_MeanR;
        
            if ( Crossing_Signals(Nsigma,Data,End) == 1 )
                if( Data[End] >  Nsigma[End]  )
                    return DOWN_MeanR;
        
        return WAIT_MeanR;
    }
    double FetchExit(std::vector<double> Data)
    {
        
        double epsilon=0.0;
        double epsilon_min= Data.back();
        double epsilon_max= Data.back();
        
        for ( int unsigned i =0 ; i!= Data.size() ; i++)
        {
            epsilon_min =(double) (epsilon_min > (double)Data[i])? Data[i]: epsilon_min;
            epsilon_max =(double) (epsilon_max < (double)Data[i])? Data[i]: epsilon_max;
        }
        
        epsilon= (double) 0.001 *( epsilon_max - epsilon_min);
        int End= Data.size()-1;
        
        //std::cout << "*********D=" << Data[End] << " M=" << Mean[End] << " eps=" << epsilon << std::endl;
        if ( Crossing_Signals(Data,Mean,End) == 1 )
            return  EXIT_MeanR;
        
        if ( fabs (Data[End] - Mean[End]) < epsilon  )
            return EXIT_MeanR;
        
        return WAIT_MeanR;
    }
    void PrintToFile( std::string File,std::vector<double> Data)
    {
        std::ofstream my;
        my.open (File.c_str());
        my << "i,Data,Mean,PSigma,NSigma,CrossP_D,CrossN_D,Data_Mean" << std::endl;
        for (int unsigned i =1 ; i != Data.size(); i++)
        {
         my << i<<","<<
                Data[i]<<","<<
                Mean[i]<<","<<
                Psigma[i] <<","<<
                Nsigma[i] << "," <<
                Crossing_Signals(Psigma,Data,i ) << ","<<
                Crossing_Signals(Nsigma,Data,i ) << "," <<
                Crossing_Signals(Data,Mean,i ) <<std::endl;
        
        }
        int i =  Data.size()-1;
        my << i<<","<<
        Data[i]<<","<<
        Mean[i]<<","<<
        Psigma[i] <<","<<
        Nsigma[i] << "," <<
        Crossing_Signals(Psigma,Data,i ) << ","<<
        Crossing_Signals(Nsigma,Data,i ) << "," <<
        Crossing_Signals(Data,Mean,i )<< std::endl;
        
        my.close();
    
    }
    

};
#endif
