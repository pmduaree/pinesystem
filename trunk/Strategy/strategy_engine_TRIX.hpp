#ifndef TRIX_strategy_engine
#define TRIX_strategy_engine
/////////////////////////////////////////////////////////////////////////////
// strategy_EMA
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include "strategy_engine.hpp"
#include <algorithm>

class Strategy_TRIX : public StrategyEngine
{
  public:
    Strategy_TRIX() : StrategyEngine() {}
    virtual ~Strategy_TRIX() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        // Parse the input parameters and check the validity
        StringParamParser params(param);
        size_t nperiods = params.get<int>("nperiods", 15);
        if (nperiods <= 1) {
            return err_result;
        }
       size_t nstat = std::max<size_t>(80, 5*nperiods);
	   tiempo::duration dfreq(freq * 60);
	   const auto& tix = asset->get_ticks();   
	   auto it =tix.rbegin();
       auto date_x = date - dfreq;
       while(it != tix.rend() && it->first > date_x) ++it;
	   std::vector<double> PC;
	   PC.reserve(nstat+2);
	   while(it != tix.rend() && PC.size() < nstat + 2) {
		   auto p =  it->second.p; 
		   PC.push_back(p);
		   ++it;
	   }
	   std::vector<double> EMAOne;
	   EMAOne.reserve(PC.size() - nperiods+1);
	   for (size_t i=0;i<2*nperiods+1;++i)
	   {           
            double P = 0.0;
            double W = 0.0;
			for (size_t navg = 0;navg<nperiods;++navg){
			double w = exp( - navg * 5.0 / nperiods );
			if (w < 1e-8) break;
			//double w = period - navg;
			W += w;
			P += w * (PC[navg+i]);
			} 
			if (W==0.0) return err_result;
            double ema = P / W;
            EMAOne.push_back(ema);
        };

	   std::vector<double> EMADouble;
	   EMADouble.reserve(EMAOne.size() - nperiods+1);
	   for (size_t j=0;j<nperiods+2;++j)
	   {           
            double P2 = 0.0;
            double W2 = 0.0;
			for (size_t navg2 = 0;navg2<nperiods;++navg2){
			double w2 = exp( - navg2 * 5.0 / nperiods );
			if (w2 < 1e-8) break;
			//double w = period - navg;
			W2 += w2;
			P2 += w2 * (EMAOne[navg2+j]);
			} 
			if (W2==0.0) return err_result;
            double ema2 = P2 / W2;
            EMADouble.push_back(ema2);
        };

	   std::vector<double> EMATriple;
	   EMATriple.reserve(EMADouble.size() - nperiods+1);
	   for (size_t k=0;k<3;++k)
	   {           
            double P3 = 0.0;
            double W3 = 0.0;
			for (size_t navg3 = 0;navg3<nperiods;++navg3){
			double w3 = exp( - navg3 * 5.0 / nperiods );
			if (w3 < 1e-8) break;
			//double w = period - navg;
			W3 += w3;
			P3 += w3 * (EMADouble[navg3+k]);
			} 
			if (W3==0.0) return err_result;
            double ema3 = P3 / W3;
            EMATriple.push_back(ema3);
        };

       // double Trix_0 =(EMATriple[0]-EMATriple[1]);
       // double Trix_1 =(EMATriple[1]-EMATriple[2]);
	    double Trix_0 =(EMATriple[0]-EMATriple[1])/fabs(EMATriple[1]);
        double Trix_1 =(EMATriple[1]-EMATriple[2])/fabs(EMATriple[2]);
        //std::cout << "TRIX: " << Trix_0 << " and " << Trix_1 << std::endl;
        double p_buy = 0.5 ;
        if( Trix_0 > 0.0 &&  Trix_1 < 0.0)// p_buy=0.5 + atan(1e5 * Trix_0) / M_PI;
			p_buy=1.0;
        if( Trix_0 < 0.0 &&  Trix_1 > 0.0)// p_buy=0.5 + atan(1e5 * Trix_0) / M_PI;
			p_buy=0.0;
		std::string r = (boost::format(" Trix_0=%.2f, Trix_1=%.2f") %  Trix_0 %  Trix_1).str();
		return std::make_tuple(p_buy,r);
    }
};
         


#endif


