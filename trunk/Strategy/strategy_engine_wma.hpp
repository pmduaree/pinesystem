#ifndef wma_strategy_engine
#define wma_strategy_engine
/////////////////////////////////////////////////////////////////////////////
// strategy_WMA
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include "strategy_engine.hpp"

class Strategy_WMA : public StrategyEngine
{
  public:
    Strategy_WMA() : StrategyEngine() {}
    virtual ~Strategy_WMA() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        // Parse the input parameters and check the validity
        StringParamParser params(param);
        int period = params.get<int>("period", 0);
        if (period <= 1) {
            return err_result;
        }
        const auto& tix = asset->get_ticks();

        double err_wma = -9999.9999;

        auto calc_wma = [freq,&tix,period,err_wma](const tiempo::datetime_type& d) -> double
        {
            time_series<CandleType> candles;
            make_candles(tix, freq, d, period + 8, candles);
            
            // safe guard.
            auto date_x = d - std::chrono::minutes(freq);
            auto it = candles.rbegin();
            while(it != candles.rend() && it->first > date_x) ++it;
            if (it == candles.rend()) {
                return err_wma;
            }
            double P = 0.0;
            double W = 0.0;
            int navg = 0;
            while(navg < period && it != candles.rend() ) {
                double w = period - navg;
                W += w;
                P += w * it->second.p_c;
                ++navg;
                ++it;
            }
            if (navg < period) return err_wma;
            double wma = P / W;
            return wma;
        };

        double wma = calc_wma(date);
        if (wma == err_wma) return err_result;
        
        auto it_tix = tix.rbegin();
        while(it_tix != tix.rend() && it_tix->first > date) ++it_tix;
        double p_close = it_tix->second.p;
        
        if (it_tix == tix.rend() ) return err_result;
        
        auto get_sign = [](double wma, double p)  -> int 
        { return (p > wma ? 1 : (p < wma ? -1 : 0) ); };
        
        int current_sign = get_sign(wma, it_tix->second.p);
        std::string r;
        double p_buy = 0.5;
        if (current_sign == 0) {
            // it's a special consideration, very unlikely to happen
            ++it_tix;
            auto csign = get_sign(wma, it_tix->second.p);
            r = (boost::format("dur=0,sign=0,wma=%.3f,pcl=%.3f")%wma % p_close).str();
            p_buy = csign > 0 ? 1.0 : (csign < 0 ? 0.0 : 0.5) ;
        } else {
            auto date_cut = date - std::chrono::minutes(3 * freq);
            while( ++it_tix != tix.rend() && it_tix->first >= date_cut ) {
                auto csign = get_sign(wma, it_tix->second.p);
                if (csign*current_sign <= 0) {
                    break;
                }
            }
            if (it_tix == tix.rend() ) return err_result;
            tiempo::duration duration_from_crossing = date - it_tix->first;
            double p_either = std::max<double>(0.0, 1.0 - duration_from_crossing.seconds() * 1.0 / (60.0 * freq) );
            p_buy = 0.5 + current_sign * p_either * 0.5;
            r = (boost::format("dur=%d,sign=%d,wma=%.3f,pcl=%.3f") % (duration_from_crossing.seconds()/60) % 
                       current_sign % wma % p_close).str();
        }
        return std::make_tuple(p_buy, r);
    }
};

#endif

