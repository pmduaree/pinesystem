#include "../SQL/sql_connection.hpp"
#include "strategy.hpp"
#include "strategy_engines.hpp"

#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <boost/format.hpp>


Strategy::Strategy(std::shared_ptr<sql_connection> conn_, 
         const std::string& type_, const std::string& param_, int freq_, int Assettype_)
    : strategy_id(0), type(type_), param(param_), freq(freq_),TypeofVasset(Assettype_), conn(conn_) 
{
    if(TypeofVasset==MULTIPLE_ASSET_STRAT)
    {
        _engine_M = create_strategy_M(type);
        _engine_S = NULL;
    }else
    {
        _engine_S = create_strategy_S(type);
        _engine_M = NULL;
    }

    // check if this strategy exits.
    conn->lock();
    auto con = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::string query = (boost::format(
        "SELECT `id` AS strategy_id FROM `Main`.`Strategies` WHERE type=\'%s\' AND param=\'%s\' AND freq=%d "
        ) % type % param % freq ).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() == 1) {
        res->first();
        strategy_id = res->getInt("strategy_id");
    } else {
        strategy_id = 0;
    }
    conn->unlock();
}

Strategy::Strategy(std::shared_ptr<sql_connection> conn_, int id_  )
    : strategy_id(id_), conn(conn_)
{
    conn->lock();
    auto con = conn->connection();;
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    auto query= (boost::format("SELECT * FROM `Main`.`Strategies`  S \
                               Inner JOIN `Main`.`SignalerEngines` SE ON \
                               `S`.`SignalerEngine_id` = `SE`.`id` \
                               Inner JOIN `Main`.`SignalerType` ST ON \
				`SE`.`Type_id` = `ST`.`id`\
				WHERE `S`.`id` =%d")%strategy_id).str();
   
   std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
   if (res->rowsCount() > 0) {
        res->first();
        type = res->getString("Name");
        param = res->getString("CurrentParam");
        freq = res->getInt("freq");
        TypeofVasset = res->getInt("assetType");
       
    }
    conn->unlock();
    if (type.empty() ) {
        std::cout << "Empty Strategy Type." << std::endl;
        throw std::runtime_error("Unable to fetch strategy");
    }
	
    if(TypeofVasset==MULTIPLE_ASSET_STRAT)
    {
        _engine_M = create_strategy_M(type);
        _engine_S = NULL;
    }else
        {
        _engine_S = create_strategy_S(type);
        _engine_M = NULL;
        }
}

Strategy::~Strategy()
{
}

void Strategy::save()
{
    conn->lock();
    auto con = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::string query = (boost::format(
        "SELECT `id` AS strategy_id FROM `Main`.`Strategies` WHERE type=\'%s\' AND param=\'%s\' AND freq=%d"
        ) % type % param % freq).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() == 0) {
        // make a new one.
        query = (boost::format("INSERT INTO `Main`.`Strategies` (`type`,`param`,`freq`) VALUES (\'%s\',\'%s\',%d)")
                    % type % param % freq).str();
        stmt.reset(con->createStatement());
        stmt->execute(query);
        stmt.reset(con->createStatement());
        res.reset(stmt->executeQuery("SELECT LAST_INSERT_ID() AS strategy_id"));
    }
    res->first();
    strategy_id = res->getInt("strategy_id");
    conn->unlock();
}

std::tuple<int,double,std::string>
Strategy::calculate_signal(std::shared_ptr<MAsset> masset, const tiempo::datetime& date,bool TradeStatus)
{
    //std::cout << boost::format("Strategy[%s,%s,f=%d]::calculate_signal (on date %s)")
    //                % type % param % freq % date.to_string() << std::endl;
    if (!_engine_M) {
        std::cerr << "The engine type Multiple Asset is null." << std::endl;
        throw std::runtime_error("null strategy engine");
    }
    auto s = _engine_M->calculate_signal(masset, param, freq, date,TradeStatus);
    return s;
}
std::tuple<int,double,std::string>
Strategy::calculate_signal(std::shared_ptr<Asset> asset, const tiempo::datetime& date,bool TradeStatus)
{
    //std::cout << boost::format("Strategy[%s,%s,f=%d]::calculate_signal (on date %s)")
    //                % type % param % freq % date.to_string() << std::endl;
    if (!_engine_S) {
        std::cerr << "The engine Type Single Asset is null." << std::endl;
        throw std::runtime_error("null strategy engine");
    }
    auto s = _engine_S->calculate_signal(asset, param, freq, date,TradeStatus);
    return s;
}

std::string Strategy::get_info() const
{
    std::string s = (boost::format("%s [f=%d,p={%s}]") % type % freq % param).str();
    return s;
}


