#include <string>
#include <boost/algorithm/string/case_conv.hpp>
#include "strategy_engine.hpp"
#include "../Utils/stringparpars.hpp"

#include "strategy_engines.hpp"
#include "strategy_engine_hmm.hpp"
//#include "strategy_engine_ma.hpp"
//#include "strategy_engine_st.hpp"
//#include "strategy_engine_RSI.hpp"
//#include "strategy_engine_CCI.hpp"
//#include "strategy_engine_ROC.hpp"
//#include "strategy_engine_wma.hpp"
//#include "strategy_engine_ema.hpp"
//#include "strategy_engine_BB.hpp"
//#include "strategy_engine_TRIX.hpp"
#include "strategy_engine_meanR.hpp"
#include "strategy_engine_vectorCorr.hpp"

std::shared_ptr<StrategyEngine_Multiple> create_strategy_M(std::string type)
{  std::cout << "strategy type " << type << std::endl;
    boost::to_upper(type);
    std::shared_ptr<StrategyEngine_Multiple> engine;
    if (type == "MEANR") {
        engine = std::make_shared<Strategy_MeanReversion>();
    } 
    else if (type == "VECTORC") {
        engine = std::make_shared<Strategy_VectorCorr>();
    }
    else {
        throw std::runtime_error("unknown strategy = "+ type );
    }
    return engine;
}



std::shared_ptr<StrategyEngine_Single> create_strategy_S(std::string type)
{  std::cout << "strategy type " << type << std::endl;
    boost::to_upper(type);
    std::shared_ptr<StrategyEngine_Single> engine;
    if (type == "HMM") {
        engine = std::make_shared<Strategy_hmm>();
    }
    //else if (type == "HMM") {
    //        engine = std::make_shared<Strategy_hmm>();
    //    }
    //      else if (type == "ST") {
    //        engine = std::make_shared<Strategy_st>();
    //    } else if (type == "RSI") {
    //        engine = std::make_shared<Strategy_RSI>();
    //    } else if (type == "CCI") {
    //        engine = std::make_shared<Strategy_CCI>();
    //    } else if (type == "ROC") {
    //       engine = std::make_shared<Strategy_ROC>();
    //    } else if (type == "WMA") {
    //        engine = std::make_shared<Strategy_WMA>();
    //    } else if (type == "EMA") {
    //        engine = std::make_shared<Strategy_EMA>();
    //    } else if (type == "BO") {
    //        engine = std::make_shared<Strategy_BB>();
    //    } else if (type == "TRIX") {
    //        engine = std::make_shared<Strategy_TRIX>();
    //    }
    //else if (type == "VECTCORR") {
       // engine = std::make_shared<Strategy_VectorCorr>();
    //}
    //    else if (type == "MA") {
    //        engine = std::make_shared<Strategy_ma>();
    //    }
    else {
        throw std::runtime_error("unknown strategy = "+ type );
    }
    return engine;
}

