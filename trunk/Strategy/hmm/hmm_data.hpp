#ifndef __HMM_DATA__
#define __HMM_DATA__

#include <vector>
#include "../../Utils/tensor.hpp"

#include "hmm_common.hpp"

// there should be no include of hmm_bmm or hmm_discrete or any other. 

namespace HMM {

/////////////////////////////////////////////////////////////////////////////
/// \class HMM_data
/// \brief Hidden Markov Model data structure
///
/// HMM_data contains all parameters that define the Hidden Markov Model.
/// The probability of observing 'O', given a markov state 'q' (B_q(O))
/// is implemented in the template class BTYPE. We don't care about it here.
///
/////////////////////////////////////////////////////////////////////////////
template<class BTYPE>
class HMM_data
{
  public:
    //size_t N;  						//!< Number of Markov states
    // this is not embedded in Pi.
	std::vector<double> Pi; 		//!< Probabilities for the initial Markov state
    morana::tensor<double,2> A;     //!< Markov transition matrix
    BTYPE B;                		//!< Observation probability density module

    static_assert(std::is_base_of<HMM_B, BTYPE>::value, "BTYPE must be HMM_B");
    
    HMM_data() {}
    HMM_data(const std::vector<double>& Pi_, const morana::tensor<double,2>& A_,
             const BTYPE& B_)
        : Pi(Pi_), A(A_), B(B_) {}
    

    size_t getN() const { return Pi.size(); }
    size_t getM() const { return B.getM(); }
    HMM_data<BTYPE>& operator=(const HMM_data<BTYPE>& x)
    {
        Pi = x.Pi;
        A = x.A;
        B = x.B;
        return *this;
    }
};


} // end of namespace HMM

#endif
