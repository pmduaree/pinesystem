#ifndef hmm_indicator_hpp
#define hmm_indicator_hpp

#include <string>
#include <tuple>
#include <vector>
#include "../strategy_engine.hpp"

class HMM_indicator
{
  public:
    HMM_indicator() {}
    
    std::tuple<int,double,std::string>
    calculate(const std::string& param, const std::vector<double>& O_train, bool TradeStatus);
};

#endif
