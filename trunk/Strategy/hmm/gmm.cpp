#include <functional>
#include <algorithm>

#include <random>

#include "../../Utils/tensor.hpp"
#include "../../Utils/array_algebra.hpp"
#include "../../Utils/random.hpp"
#include "hmm_common.hpp"
#include "gmm.hpp"

namespace HMM {

ExpectationMaximizationGmm::ExpectationMaximizationGmm() 
    : M(0)
{
}
    
void ExpectationMaximizationGmm::init(size_t nM, vectorAccess m, vectorAccess v, vectorAccess a)
{
    M = nM;
    mean = m;
    variance = v;
    alpha = a;
}
    
double ExpectationMaximizationGmm::logLikelihood(const std::vector<double>& x)
{
    double L = 0;
    
    for (size_t i = 0; i < x.size(); ++i)  {
        double Lm = 0;
        for (size_t m = 0; m < M; ++m) {
            Lm += alpha(m) * gauss(x[i], mean(m), variance(m));
        }
        L += log(Lm);
    }
    
    return L;
}

void ExpectationMaximizationGmm::perform(const std::vector<double>& x, 
                                         size_t maxNumSteps, 
                                         double likelihoodConvergenceFactor)
{
    // safety first
    if (M == 0)
        throw std::runtime_error("in class ExpectationMaximizationGmm: perform has been called before init, or after an invalid initialization");
        
    // get minimal distance between observations (those that are not the same)
    double xEps = 0;
    for (size_t i = 1; i < x.size(); ++i) {
        double e = fabs(x[i] - x[i-1]);
        if (xEps == 0 || (e > 0 && e < xEps))
            xEps = e;
    }
    // assure variance is not much smaller than the smallest distance (this factor is over the thumb estimate)
    xEps /= 2; 
    xEps = xEps * xEps;
    if (xEps == 0)
        xEps = 1e-8;
    
    // estimation-maximization
    logL.clear();
    logL.reserve(maxNumSteps + 1);
    for (size_t step = 0; step < maxNumSteps; ++step) {
        morana::tensor<double, 2> r(morana::make_array<size_t,2>(x.size(), M)); // weights (the portion of each observation that belongs to a certain gaussian); also called "responsibility", therefore name "r"
        
        auto sqr = [](double a) -> double { return a*a; };
        auto normal_pdf = [](double x, double m, double stdVar)	{
            static const double inv_sqrt_2pi = 0.3989422804014327;
            double a = (x - m) / stdVar;
            return inv_sqrt_2pi / stdVar * std::exp(-0.5 * a * a);
        };
        
        // E step
        for (size_t i = 0; i < x.size(); ++i) {
            double r_sum = 0.0;
            for (size_t m = 0; m < M; ++m) {
                r(i, m) = alpha(m) * normal_pdf(x[i], mean(m), sqrt(variance(m)));
                r_sum += r(i, m);
            }
            for (size_t m = 0; m < M; ++m) {
                r(i, m) /= r_sum;
            }
        }
        
        // M step
        for (size_t m = 0; m < M; ++m) {
            double nf = 0.0;
            for (size_t i = 0; i < x.size(); ++i)  {
                nf += r(i, m);
            }
            
            alpha(m) = nf / x.size();
            
            mean(m) = 0;
            for (size_t i = 0; i < x.size(); ++i)  {
                mean(m) += r(i, m) * x[i];
            }
            mean(m) /= nf;
            
            variance(m) = 0;
            for (size_t i = 0; i < x.size(); ++i)  {
                variance(m) += r(i, m) * sqr(x[i] - mean(m));
            }
            variance(m) /= nf;
                
            // watch out for making U zero (it may happen but it is not good for further calculations)
            if (variance(m) < xEps)
                variance(m) = xEps;
        }
        
        // log likelihood
        logL.push_back(logLikelihood(x));
    }
}

void ExpectationMaximizationGmm::initialGuess(const std::vector<double>& x)
{
    auto& grand = randomGenerator();
    // safety first
    if (M == 0)
        throw std::runtime_error("in class ExpectationMaximizationGmm: initialGuess has been called before init, or after an invalid initialization");
    
    // get x stats and define a normal distributio for randomization within those stats    
    auto minmax = std::minmax_element(x.begin(), x.end());
    double rng = (*minmax.second - *minmax.first);
    if (rng == 0) rng = 1e-8;
    std::normal_distribution<double> nD(0, rng / (M * 3.0));
    
    // randomize variables
    for (size_t m = 0; m < M; ++m) { 
        mean(m) = *minmax.first + m * rng / M + nD(grand);
        variance(m) = std::max(rng / (2.0*M) + nD(grand), rng / 1000);
        alpha(m) = 1.0 / M;
    }
}

}


