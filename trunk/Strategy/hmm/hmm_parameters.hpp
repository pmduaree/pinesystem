#ifndef __HMM_PARAMETERS__
#define __HMM_PARAMETERS__
#include <iostream>
#include <string>

#include "../../Tiempo/tiempo.hpp"

namespace HMM  {

// Main set of parameters. These parameters define the model.
struct PRED_parameters
{
    size_t N;  // number of markov states
    size_t M;   // number of observations 
    size_t T;   // training window length
    std::string hmm_type;
    std::string random_type;

    std::string to_xml() const;
    void init(const std::string& s);
    std::string to_string() const;
};

// These parameters don't affect the model but only optimization 
// parameters to fit the model.
struct HMSIM_parameters
{
    size_t numBwSteps;              // maximal number of BW steps
    size_t bwConvergenceCheckLen;   // something in BW: stopping comparison
    double bwConvergenceCheckEps;   // BW eps convergence criterion
    double eps;                     // eps used in BW
    size_t numMcRuns;               // number of monte carlo runs
    size_t numModels;
    
    std::string to_xml() const;
};

// Parameters definining the data -- where it's taken from.
struct DATA_parameters
{
    std::string inputFilename, inputFormat;
    std::string exchange, ticker, field;
    int frequency;
    tiempo::datetime dateFrom, dateTo;
    std::string to_xml() const;
};

struct HMM_sim_param: public PRED_parameters,
                      public HMSIM_parameters
{
    std::string to_xml() const;
};

void set_simulation_parameters(HMSIM_parameters& p, int quality);


}



#endif
