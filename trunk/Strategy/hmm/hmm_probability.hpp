#ifndef __probability_HPP_
#define __probability_HPP_

#include <string>
#include <vector>
#include <type_traits>

/// ////////////////////////////////////////////////////////
/// PROPABIITY CLASS 
/// ////////////////////////////////////////////////////////
/// ////////////////////////////////////////////////////////
/// PDFCDF 
/// computes PDF (probability density func.) and CDF 
//  (cumulative dist. func) of 
/// the last state in the chain. NN - resolution
/// basicaly the function returns the PDF and CDF for next event O_t+1
/// /////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////

#include "hmm_viterbi.hpp"

namespace HMM {
//the last state in the chain. oSortedClasses - predefined O range
//basicaly the function returns the PDF and CDF for next event O_t+1
//
// what it does:
//  calculates the probability distribution function and the
//  cumulative distirbution function and retursnt eh likelihood of the
//  model
//
struct XDF
{
    template<class Model>
    double operator()(const Model& mod, const std::vector<double>& qdist, double cO)
    {
        double pdf = 0.0;
        for(size_t i=0;i<qdist.size();i++) { //runs over all current states 
            double sumTemp=0;
            for(size_t j=0;j<qdist.size();j++) { 
                //"jumps" from current to all states with P(Aij)
                sumTemp+=mod.B(cO,j)*mod.A(i,j);	
            }	
            pdf += qdist[i]*sumTemp;
        }
        return pdf;
    }
};

template<class Model>
struct PDFCDF
{
    // calculate the PDF and CDF, discretized on the intervals defined 
    // with bounds in 'vals'.
    void operator()(const Model& mod, const std::vector<double>& qdist,
                      const std::vector<double>& vals, 
                      std::vector<double>& PDF, std::vector<double>& CDF)
    {
        size_t nO = vals.size();
        PDF.clear();
        CDF.clear();
        XDF xdf;
        CDF.push_back(0.0);
        for(size_t i=0;i<nO-1;++i) {
            double c1 = vals[i], c2 = vals[i+1];
            double p = xdf(mod, qdist, (c1+c2)/2.0);
            double dc = c2- c1;
            PDF.push_back(p);
            CDF.push_back(p * dc + CDF.back());
        }
        // CDF[i] is the probability than x <= vals[i].
        assert( PDF.size() == CDF.size() - 1);
        assert( PDF.size() == vals.size() -1);
        assert( CDF.size() == vals.size() );
    }
};


class Probability
{
  public:
    template<class FX>
    double integrate(const std::vector<double>& bounds, 
                     const std::vector<double>& pdf, FX Fx)
    {
        size_t n_points = bounds.size();
        double csum = 0.0;
        for(size_t i=0;i<n_points-1;++i) {
            double c = (bounds[i+1] + bounds[i])/2;
            double bw = bounds[i+1] - bounds[i];
            double p = pdf[i];
            double v = Fx(c);
            csum += bw * p * v;
        }
        return csum;
    }
};


class linspace: public std::vector<double>
{
    typedef std::vector<double> VEC;
  public:
    linspace(size_t npoints, double min, double max)
    {
        VEC::resize(npoints);
        for(size_t i=0;i<npoints;++i) {
            (*this)[i] = (i * 1.0 / (npoints-1) ) * (max-min) + min;
        }
    }
};



}   // end of namespace HMM

#endif
