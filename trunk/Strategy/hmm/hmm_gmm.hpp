#ifndef __HMM_GMM__
#define __HMM_GMM__
#include <iostream>
#include <cmath>
#include <cassert>
#include <vector>
#include <string>

#include "../../Utils/tensor.hpp"
#include "../../Utils/array_algebra.hpp"

#include "hmm_statistics.hpp"

#include "hmm_common.hpp"
#include "hmm_baumwelch.hpp"
#include "hmm_random.hpp"



namespace HMM {

/// \class Bjgm
/// \brief Calculate the observation density from gaussian mixtures
///
/// Calculate the probability density B_j(o) defined as
///     B_j(o) = \sum_{m=1}^{M} c_{j,m} N(o | mu_{j,m}, U_{j,m} )
/// and $N(x | mu, sigma)$ is the normal distribution with a mean $mu$
/// and a variance $U$.
class Bjgm: public HMM_B
{
  public:
    morana::tensor<double,2> C;  //!< Gaussian mixture probabilities
    morana::tensor<double,2> mu; //!< Gaussian mixture mean values
    morana::tensor<double,2> U;  //!< Gaussian mixture variances
    std::string bType ="'Bjgm::gauss mixture';";
	
    size_t getN() const { return C.M(0); }
    size_t getM() const { return C.M(1); }
    
    Bjgm(): HMM_B() {}

    Bjgm(size_t N_, size_t M_)
    {
        auto NM = morana::make_array<size_t,2>(N_,M_);
        C.resize(NM);
        mu.resize(NM);
        U.resize(NM);
    }

    // Initialize from C-arrays
    Bjgm(size_t N_, size_t M_, 
         const double* C_, const double* mu_, const double* U_)
        : HMM_B()
    {
        auto NM = morana::make_array<size_t,2>(N_,M_);
        C.resize(NM);
        mu.resize(NM);
        U.resize(NM);
        std::copy(C_, C_ + N_*M_, C.begin());
        std::copy(mu_, mu_ + N_*M_, mu.begin());
        std::copy(U_, U_ + N_*M_, U.begin());
    }

	//sets B
	//currently we take vectors for input and fill tensors<2> with them
     void setB(size_t N_, size_t M_, const std::vector<double>& iC,
              const std::vector<double>& imu, const std::vector<double>& iU)
    {
        auto NM = morana::make_array<size_t,2>(N_,M_);
        C.resize(NM);
        mu.resize(NM);
        U.resize(NM);
        for(size_t k=0;k<M_;++k) {
            std::fill(&C(0,k), &C(0,k)+N_, iC[k]);
            std::fill(&mu(0,k), &mu(0,k)+N_, imu[k]);
            std::fill(&U(0,k), &U(0,k)+N_, iU[k]);
        }
	}

    Bjgm& operator=(const Bjgm& x)
    {
        C = x.C;
        mu = x.mu;
        U = x.U;
        return *this;
    }
    
    /// Calculate the partial probability density
    //  $c_{j,k} N(O | mu_{j,k}, U_{j,k})$
    double operator()(double O, size_t j, size_t k) const
    {
        double c = C(j,k);
        double m = mu(j,k);
        double u = U(j,k);
        if (c < 1e-15 || u < 1e-15) return 0;
        assert( c==0 || u != 0);
        double v= c * gauss(O, m, u);
        assert(!is_nan(v) );
        return v;
    }

    /// Calculate the probability density B_j(o)
    double operator()(double O, size_t j) const
    {
        double v = 0.0;
        size_t M = getM();
        for(size_t k=0;k<M;++k) {
            v += operator()(O, j, k);
        }
        return v;
    }
    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(double O, double* Bo) const
    {
        size_t N = getN();
        for(size_t i=0;i<N;++i) {
            Bo[i] = operator()(O, i);
        }
    }
    
    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(double O, std::vector<double>& Bo)
    {
        size_t N = getN();
        Bo.resize(N);
        operator()(O, Bo.data());
    }


    double predict(size_t q) const
    {
        size_t M = getM();
        double O_p = 0.0;
        for(size_t m=0;m<M;++m) {
            double o = C(q,m) * mu(q,m);
            O_p += o;
        }
        return O_p;
    }
    
    double calcPref(size_t q, double yref) const
    {
        size_t M = getM();
        double x = 0.0;
        for(size_t k=0;k<M;++k) {
            double p = (U(q,k)?
                        0.5 * std::erfc( (yref - mu(q,k))/(sqrt(2*U(q,k)))) :
                        //0.5 * (1 + std::erf(B.mu(q,k)/(sqrt(2*B.U(q,k))))) : 
                        (mu(q,k)> yref ? 1.0 : 0.0) );
            x += C(q,k) * p;
        }
        return x;
    }
};


template<>
struct BW_B<Bjgm>
{
    void operator()(Bjgm& B, const Viterbi& viterbi,
                    const std::vector<double>& O)
    {
        size_t N = B.getN();
        size_t M = B.getM();
        size_t T = O.size();
        // requires: gj, gjkt, O, eta

        std::vector<double> eta(T); for(auto& e : eta) e = 1.0;
        
        assert( !is_nan(B.C) );
        assert( !is_nan(B.mu) );
        assert( !is_nan(B.U) );
        
        std::vector<double> gj(N);
        
        // the gamma_{t}(j,k) are defined in a non-number equation following 
        // equation (54) in Rabiner.
        // we can write:
        //   gamma_t(j,k) = gamma_j(t) * W_t(j,k) / sum_m W_t(j,m)
        // where 
        //   W_t(j,k) = c_{j,k} g(Ot,mu_{jk}, U_{j,k}) 
        std::array<size_t,3> NMT; NMT[0]=N; NMT[1]=M; NMT[2]=T;
        morana::tensor<double,3> gjkt(NMT);
        std::fill(gjkt.begin(), gjkt.end(), 0.0);
        for(size_t t=0;t<T;++t)
        for(size_t j=0;j<N;++j) {
            std::vector<double> Wjk(M);
            double vk = 0.0;
            for(size_t k=0;k<M;++k)  {
                Wjk[k] = B(O[t], j, k);
                vk += Wjk[k];
            }
            blas::scal(M, (vk>1e-16 ? 1.0/vk : 0.0), &Wjk[0], 1);
            blas::axpy(M, viterbi.gjt(j,t), &Wjk[0],1,&gjkt(j,0,t),N);
        }
        assert( !is_nan(gjkt));
        
        
        
        // g(j) = \sum_t g_t(j)
        blas::gemv("N", N, T, 1.0, viterbi.gjt().data(), N, &eta[0], 1, 
             0.0, &gj[0], 1);
        assert( !is_nan(gj));

        // c(j,k) = \sum_t \gamma_t(j,k) / \sum_t \sum_k \gamma_t(j,k)
        //        = \gamma(j,k) / gamma(j)
        
        // TODO: rewrite this garbage, it looks ugly and that means it might 
        // just as well be wrong.
        for(size_t j=0;j<N;++j)
        for(size_t k=0;k<M;++k) {
            double& c = B.C(j,k);
            double& mu = B.mu(j,k);
            double& u = B.U(j,k);
            if (gj[j] < 1e-16) {
                c = 0.0;
                mu = 0.0;
                u = 0.0;
                continue;
            }
            double mu2 = 0, u2=0, c2=0;
            double r=0;
            for(size_t t=0;t<T;++t) {
                double g = gjkt(j,k,t) * eta[t];
                r += g;
                c2 += g;
                u2 += g * (O[t]-mu)*(O[t]-mu);
                mu2 += g * O[t];
            }
            if (r < 1e-16) {
                c = 0;
                u = 0;
                mu = 0;
            } else {
                c = c2;
                // make an artificial lower bound for the variance.
                u = std::max(1e-8, u2 / r);
                mu = mu2 / r;
            }
            assert( c==0 || u != 0);
        }
        // just rescale the B_c.
        for(size_t j=0;j<N;++j) {
            double cs = 0.0;
            for(size_t k=0;k<M;++k) cs += B.C(j,k);
            blas::scal(M, (cs<1e-16?0.0:1.0/cs), &B.C(j,0), N);
        }
        assert( !is_nan(B.C) );
        assert( !is_nan(B.U) );
        assert( !is_nan(B.mu) );
    }
};

template<>
class RandomHMM_B<Bjgm>
{
  public:
    void operator()(size_t N, size_t M, Bjgm& B)
    {
        std::uniform_real_distribution<double> flatd(-1.0,1.0);
		std::normal_distribution<double> nD(2.0/M, 1.0/M);
        B.mu.resize(morana::make_array<size_t,2>(N,M));
        B.C.resize(morana::make_array<size_t,2>(N,M));
        B.U.resize(morana::make_array<size_t,2>(N,M));
        make_stochastic_tensor(B.C, 0);
        for(auto& z : B.mu) z = flatd(randomGenerator());
        for(auto& u : B.U) u = std::pow(nD(randomGenerator()),2.0);
    }
};
} // end of namespace HMM





#endif
