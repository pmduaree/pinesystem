#ifndef __STATISTICS_HPP_
#define __STATISTICS_HPP_


#include <type_traits>
#include <limits>


template<class Iterator>
auto mean(Iterator beg, Iterator end) -> 
    typename std::remove_reference<decltype(*beg)>::type
{
    typedef typename std::remove_const<
        typename std::remove_reference<decltype(*beg)>::type>::type T;
    T v = 0;
    size_t n = 0;
    for(Iterator it = beg; it != end; ++it) {
        v += *it;
        ++n;
    }
    if (n==0) return 0;
    return v/n;
}


template<class T>
auto mean(const T& x)
    -> decltype(mean(x.begin(),x.end()))
{
    return mean(x.begin(), x.end() );
}


template<class Iterator>
double variance(Iterator beg, Iterator end)
{
    typedef typename std::remove_const<typename std::remove_reference<decltype(*beg)>::type>::type T;
    static_assert(std::is_same<T,double>::value, "right now only double");
    T m = mean(beg, end);
    double v1 = 0.0;
    size_t n=0;
    for(auto it = beg; it != end; ++it) {
        const auto& v = *it;
        v1 += (v -m)*(v-m);
        ++n;
    }
    if (n <= 1) return 0;
    return v1 / (n-1);
}

template<class T>
double variance(const T& a)
{
    return variance(a.begin(), a.end());
}

template<class It1, class It2>
auto covariance(It1 begx, It1 endx, It2 begy, It2 endy)
    -> typename std::common_type<decltype(*begx),decltype(*begy)>::type
{
    typedef typename std::remove_const<typename std::remove_reference<decltype(*begx)>::type>::type Tx;
    typedef typename std::remove_const<typename std::remove_reference<decltype(*begy)>::type>::type Ty;
    typedef typename std::common_type<Tx,Ty>::type T;

    Tx m_x = mean(begx, endx);
    Ty m_y = mean(begy, endy);

    T v = 0;
    size_t n = 0;
    auto itx = begx;
    auto ity = begy;
    while(itx != endx && ity != endy) {
        v += ((*itx)-m_x) * ((*ity)-m_y);
        ++itx;
        ++ity;
        ++n;
    }
    if (n <= 1) return 0;
    return v / (n-1);
}

template<class TX, class TY>
auto covariance(const TX& x, const TY& y)
    -> decltype(covariance(x.begin(),x.end(),y.begin(),y.end()))
{
    return covariance(x.begin(),x.end(),y.begin(),y.end());
}

// Linear regression with ordinary least squares.
//  Y_i = alpha + beta * X_i + epsilon_i
// where parameters are determined such that \sum_j epsilon_j^2 = min.
//
// if A0 is given, then alpha = A0 and only beta is optimized.
// otherwise, both (alpha,beta) are optimized.
template<class VectorY, class VectorX>
std::tuple<double,double>
OLS(const VectorY& Y, const VectorX& X, 
         double A0=std::numeric_limits<double>::quiet_NaN())
{
    // fit Y = beta * X + A0
    auto Nan = std::numeric_limits<double>::quiet_NaN();
    double alpha = A0, beta = Nan;
    size_t n = X.end() - X.begin();
    double mean_x = mean(X), mean_y = mean(Y);
    double var_x = variance(X);
    double cov_xy = covariance(X,Y);
    
    //double sum_x = n * mean_x;
    double sum_xx = (n-1)* var_x + n*mean_x*mean_x;
    double sum_xy = (n-1)* cov_xy + n * mean_x * mean_y;
    
    if (alpha == alpha) {
        beta = (sum_xy - alpha * n * mean_x) / sum_xx;
    } else {
        beta = cov_xy / var_x;
        alpha = mean_y - beta * mean_x;
    }
    return std::make_tuple(alpha,beta);
}
#endif
