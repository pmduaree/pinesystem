#ifndef __HMM_PREDICTION_
#define __HMM_PREDICTION_

#include <vector>
#include "../../Tiempo/tiempo.hpp"

namespace HMM {

struct pred_prob
{
    pred_prob(double V1, double V2, double P)
        : v1(std::min(V1,V2)), v2(std::max(V1,V2)), p(P) {}
    double v1, v2;
    double p;
};

struct hmm_prediction
{
    tiempo::datetime train_beg, train_end;
    tiempo::datetime date;
    double E;
    std::vector<pred_prob> P;
};



}

#endif

