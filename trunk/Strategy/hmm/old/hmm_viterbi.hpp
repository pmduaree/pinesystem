#ifndef __HMM_VITERBI__
#define __HMM_VITERBI__

#include <cmath>
#include <type_traits>
#include <vector>

#include "../../Utils/blas.hpp"
#include "hmm_data.hpp"

namespace HMM {

/// Multiply over a range from beg (inclusive) to end (exclusive)
template<class Iterator>
auto prod(const Iterator beg, const Iterator end)
    -> typename std::remove_reference<decltype(*beg)>::type
{
    typedef typename std::remove_reference<decltype(*beg)>::type const_Type;
    typedef typename std::remove_const<const_Type>::type Type;
    static_assert(std::is_integral<Type>::value || std::is_floating_point<Type>::value, 
                   "must be either integral of floating point type");
    Type v = static_cast<Type>(1);
    for(Iterator it = beg; it != end; ++it) v *= (*it);
    return v;
}

//////////////////////////////////////////////////////////////////////////////
/// \class Viterbi algorithm
/// \brief Calculate alpha_t(j), beta_t(j), and gamma_t(j)
///
/// Given a time series O and the parameters of the HMM, perform the Viterbi
/// algorithm to calculate the quantities
///     alpha_t(j)  = P(O_0,...,O_t         | q_t=j , mu)
///     beta_t(j)   = P(O_{t+1},...,O_{T-1} | q_t=j , mu)
///     gamma_t(j)  = P(q_t = j | O, mu)
/// This is a general version of the Viterbi algorithm and can be used for
/// both the discrete HMM and HMM with gaussian mixtures or anything actually
/// 
/// Status:
///     * This class is finished, There's nothing more one can do here, 
///       except for some optimizations... 
//////////////////////////////////////////////////////////////////////////////
class Viterbi
{
	size_t T,N; //number of states and time steps
  
  public:
    Viterbi() {}

    template<class BTYPE, class OT>
    Viterbi(const HMM_data<BTYPE>& data, const std::vector<OT>& O)
    {
       operator()(data,O);
    }

	// ALPHA
    /// return the rescaled matrix alpha_{j,t}
    const morana::tensor<double,2>& alpha_x() const { return _alpha; }
    /// return the rescaled alpha_t(j) - scaling to 1 
    const double& alpha_x(size_t j, size_t t) const { return _alpha(j,t); }
    /// return the true alpha_t(j) - scaling to 1 
    double alpha_t(size_t j, size_t t) const { return alpha_x(j,t) * prod(_ca.begin(), _ca.begin()+t+1); }
	
	//BETA
    /// return the rescaled matrix beta_{j,t} - scaling to 1 
    const morana::tensor<double,2>& beta_x() const { return _beta; }
    /// return the rescaled beta_t(j) - scaling to 1 
    const double& beta_x(size_t j, size_t t) const { return _beta(j,t); }
    /// return the true beta_t(j)
    double beta_t(size_t j, size_t t) const { return beta_x(j,t) * prod(_cb.begin()+t, _cb.end()); }

	
	
    /// \brief return the scaling factors for alpha_t(j)
    /// The true alpha_t(j) = (c_0*c_1*c_2*..*c_t) alphax_t(j)
    const std::vector<double>& ca() const { return _ca; }

    /// \brief return the scaling factors for beta_t(j)
    /// The true beta_t(j) = (c_t*c_{t+1}*...*c_{T-1}) betax_t(j)
    const std::vector<double>& cb() const { return _cb; }

    /// \brief return \gamma_t(j)
    /// \gamma_t(j) = P(q_t = j | O, mu), this is the probability that the
    /// hidden state at time t equals q_t = j, assuming the time series O
    /// and some HMM parameters.
    const morana::tensor<double,2>& gjt() const { return _gjt; }
    const double& gjt(size_t j, size_t t) const { return _gjt(j,t); }
    
    /// \brief return a matrix B_{j,t} = B_j(O_t)
    const morana::tensor<double,2>& Bo() const { return _Bo; }
    
    /// \brief return B_j(O_t)
    const double& Bo(size_t j, size_t t) const { return _Bo(j,t); }


    size_t getN() const { return N; }
    size_t getT() const { return T; }


    //////////////////////////////////////////////////////////////////////////
    /// \brief Calculate quantities alpha,beta,gamma,xi
    ///
    /// (Re-)calculate all quantities in Viterbi forward and backward
    /// algorithm.
    //////////////////////////////////////////////////////////////////////////
    template<class BTYPE, class OT>
    void operator()(const HMM_data<BTYPE>& data, 
                    const std::vector<OT>& O)
    {
        static_assert( std::is_base_of<HMM_B, BTYPE>::value,
                      "We require a HMM_B object as BTYPE");

        T = O.size();
        const BTYPE& B = data.B;
        auto& A = data.A;
        auto& Pi = data.Pi;
        N = data.getN();
		
        assert( A.size() );
        
        assert( !is_nan(A) );
        assert( !is_nan(Pi) );
        
        //--------------------------------------------------------------------
        // let's define the forward and backward quantities alpha and beta 
        // from the Viterbi algorithm.
        // they are defined in eqns. (19,20) for alpha and (23-25) for beta,
        // in Rabiner's paper.
        //--------------------------------------------------------------------
        
		std::array<size_t,2> NT; NT[0]=N; NT[1]=T;
        
		_alpha.resize(NT);
        _beta.resize(NT);
        _ca.resize(T);
        _cb.resize(T);
        _Bo.resize(NT);
        _gjt.resize(NT);
        
        std::vector<double> foo(N);
        
        std::fill(_ca.begin(), _ca.end(), 1.0);
        std::fill(_cb.begin(), _cb.end(), 1.0);

        for(size_t t=0; t<T; ++t) {
            B(O[t], &_Bo(0,t));
        }
        
        assert( !is_nan(_Bo) );
        
        //--------------------------------------------------------------------
        // calculate alpha_t(j)
        // actually we calculate the renormalized beta_t(j) and store the 
        // renormalization factors separately. 
        //--------------------------------------------------------------------
        
		//init 
		for(size_t i=0;i < N; ++i) _alpha(i,0) = Pi[i] * _Bo(i,0);
		
		_ca[0] = sum(&_alpha(0,0), &_alpha(0,0) + N);//summates alpha_t over all states
        
            
        if (!( _ca[0] > 0)) {
			std::cout << "Error in Viterbi!\n";
            throw std::runtime_error("Viterbi error: _ca[0] == 0");
        }
		blas::scal(N, 1.0/_ca[0], &_alpha(0,0), 1); //scales alpha
		
		//induction
		for(size_t t=1; t< T; ++t) {
            
			blas::gemv("T", N, N, 1.0, A.data(), N, &_alpha(0,t-1), 1, 
                       0.0, foo.data(), 1);

            double* a = &_alpha(0,t);				
            _ca[t] = 0.0;
			
            for(size_t i=0;i<N;++i) {				//weightening
                a[i] = foo[i] * _Bo(i,t);
                _ca[t] += a[i];
            }
            
            if (!( _ca[t] > 0)) {
				std::cout << "Error in Viterbi!\n";
				throw std::runtime_error("Viterbi error: _ca[0] == 0");
            }
            blas::scal(N, 1.0/_ca[t], a, 1);		//scales 
        }
        assert( !is_nan(_alpha));

        //--------------------------------------------------------------------
        // calculate beta_t(j). 
        // actually we calculate the renormalized beta_t(j) and store the 
        // renormalization factors separately. 
        //--------------------------------------------------------------------
        
		//init
		for(size_t i=0;i<N;++i) _beta(i,T-1) = 1.0;
        _cb[T-1] = sum(&_beta(0,T-1), &_beta(0,T-1)+N);
        blas::scal(N, 1.0/_cb[T-1], &_beta(0,T-1), 1);
		
		//induction
        for(size_t tx=1; tx<T; ++tx) {
            size_t t = T-1-tx;
            assert( t < T-1 && t>= 0);
            
			for(size_t i=0;i<N;++i) foo[i] = _Bo(i,t+1) * _beta(i,t+1);
            
			double* b = &_beta(0,t);

            blas::gemv("N", N,N, 1.0, A.data(), N, foo.data(), 1, 0.0, b, 1);
            _cb[t] = sum(b, b+N);

            //scaling
			blas::scal(N, 1.0/_cb[t], b, 1);
        }
        assert( !is_nan(_beta));
        
        //--------------------------------------------------------------------
        // And the gamma_{t}(j) are given in eqns. (26,27)
        // namely
        // gamma_t(j) = alpha_t(j) beta_t(j) / \sum_{i} alpha_t(i) beta_t(i)
        //--------------------------------------------------------------------
        for(size_t t=0;t<T;++t) {
            const double* a = &_alpha(0,t);
            const double* b = &_beta(0,t);
            double* g = &_gjt(0,t);
            // this one should be vectorized
            for(size_t i=0;i<N;++i)
                g[i] = a[i] * b[i];
            double vs = sum(g, g + N);
            blas::scal(N, 1.0/vs, g, 1);
        }
        assert( !is_nan(_gjt));
    }
    
    // return the time duration
    size_t size() const { return _alpha.M(1); }

  private:
    morana::tensor<double,2> _alpha, _beta, _gjt; //alpha, beta, gamma
    std::vector<double> _ca,_cb; //alpha and beta scaling factors - normalization
								 //at each t 1=ca_t^-1*sum(alpha, over all states)	
    morana::tensor<double,2> _Bo; //probability B
};

/// Given a model mu = (A,B,Pi), calculate the probability of observing a 
/// sequence O = (o_1,o_2,...o_T).
/// 
/// i.e. 
/// calculate log[ P(O|mu) ]
/// 
/// This, in turn, is the likelihood measure for the Hidden Markov Model and
/// the whole optimizatino procedure is aimed at maximizing this measure.
struct Likelihood_PO
{
    double operator()(const Viterbi& viterbi)
    {
        double v = log(sum(viterbi.alpha_x().begin(), viterbi.alpha_x().end()));
        for(const auto& c : viterbi.ca() ) v += log(c);
        return v;
    }
};

double calc_PO(const Viterbi& viterbi)
{
    Likelihood_PO ll;
    return ll(viterbi);
}

template<class BJ,class OT>
double calc_PO(const std::vector<double>& Pi, 
               const morana::tensor<double,2>& A, 
               const BJ& Bj, 
			   const std::vector<OT>& O)
{
    HMM_data<BJ> data(Pi, A, Bj);
    Viterbi viterbi(data,O);
    return calc_PO(viterbi);
}

/// calculate a likelihood measure which might be a candidate for the
/// recipe with an exponential weights.
/// It's given as this:
///     log(L) = \sum_{t=1}^{T} log( \sum_{j=1}^{N} \gamma_t(j) B_j(O[t]) )
/// where
///     \gamma_t(j) is the probability for a Markov hidden state 'j' at
///     the time step 't', given a fixed sequence {O_t}.
///
template<class BJ, class OT>
double calc_L(const std::vector<double>& Pi, 
              const morana::tensor<double,2>& A, 
              const BJ& Bj, const std::vector<OT>& O)
{
    HMM_data<BJ> data(Pi,A,Bj);
    Viterbi viterbi(data,O);
    size_t N = Pi.size();
    size_t T = O.size();
    double ll = 0.0;
    for(size_t t=0;t<T;++t) {
        double v = blas::dot(N, &viterbi.Bo(0,t), 1, &viterbi.gjt(0,t), 1);
        ll += log(v);
    }
    return ll;
}

template<class BTYPE, class OT>
double calc_PO(const HMM_data<BTYPE>& data, const std::vector<OT>& O)
{
    return calc_PO(data.Pi, data.A, data.B, O);
}

template<class BTYPE, class OT>
double calc_L(const HMM_data<BTYPE>& data, const std::vector<OT>& O)
{
    return calc_L(data.Pi, data.A, data.B, O);
}




} // end of namespace HMM

#endif
