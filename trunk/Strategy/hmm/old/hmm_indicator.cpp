#include "hmm_indicator.hpp"
#include <iostream>
#include <tuple>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include "../../Tiempo/tiempo.hpp"
#include "../../Tiempo/time_series.hpp"
#include "../../Utils/linalg.hpp"
#include "../../Tiempo/ticks.hpp"
#include "hmm_simulator.hpp"

std::tuple<int,double,std::string>
HMM_indicator::calculate(const std::string& param, const std::vector<double>& O_train)
{
    //------------------------------------
    // set parameters
    //------------------------------------
    HMM::HMM_sim_param xparam;
    xparam.numMcRuns = 40;
    xparam.numModels = 40;
    xparam.eps = 1e-11;
    xparam.numBwSteps = 3000;
    xparam.bwConvergenceCheckLen = 30;
    xparam.bwConvergenceCheckEps = 1e-6;
    // read from string
    xparam.init(param);
    
    std::cout << "CALC HMM" << std::endl;

    //std::cout << "time series has " << O_train.size() << " items." << std::endl;
    //std::cout << "Running the HMM predictor..." << std::endl;
    //std::cout << "DATA:\n" << O_train << std::endl;
    
    HMM::predictor_type_struct pred_type(xparam.hmm_type);
    HMM::Predictor pHMM(pred_type.type());
    // run the predictor and store results in a slice (def in predictor.hpp)
    HMM::train_slice t_slice;
    bool suc = HMM::run_predictor(pHMM, O_train, xparam, t_slice);

    // the return value is here.
    std::string r = t_slice.to_string();
    if (!suc) return std::make_tuple(0,0.0,"Error");
    // else, make it a result alright.
    double p1 = t_slice.ratio_pos;
    double p2 = 1.0 - t_slice.cum_prob_negative;
    double p = 0.5 * (p1 + p2);
    std::cout << "p = " << p << std::endl ;   


    if (0.30 < p && p < 0.70   )
      return std::make_tuple(EMIT_WAIT_TRADE_SIGNAL,p, r);
    
    if (p <= 0.3 )
        return std::make_tuple(EMIT_ENTER_TRADE_SIGNAL,p, r);
    
    if (p >= 0.7  )
        return std::make_tuple(EMIT_EXIT_TRADE_SIGNAL,p, r);

    return  std::make_tuple(0,-1.0,"Error");
    
}

