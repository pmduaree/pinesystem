#include <iostream>
#include <string>

#include "../../Tiempo/tiempo.hpp"
#include "../../Utils/stringparpars.hpp"
#include "hmm_parameters.hpp"

namespace HMM {

std::string PRED_parameters::to_xml() const
{
    std::string s;
    s += (boost::format("<parameter name=\"N\">%d</parameter>")%N).str();
    s += "\n";
    s += (boost::format("<parameter name=\"M\">%d</parameter>")%M).str();
    s += "\n";
    s += (boost::format("<parameter name=\"T\">%d</parameter>")%T).str();
    s += "\n";
    s += (boost::format("<parameter name=\"hmm_type\">%s</parameter>")%hmm_type).str();
    s += "\n";
    s += (boost::format("<parameter name=\"random_type\">%s</parameter>")%random_type).str();
    return s;
}

void PRED_parameters::init(const std::string& s)
{
    StringParamParser spp(s);
    N = spp.get<size_t>("N", 0);
    M = spp.get<size_t>("M", 0);
    T = spp.get<size_t>("T", 0);
    hmm_type = spp.get<std::string>("hmmtype", "sg");
    random_type = spp.get<std::string>("rndtype", "gmm");
}

std::string PRED_parameters::to_string() const
{
    StringParamParser spp;
    spp.put("N", N);
    spp.put("M", M);
    spp.put("T", T);
    spp.put("hmmtype", hmm_type);
    spp.put("rndtype", random_type);
    return spp.to_string();
}



std::string HMSIM_parameters::to_xml() const
{
    std::string s;
    s += (boost::format("<parameter name=\"numBwSteps\">%d</parameter>")%numBwSteps).str();
    s += "\n";
    s += (boost::format("<parameter name=\"numMcRuns\">%d</parameter>")%numMcRuns).str();
    s += "\n";
    s += (boost::format("<parameter name=\"numModels\">%d</parameter>")%numModels).str();
    s += "\n";
    s += (boost::format("<parameter name=\"bwConvergenceCheckLen\">%d</parameter>")%bwConvergenceCheckLen).str();
    return s;
}

    
std::string DATA_parameters::to_xml() const
{
    std::string s;
    s += (boost::format("<parameter name=\"inputfile\">%s</parameter>")%inputFilename).str();
    s += "\n";
    s += (boost::format("<parameter name=\"inputformat\">%s</parameter>")%inputFormat).str();
    s += "\n";
    s += (boost::format("<parameter name=\"exchange\">%s</parameter>")%exchange).str();
    s += "\n";
    s += (boost::format("<parameter name=\"ticker\">%s</parameter>")%ticker).str();
    s += "\n";
    s += (boost::format("<parameter name=\"field\">%s</parameter>")%field).str();
    s += "\n";
    s += (boost::format("<parameter name=\"frequency\">%d</parameter>")%frequency).str();
    return s;
}



    
std::string HMM_sim_param::to_xml() const
{
    std::string s;
    s += PRED_parameters::to_xml() + "\n";
    s += HMSIM_parameters::to_xml();
    return s;
}


void set_simulation_parameters(HMSIM_parameters& p, int quality)
{
    p.eps = 1e-11;
    p.bwConvergenceCheckLen = 40;
    p.bwConvergenceCheckEps = 1e-5;
    if (quality > 4) {
        p.numBwSteps = 5000;
        p.bwConvergenceCheckEps = 1e-7;
        p.bwConvergenceCheckLen = 40;
        p.numMcRuns = 100;
        p.numModels = 40;
    } else if (quality > 3) {
        p.numBwSteps = 3000;
        p.bwConvergenceCheckEps = 1e-6;
        p.bwConvergenceCheckLen = 40;
        p.numMcRuns = 50;
        p.numModels = 40;
    } else if (quality > 2) {
        p.numBwSteps = 3000;
        p.bwConvergenceCheckEps = 1e-6;
        p.bwConvergenceCheckLen = 30;
        p.numMcRuns = 40;
        p.numModels = 40;
    } else if (quality > 1) {
        p.numBwSteps = 1000;
        p.bwConvergenceCheckEps = 2e-5;
        p.bwConvergenceCheckLen = 20;
        p.numMcRuns = 20;
        p.numModels = 20;
    } else if (quality > 0) {
        p.numBwSteps = 800;
        p.bwConvergenceCheckEps = 2e-5;
        p.bwConvergenceCheckLen = 10;
        p.numMcRuns = 10;
        p.numModels = 10;
    } else {
        p.numBwSteps = 500;
        p.bwConvergenceCheckEps = 2e-5;
        p.bwConvergenceCheckLen = 10;
        p.numMcRuns = 8;
        p.numModels = 2;
    }
}


}

