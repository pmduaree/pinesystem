#ifndef __BAUM_WELCH__
#define __BAUM_WELCH__


#include <vector>
#include "hmm_common.hpp"
#include "hmm_data.hpp"
#include "hmm_viterbi.hpp"

// there should be no include of hmm_bmm or hmm_discrete or any other. 


namespace HMM {
//Update PI
struct BW_Pi
{
    void operator()(std::vector<double>& Pi, 
                    const Viterbi& viterbi)
    {
        size_t N = Pi.size();
        assert( !is_nan(Pi));
        // Pi_j = \gamma_0(j)   (40a)
        std::copy(viterbi.gjt().data(), viterbi.gjt().data() + N,  &Pi[0]);
        assert( !is_nan(Pi));
    }
};
//Update A
struct BW_A
{
    void operator()(morana::tensor<double,2>& A, const Viterbi& viterbi)
    {
        size_t T = viterbi.size();
        size_t N = A.M(0);

        std::vector<double> eta(T);
        for(auto& e: eta) e = 1.0;
        
        assert( !is_nan(A) );
        
        std::array<size_t,3> NNT1; NNT1[0]=N; NNT1[1]=N; NNT1[2]=T-1;
        morana::tensor<double,3> xi(NNT1, 0.0);
        // The updated matrices can be expressed in terms of matrices \xi and \gamma
        // The matrices xi_{t}(i,j) are given in eqn. (37) in Rabiner's paper.
        // xi_{t}(i,j) = x_t(i,j) / \sum_{i,j} x_t(i,j)
        // where
        //   x_{t}(i,j) = alpha_t(i) a_{i,j} b_j(O_{t+1}) beta_{t+1}(j)
        for(size_t t=0; t<T-1;++t) {
            double vs = 0.0;
            for(size_t j=0;j<N;++j) {
                for(size_t i=0;i<N;++i) {
                    double& x = xi(i,j,t);
                    x = viterbi.alpha_x(i,t) * A(i,j) * 
                        viterbi.beta_x(j,t+1) * viterbi.Bo(j,t+1);
                    vs += x;
                }
            }
            blas::scal(N*N, 1.0/vs, &xi(0,0,t), 1);
        }
        assert( !is_nan(xi));

        // the gamma_{t}(j,k) are defined in a non-number equation following 
        // equation (54) in Rabiner.
        // we can write:
        //   gamma_t(j,k) = gamma_j(t) * W_t(j,k) / sum_m W_t(j,m)
        // where 
        //   W_t(j,k) = c_{j,k} g(Ot,mu_{jk}, U_{j,k}) 
        
        std::vector<double> gjx(N);
        std::array<size_t,2> NN; NN[0]=N; NN[1]=N;
        morana::tensor<double,2> xiij(NN);
        
        blas::gemv("N", N, T-1, 1.0, viterbi.gjt().data(), N, &eta[0], 1, //multiplication
             0.0, &gjx[0], 1);
        assert( !is_nan(gjx));
        
		blas::gemv("N", N*N, T-1, 1.0, xi.data(), N*N, &eta[0], 1, 
             0.0, xiij.data(), 1);
        assert( !is_nan(xiij));
        
        //-----------------------------------------------------------------
        // the parameters can be updated according to the equation (40) and
        // (52-54) in the Rabiner's tutorial
        //-----------------------------------------------------------------
        
        // A(i,j) = \sum_{t=0}^{T-1} xi_t(i,j) / \sum_{t=0}^{T-2} \gamma_t(i)
        // (40b)
        A = xiij;
        for(size_t i=0;i<N;++i) {
            blas::scal(N, (gjx[i]>1e-16 ? 1.0/gjx[i] : 0.0),  &A(i,0), N);
        }
        assert( !is_nan(A));
    }

};

//Update B
template<class BJ>
struct BW_B
{
    template<class OT>
    void operator()(BJ& B, const Viterbi& viterbi, const std::vector<OT>& O);
};

// this class is specialized in hmm_gmm for gaussian mixtures,
// in hmm_discrete for the discrete case
// and will be in hmm_student.hpp for the t-student mixture

/////////////////////////////////////////////////////////////////////////////
/// \class BaumWelch
/// \brief Implementation of the Baum Welch algorithm
///
/// Baum Welch algorithm locally maximizes the expectation value of the 
/// Likelihood function
/////////////////////////////////////////////////////////////////////////////
class BaumWelch
{
  public:
    ////////////////////////////////////////////////////////////////////////
    /// \brief Optimize the HMM data against the observation sequence O
    ///
    /// Make one Baum Welch optimization step to maximize the recursive
    /// maximum likelihood function against the observation sequence O
    ////////////////////////////////////////////////////////////////////////
    template<class BJ, class OT>
    void operator()(HMM_data<BJ>& data, const std::vector<OT>& O)
    {
        Viterbi viterbi(data,O);
        operator()(data, viterbi, O);
    }
    
    template<class BJ, class OT>
    void operator()(HMM_data<BJ>& data, const Viterbi& viterbi, 
                    const std::vector<OT>& O)
    {
        
		BW_A bwa;
        BW_Pi bwpi;
        BW_B<BJ> bwb;
        bwpi(data.Pi, viterbi);
        bwa(data.A, viterbi);
        bwb(data.B, viterbi, O);
    }
};


} // end of namespace HMM


#endif
