
#include <iostream>
#include <tuple>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>

#include "../../Tiempo/tiempo.hpp"
#include "../../Tiempo/time_series.hpp"
#include "../../Utils/linalg.hpp"
#include "../../Tiempo/ticks.hpp"
#include "../../Utils/stringparpars.hpp"

#include "hmm_simulator.hpp"
#include "hmm_indicator.hpp"


std::tuple<int,double,std::string>
HMM_indicator::calculate(const std::string& param, const std::vector<double>& O_train, bool TradeStatus)
{
    //------------------------------------
    // set parameters
    //------------------------------------
    // HMM::HMM_sim_param xparam;
    // xparam.numMcRuns = 40;
    // xparam.numModels = 40;
    // xparam.eps = 1e-11;
    // xparam.numBwSteps = 3000;
    // xparam.bwConvergenceCheckLen = 30;
    // xparam.bwConvergenceCheckEps = 1e-6;
    // // read from string
    // xparam.init(param);
    
    // //std::cout << "time series has " << O_train.size() << " items." << std::endl;
    // //std::cout << "Running the HMM predictor..." << std::endl;
    // //std::cout << "DATA:\n" << O_train << std::endl;
    
    // HMM::predictor_type_struct pred_type(xparam.hmm_type);
    // HMM::Predictor pHMM(pred_type.type());
    // // run the predictor and store results in a slice (def in predictor.hpp)
    // HMM::train_slice t_slice;
    // bool suc = HMM::run_predictor(pHMM, O_train, xparam, t_slice);

    // // the return value is here.
    // std::string r = t_slice.to_string();
    // if (!suc) return std::make_tuple(0,0.0,"Error");
    // // else, make it a result alright.
    // double p1 = t_slice.ratio_pos;
    // double p2 = 1.0 - t_slice.cum_prob_negative;
    // double p = 0.5 * (p1 + p2);
    // std::cout << "P = " << p << std::endl;
    
    // if (0.4 < p1 && p < 0.6   )
    //   return std::make_tuple(EMIT_WAIT_TRADE_SIGNAL,p, r);
    
    // if (p1 <= 0.4 )
    //     return std::make_tuple(EMIT_ENTER_TRADE_SIGNAL,p, r);
    
    // if (0.6  <= p1  )
    //     return std::make_tuple(EMIT_EXIT_TRADE_SIGNAL,p, r);

    StringParamParser spp(param);

    HMM::HMM_sim_param xparam;
    xparam.numMcRuns = 10;
    xparam.numModels = 70;
    xparam.eps = 1e-11;
    xparam.numBwSteps = 1000;
    xparam.bwConvergenceCheckLen = 30;
    xparam.bwConvergenceCheckEps = 1e-4;
    xparam.random_type = "gmm";
    xparam.N = spp.get<size_t>("N", 0);
    xparam.M = spp.get<size_t>("M", 0);
    xparam.T = spp.get<size_t>("T", 0);
    xparam.hmm_type = spp.get<std::string>("hmmtype", "sg");
    double threshold = spp.get<double>("threshold", 5.0);
    //std::cout << threshold << std::endl;

    
    //std::cout << "time series has " << O_train.size() << " items." << std::endl;
    //std::cout << "Running the HMM predictor..." << std::endl;
    //std::cout << "DATA:\n" << O_train << std::endl;
    
    // run the predictor and store results in a slice (def in predictor.hpp)
    HMM::train_slice t_slice;
    bool suc = HMM::run_predictor(O_train, xparam, t_slice);
    if (!suc) 
        return  std::make_tuple(0,-1.0,"Error");

    // the return value is here.
    StringParamParser parser;
    parser.put("r_pos", t_slice.ratio_pos);
    parser.put("exval", t_slice.exval);
    parser.put("p_cumneg", t_slice.cum_prob_negative);
    std::string r = parser.to_string();
    
    // else, make it a result alright.
    double p1 = t_slice.ratio_pos;
    double p2 = 1.0 - t_slice.cum_prob_negative;
    double p = 0.5 * (p1 + p2);
    std::cout << "P = " << p << std::endl;
    const double of = (threshold <= 0 || threshold >= 50) ? 0.05 : threshold / 100;
    if(p == 1 || p == 0)
        return std::make_tuple(EMIT_WAIT_TRADE_SIGNAL, 0.5, r);

    if (0.5 - of <= p && p <= 0.5 + of)
      return std::make_tuple(EMIT_WAIT_TRADE_SIGNAL,p, r);

    if(TradeStatus)
        return std::make_tuple(EMIT_EXIT_TRADE_SIGNAL, p, r);
    else 
        return std::make_tuple(EMIT_ENTER_TRADE_SIGNAL,p, r);

    // if (0.5 - of <= p && p <= 0.5 + of)
    //   return std::make_tuple(EMIT_WAIT_TRADE_SIGNAL,p, r);
    
    // if (p <= 0.4 )
    //     return std::make_tuple(EMIT_ENTER_TRADE_SIGNAL,p, r);
    
    // if (p  >= 0.6  )
    //     return std::make_tuple(EMIT_EXIT_TRADE_SIGNAL,p, r);


    //return  std::make_tuple(0,-1.0,"Error");
    
}

