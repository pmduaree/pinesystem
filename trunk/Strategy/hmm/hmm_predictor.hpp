#ifndef PREDICTOR_hpp_
#define PREDICTOR_hpp_

#include <map>

#include "hmm_data.hpp"  // required for hmm extensions of predictor that 
                         // are also included in this file
#include "gmm.hpp" // ExpectationMaximization for gaussian mixtures fitting
#include "hmm_parameters.hpp"
#include "hmm_probability.hpp"
#include "hmm_predictor_engine.hpp"
#include "hmm_prediction.hpp"


namespace HMM {


struct hmm_result
{
    double like;
    std::vector<double> qdist;
    std::vector<double> gmmFactors; // "c"s of the hmm predicted states are weighted
                                    // by state probabilites and summed into a single vector
};

    
enum class predictor_type 
{invalid, Discrete, Gmm, SimpleGmm, RestrictedGmm};

class predictor_type_struct
{
  public:
    predictor_type_struct(const std::string& hmm_type)
    {
        init_legend();
        pred_type_str = hmm_type;
    }

    predictor_type type() const
    {
        if (Legend.count(pred_type_str))
            return Legend.find(pred_type_str)->second;
        else
            return predictor_type::invalid;
    }
  private:
    void init_legend()
    {
        Legend.clear();
        Legend["d"] = predictor_type::Discrete;
        Legend["g"] = predictor_type::Gmm;
        Legend["sg"] = predictor_type::SimpleGmm;
        Legend["rg"] = predictor_type::RestrictedGmm;
    }
    std::string pred_type_str;
    std::map<std::string, predictor_type> Legend;
};


/**
 * 
 * This file defines Predictor class, which unifies access to underlying 
 * predictor models (like HMMs)
 * 
 * **/
class Predictor
{
  public:
    PredEngine* my_predict; // no smart pointers because I want each predictor
                            // to own the engine.
    predictor_type hmm_type;
  public:
    //Predictor(): my_predict(nullptr) {  } // not allowed
    
    Predictor(predictor_type hmmtype)
        : my_predict(nullptr), hmm_type(hmmtype)
    {
        alloc_engine();
    }

    Predictor(const Predictor& p)
        : my_predict(nullptr), hmm_type(p.hmm_type)
    {
        //std::cout << "Copy constructor: " << &p<<"->"<<this << std::endl;
        dealloc_and_alloc_engine(p.my_predict);
        R = p.R;
        //std::cout << "Now it points to " << my_predict << std::endl;
    }
    
    ~Predictor()
    {
        if (my_predict) {
            delete my_predict;
            my_predict = nullptr;
        }
    }
     
    
    Predictor& operator=(const Predictor& pred)
    {
        hmm_type = pred.hmm_type;
        dealloc_and_alloc_engine(pred.my_predict);
        R = pred.R;
        return *this;
    }
    
    void train(const HMM_sim_param& params, 
               const std::vector<double>& trainingSet)
    {
        assert(my_predict);
        if (!my_predict) throw std::runtime_error("blabla bla");
        my_predict->train(params, trainingSet);
        R.like = my_predict->likelihoodEstimation();
        R.qdist = my_predict->alphaTT;
        my_predict->getGmmFactors(R.gmmFactors);   
        // should make an exception here ^^ when dealing with non-gmm models
    }
    
    const hmm_result& result() const { return R; }
    double likelihood() const { return R.like; }
    const std::vector<double>& Qdist() const { return R.qdist; }
    
  private:
    void dealloc_and_alloc_engine(const PredEngine* p)
    {
        if (my_predict) delete my_predict;
        my_predict = nullptr;
        if (hmm_type == predictor_type::Gmm) {
            my_predict = new PredEngineHMM<Bjgm>(
          *dynamic_cast<const PredEngineHMM<Bjgm>*>(p));
        } else if (hmm_type == predictor_type::SimpleGmm) {
            my_predict = new PredEngineHMM<Bjsg>(
          *dynamic_cast<const PredEngineHMM<Bjsg>*>(p));
        } else if (hmm_type == predictor_type::RestrictedGmm) {
            my_predict = new PredEngineHMM<BjGlobalRestrictedGauss>(
          *dynamic_cast<const PredEngineHMM<BjGlobalRestrictedGauss>*>(p));
        } else if (hmm_type == predictor_type::Discrete) {
            my_predict = new PredEngineHMM<Bjdiscrete>(
          *dynamic_cast<const PredEngineHMM<Bjdiscrete>*>(p));
        } else {
            throw std::runtime_error("we don't support the default yet");
        }
    }
    void alloc_engine()
    {
        if (my_predict) delete my_predict;
        my_predict = nullptr;
        if (hmm_type == predictor_type::Gmm) {
            my_predict = new PredEngineHMM<Bjgm>();
        } else if (hmm_type == predictor_type::SimpleGmm) {
            my_predict = new PredEngineHMM<Bjsg>();
        } else if (hmm_type == predictor_type::RestrictedGmm) {
            my_predict = new PredEngineHMM<BjGlobalRestrictedGauss>();
        } else if (hmm_type == predictor_type::Discrete) {
            my_predict = new PredEngineHMM<Bjdiscrete>();
        } else {
            throw std::runtime_error("we don't support the default yet");
        }
    }
    hmm_result R;
};



class Probabilizer
{
    std::vector<double> pts;
    std::vector<double> avg_pdf;
    size_t counter;
  public:
    Probabilizer(const std::vector<double>& points)
        : pts(points), counter(0)
    {
        avg_pdf.resize(points.size()-1);
        std::fill(avg_pdf.begin(), avg_pdf.end(), 0.0);
    }
    
    void push(const Predictor& pred)
    {
        std::vector<double> pdf, cdf;
        pred.my_predict->calc_probs_you_MF(pts, pdf, cdf);
        //std::cout << "sizes: " << avg_pdf.size() << " vs " << pdf.size() << std::endl;
        assert( avg_pdf.size() == pdf.size());
        for(size_t i=0;i<pdf.size();++i) {
            avg_pdf[i] = (counter * avg_pdf[i] + pdf[i]) / (counter + 1.0);
        }
        ++counter;
    }

    void get(hmm_prediction& pred)
    {
        // just calcualte the cumulative one right now.
        std::vector<double> avg_cum;
        avg_cum.push_back(0.0);
        pred.E = 0.0;
        pred.P.clear();
        for(size_t i=0;i<pts.size()-1;++i) {
            double c= (pts[i] + pts[i+1])/2.0;
            double dc= pts[i+1] - pts[i];
            avg_cum.push_back(avg_pdf[i] * dc + avg_cum.back());
            pred.E += c * dc * avg_pdf[i];
        }
        size_t z1 = 0, z2 = pts.size()-1;
        while(z1 < pts.size()-1 && pts[z1+1] <= 0.0) ++z1;
        while(z2 > 0 && pts[z2-1] >= 0.0) --z2;
        assert(z1 <= z2);

        for(size_t i=0;i<z1;++i) {
            pred.P.push_back( pred_prob(pts[i],pts[z1], avg_cum[z1]-avg_cum[i]));
        }
        if (z1 < z2) {
            pred.P.push_back( pred_prob(pts[z1],pts[z2], avg_cum[z2]-avg_cum[z1]));
        }
        for(size_t i=z2+1;i<pts.size();++i) {
            pred.P.push_back( pred_prob(pts[z2],pts[i], avg_cum[i]-avg_cum[z2]));
        }
    }
};



#if 0
/**
 * decision function for the discrete_hmm based predictor
 * it cannot be used on a non-discrete predictor
 * **/
double discretePredictorDecide(Predictor& p, const std::vector<double>& O)
{
    auto discreteHmm = reinterpret_cast<PredEngineHMM<Bjdiscrete>* >(p.my_predict);
    if (!discreteHmm) throw std::runtime_error("invalid pointer discretehmm");
    probability<size_t> tP;
    tP.NN = discreteHmm->getHmm().getM();
    #if 1
    auto rangeScale = 0;
    const auto Od = discreteHmm->discretization->dData;
    double min,max; 
    auto mm = std::minmax_element(Od.begin(), Od.end());
    min=(*mm.first-rangeScale*(*mm.second-*mm.first));
    max=(*mm.second+rangeScale*(*mm.second-*mm.first));
    
    #endif
    const auto& R = p.result();
    tP(discreteHmm->getHmm(), R, min, max);
    size_t besti = 0;
    double bestVal = 0.0;
    for (size_t ii = 0; ii < tP.PDF.size(); ++ii) {
        if (tP.PDF[ii] > bestVal) {
            bestVal = tP.PDF[ii];
            besti = ii;
        }
    }
    //double bmin = discreteHmm->discretization->bounds[besti];
    //double bmax = discreteHmm->discretization->bounds[besti+1];
    assert( false );
    throw std::runtime_error("not implemented");
    // TODO: make this some other time...
    return 0.5;
    //return bmin < 0.0 ? (bmax < 0.0 ? -1 : 0) : 1;
}
#endif

template<>
struct PDFCDF<HMM::Predictor>
{
    typedef HMM::Predictor Model;
    template<class dType>
    void operator()(const HMM::Predictor& p, const std::vector<double>& qdist, 
                      const std::vector<dType>& rangeO, 
                      std::vector<double>& PDF, std::vector<double>& CDF)
    {
        //std::cout << "delegating the call from " << &p << " to " << p.my_predict << std::endl;
        p.my_predict->calcPDFCDF(qdist, rangeO, PDF, CDF);
    }
};


template<>
struct Likelihooder<HMM::Predictor>
{
    template<class dType>
    double operator()(const HMM::Predictor& p, const std::vector<dType>& O)
    {
        return p.likelihood();
    }
};


template<>
struct Qcalcer<HMM::Predictor>
{
    template<class dType>
    const std::vector<double>& 
    operator()(const HMM::Predictor& p, const std::vector<dType>& O)
    {
        return p.my_predict->alphaTT;
    }
};



struct decision_maker
{
    std::tuple<double,double>
    operator()(Predictor& predictor, const std::vector<double>& O)
    {
        auto mm = std::minmax_element(O.begin(), O.end());	
        double zval = std::max(fabs(*mm.first), fabs(*mm.second));
        double min = -1.7 * zval;
        double max =  1.7 * zval;
        
        Qcalcer<Predictor> qcalcer;
        const auto& qdist = qcalcer(predictor, O);
        
        linspace RG_all(300, min, max);
        linspace RG_neg(150, min, 0.0);
        PDFCDF<Predictor> pdfcdf;
        
        std::vector<double> PDF, CDF;
		pdfcdf(predictor,qdist,RG_neg,PDF,CDF);
        Probability prob;
        double xcum_neg = prob.integrate(RG_neg, PDF, [](double x) { return 1.0; });
		pdfcdf(predictor,qdist,RG_all,PDF,CDF);
        double xcum_all = prob.integrate(RG_all, PDF, [](double x) { return 1.0; });
        double xavg = prob.integrate(RG_all, PDF, [](double x) { return x; } );
        //std::cout << "negative: " << xcum_neg << " vs " << cum_neg << ",avg:" << xavg << std::endl;
        double pneg = xcum_neg / xcum_all;
        xavg *= 1.0 / xcum_all;
        if (pneg < -1e-8 || pneg > 1 + 1e8) {
            std::cout << "Weird value for pneg = " << pneg << std::endl;
        }
        pneg = std::max(0.0, std::min(pneg, 1.0) );
        return std::make_tuple(pneg, xavg);
    }
};


}   // end of HMM




#endif //PREDICTOR_hpp_
