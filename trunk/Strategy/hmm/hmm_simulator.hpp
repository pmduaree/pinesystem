#ifndef __HMM_SIMULATOR__
#define __HMM_SIMULATOR__
/////////////////////////////////////////////////////////////////////////////
// HMM SIMULATOR
//
//
// This is a wrapper class. It takes a time series, trains a coupled of HMMs
// on it, and uses that to predict future values.
/////////////////////////////////////////////////////////////////////////////
#include <unordered_map>
#include <atomic>
#include <future>

#include "../../Tiempo/tiempo.hpp"
#include "../../Tiempo/time_series.hpp"
#include "hmm_parameters.hpp"
#include "hmm_prediction.hpp"
#include "hmm_predictor.hpp"
#include "../../Utils/stringparpars.hpp"

namespace HMM {

/// add elements of v2 to v1 inline 
template<class C>
    void add(std::vector<C> &v1, const std::vector<C> &v2)
    {
        assert(v1.size() == v2.size());
        for(size_t i = 0; i<v1.size(); ++i)
            v1[i] += v2[i];
    }
    
    
    
struct train_slice
{
    int signal;
    double ratio_pos;
    double exval;
    double cum_prob_negative;
    std::vector<double> qdist, gmmfactors;
    train_slice()
        : signal(0), ratio_pos(0.5), exval(0.0), cum_prob_negative(0.5)
    {}


    std::string to_string() const
    {
        StringParamParser parser;
        parser.put("r_pos", ratio_pos);
        parser.put("exval", exval);
        parser.put("p_cumneg", cum_prob_negative);
        auto s = parser.to_string();
        return s;
    }
};


struct PredZ
{
    int decision;
    double cumPneg, predavg;
    double like;
    std::vector<double> Qdist, Gmmf;
    bool succ;
};


#ifndef _OPENMP
#define NO_PARALLEL_PREDICTOR
#endif


// called from simulate.hpp and signallers.hpp
bool run_predictor(Predictor& i_predictor, const std::vector<double>& oTrain, 
                   const HMM_sim_param& params, train_slice& slice)
    

    { int nAnalysis = params.numModels;
        decision_maker decisionFunction;
        // do the optimization of hmm training here
        // generate HMM and its probability function
        
        
        // let's make two versions. For the parallel one and for the sequential one.
        
        auto make_calc = [&params,&oTrain,&decisionFunction,&i_predictor](PredZ& Z, std::atomic<bool>& d) -> void
        {
            d = false;
            Z.succ = false;
            Predictor pr(i_predictor);
            try {
                pr.train(params, oTrain);
            } catch( ... ) {
                d = true;
                return;
            }
            std::tie(Z.cumPneg, Z.predavg) = decisionFunction(pr, oTrain);
            Z.decision = Z.cumPneg < 0.45 ? 1 : (Z.cumPneg > 0.55 ? -1 : 0);
            Z.like = pr.likelihood();
            Z.Qdist = pr.result().qdist;
            Z.Gmmf = pr.result().gmmFactors;
            Z.succ = true;
            d = true;
        };
        
        //std::cout << "Spawning threads..." << std::endl;
        // spawn threads
        std::unordered_map<int, PredZ> RZ;
        std::unordered_map<int, std::thread> threads;
        std::unordered_map<int, std::atomic<bool> > done;
        size_t max_threads = std::min<size_t>(16, std::thread::hardware_concurrency() );
        for(int i=0;i<nAnalysis;++i) {
            while(threads.size() >= max_threads) {
                for(const auto& d : done) {
                    if (d.second && threads.count(d.first) ) {
                        //std::cout << "Erasing thread " << d.first << std::endl;
                        auto it_thr = threads.find(d.first);
                        it_thr->second.join();
                        threads.erase(it_thr);
                    }
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100) );
            }
            int id = i + 1;
            done[id] = false;
            RZ[id].succ = false;
            //std::cout << "Making threaad " << id << std::endl;
            threads.emplace(id, std::thread(make_calc, std::ref(RZ[id]), std::ref(done[id]) ));
            //std::thread t(make_calc, std::ref(RZ[id]) );
            //threads[id].swap(t);
            //t.join();
        }
        
        //std::cout << "Joining the remaining " << threads.size() << " threads..." << std::endl;
        for(auto& t : threads) t.second.join();
        //std::cout << "Joining threads... DONE" << std::endl;
        
        // delete the failed ones.
        auto it = RZ.begin();
        while( it != RZ.end() ) {
            if (it->second.succ)
                ++it;
            else {
                auto it_to_del = it;
                ++it;
                RZ.erase(it_to_del);
            }
        }
        
        int n_actual = RZ.size();
        if (!n_actual) return false;
        
        it = RZ.begin();
        hmm_result result;
        result.qdist = it->second.Qdist;
        result.gmmFactors = it->second.Gmmf;
        while(++it != RZ.end()) {
            add(result.qdist, it->second.Qdist);
            add(result.gmmFactors, it->second.Gmmf);
        }
        for(auto& z : result.qdist) z *= 1.0 / n_actual;
        for(auto& z : result.gmmFactors) z *= 1.0 / n_actual;
        
        double cumprobN = 0.0, xavgN = 0.0, likelihood = 0.0;
        int decision = 0;
        for(const auto& z: RZ) {
            cumprobN += z.second.cumPneg;
            xavgN += z.second.predavg;
            decision += z.second.decision;
            likelihood += z.second.like;
        }
        cumprobN *= 1.0 / n_actual;
        xavgN *= 1.0 / n_actual;
        likelihood /= n_actual;
        
        double pnegy = cumprobN;
        double pposy = 0.5 + 0.5 * (decision*1.0/n_actual);
        
        // data returned by the function aTimeStep
        // the position (short = -1, long = 1) for the next time step,
        // based on the probability that the output of HMM is positive
        int sig1 = pposy > 0.51 ? 1 : (pposy < 0.49 ? -1 : 0);
        int sig2 = pnegy < 0.49 ? 1 : (pnegy > 0.51 ? -1 : 0);
        slice.signal = (sig1==sig2) ? sig1 : 0;
        slice.ratio_pos = pposy;
        slice.qdist = result.qdist;
        slice.gmmfactors = result.gmmFactors;
        slice.cum_prob_negative = pnegy;
        slice.exval = xavgN;
        return true;
    };
    



bool run_predictor(const std::vector<double>& oTrain, 
                   const HMM_sim_param& params, train_slice& slice)
{

    predictor_type_struct predictor_type(params.hmm_type);
    Predictor i_predictor(predictor_type.type());

    int nAnalysis = params.numModels;
    decision_maker decisionFunction;
    // do the optimization of hmm training here
    // generate HMM and its probability function        
    
    // let's make two versions. For the parallel one and for the sequential one.
    auto make_calc = [&params,&oTrain,&decisionFunction,&i_predictor]() -> PredZ
    {
        PredZ Z;
        //d = false;
        Z.succ = false;
        Predictor pr(i_predictor);
        try {
            pr.train(params, oTrain);
            std::tie(Z.cumPneg, Z.predavg) = decisionFunction(pr, oTrain);
            Z.decision = Z.cumPneg < 0.45 ? 1 : (Z.cumPneg > 0.55 ? -1 : 0);
            Z.like = pr.likelihood();
            Z.Qdist = pr.result().qdist;
            Z.Gmmf = pr.result().gmmFactors;
            Z.succ = true;
        } catch( ... ) {
            Z.succ = false;
        }
        return std::move(Z);
    };
    
    std::unordered_map<int, PredZ> RZ;
    bool parallel = false;
    if (parallel) {
        // submit work -- spawn new threads
        std::unordered_map<int, std::future<PredZ> > xRZ;
        for(int i=0;i<nAnalysis;++i) {
            int id = i + 1;
            xRZ.emplace(id, std::async(make_calc));
        }
        // collect the results from futures.
        for(auto& f : xRZ) RZ[f.first] = f.second.get();
    } else {
        for(int i=0;i<nAnalysis;++i) {
            int id = i + 1;
            auto z = make_calc();
            RZ.emplace(id, z);
        }
    }


    // process the results.
    // first delete the failed ones.
    auto it = RZ.begin();
    while( it != RZ.end() ) {
        if (it->second.succ)
            ++it;
        else {
            auto it_to_del = it;
            ++it;
            RZ.erase(it_to_del);
        }
    }
    
    int n_actual = RZ.size();
    if (!n_actual) return false;

    it = RZ.begin();
    hmm_result result;
    result.qdist = it->second.Qdist;
    result.gmmFactors = it->second.Gmmf;
    while(++it != RZ.end()) {
        add(result.qdist, it->second.Qdist);
        add(result.gmmFactors, it->second.Gmmf);
    }
    for(auto& z : result.qdist) z *= 1.0 / n_actual;
    for(auto& z : result.gmmFactors) z *= 1.0 / n_actual;
    
    double cumprobN = 0.0, xavgN = 0.0, likelihood = 0.0;
    int decision = 0;
    for(const auto& z: RZ) {
        cumprobN += z.second.cumPneg;
        xavgN += z.second.predavg;
        decision += z.second.decision;
        likelihood += z.second.like;
    }
    cumprobN *= 1.0 / n_actual;
    xavgN *= 1.0 / n_actual;
    likelihood /= n_actual;

    double pnegy = cumprobN;
    double pposy = 0.5 + 0.5 * (decision*1.0/n_actual);
    
    // data returned by the function aTimeStep
    // the position (short = -1, long = 1) for the next time step, 
    // based on the probability that the output of HMM is positive
    int sig1 = pposy > 0.51 ? 1 : (pposy < 0.49 ? -1 : 0);
    int sig2 = pnegy < 0.49 ? 1 : (pnegy > 0.51 ? -1 : 0);
    slice.signal = (sig1==sig2) ? sig1 : 0;
    slice.ratio_pos = pposy;
    slice.qdist = result.qdist;
    slice.gmmfactors = result.gmmFactors;
    slice.cum_prob_negative = pnegy;
    slice.exval = xavgN;
    return true;
}




}



#endif
