#ifndef __SIMULATE_HPP_
#define __SIMULATE_HPP_

#include <iostream>

#include "time_series.hpp"
#include "date_time.hpp"
#include "random.hpp"

#include "hmm.hpp"
#include "hmm_common.hpp"
#include "hmm_simplegauss.hpp"
#include "hmm_gmm.hpp"
#include "hmm_discrete.hpp"
#include "hmm_globalrestrictedgauss.hpp"
#include "hmm_discrete.hpp"
#include "hmm_predictor.hpp"
#include "hmm_parameters.hpp"
#include "hmm_random.hpp"
#include "hmm_probability.hpp"
#include "hmm_simulator.hpp"

namespace HMM {

// Here we store the training results for historical data.
// Each item in 'items' corresponds to a prediction on a given 
// date (slice.on_date).

class historical_simulator
{
    Predictor* pHMM;
    HMM_signaller sig;
  public:
    historical_simulator(const time_series<double>& P, const HMM_sim_param& parameters)
        : pHMM(nullptr), sig(P, parameters)
    {
        // --------------------------------------------------------------
        // NOW RUN THE SIMULATION
        //---------------------------------------------------------------
        // define predictor (pHMM) and simulation based on the predictor
        predictor_type_struct pred_type(parameters.hmm_type);
        pHMM = new Predictor(pred_type.type());
    }
    
    ~historical_simulator()
    {
        if (pHMM) delete pHMM;
    }
    
    void simulate(const std::vector<tiempo::datetime_type>& dates,
                  time_series<train_slice>& results)
    {
        // --------------------------------------------------------------
        // NOW RUN THE SIMULATION for selected dates
        //---------------------------------------------------------------
        for(const auto& on_date : dates) {
            // we got it already.
            if (results.count(on_date)) continue;
            train_slice slx;
            bool suc = sig(on_date, slx);
            if (suc)
                results[on_date] = slx;
        }
    }
};


}

#endif
