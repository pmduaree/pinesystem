#ifndef ma_strategy_engine
#define ma_strategy_engine
/////////////////////////////////////////////////////////////////////////////
// Strategy MA:
//  
// Oleum Trading Systems 2014.
//
// At a given time 't' and for given time windows 'w1' < 'w2'
// we calculate mean prices:
//  p1 = E[p_t, t in [t - w1, t]) and 
//  p2 = E[p_t, t in [t - w2, t])
//
// And give the following signal:
//  BUY         : if p1 >> p2
//  SELL        : if p1 << p2
//  WAIT        : if p1 <> p2   (i.e. p1 ~ p2)
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include "strategy_engine.hpp"

class Strategy_ma : public StrategyEngine
{
  public:
    Strategy_ma() : StrategyEngine() {}
    virtual ~Strategy_ma() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        // Parse the input parameters and check the validity
        StringParamParser params(param);
        int ma_long = params.get<int>("long", 0);
        int ma_short = params.get<int>("short", 0);
        if (ma_long <= ma_short || ma_short==0 || ma_long==0)
            return err_result;
        // Obtain one-minute data for this asset
        const auto& tix = asset->get_ticks();
        size_t c_s = 0, c_l = 0;
        double p_s = 0.0, p_l = 0.0;
        auto date_short = date - tiempo::duration(ma_short * 60 * freq);
        auto date_long = date - tiempo::duration(ma_long * 60 * freq);
        auto it = tix.rbegin();
        while(it != tix.rend() && it->first > date) ++it;
        while(it != tix.rend() && it->first > date_short) {
            auto p = it->second.p;
            p_s += p;
            p_l += p;
            ++c_l;
            ++c_s;
            ++it;
        }
        while(it != tix.rend() && it->first > date_long) {
            auto p = it->second.p;
            p_l += p;
            ++c_l;
            ++it;
        }
        if (c_l == 0) {
            return std::make_tuple(0.5, "plong=n/a,pshort=n/a");
        }
        p_l *= 1.0 / c_l;
        if (c_s == 0) {
            return std::make_tuple(0.5, (boost::format("plong=%.8e,pshort=n/a")%p_l).str() );
        }
        p_s *= 1.0 / c_s;
        double rdif = (p_s - p_l) / fabs(p_l);
        double ppos = 0.5 + atan(1e5 * rdif) / M_PI;
        if (ppos < 0.0 || ppos > 1.0) {
            std::cout << "WARNING: really weird: (MA) ppos : " << ppos << std::endl;
        }
        std::string r = (boost::format("plong=%.8e,pshort=%.8e") % p_l % p_s).str();
        return std::make_tuple(ppos,r);
    }
};

#endif

