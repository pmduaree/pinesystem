#ifndef PORTFOLIO
#define PORTFOLIO

#include "io.hpp"
#include "common.hpp"
#include "permutator.hpp"
#include <armadillo>
#include <boost/tokenizer.hpp>
#include "johansen.hpp"
#include "seriesCol.hpp"


std::vector<std::string> &splitString(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
/// ////////////////////////////////////////////////////////////////////////
/// ASSETS CLASS
/// ////////////////////////////////////////////////////////////////////////
class assets{										
public:			
	//std::vector<std::vector<double>> CC;
	arma::Mat<double> data;
	std::vector<std::string> titles;

	assets() {};
	//loading data and titles
	assets(std::string fileName){
		std::vector<std::string> tmp;
		splitString(fileName,'.',tmp);		
		std::stringstream str;
		str<<tmp[0]<<"Titles."<<tmp[1];	
		loadCSV(str.str(),titles);
		//prn(titles);
		data.load(fileName);
		if(data.n_cols!=titles.size()) throw std::runtime_error("ERROR::input degenerated");
	}
    
	//init assets from tix_series_collection and vector of titles. 
    assets(const tix_series_collection& matrix, std::vector<std::string>& names) {
		data = convert(matrix);
        titles = names;
		if (data.n_cols != titles.size()) 
            throw std::runtime_error("ERROR: assets was given a degenerated input");
		//debug
		//data.save("outputs//assets.dat",arma::raw_ascii);
	}
    
	//size returns 2D size :: dim(data)
	std::vector<size_t> size() const {
		std::vector<size_t> size;
		size.push_back(data.n_rows);
		size.push_back(data.n_cols);
		return size;;
	}
};
/// ////////////////////////////////////////////////////////////////////////
///PORTFOLIO CLASS
/// ////////////////////////////////////////////////////////////////////////
template<class T>
class portfolio{
public:
	//T data;							//we do not need to store data right now
	double R;							//cointegration level
	double Rconf;						//confidence level
	std::vector<double> w;				//eigen values
	std::vector<size_t> dataIndices;	//indeces of data stores in assets
	std::vector<std::string> assetsNames;//indeces of data stores in assets
		
	double mean;						//poerfolio series mean value
	double stdDev;						//std dev
	
	portfolio(){}
	portfolio(T& input,std::vector<size_t>& indeces, size_t VARmodel_k){
		
		//copy relevant data to local variables
		dataIndices.resize(indeces.size());
		std::copy(indeces.begin(),indeces.end(),dataIndices.begin());
		for(auto i=0;i<indeces.size();++i) assetsNames.push_back(input.titles[indeces[i]]);
		
		//prep fo coint >> convert std:vec >> arma::vec
		arma::uvec tmpVec = arma::conv_to<arma::uvec>::from(dataIndices); 
		//select data from mat(vec)
		mat testData = input.data.cols(tmpVec); //testData matrix contains only assets for this portfolio
		Johansen johan;
		/// Johan call
		/// ///////////////////////////////////////////////////////////////////////////
		/// input params:: 0 input detrended with polinomom 0, k=1 :: var(1) model
		/// outputs :: structure Results
		///	.eigenVectors     (mat)		- eigenVectors[0] gives you weights
		///	.traceTestStat[0] (double)	- the level of cointegration of all series (unscaled number)
		///	.confidence(2)	  (function)- confidence in % that data is cointegrated (basically normalized traceTestStat with respect to criticalValuesTrace)
		/// //////////////////////////////////////////////////////////////////////////
		try{
			auto out = johan(testData, 0, VARmodel_k);
		
			R=out.traceTestStat[0];
			Rconf=out.confidence(dataIndices.size()-1);	//binSize=dataIndices.size()-1

			w=arma::conv_to<std::vector<double>>::from(out.getLinearComboCoeffs());
		}catch(std::exception& e){ std::cout<<"johansen :: exception: " << e.what() << std::endl;}
		if(w.size()!=testData.n_cols) throw std::runtime_error("ERROR::something wrong with Johansen");
		
        double norm = 0.0;
        for(const auto& x : w) norm += x;
		for( auto& c:w ) c=c/norm;
		///compute portfolio series, mean and stddev
		arma::vec portfolioSeries(testData.n_rows);
		portfolioSeries.zeros();
		for(auto i=0;i<testData.n_cols;++i) portfolioSeries+=testData.col(i)*w[i];
		
		mean	= arma::mean(portfolioSeries);
		stdDev  = arma::stddev(portfolioSeries);
	
	}
	//sort operator
	bool operator < (const portfolio& B) const{
		if(R>B.R) return true;
		return false;
	}
};
/// ////////////////////////////////////////////////////////////////////////
/// COLLECTION CLASS
/// ////////////////////////////////////////////////////////////////////////
template<template<class> class pT,class aT>
class collection{
public:
	size_t binSize; 
	size_t inputSize;
	
	std::vector<pT<aT>> members;		
	std::vector<std::vector<size_t>> permutations;

	collection(){};
	collection(size_t bSize, aT& input,size_t VARmodel_k){
		if(!(VARmodel_k>0 && VARmodel_k<15)) throw std::runtime_error("ERROR::var k has to be within [1-15]");
		if(!(bSize>0 && bSize<12)) throw std::runtime_error("ERROR::bin size has to be within [1-12]");
		binSize=bSize;
		inputSize=input.size()[1];

		permutator<size_t> PeM(inputSize,binSize);
		permutations=PeM();

		generatePopulation(input,VARmodel_k);
		std::sort (members.begin(), members.end() ); 
	}
	//generates  all combinations
	void generatePopulation(aT& input, size_t VARmodel_k){
		int i;
		members.resize(permutations.size());
		#pragma omp parallel for shared(i) schedule(static)	
		for(i=0;i<permutations.size();++i){
			pT<aT> newGuy(input,permutations[i],VARmodel_k);
			members[i]=newGuy;
		}
		//if(permutations.size()!=members.size()) throw std::runtime_error("ERROR b1");
	}
	size_t size(){ return permutations.size();}
};


/// ////////////////////////////////////////////////////////////////////////
///OUTPUTS
/// ////////////////////////////////////////////////////////////////////////
template<class T>
std::ostream& operator<<(std::ostream& xx,const portfolio<T>& arr)
{
    std::stringstream str;
	str<<"[";
	str << arr.mean << ";";
	str << arr.stdDev << ";";
	str << arr.R << ";";
	str << arr.Rconf<<";";
	str << arr.w.size()<<";";
	str << arr.dataIndices<<";";
	str << arr.w <<";";
	str <<"{'"<<arr.assetsNames<<"'}";
	str<< "]";
	xx << str.str();
    return xx;
}
#endif
