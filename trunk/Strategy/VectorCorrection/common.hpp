#ifndef vec_COMMON_hpp
#define vec_COMMON_hpp
/////////////////////////////////////////////////////////////////////////////
/// common.hpp
/// This header provides a couple of generic routines, typically a sum, 
/// a product and other stuff
/// 
/////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>
#include <type_traits>
#include <vector>
#include <cstdlib>
#include <boost/format.hpp>
#include <armadillo>

//print macro
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 5,4,3,2,1)
#define VA_NUM_ARGS_IMPL(_1,_2,_3,_4,_5,N,...) N
#define macro_dispatcher(func, ...)     macro_dispatcher_(func, VA_NUM_ARGS(__VA_ARGS__))
#define macro_dispatcher_(func, nargs)  macro_dispatcher__(func, nargs)
#define macro_dispatcher__(func, nargs) func ## nargs
#define prn(...) macro_dispatcher(prnv, __VA_ARGS__)(__VA_ARGS__)
#define prnv1(a)   {std::cout << #a << "=" << (a) <<";"<<std::endl;}; 
#define prnv2(a,b) {std::cout << b << "=" << (a) <<";"<<std::endl;};

// << VECTOR 
template<class T>
std::ostream& operator<<(std::ostream& xx, const std::vector<T>& arr)
{
	std::stringstream str;
    // do it like the matlab does it.
    str << "[";
    
	for(size_t i=0;i<arr.size();++i) {
        str << arr[i];
		if(i<arr.size()-1) str <<",";
    }
	
    str << "]" ;//<< std::endl;
    xx << str.str();
    return xx;
}
//ARMADILO
template<class T>
std::ostream& operator<<(std::ostream& xx, const arma::Mat<T>& arr)
{
	std::stringstream str;
    // do it like the matlab does it.
    str << "[";
    for (size_t i = 0; i < arr.n_rows; ++i) {
        if (i > 0)
            str << ';';
        for (size_t j = 0; j < arr.n_cols; ++j) {
            str << (j > 0 ? ',' : ' ') << arr(i, j);
        }
    }
    str << "]";
	xx << str.str();
    return xx;
}


std::ostream& operator<<(std::ostream& xx, const arma::vec& arr){
    std::ostringstream str;
    str <<"[";
    for (size_t i = 0; i < arr.n_elem; ++i) {
        if (i > 0) 
            str << ',';
        str << arr(i);
    }
    str << "]";
    xx << str.str();
    return xx;
}



#endif
