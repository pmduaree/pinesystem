#ifndef __PERM__
#define __PERM__

/// ////////////////////////////////////////////////////////////////////////
/// PERMUTATOR CLASS
/// ////////////////////////////////////////////////////////////////////////
//generator of all (n,k) combination 
template<class T>
class permutator{
public:
	//Generate linear vector of size_t indeces
	permutator(size_t n, size_t binSize){	
		//generate index list [0:inputSize-1]
		for (auto i=0;i<n;++i){uniqueElements.push_back(i);}
		crawler(0, binSize);
	}
	std::vector<std::vector<T>> operator()(){
		return allCombinations;
	}
private:	
	std::vector<T> uniqueElements;	//list of all possible indeces
	std::vector<T> combination;		//recursinon temp, current combination
	std::vector<std::vector<T>> allCombinations; //container of all combinations	
	void crawler(size_t offset, size_t k) {
		if (k == 0) {
			allCombinations.push_back(combination);
			return;
	  }
	  for (size_t i = offset; i <= uniqueElements.size() - k; ++i) {
			combination.push_back(uniqueElements[i]);
			crawler(i+1, k-1);
			combination.pop_back();
	  }
	}
};
#endif
