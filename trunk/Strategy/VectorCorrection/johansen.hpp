#ifndef __JOHAN__
#define __JOHAN__

#include <iostream>
#include <string>
#include <sstream>
#include <armadillo>
using namespace std;
using namespace arma;

class Johansen {
protected:
    /// remove a polynome of order p from a set of column vectors y (stored as a matrix)
    /// works pretty much as octave's detrend
    mat detrend(const mat& y, unsigned int p) {
    // TODO: optimize (eliminate temporaries, streamline the code)
        mat resid;
        
        // number of observations
        size_t nobs = y.n_rows;
        size_t ncols = y.n_cols;
        
        mat u(nobs, 1);
        u.ones();
        mat xpxi(nobs, ncols), xmat(nobs, 1 + p);
        
        if (p > 0) {
            mat timep(nobs, p);
            timep.zeros();
            vec t(nobs);
            for (size_t i = 0; i < nobs; ++i) 
                t[i] = i+1;
            vec tp(nobs);
            tp = t / nobs;
            timep.col(0) = tp;
            for (unsigned int m = 1; m < p; ++m) {
                timep.col(m) = tp % timep.col(m-1);
            }
            
            xmat.col(0) = u; 
            xmat.cols(1, p) = timep;
        } else {
            xmat = u;
        }
        
        xpxi = inv(xmat.t() * xmat);
        //std::cout << "xpxi=" << xpxi.n_rows << "; "<<xpxi.n_cols <<"\n";
        mat beta = xpxi * (xmat.t() * y);
        resid = y - xmat * beta;
        return resid;
    }

    /// make a diff of column vectors x (stored as a matrix) of order k; 
    /// either trim the result or make the resulting matrix the same size as the input by padding its start with zeros
    mat tdiff(const mat& x, unsigned int k, bool trim = false) {
        size_t nobs = x.n_rows;
        size_t nvar = x.n_cols;
        
        if (k == 0) return x; 
        else {
            if (trim) {
                mat dmat(nobs-k, nvar);
                dmat.rows(0, nobs-k-1) = x.rows(k, nobs-1) - x.rows(0, nobs-k-1);
                return dmat;
            } else {
                mat dmat(nobs, nvar);
                dmat.rows(k, nobs-1) = x.rows(k, nobs-1) - x.rows(0, nobs-k-1);
                dmat.row(0).zeros();
                return dmat;
            }
        }
    }

    /// create a matrix of lags [1...n] of the input column vectors x (stored as a matrix)
    /// result contains in columns of the return matrix: n lags of the first vector, n lags of the second vector, ... 
    /// matrix is wider n times than the input matrix
    /// elements that pad the lagged vectors are set to init
    mat mlag(const mat& x, unsigned int n = 1, double init = 0.0) {
        size_t nobs = x.n_rows;
        size_t nvar = x.n_cols;
        
        mat xlag(nobs, nvar*n);
        
        unsigned int icnt = 0;
        for (size_t i=0; i < nvar; ++i) {
            for (size_t j=0; j < n; ++j) {
                xlag(span(0, j), icnt+j) = ones(j+1, 1)*init;
                xlag(span(j+1, nobs-1), icnt+j) = x(span(0, nobs-2-j), i);
                // xlag(j+1:nobs,icnt+j) = x(1:nobs-j,i);
            }
            icnt = icnt+n;
        }
        return xlag;
    }

    /// trims first the input matrix by fromStart starting rows and fromEnd ending rows
    mat trimr(const mat& x, unsigned int fromStart, unsigned int fromEnd) {
        return x.rows(fromStart, x.n_rows-fromEnd-1);
    }

    /// sorts vector by an index vector (index vector contains indices of input vector in sorted order)
    vec sortByIndex(const vec& v, const uvec& index) {
        if (index.n_elem != v.n_elem)
            throw std::logic_error("sortByIndex<vec>: index vector has different number of elements than the vector to be sorted");
        vec ret(v.n_elem);
        for (unsigned int i = 0; i < index.n_elem; ++i)
            ret(index(i)) = v(i);
        return ret;
    }

    /// sorts columns of matrix by an index vector (index vector contains indices of input matrix in sorted order)
    mat sortColumnsByIndex(const mat& m, const uvec& index) {
        if (index.n_elem != m.n_cols)
            throw std::logic_error("sortColumnsByIndex: index vector has different number of elements than matrix has columns");
        mat ret(m.n_rows, m.n_cols);
        for (unsigned int i = 0; i < index.n_elem; ++i)
            ret.col(index(i)) = m.col(i);
        return ret;
    }

    /// precalculated critical values for trace statistics
    mat criticalValuesTrace(unsigned int n, int p) {
        // PURPOSE: find critical values for Johansen trace statistic
        // n = dimension of the VAR system
        //      p = order of time polynomial in the null-hypothesis
        //             p = -1, no deterministic part
        //             p =  0, for constant term
        //             p =  1, for constant plus time-trend
        //             p >  1  returns no critical values
        // ------------------------------------------------------------
        //% RETURNS: a (3x1) vector of percentiles for the trace 
        //%          statistic for [90% 95% 99%] 
        //% ------------------------------------------------------------
        //% NOTES: for n > 12, the function returns a (3x1) vector of zeros.
        //%        The values returned by the function were generated using
        //%        a method described in MacKinnon (1996), using his FORTRAN
        //%        program johdist.f                                 
        //% ------------------------------------------------------------
        //% SEE ALSO: johansen()
        //% ------------------------------------------------------------
        //% % References: MacKinnon, Haug, Michelis (1996) 'Numerical distribution 
        //% functions of likelihood ratio tests for cointegration', 
        //% Queen's University Institute for Economic Research Discussion paper.
        //% -------------------------------------------------------
        if (n > 12)
            throw std::logic_error("criticalValuesTrace: supported dimension of the VAR system is up to (including) 12");
        
        mat jcp0(12, 3);
        jcp0 << 2.9762 <<   4.1296 <<   6.9406 << endr
            << 10.4741 <<  12.3212 <<  16.3640 << endr
            << 21.7781 <<  24.2761 <<  29.5147 << endr
            << 37.0339 <<  40.1749 <<  46.5716 << endr
            << 56.2839 <<  60.0627 <<  67.6367 << endr
            << 79.5329 <<  83.9383 <<  92.7136 << endr
           << 106.7351 << 111.7797 << 121.7375 << endr
           << 137.9954 << 143.6691 << 154.7977 << endr
           << 173.2292 << 179.5199 << 191.8122 << endr
           << 212.4721 << 219.4051 << 232.8291 << endr
           << 255.6732 << 263.2603 << 277.9962 << endr
           << 302.9054 << 311.1288 << 326.9716 << endr;

        mat jcp1(12, 3);
        jcp1 << 2.7055  <<  3.8415  <<   6.6349 << endr
             << 13.4294 <<  15.4943 <<  19.9349 << endr
             << 27.0669 <<  29.7961 <<  35.4628 << endr
             << 44.4929 <<  47.8545 <<  54.6815 << endr
             << 65.8202 <<  69.8189 <<  77.8202 << endr
             << 91.1090 <<  95.7542 << 104.9637 << endr
            << 120.3673 << 125.6185 << 135.9825 << endr
            << 153.6341 << 159.5290 << 171.0905 << endr
            << 190.8714 << 197.3772 << 210.0366 << endr
            << 232.1030 << 239.2468 << 253.2526 << endr
            << 277.3740 << 285.1402 << 300.2821 << endr
            << 326.5354 << 334.9795 << 351.2150 << endr;

        mat jcp2(12, 3);
        jcp2 <<   2.7055 <<   3.8415 <<   6.6349 << endr
            <<   16.1619 <<  18.3985 <<  23.1485 << endr
            <<   32.0645 <<  35.0116 <<  41.0815 << endr
            <<   51.6492 <<  55.2459 <<  62.5202 << endr
            <<   75.1027 <<  79.3422 <<  87.7748 << endr
            <<  102.4674 << 107.3429 << 116.9829 << endr
            <<  133.7852 << 139.2780 << 150.0778 << endr
            <<  169.0618 << 175.1584 << 187.1891 << endr
            <<  208.3582 << 215.1268 << 228.2226 << endr
            <<  251.6293 << 259.0267 << 273.3838 << endr
            <<  298.8836 << 306.8988 << 322.4264 << endr
            <<  350.1125 << 358.7190 << 375.3203 << endr;
           
        mat jc;
        if (p > 1 || p < -1) {
            jc = zeros(1,3);
        } else if (p == -1) {
            jc = jcp0.row(n-1);
        } else if (p == 0) {
            jc = jcp1.row(n-1);
        } else if (p == 1) {
            jc = jcp2.row(n-1);
        }
        return jc;
    }

    mat criticalValuesEigen(unsigned int n, int p) {
    /*
    jcp0 = [ 2.9762  4.1296  6.9406
             9.4748 11.2246 15.0923
            15.7175 17.7961 22.2519
            21.8370 24.1592 29.0609
            27.9160 30.4428 35.7359
            33.9271 36.6301 42.2333
            39.9085 42.7679 48.6606
            45.8930 48.8795 55.0335
            51.8528 54.9629 61.3449
            57.7954 61.0404 67.6415
            63.7248 67.0756 73.8856
            69.6513 73.0946 80.0937];
      
    jcp1 = [ 2.7055   3.8415   6.6349  
            12.2971  14.2639  18.5200  
            18.8928  21.1314  25.8650  
            25.1236  27.5858  32.7172  
            31.2379  33.8777  39.3693  
            37.2786  40.0763  45.8662  
            43.2947  46.2299  52.3069  
            49.2855  52.3622  58.6634  
            55.2412  58.4332  64.9960  
            61.2041  64.5040  71.2525  
            67.1307  70.5392  77.4877
            73.0563  76.5734  83.7105];
            
    jcp2 = [ 2.7055   3.8415   6.6349        
            15.0006  17.1481  21.7465        
            21.8731  24.2522  29.2631
            28.2398  30.8151  36.1930
            34.4202  37.1646  42.8612
            40.5244  43.4183  49.4095
            46.5583  49.5875  55.8171
            52.5858  55.7302  62.1741
            58.5316  61.8051  68.5030
            64.5292  67.9040  74.7434
            70.4630  73.9355  81.0678
            76.4081  79.9878  87.2395];

       if ((p > 1) | (p < -1));
        jc = zeros(1,3);
       elseif ((n > 12) | (n < 1));
        jc = zeros(1,3);
       elseif p == -1
        jc = jcp0(n,:);
       elseif p == 0
        jc = jcp1(n,:);
       elseif p == 1
        jc = jcp2(n,:);
       end;
    */
        return zeros(1, 1);
    }

public:
    /// structure of the Johansen test return
    struct Results {
        mat eigenVectors; // as columns of the matrix
        vec eigenValues;
        vec traceTestStat, eigenTestStat;   // [90% 95% 99%] confidence levels of test statistics
        // if test statistic > significant value at one of the levels then the variables are cointegrateable with that confidence
        mat criticalTraceValues;            // [90% 95% 99%] critical values for test statistics
        
        /// returns confidence level (in percents) for the variables to be cointegratable at given r (r = 0 ... individual inputs are stationary, r = number_of_inputs ... linear combination of inputs is stationary)
        /// default value (-1) is translated to the number of inputs
        int confidence(int r = -1) const {
            int m = traceTestStat.n_elem;
            if (r == -1) {
                r = m-1;
            }
            if (r < -1 || r >= m) 
                throw std::logic_error("Johansen test results: r must be between -1 and number of input columns -1");
            
            int level = 0;
            if (traceTestStat(m-1 - r) > criticalTraceValues(m-1 - r, 0))
                level = 90;
            if (traceTestStat(m-1 - r) > criticalTraceValues(m-1 - r, 1))
                level = 95;
            if (traceTestStat(m-1 - r) > criticalTraceValues(m-1 - r, 2))
                level = 99;
            return level;
        }
        
        /// get the coefficients of the best linear combination (the one that produces the highest cointegration level)
        /// give a better name to the function?
        vec getLinearComboCoeffs() const {
            return eigenVectors.col(0);
        }
    };
    
    /// run the Johansen test on column vectors x (in form of a matrix), with trend poly of degree p, using VAR model of order k
    /// todo: optimize
    Results run(mat x, int p, int k) {
        int f = p;
        if (p > -1)
            f = 0;
           
        if (p != 0) 
            x = detrend(x, p);
        // dx of the same dimension as x, diff performed on individual columns (rows are subtracted from each other), first row set to [0, 0, ..., 0]
        // trim 1 row from the begining and 0 rows from the end of dx
        mat dx = tdiff(x, 1, true);
        // z is of size: size(dx) * k; dx is shifted down k times (first column is shifted down by 1, 2, ..., k, second column is shifted down by 1, 2, ..., k)
        mat z = mlag(dx, k);
        
        // z  will contain detrended (degree f) trimmed lagged differences of detrended (k) input columns
        z = detrend(z.rows(k, z.n_rows-1), f);
        //cout << matlab(z) << endl;
        
        // dx will contain detrended (f) trimmed        differences of detrended (k) input columns (z and dx have equal number of rows, but may differ in number of columns)
        dx = detrend(dx.rows(k, dx.n_rows-1), f);  
        //cout << matlab(dx) << endl;
        
        // solve something here   
        mat r0t = dx - z*solve(z,dx);
        //cout << matlab(r0t) << endl;
        
        dx = detrend(x.rows(1, x.n_rows-1-k), f); // num_rows(dx) must equal num_rows(z)
        //cout << matlab(dx) << endl;
        
        mat rkt = dx - z*solve(z, dx);
        //cout << matlab(rkt) << endl;
        mat skk = rkt.t() * rkt / rkt.n_rows;
        mat sk0 = rkt.t() * r0t / rkt.n_rows;
        mat s00 = r0t.t() * r0t / r0t.n_rows;
        // mat sig = sk0 * inv(s00) * (sk0.t()); // why not using the more efficient solve?
        mat sig = sk0 * solve(s00, sk0.t());
        //cout << matlab(skk) << endl;
        //cout << matlab(sk0) << endl;
        //cout << matlab(s00) << endl;
        //cout << matlab(sig) << endl;
        
        mat tmp = inv(skk);
        cx_vec eigvalC;
        cx_mat eigvecC;
        eig_gen(eigvalC, eigvecC, tmp * sig);
        mat du = conv_to<mat>::from(eigvecC); // eigen vectors as columns of matrix
        vec au = conv_to<vec>::from(eigvalC); // eigen values as a vector
        // sort au (which is now unsorted) in descending order (also fix du)
        uvec sort_indices = sort_index(au, 1); //1 marks descending sort
        du = sortColumnsByIndex(du, sort_indices);
        au = sortByIndex(au, sort_indices);
        
        //mat orig = tmp * sig;
        //cout << matlab(tmp) << endl;
        //cout << matlab(orig) << endl;
        
        mat d = du * inv(chol(du.t() * skk * du));
        //cout << matlab(d) << endl;
        
        // some code from matlab source is skipped since eigenvectors are already sorted
        size_t m = x.n_cols;
        vec lr1 = zeros(m);
        vec lr2 = zeros(m);
        mat cvm = zeros(m,3);
        mat cvt = zeros(m,3);
        vec iota = ones(m);
        int t = rkt.n_rows;
        for (unsigned int i=0; i < m; ++i) {
            vec tmp = trimr(log(iota-au), i, 0);
            //cout << matlab(tmp) << "\n" ;
            lr1(i) = -t*sum(tmp);
            
            lr2(i) = -t*log(1-au(i));
    //        cvm.row(i) = criticalValuesEigen(m-i,p); // eigen value test not implemented
            cvt.row(i) = criticalValuesTrace(m-i,p);
        }
        
        Results temp;
        temp.criticalTraceValues = cvt;
        temp.eigenValues = au;
        temp.eigenVectors = d;
        temp.traceTestStat = lr1;
        temp.eigenTestStat = lr2;
        return temp;
    }

    /// run the Johansen test on column vectors x (in form of a matrix), with trend poly of degree p, using VAR model of order k
    inline Results operator()(mat x, int p, int k) {return run(x, p, k);}
};

#endif
