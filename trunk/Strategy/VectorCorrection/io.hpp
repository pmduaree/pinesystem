#ifndef __IO__
#define __IO__

#include <boost/tokenizer.hpp>
#include "../../../ticks/textfile_io.hpp"

//READ CSV file to vectot<vector<T>>
//	example::
//	std::vector<std::vector<double>> asset;
//	loadCSV("inputs/assets.out",asset); 

template<class T>
int loadCSV(std::string fileName,std::vector<std::vector<T>>& out){
	
    std::ifstream file(fileName);
	if (!file) {
		throw std::runtime_error("Exception: error opening input file");
	}
    
	typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;
    boost::char_separator<char> sep(",");
    std::string line;

    while (getline(file, line))
    {
        Tokenizer info(line, sep);   // tokenize the line of data
        std::vector<T> values;
        for (Tokenizer::iterator it = info.begin(); it != info.end(); ++it)
        {
            // convert data into double value, and store
            values.push_back(strtod(it->c_str(), 0));
        }
        // store array of values
        out.push_back(values);
    }
	return 0;
}  

template<class T>
int loadCSV(std::string fileName,std::vector<T>& out){
	
    std::ifstream file(fileName);
	if (!file) {
		throw std::runtime_error("Exception: error opening input file");
	}
    
    std::string line;

    while (getline(file, line))
    {
		//std::cout << "reading: " << line << std::endl;
        out.push_back(line);
    }
	return 0;
}   

// this is copied from textfile_io.h, but modifed from time_series to tix_series
// it serves for reading data in absence of sql database
void read_data_from_file(const std::string& fname, const std::string& format, tix_series& data) {
    data.clear();
    std::ifstream fp(fname.c_str());
    if (!fp.is_open()) {
        std::cout << "Couldn't open file" << std::endl;
        throw std::runtime_error("reading failed");
    }
    std::string line;
    while(std::getline(fp, line)) {
        boost::trim(line);
        if (line.empty()) continue;
        if (line.find("#") == 0) continue;
        try {
            tiempo::datetime_type date;
            double val;
            if (parse_line(line, format, date, val)) {
                data[date].p = val;
            }
        } catch (...) {
            //std::cout << "textfile I/O exception: " << line << std::endl;
        }
    }
    fp.close();
}
// this function could be moved to textfile_io.hpp
// read first row of an csv file and interpret it as the header row (containing names/titles of columns)
// it serves for reading data in absence of sql database
std::vector<std::string> csv_read_title_row(const std::string& fname) {
    std::vector<std::string> fields;
	std::ifstream fp(fname.c_str());
    if (!fp.is_open())
        throw std::runtime_error("Error: csv_read_title_row failed to open input file");
        
    std::string line;
    std::getline(fp, line);
    boost::trim(line);
    boost::split(fields, line, boost::is_any_of(","));
	// first entry in titles is time - remove it
    fields.erase(fields.begin());
	return fields;
}

#endif
