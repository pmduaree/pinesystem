#ifndef __SRCOL__
#define __SRCOL__

#include <armadillo>


/// ////////////////////////////////////////////////////////////////////////
/// SUPPORT FUNCTIONS
/// ////////////////////////////////////////////////////////////////////////
// collection of time-aligned time series - input to portfolio optimization / vector correction 
// this code is now superseded by a tix_series_collection version, which is the same in all respects, but uses tix_series instead of time_series
template<class T>
class time_series_collection {
    typedef time_series<T> TS;
    std::vector<TS> collection;
    std::vector<tiempo::datetime_type> sampleTimes;

  public:
    void generateSampleTimes() {
        sampleTimes.clear();
        if (collection.size() == 0)
            return;
        sampleTimes.reserve(collection[0].size());
        for (auto it = collection[0].begin(); it != collection[0].end(); ++it) {
            sampleTimes.push_back(it->first);
        }
        std::sort(sampleTimes.begin(), sampleTimes.end());
    }
    
    // align the collection of time series (keep only the samples that are available for all the time series in collection, remove all others, do not interpolate samples)
    // returns the number of removed samples
    bool align() {
        throw std::runtime_error("THIS IS BAD");
        return true;
    }
    
    // matrix access operator (first parameter is time index, second is series index)
    const T& operator() (size_t t, size_t i) const {
        assert(size() > i);
        assert(numSamples() > t);
        auto it = collection[i].find(sampleTimes[t]);
        assert(it != collection[i].end());
        return it->second;
    }
    
    size_t size() const {
        return collection.size();
    }
    
    size_t numSamples() const {
        return size() > 0 ? sampleTimes.size() : 0;
    }
    
    void push_back(const tix_series& series) {
        collection.push_back(series);
    }
};


// collection of time-aligned tix series - input to portfolio optimization / vector correction 
// it serves as a cache of Assets to be used for the calculation of cointegration
// how to use: 
//  - create
//  - insert series with push_back member function
//  - call align (to make sure all series are time aligned)
//  - convert it to armadillo mat with external function convert(const tix_series_collection&) which is in tudn used to calculate cointegration
class tix_series_collection {
    std::vector<tix_series> collection;
    std::vector<tiempo::datetime_type> sampleTimes;
 
  public:
    tix_series_collection() {
        collection.reserve(100);
    }
    
    void generateSampleTimes() {
        sampleTimes.clear();
        if (collection.size() == 0)
            return;
        sampleTimes.reserve(collection[0].size());
        for (auto it = collection[0].begin(); it != collection[0].end(); ++it) {
            sampleTimes.push_back(it->first);
        }
        std::sort(sampleTimes.begin(), sampleTimes.end());
    }
    
    // align the collection of time series (keep only the samples that are available for all the time series in collection, remove all others, do not interpolate samples)
    // returns the number of removed samples
    bool align() {

        tiempo::datetime date1 = collection.front().begin()->first;
        tiempo::datetime date2 = collection.front().rbegin()->first;
        for(const auto& tix: collection) {
            date1 = std::max(date1, tix.begin()->first);
            date2 = std::min(date2, tix.rbegin()->first);
        }
        std::cout << "All assets are defined between: " << date1.to_string() << " and " << date2.to_string() << std::endl;

        
        std::vector<TixSeries::const_reverse_iterator> It;
        std::vector<TixSeries::const_reverse_iterator> ItEnd;
        std::vector<TixSeries> ManipSer;
        for(const auto& col : collection) {
            It.push_back(col.rbegin());
            ItEnd.push_back(col.rend());
            ManipSer.push_back(TixSeries());
        }
        
        tiempo::datetime date = date2;
        while(date >= date1) {
            //std::cout << "Trying for date : " << date.to_string() << std::endl;
            std::vector<tiempo::datetime> last_date;
            std::vector<TickType> last_val;
            for(size_t i=0;i<collection.size();++i) {
                auto& it = It[i];
                const auto& it_end = ItEnd[i];
                while(it != it_end && it->first > date) ++it;
                if (it == it_end) {
                    std::cerr << "warning: it is end" << std::endl;

                }
                last_date.push_back(it->first);
                last_val.push_back(it->second);
            }
            tiempo::datetime max_d = *std::max_element(last_date.begin(),last_date.end());
            date = max_d;
            for(size_t i=0;i<collection.size();++i) {
                ManipSer[i][date] = last_val[i];
            }
            date -= std::chrono::minutes(1);
        }

        for(size_t i=0;i<collection.size();++i) {
            collection[i].swap(ManipSer[i]);
        }
        return true;



    }
    
    // matrix access operator (first parameter is time index, second is series index)
    const TickType& operator() (size_t t, size_t i) const {
        assert(size() > i);
        assert(numSamples() > t);
        auto it = collection[i].find(sampleTimes[t]);
        assert(it != collection[i].end());
        return it->second;
    }
    
    size_t size() const {
        return collection.size();
    }
    
    size_t numSamples() const {
        return size() > 0 ? sampleTimes.size() : 0;
    }
    
    void push_back(const tix_series& series) {
        collection.push_back(series);
    }
};

// convert time_series_collection to armadillo matrix; float or double should be used as templated type T; time_series_collection should be given in aligned state
// superseded by alternative that takes tix_series_collection as input
template <class T>
arma::Mat<T> convert(const time_series_collection<T>& tsc) {
    if (tsc.size() < 1)
        throw std::logic_error("cannot convert an empty time_series_collection (no time series) to matrix");
    if (tsc.numSamples() < 1) {
        std::cout <<" num samples: " << tsc.numSamples() << std::endl;
        throw std::logic_error("cannot convert a time_series_collection with no samples (possibly just not aligned) to matrix");
    }
    
    arma::Mat<T> matrix(tsc.numSamples(), tsc.size());
    for (size_t i = 0; i < tsc.size(); ++i) {
        for (size_t t = 0; t < tsc.numSamples(); ++t) {
            matrix(t, i) = tsc(t, i);
        }
    }
    
    return matrix;
}

// convert tix_series_collection to armadillo matrix; tix_series_collection should be given in aligned state
// armadillo matrix will then be used to calculate cointegration
arma::mat convert(const tix_series_collection& tsc) {
    if (tsc.size() < 1)
        throw std::logic_error("cannot convert an empty time_series_collection (no time series) to matrix");
    if (tsc.numSamples() < 1) {
        std::cout << "num tamples: " << tsc.numSamples() << std::endl;
        throw std::logic_error("cannot convert a time_series_collection with no samples (possibly just not aligned) to matrix");
    }
    
    arma::mat matrix(tsc.numSamples(), tsc.size());
    for (size_t i = 0; i < tsc.size(); ++i) {
        for (size_t t = 0; t < tsc.numSamples(); ++t) {
            matrix(t, i) = tsc(t, i).p;
        }
    }
    return matrix;
}
tix_series_collection loadData(std::string input_file){
	 /*
     * The procedure is as follows:
     *  - load assets data (for now this is via textfile_io and input is a csv with column 1 = date, other columns = values of assets);
     *    assets are represented with time_series<double>, a new class is introduced: time_series_collection that holds a vector of time_series
     *  - make sure assets are sampled at the same times (call time_series_collection.align()!)
     *  - load asset names (since time_series does not hold asset name)
     *  - convert time_series_collection to armadillo matrix, which can be loaded by the vectorCorrection training code
     */
        tix_series_collection ts;    // cache of all time_series (read from csv)
        // try reading forever, and break out when a read fails (an empty vector is read)
        // assume date is the first and only the first column!
        for (int i = 2;true;++i) {
            std::cout << " reading input file " << input_file << ", column:" << i << "  \n";
            std::stringstream input_file_format;
            input_file_format << "csv:" << i;

            tix_series temp_ts;
            read_data_from_file(input_file, input_file_format.str(), temp_ts);
            if (temp_ts.size() == 0)
                break;
            ts.push_back(temp_ts);
        }
        std::cout << "Reading time series done" << std::endl;
        // TODO: change reading from csv to reading from Asset (Asset.get_ticks())
		std::cout << " read total of " << ts.size() << " series\n";
        size_t wasAligned = ts.align();
        std::cout << (wasAligned==0 ? " series were already aligned" : " series required alignment") << "\n";
        
        
		return ts;
}

#endif
