#ifndef RSI_strategy_engine
#define RSI_strategy_engine

#include <iostream>
#include <vector>
#include <list>

#include "strategy_engine.hpp"
#include "../Utils/stringparpars.hpp"

class Strategy_RSI : public StrategyEngine
{
  public:
    Strategy_RSI() : StrategyEngine() {}
    virtual ~Strategy_RSI() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        StringParamParser paramparser(param);
        //------------------------------------
        //StringParamParser params(param);
        int nperiods = paramparser.get<int>("periods", 14);
        //double thres = paramparser.get<double>("thres", 0.1);   // some threshold, 
                                                                  // not used right now
        tiempo::duration dfreq(freq * 60);
        const auto& tix = asset->get_ticks();
        auto date_x = date - dfreq; // because the closing price candle(date - period) gives 
                                    // us the price at (date)
        time_series<CandleType> candles;
        size_t howmany = nperiods + 250;
        make_candles(tix, freq, date, howmany, candles);
        // make sure we don't include future prices
        double f = (nperiods - 1.0) / nperiods; // discounting factor
        double w = 1.0 / nperiods;              // weight 
        auto it = candles.rbegin();
        while(it != candles.rend() && it->first > date_x) ++it;
        if (candles.empty()) {
            std::cerr << "NO PREDICTION.....[RSI]" << std::endl;
            return std::make_tuple(0.5, "rsi=n/a");
        }
        double x_neg = 0.0, x_pos = 0.0;
        while(true ) {
            auto it_back = it;
            ++it_back;
            if (it_back == candles.rend()) break;
            double r = it->second.p_c - it_back->second.p_c;
            r *= w;
            if (r > 0) x_pos += r ; else x_neg -= r;
            
            // prepare for the next one.
            w *= f;
            if (w < 1e-15) break;   // no point continuing, it's very small already
            it = it_back;
        }
        double rs =  x_pos / x_neg;
        //std::cout << "rs : " << rs << std::endl;
        double rsi = 100 - 100 / (1 + rs);
        assert( rsi >= 0.0 && rsi <= 100.0 );
        std::string r = (boost::format("rsi=%.2f,xpos=%.3e,xneg=%.3e") % rsi % x_pos % x_neg).str();
        double p_buy = 1.0 - (rsi/100.0);
        return std::make_tuple(p_buy,r);
    }
};


#endif

