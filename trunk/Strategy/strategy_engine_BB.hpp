#ifndef BB_strategy_engine
#define BB_strategy_engine

#include <iostream>
#include <vector>
#include <list>

#include "strategy_engine.hpp"

#include "../Utils/stringparpars.hpp"

class Strategy_BB : public StrategyEngine
{
  public:
    Strategy_BB() : StrategyEngine() {}
    virtual ~Strategy_BB() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        
	//------------------------------------
	StringParamParser params(param);


	size_t nperiods= params.get<int>("nperiods", 20);
	if (nperiods < 3) {
		std::cerr << "Nperiods is invalid\n";
		return err_result;
	}
	double multiplier = params.get<double>("multiplier", 1.5);
	double param3 = params.get<double>("param3", 0.003);                  
	tiempo::duration dfreq(freq * 60);
	const auto& tix = asset->get_ticks();
	auto date_x = date - dfreq; // because the closing price candle(date - period) gives us the price at (date)
	time_series<CandleType> candles;
	size_t howmany = nperiods + 10;
	make_candles(tix, freq, date, howmany, candles);
	auto it = candles.rbegin();
	while(it != candles.rend() && it->first > date_x) ++it;
	if (candles.empty()) {
		std::cerr << "NO PREDICTION.....[BB]" << std::endl;
		return err_result;
	}
	std::vector<double> PC;
	PC.reserve(nperiods);
	double MB=0.0;
	size_t c_s=0;
	while(it != candles.rend() && c_s < nperiods ) {
		auto p =  it->second.p_c;
		PC.push_back(p);
		MB+=p;
		++c_s;
		++it;
	}
	if (c_s < nperiods) {
		std::cerr << "Insufficient data IN BB, nperiods="<<nperiods << ", datarange: " 
                  << candles.begin()->first.to_string() << " to " << candles.rbegin()->first.to_string() << 
                    " on date: " << date.to_string() << " freq="<<freq<<"\n";
		return err_result;
	}
	if (PC.size() != nperiods) {
		std::cerr << "sth wrong.\n";
		return err_result;
	}
	MB*=1.0/c_s;	// mean price
	double diff = 0.0;
	assert( PC.size() == nperiods);
	for(const auto& p : PC) {
		diff += (p - MB) * (p - MB);
	}
	double std_MB=sqrt(diff/nperiods);
	double UB=MB + multiplier*std_MB;
	double LB=MB - multiplier*std_MB;
	double BBwidth = (UB - LB)/ fabs(MB);

	if (BBwidth < param3) {
		double p_buy = 0.5;
		StringParamParser resultparser;
		resultparser.put("p", p_buy);
		resultparser.put("BBwidth", BBwidth);
		resultparser.put("UB", UB);
		resultparser.put("LB", LB);
		resultparser.put("age", 0.0);
		std::string r = resultparser.to_string();
		return std::make_tuple(p_buy,r);
	}

	auto tix_it = tix.rbegin();
	while(tix_it != tix.rend() && tix_it->first > date) ++tix_it;
	if (tix_it == tix.rend()) return err_result;

	auto date_prev = tiempo::datetime_type(date - dfreq);
	//std::cout << "Prices in range: " << date_prev.to_string() << " to " << date.to_string() << std::endl;
	//double p_h = tix_it->second.p, p_l = tix_it->second.p;
	while(tix_it != tix.rend() && tix_it->first > date_prev) {
		auto p = tix_it->second.p;	//price
		//std::cout << "d: " << tix_it->first.to_string() << "p : " << p << std::endl;
		//p_h = std::max(p_h, p);
		//p_l = std::min(p_l, p);
		if (p > LB && p < UB) {
			// no signal, go fuirther bac
			++tix_it;
		} else break;
	}
	if (tix_it == tix.rend()) return err_result;
	auto p = tix_it->second.p;
	double signal_age = tiempo::duration(date - tix_it->first).seconds() * 1.0 / dfreq.seconds();	// between 0 and 1 + eps
	signal_age = std::min<double>(1.0, signal_age);
	double signal_strength = std::max<double>(0.0, 1.0 - signal_age);
	assert( signal_strength >= 0 && signal_strength <= 1.0 );
	double p_buy = 0.5;
	if (p > UB) {
		p_buy = std::max<double>(0.0, 0.5 - signal_strength * 0.5);
	} else if (p < LB) {
		p_buy = std::min<double>(1.0, 0.5 + signal_strength * 0.5);
	}
	StringParamParser resultparser;
	resultparser.put("p", p_buy);
	resultparser.put("BBwidth", BBwidth);
	resultparser.put("UB", UB);
	resultparser.put("LB", LB);
	resultparser.put("age", signal_age);
	//resultparser.put("pl", p_l);
	//resultparser.put("ph", p_h);
	std::string r = resultparser.to_string();
	return std::make_tuple(p_buy,r);
    }
	
	
};
	
#endif
    
      
