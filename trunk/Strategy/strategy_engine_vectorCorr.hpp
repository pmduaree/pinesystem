//
//
//
//  Created by David de la Rosa on 5/19/14.
//  Copyright (c) 2014 ___VixTrend___. All rights reserved.
//

#ifndef _VectorCorrection_strategy_engine
#define _VectorCorrection_strategy_engine

#include <iostream>
#include <vector>
#include <unordered_map>
#include "strategy_engine.hpp"
#include "../Tiempo/ticks.hpp"
#include "VectorCorrection/common.hpp"
#include "VectorCorrection/vectorCorr.hpp"
#include "VectorCorrection/seriesCol.hpp"


class Strategy_VectorCorr : public StrategyEngine_Multiple
{
    public:
    Strategy_VectorCorr() : StrategyEngine_Multiple(){}
    virtual  ~Strategy_VectorCorr() {}
    
    virtual std::tuple<int,double,std::string>
    calculate_signal(std::shared_ptr<MAsset> assets_, const std::string& param, int freq, const tiempo::datetime_type& date,bool TradeStatus)
    {
        std::cout << "Vector Correction " << std::endl;
        StringParamParser MParam(param);
    
        std::cout << "MParam: " << MParam.to_string() <<
      //  " last date " <<  assets_->get_last_date() <<
        " Calculating on date" <<  date.to_string() << std::endl;
        
        
        double n_std=MParam.get<double>("n_std",0.9);
        double m_std=MParam.get<double>("m_std",0.75);
        size_t binSize=MParam.get<size_t>("binSize",3);
        size_t VARmodel_k=MParam.get<size_t>("VARmodel_k",1);
        //int q=MParam.get<int>("freq");
        //std::string date_from = MParam.option<>("date_from");
        //std::string date_to = MParam.option<>("date_to");
         auto last_2month = std::chrono::hours(24 * 15);
    
        tiempo::datetime from = (date - last_2month );
        //std::vector<int> id_array = assets_->get_ids();
        
        //std::cout  << "Assets on Portfolio " << id_array.size() << std::endl;
        
        std::vector<std::string> names  = assets_->get_names();
       // std::vector<std::string> names={{"AZN","PFE","DRGS","LLY"}};
        
        auto conn = std::make_shared<sql_connection>();
        

        std::vector<TixSeries> ticks;
        std::cout << "dates 1 = " << from.to_string() << " 2= " << date.to_string()<<std::endl;
        
        
        for(const auto& asset_name : names) {
            auto asset = std::make_shared<Asset>(conn, asset_name);
            asset->init(last_2month);
            auto tix = asset->get_ticks();
            ticks.emplace_back(tix);
        }
        
        if (ticks.empty()) {
            std::cerr << "Somethning wrong, no assets.\n";
            return err_result;
        }
        
        tix_series_collection ts;
        for(const auto& tt: ticks) {
            ts.push_back(tt);
        }
        ts.align();
        ts.generateSampleTimes();
        std::cout << "ts.size: " << ts.size() << ", num samples: " << ts.numSamples() << std::endl;
        std::cout << "We have everything, ts.size: " << ts.size() << ", num samples: " << ts.numSamples()<<std::endl;
        //prep data
        assets assetsData(ts, names);
        ///console outputs
        prn(assetsData.titles,"InputAssets");
        prn(assetsData.size(),"Input_size");
        prn(binSize);
        ///do everything
        collection<portfolio,assets> population(binSize,assetsData,VARmodel_k);
        //console outputs
        std::cout<<std::endl;
        const auto& BestM = population.members[0];
        double stdd=BestM.stdDev;
        double MB=BestM.mean;
        prn(population.size(),"popSize");
        prn(population.members[0].assetsNames,"bestCombination");
        prn(population.members[0].dataIndices,"bestCombination indices");
        prn(population.members[0].R,"traceTestStat");
        prn(population.members[0].Rconf,"ConfidenceLevel");
        prn(BestM.w ,"weights");
        prn(stdd,"std of portfolio series");
       
        std::string exit= (boost::format("binsize=%u,meanVP=%f,bestComb=")% binSize % MB ).str();
        
        for (unsigned int i =0; i!=binSize ; i++ ){
            exit+= population.members[0].assetsNames[i];
            
            if ( i < binSize -1   )
                exit += "-";
        }
        
        exit +=",bestComIndx=";
        for (unsigned int i =0; i!=binSize ; i++ ){
            exit+= std::to_string(population.members[0].dataIndices[i]);
            
            if ( i < binSize -1   )
                exit += "-";
        }
        exit +=",weights=";
        for (unsigned int i =0; i!=binSize ; i++ ){
            exit+= std::to_string(BestM.w[i]);
            
            if ( i < binSize -1   )
                exit += "|";
        }
        
        
        std::cout << "e=" << exit << std::endl;
        
        std::unordered_map<int, std::shared_ptr<Asset> > all_assets;
        auto conn___ = std::make_shared<sql_connection>();
        for(int unsigned i=0;i<BestM.assetsNames.size();++i){
            auto asset=std::make_shared<Asset>(conn___,BestM.assetsNames[i]);
            auto how_far_back_from_now = std::chrono::hours(24 * 60);
            asset->init(how_far_back_from_now);
            all_assets[i]=asset;
        }
        
        auto calc_PPP = [&all_assets,MB,stdd,&BestM](const tiempo::datetime& date) -> std::tuple<double,double>
        {
            double Port=0.0;
            for (auto& x: all_assets){
                const auto  &tix=x.second->get_ticks();
                auto it=tix.rbegin();
                while (it != tix.rend() && it->first > date) ++it;
                if (it == tix.rend()) {
                    std::cerr << "NO DATA... Quitting." << std::endl;
                    return std::make_tuple(0.0, -1.0);
                }
                Port+=BestM.w[x.first] * it->second.p;
            }
            double prob = 0.5 - 0.5 * erf((Port-MB)/(stdd * sqrt(2)));
            return std::make_tuple(Port, prob);
        };
        
        
        double n = std::min(n_std,m_std);
        double m = std::max(n_std,m_std);
        
        
        
        if(n == m){
            std::cerr << "sth wrong n == m" << std::endl;
            return err_result;
        }
       
        double Response=(double)0.0;
        int    Emited_signal= EMIT_WAIT_TRADE_SIGNAL;
        std::string comment("ACTION=WAIT,comment=DoNothing");
        
        double P_cur, Pbuy_cur;
        std::tie(P_cur, Pbuy_cur) = calc_PPP(date);
        if (Pbuy_cur < 0 || Pbuy_cur > 1) {
            std::cerr << "sth wrong  Pbuy_cur < 0 || Pbuy_cur > 1 " << std::endl;
            // thing about what to do. maybe skip
            return err_result;
        }
        
        if (TradeStatus == _Trade_Status_OffTrade )//current no position
        { if(Pbuy_cur > m){
            
            //status=" long";
            Emited_signal = EMIT_ENTER_TRADE_SIGNAL;
            Response= Pbuy_cur;
            comment=",ACTION=LONG";
        }//go long
        else if(Pbuy_cur < (1.0-m)) {
            //status="short";
            Emited_signal = EMIT_ENTER_TRADE_SIGNAL;
            Response= Pbuy_cur;
            comment=",ACTION=SHORT";
        }//go short
        else {
            comment=",ACTION=WAIT";
        }
        }
        else if(TradeStatus == _Trade_Status_OnTrade)//current long
        {
            if(Pbuy_cur<n) {
                Emited_signal = EMIT_EXIT_TRADE_SIGNAL;
                Response= Pbuy_cur;
                comment="Exit";
            }// close
            else{
     
                Emited_signal = EMIT_EXIT_TRADE_SIGNAL;
                Response= Pbuy_cur;
                comment="ACTION=WAIT";
     
            }
        }
       /* else if (position ==-1)//current short
        {
            if(Pbuy_cur>(1.0-n)){
                //position+=1;
                //status="close";
                //profit= diff-P_cur;
                //diff=0.0;
            }//close
            else {
              // status=" hold";
              //  profit=0.0;
     
            }
        }*/
        std::cout <<
        (boost::format("Signal Signal[%d] P[%f]   String[%s %s]") %Emited_signal % Response % exit %comment ).str() << std::endl;
        return std::make_tuple(Emited_signal,Response,exit+comment);
    }
    
};
#endif /* defined(__MultiAsset__File__) */
