#ifndef strategy_engines
#define strategy_engines

#include <string>
#include "strategy_engine.hpp"

std::shared_ptr<StrategyEngine_Single>   create_strategy_S(std::string type);
std::shared_ptr<StrategyEngine_Multiple> create_strategy_M(std::string type);


#endif

