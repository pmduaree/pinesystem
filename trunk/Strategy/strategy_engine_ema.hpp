#ifndef ema_strategy_engine
#define ema_strategy_engine
/////////////////////////////////////////////////////////////////////////////
// strategy_EMA
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <boost/format.hpp>
#include "../Tiempo/ticks.hpp"
#include "strategy_engine.hpp"

class Strategy_EMA : public StrategyEngine
{
  public:
    Strategy_EMA() : StrategyEngine() {}
    virtual ~Strategy_EMA() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        // Parse the input parameters and check the validity
        StringParamParser params(param);
        int period = params.get<int>("period", 0);
        if (period <= 1) {
            return err_result;
        }
        const auto& tix = asset->get_ticks();

        double err_ema = -9999.9999;

        auto calc_ema = [freq,&tix,period,err_ema](const tiempo::datetime_type& d) -> double
        {
            CandleSeries candles;
            // 3 * period is enough for 10^{-7} error.
            make_candles(tix, freq, d, 3*period + 10, candles);
            
            // safe guard: shift the iterator in case it includes future prices.
            // because the candle at time 'date' includes information from 
            // 'date' to 'date' + minutes(freq)
            auto date_x = d - std::chrono::minutes(freq);
            auto it = candles.rbegin();
            while(it != candles.rend() && it->first > date_x) ++it;
            if (it == candles.rend()) {
                return err_ema;
            }
            double P = 0.0;
            double W = 0.0;
            int navg = 0;
            while(it != candles.rend() ) {
                double w = exp( - navg * 5.0 / period );
                if (w < 1e-8) break;
                //double w = period - navg;
                W += w;
                P += w * it->second.p_c;
                ++navg;
                ++it;
            }
            if (navg < period) return err_ema;
            assert( W > 0.0 );
            double ema = P / W;
            return ema;
        };
        double ema = calc_ema(date);
        if (ema == err_ema) return err_result;
        
        auto it_tix = tix.rbegin();
        while(it_tix != tix.rend() && it_tix->first > date) ++it_tix;
        double p_close = it_tix->second.p;
        
        if (it_tix == tix.rend() ) return err_result;
        
        // check if the price is above or below the moving average line
        auto get_sign = [](double ema, double p)  -> int 
        { return (p > ema ? 1 : (p < ema ? -1 : 0) ); };
        
        int current_sign = get_sign(ema, it_tix->second.p);
        std::string r;
        double p_buy = 0.5;
        if (current_sign == 0) {
            // it's a special consideration, very unlikely to happen
            ++it_tix;
            auto csign = get_sign(ema, it_tix->second.p);
            p_buy = csign > 0 ? 1.0 : (csign < 0 ? 0.0 : 0.5) ;
            r = (boost::format("p=%.2f,dur=0,sign=0,ema=%.3f,pcl=%.3f")%p_buy % ema % p_close).str();
        } else {
            auto date_cut = date - std::chrono::minutes(3 * freq);
            while( ++it_tix != tix.rend() && it_tix->first >= date_cut ) {
                auto csign = get_sign(ema, it_tix->second.p);
                if (csign*current_sign <= 0) {
                    break;
                }
            }
            if (it_tix == tix.rend() ) return err_result;
            tiempo::duration duration_from_crossing = date - it_tix->first;
            double p_either = std::max<double>(0.0, 1.0 - duration_from_crossing.seconds() * 1.0 / (60.0 * freq) );
            p_buy = 0.5 + current_sign * p_either * 0.5;
            r = (boost::format("p=%.2f,dur=%d,sign=%d,ema=%.3f,pcl=%.3f") % p_buy % 
                               (duration_from_crossing.seconds()/60) % 
                               current_sign % ema % p_close).str();
        }
        return std::make_tuple(p_buy, r);
    }
};

#endif

