#ifndef __strategy_engine__
#define __strategy_engine__

#include <string>
#include <memory>
#include <tuple>
#include "../Tiempo/tiempo.hpp"
#include "../Assets/vasset.hpp"
#include "../Assets/asset.hpp"
#include "../Assets/masset.hpp"
#define EMIT_ENTER_TRADE_SIGNAL 1
#define EMIT_EXIT_TRADE_SIGNAL  -1
#define EMIT_WAIT_TRADE_SIGNAL  0

#define _Trade_Status_OnTrade   1
#define _Trade_Status_OffTrade  0

class StrategyEngine_Single
{
  public:
    StrategyEngine_Single(): err_result(std::make_tuple(0,-1.0,"Error")) {}
    virtual ~StrategyEngine_Single() {};

    virtual 
    std::tuple<int,double,std::string>
    calculate_signal(std::shared_ptr<Asset> asset, const std::string& param, 
                     int freq, const tiempo::datetime_type& date,bool TradeStatus) = 0;

    std::tuple<int,double,std::string> err_result;
};
class StrategyEngine_Multiple
{
public:
    StrategyEngine_Multiple(): err_result(std::make_tuple(0,-1.0,"Error")) {}
    virtual ~StrategyEngine_Multiple() {};
    
    virtual
    std::tuple<int,double,std::string>
    calculate_signal(std::shared_ptr<MAsset> asset, const std::string& param,
                     int freq, const tiempo::datetime_type& date,bool TradeStatus) = 0;
    
    
    std::tuple<int,double,std::string> err_result;
};
#endif
