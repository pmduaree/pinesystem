#ifndef ROC_strategy_engine
#define ROC_strategy_engine

#include <iostream>
#include <vector>
#include <list>
#include<algorithm>

#include "strategy_engine.hpp"
#include "../Utils/stringparpars.hpp"

class Strategy_ROC : public StrategyEngine
{
  public:
    Strategy_ROC() : StrategyEngine() {}
    virtual ~Strategy_ROC() {}
    
    virtual std::tuple<double,std::string>
    calculate_signal(std::shared_ptr<VAsset> asset, const std::string& param, int freq, 
                     const tiempo::datetime_type& date)
    {
        StringParamParser paramparser(param);
        //------------------------------------
        //StringParamParser params(param);
        int nperiods = paramparser.get<int>("periods", 14);
        // check the parameters.
        if (nperiods < 1 || nperiods > 50) {
            std::cerr << boost::format("WRONG PARAMETERS.....[ROC, nperiods=%d]")%nperiods << std::endl;
            return std::make_tuple(0.5, "roc=n/a");
        }
        tiempo::duration dfreq = std::chrono::minutes(freq);
        const auto& tix = asset->get_ticks();
        auto date_x = date - dfreq; // because the closing price candle(date - period) gives 
                                    // us the price at (date)
        time_series<CandleType> candles;
        size_t howmany = nperiods + 6;
        make_candles(tix, freq, date, howmany, candles);
        
        // make sure we don't include future prices
        while( !candles.empty()  && candles.rbegin()->first > date_x)
            candles.erase(candles.rbegin()->first);

        if (candles.empty() || static_cast<long>(candles.size()) < nperiods + 2) {
            std::cerr << "NO PREDICTION.....[ROC]: " << candles.size() << " vs " << nperiods << std::endl;
            return std::make_tuple(0.5, "roc=n/a");
        }
        auto calc_roc = [](const CandleType& c_now, const CandleType& c_past) -> double
            { return 100.0 * (c_now.p_c - c_past.p_c) / fabs(c_past.p_c); };
        
        // we are guaranteed to have enough data now.
        auto it = candles.rbegin();
        auto it_1 = it;
        ++it;
        auto it_2 = it;
        auto itb_1 = it_1; std::advance(itb_1, nperiods);
        auto itb_2 = it_2; std::advance(itb_2, nperiods);
        double roc_1 = calc_roc(it_1->second, itb_1->second);
        double roc_2 = calc_roc(it_2->second, itb_2->second);
        
        std::string r = (boost::format("roc=%.2f,roc_past=%.3e") % roc_1 % roc_2 ).str();
        
        double p_buy = 0.5 ;
        if(roc_2 < 0.0 && roc_1 > 0.0) p_buy=1.0;
        if(roc_2 > 0.0 && roc_1 < 0.0) p_buy=0.0;
        return std::make_tuple(p_buy,r);
    }
};


#endif

