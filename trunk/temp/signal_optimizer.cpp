#include "signal_optimizer.hpp"

#include "io_vector.hpp"

SignalOptimizer::SignalOptimizer(std::shared_ptr<sql_connection> conn_,
                std::shared_ptr<Asset> asset_, int freq_, int offset_,
                const tiempo::datetime_type& date1_, const tiempo::datetime_type& date2_)
    : conn(conn_), asset(asset_), freq(freq_), offset(offset_), date_1(date1_), date_2(date2_),
      dfreq(std::chrono::minutes(freq)), 
      sig_measure(asset, dfreq)

{
    date_1 = offset_date(date_1, freq, offset);
    load_strategies();
    
    // make trading dates.
    const auto& exchange = asset->get_exchange();
    auto date = offset_date(date_1, freq, offset);
    trading_dates.clear();
    while(date <= date_2) {
        if (!exchange.is_in_trading_day(date)) {
            date += dfreq;
            continue;
        }
        trading_dates.push_back(date);
        date += dfreq;
    }
    
    check_strategies();
}


bool SignalOptimizer::make_signals(const std::vector<double>& Wj, time_series<signal_type>& signals)
{
    if (Wj.size() != strategies.size())
        throw std::runtime_error("Inconsistent number of strategies in the portfolio");
    auto engine = get_portfolio(Wj);
    signals.clear();
    engine->calculate_signals(trading_dates, signals);
    return true;
}

double SignalOptimizer::operator()(const std::vector<double>& Wj)
{
    //std::cout << "Calculating signals... for W: " << Wj << std::endl;
    // calculate measure
    time_series<signal_type> signals;
    make_signals(Wj, signals);
    // what are the signals?
    //for(const auto& z: signals) std::cout << z.first.to_string() << " : " << signal_to_string(z.second) << std::endl;
    //std::cout << "Calculating measure... for W: " << Wj << std::endl;
    double measure = sig_measure(signals);
    //std::cout << "Measure: " << measure << std::endl;
    return measure;
}
    
void SignalOptimizer::save(int optparam_id, const std::vector<double>& Wj)
{
    // save a strategy portfolio to the database
    auto portfolio = get_portfolio(Wj);
    int portfolio_id = portfolio->save();
    std::cout << boost::format("StrategyPortfolio [id=%d] saved to the database.") % portfolio_id << std::endl;
    int asset_id = asset->get_id();
    auto time_now = tiempo::now();
    auto datenow_str = time_now.to_string("%Y-%m-%d %H:%M:%S");
    auto date1_str = date_1.to_string("%Y-%m-%d %H:%M:%S");
    auto date2_str = date_2.to_string("%Y-%m-%d %H:%M:%S");
    std::string info_str = portfolio->get_info();
    std::string query = (boost::format("UPDATE `Main`.`SignalOptimization` SET  \
                                          `asset_id`=%d, `portfolio_id`=%d,     \
                                          `date_from`=\'%s\', `date_to`=\'%s\'  \
                                        WHERE `id`=%d") % 
            asset_id % portfolio_id % date1_str % date2_str  % optparam_id).str();
    conn->execute(query);
}

void SignalOptimizer::check_strategies()
{
    auto it = strategies.begin();

    while(it != strategies.end() ) {
        auto& strategy = *it;
        time_series<signal_type> signals;
        auto signaller = std::make_shared<Signaller>(conn, strategy, asset);
        for(auto& d : trading_dates) {
            double p_buy = (*signaller)(d);
            //double p_buy = std::get<0>(res);
            //std::cout << "p_buy: " << p_buy << std::endl;
            signal_type sig = signal_from_buy_probability(p_buy);
            signals[d] = sig;
        }
        
        double measure = sig_measure(signals);
        //std::cout << boost::format("Partial measure [strategy=%d] : %.6f") % strategy->get_id() % measure << std::endl;

        if (measure <= 0.0) {
            std::cout << boost::format("Deleting strategy %d with measure %.5f")%strategy->get_id() % measure << std::endl;
            if (strategy->get_id() != strategies.back()->get_id())
                std::swap(strategy, strategies.back() );
            strategies.back().reset();
            strategies.pop_back();
        } else {
            std::cout << boost::format("Keeping  strategy %d with measure %.5f")%strategy->get_id() % measure << std::endl;
            ++it;
        }
    }


    std::cout << "Kept strategies: " << strategies.size() << std::endl;

}
    

void SignalOptimizer::load_strategies()
{
    std::cout << "Composing the strategy portfolio..." << std::endl;
    // load the strategies from the database
    std::vector<int> strats;
    conn->lock();
    std::string q = "SELECT `id`,`type`,`param`,`freq` FROM `Main`.`Strategies`";
    auto con = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
    while(res->next()) {
        int id = res->getInt("id");
        strats.push_back(id);
    }
    conn->unlock();
    // compose the strategies
    for(int strategy_id : strats) {
        auto strategy = std::make_shared<Strategy>(conn, strategy_id);
        strategies.emplace_back(strategy);
    }
}



std::shared_ptr<StrategyPortfolio> SignalOptimizer::get_portfolio(const std::vector<double>& Wj)
{
    auto engine = std::make_shared<StrategyPortfolio>(conn, asset, freq, offset);
    for(size_t i=0;i<Wj.size();++i) {
        if (!Wj[i]) continue;
        auto strategy = strategies[i];
        //std::cout << "Adding the strategy " << strategy->get_type() << " : " << strategy->get_param() << std::endl;
        auto signaller = std::make_shared<Signaller>(conn, strategy, asset);
        engine->add_signaller(Wj[i],signaller);
    }
    return std::move(engine);
}


