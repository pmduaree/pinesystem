#include "historic_trader.hpp"




int int_signal(signal_type siq)
{
    int csig = 0;
    if (siq == signal_type::Buy) csig = 1;
    else if (siq == signal_type::Sell) csig = -1;
    return csig;
}



void HistoricTrader::operator()(std::shared_ptr<SignalInterface> sigint, 
                const TraderParams& trp, const tiempo::datetime& date_from,
                const tiempo::datetime& date_to, const tiempo::duration& step_freq, 
                double money0, std::vector<trade_slice>& RV)
{
    TradeStatus status(money0);
    operator()(sigint, trp, date_from, date_to, step_freq, status, RV);
}

/////////////////////////////////////////////////////////////////////////
// Historic Trader main routine
/////////////////////////////////////////////////////////////////////////
void HistoricTrader::operator()(std::shared_ptr<SignalInterface> sigint, 
                const TraderParams& trp, const tiempo::datetime& date_from,
                const tiempo::datetime& date_to, const tiempo::duration& step_freq, 
                TradeStatus& status, std::vector<trade_slice>& RV)
{
    const double money0 = status.cash();
    if (status.quantity()) 
        throw std::runtime_error("cant start with a position");
    double fees = 0.0;
    RV.clear();
    size_t td_cnt = 0;
    const auto& tix = iinfo->get_ticks();
    auto it_tix = tix.begin();
    while(it_tix != tix.end() && it_tix->first < date_from) ++it_tix;
    if (it_tix == tix.end() ) {
        std::cerr << "NO DATA... Quitting." << std::endl;
        return;
    }
    tiempo::datetime used_signal = date_from - std::chrono::hours(10);
    //std::cout << "Historical simulation from " << date_from << " to " << date_to << std::endl;
    const auto& exchange = iinfo->get_exchange();
    
    size_t Z1cnt=0, Z2cnt=0, Z3cnt=0, Z4cnt=0;
    while(it_tix != tix.end() && it_tix->first <= date_to) {
        ++Z1cnt;
        auto date = it_tix->first;
        int quantity = status.quantity();
        if (!quantity && !exchange.is_in_trading_day(date)) {
            auto next_open = date + exchange.time_to_open_trading_day(date);
            while(it_tix != tix.end() && it_tix->first < next_open) ++it_tix;
            //++it_tix;
            continue;
        }
        if (it_tix == tix.end()) break;
        if (date > date_to) break;
        ++Z2cnt;
        signal_type siq;
        tiempo::datetime sig_date;
        //std::cout << "Doing on date: " << date.to_string() << std::endl;
        std::tie(siq, sig_date) = (*sigint)(date);
        if (sig_date <= used_signal) {
            siq = signal_type::None;
        }
        assert( it_tix != tix.end() );
        double P_current = it_tix->second.p;
        trad_order order = tradereq.get_order(date, siq, trp, step_freq, 
                                              status.quantity(), status.date0(), 
                                              status.price0(), P_current);
        //std::cout << "Got order: " << order_to_string(order) << std::endl;
#ifndef NDEBUG
        //std::cout << boost::format("Order [%s]: %s") % date.to_string() % order_to_string(order) << std::endl;
#endif
        if (order == trad_order::Postpone) {
            ++it_tix;
            continue;
        }
        ++Z3cnt;
        used_signal = sig_date;
        if (order == trad_order::Wait || order == trad_order::Hold) {
            ++it_tix;
            continue;
        }
        ++Z4cnt;
        // there's some action very likely.
        double trade_value = 0.0;
        bool action_taken = false;
        if (order == trad_order::Close) {
            double open_value = status.value(P_current);
            status.close_position(date, P_current);
            trade_value = fabs(open_value);
            action_taken = true;
        } else if (order == trad_order::Buy || order == trad_order::Sell) {
            int sig_sign = (siq == signal_type::Buy ? 1 : -1);
            long quantity = sig_sign * static_cast<long>(status.cash() / fabs(P_current));
            assert( ( siq == signal_type::Buy && quantity > 0) ||
                    ( siq == signal_type::Sell && quantity < 0) );
            status.open_position(date, quantity, P_current);
            double current_value = status.value(P_current);
            trade_value = fabs(current_value);
            action_taken = true;
        } else if (order == trad_order::CloseAndReset) {
            double open_value = status.value(P_current);
            status.close_position(date, P_current);
            trade_value = fabs(open_value);
            action_taken = true;
            // revert back the HMM signal.
            used_signal -= std::chrono::minutes(1);
        } else {
            assert( !"Should never come to this point. Unprocessed signal.");
            std::cout << "Should never come to this point. Unprocessed signal."<<std::endl;
            throw std::runtime_error("unprocessed signal");
        }
        fees += iinfo->get_commission(fabs(trade_value));
        double eff_money = status.current_value(P_current) - fees - 
                           iinfo->get_commission(fabs(status.value(P_current)));
        if (action_taken) {
            trade_slice slice;
            slice.date = date;
            slice.signal = int_signal(siq);
            slice.siq = siq;
            slice.order = order;
            slice.tradestatus = status;
            slice.fees = fees;
            slice.action = get_action(order);
            slice.eff_money = eff_money;
            slice.roi = eff_money / money0 - 1.0;
            RV.push_back(slice);
        }
        ++td_cnt;
        ++it_tix;
    }

    //std::cout << boost::format("ZCNT: (%d,%d,%d,%d)") % Z1cnt % Z2cnt % Z3cnt % Z4cnt << std::endl;
}

std::tuple<double,double,double>
calculate_alpha(const std::vector<trade_slice>& RV)
{
    if (RV.empty()) {
        return std::make_tuple(0.0, 1.0, 0.0);
    }
    assert( !RV.empty() );
    std::vector<double> X,Y;
    for(const auto& slice : RV) {
        tiempo::duration dur = slice.date - RV.front().date;
        double x = dur.seconds() / 3600.0;
        double y = slice.roi;
        X.push_back(x);
        Y.push_back(y);
    }
    double alpha, var;
    std::tie(alpha, var) = alpha_fit_linear(X, Y);
    // let's make a combined measure.
    double fitted_r = alpha * (Y.back() - Y.front());
    double actual_r = Y.back();
    double accum_loss = 0.0;
    for(size_t i=1;i<Y.size();++i) {
        double yr = Y[i] - Y[i-1];
        if (yr < 0) accum_loss -= yr;
    }
    
    alpha *= 24 * 30 * 100.0;
    double measure = fitted_r + actual_r;// - 0.4*accum_loss;
    return std::make_tuple(alpha, sqrt(var), measure);
}



