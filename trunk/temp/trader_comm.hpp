#ifndef __trader_comm__
#define __trader_comm__

#include <memory>
#include <mutex>
#include "sql_connection.hpp"
#include "cqgtraderapi.hpp"
#include "trader_position.hpp"


class TraderComm
{
    std::shared_ptr<sql_connection> conn;
    CQGTraderAPI agent;
    std::mutex _mux;
    
  public:
    TraderComm(std::shared_ptr<sql_connection> conn_)
        : conn(conn_)
    {
    }
    

    bool is_processing(int asset_id)
    {
        bool rv = false;
        int ticket = find_ticket(asset_id);
        if (!ticket) {
            rv = false;
        } else {
            _mux.lock();
            auto tr_status = agent.TradeStatus(ticket);
            _mux.unlock();
            if (tr_status == OrderStatus::Processing) rv = true;
        }
        return rv;
    }
    

    bool get_position(int trader_id, TradePosition& pos)
    {
        int ticket = 0;
        tiempo::datetime date_sig, date_trade;
        std::tie(ticket, date_sig, date_trade) = find_by_trader(trader_id);
        if (ticket == -1 || ticket == 0) return false;
        _mux.lock();
        auto tr_status = agent.TradeStatus(ticket);
        _mux.unlock();
        if (tr_status == OrderStatus::None) {
            pos.price = 0;
            pos.quantity = 0;
            pos.date = tiempo::datetime("now");
        } else if (tr_status == OrderStatus::Open) {
            TradeDetail detail;
            _mux.lock();
            agent.TradeDetails(ticket, detail);
            _mux.unlock();
            pos.price = detail.price_open;
            pos.quantity = detail.quantity;
            pos.date = tiempo::datetime(detail.date_open);
        } else if (tr_status == OrderStatus::Close) {
            clear_position(trader_id);
            TradeDetail detail;
            _mux.lock();
            agent.TradeDetails(ticket, detail);
            _mux.unlock();
            pos.date = tiempo::datetime("now");
            pos.price = detail.price_close;
            pos.quantity = 0;
        } else {
            std::cerr << "Wrong status: " << static_cast<int>(tr_status) << std::endl;
            throw std::runtime_error("WE really shoulnt be here");
        }
        return true;
    }
    
    bool get_last_signal(int trader_id, tiempo::datetime& date)
    {
        int ticket = 0;
        tiempo::datetime date_sig, date_trade;
        std::tie(ticket, date_sig, date_trade) = find_by_trader(trader_id);
        if (ticket < 0) {
            return false;
        } else {
            date = date_sig;
            return true;
        }
    }
    
    void update_last_signal(int liveasset_id, const tiempo::datetime& date)
    {
        std::string q = (boost::format("UPDATE `Main`.`Trading` SET `date_sig`=\'%s\' WHERE `id`=%d") % date.to_string("%Y-%m-%d %H:%M:%S") % liveasset_id).str();
        conn->lock();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute(q);
        conn->unlock();
    }
    
    void enter_trade(int trader_id, int instrument_id, int quantity, double price, const tiempo::datetime& sigdate)
    {
        if (!quantity) throw std::runtime_error("invalid quantity");
        std::cout << boost::format("enter_trade [asset=%d,instrument=%d,quantity=%d,price=%.2f,sigdate=%s].")
                                    % trader_id % instrument_id % quantity % price % sigdate.to_string() << std::endl;
        
        _mux.lock();
        int trading_code = agent.GetTradingCode(instrument_id);
        _mux.unlock();
        std::cout << boost::format("Trading code[instrument_id=%d] = %d") 
                        % instrument_id % trading_code << std::endl;
        if (!trading_code) {
            std::cout << "Could not find the trading code. The trade was not executed." << std::endl;
            return;
        }
        std::string ts_pushed = sigdate.to_string("%Y-%m-%d %H:%M:%S");
        std::cout << "Calling agent.EnterTrade." << std::endl;
        _mux.lock();
        int ticket = agent.EnterTrade(trading_code, quantity, ts_pushed, price);
        _mux.unlock();
        std::cout << "Exited from  agent.EnterTrade, got ticket = " << ticket << std::endl;
        update_trader(trader_id, sigdate, tiempo::now(), ticket);
    }

    
    void exit_trade(int liveasset_id, const tiempo::datetime& date, double price)
    {
        int ticket = find_ticket(liveasset_id);
        if (!ticket) {
            std::cerr << "WARNING: NO NO  exit trade ticket doesnt exist" << std::endl;
            throw std::runtime_error("bf");
        }
        std::string ts_pull = date.to_string("%Y-%m-%d %H:%M:%S");
        _mux.lock();
        agent.ExitTrade(ticket, ts_pull, price);
        _mux.unlock();
    }

  private:
    std::tuple<int,tiempo::datetime,tiempo::datetime> 
    find_by_trader(int trader_id)
    {
        tiempo::datetime date_sig, date_trade;
        int ticket = -1;
        std::string q = (boost::format("SELECT * FROM `Main`.`Trading` WHERE `id`=%d")%trader_id).str();
        conn->lock();
        auto con = conn->connection();
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
        if (res->rowsCount() > 0) {
            res->first();
            ticket =res->getInt("ticket");
            date_sig = tiempo::datetime(res->getString("date_sig"));
            date_trade = tiempo::datetime(res->getString("date_trade"));
        }
        conn->unlock();
        return std::make_tuple(ticket, date_sig, date_trade);
    }
    
    int find_ticket(int liveasset_id)
    {
        std::string q = (boost::format("SELECT `ticket` FROM `Main`.`Trading` WHERE `id`=%d")%liveasset_id).str();
        int ticket=0;

        conn->lock();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
        if (res->rowsCount() > 0) {
            res->first();
            ticket = res->getInt("ticket");
        }
        conn->unlock();
        return ticket;
    }
    
    void update_trader(int trader_id, const tiempo::datetime& date_sig, 
                       const tiempo::datetime& date_pushed, int ticket)
    {
        conn->lock();
        auto con = conn->connection();
        std::unique_ptr<sql::Statement> stmt( con->createStatement() );
        std::string q = (boost::format("UPDATE `Main`.`Trading` SET `date_trade`=\'%s\', `date_sig`=\'%s\' , `ticket`=%d WHERE `id`=%d")%
                         date_pushed.to_string("%Y-%m-%d %H:%M:%S") % 
                         date_sig.to_string("%Y-%m-%d %H:%M:%S") %
                         ticket % trader_id).str();
        stmt->execute(q);
        conn->unlock();
    }
    void clear_position(int trader_id)
    {
        std::string q = (boost::format("UPDATE `Main`.`Trading` SET `ticket`=0 WHERE `id`=%d")%trader_id).str();
        conn->lock();
        auto con = conn->connection();
        std::unique_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute(q);
        conn->unlock();
    }
};




#endif

