#ifndef __trader_request_
#define __trader_request_

#include <iostream>
#include <tuple>
#include "tiempo.hpp"
#include "time_series.hpp"

#include "asset.hpp"
#include "signal_shorttime.hpp"

#include "trader_stoploss.hpp"
#include "trader_profitpreserver.hpp"
#include "trader_action.hpp"


class TradeRequest
{
    std::shared_ptr<Asset> iinfo;
    std::shared_ptr<Trend> local_trend;
    StopLossModule zoe;
    
  public:
    TradeRequest(std::shared_ptr<Asset> i_info)
        : iinfo(i_info),
          local_trend(std::make_shared<Trend>(i_info->get_ticks() ) ),
          zoe(i_info->get_ticks(), local_trend ) { }
    
    
    trad_order get_order(const tiempo::datetime& date, const signal_type& siq, 
                         const TraderParams& p, const tiempo::duration& trad_freq, 
                         int quantity, 
                         const tiempo::datetime& date0, double price0, 
                         double P_current) const;
  private:
    trad_order get_order_start(const tiempo::datetime& date, const signal_type& siq, 
                               const TraderParams& p, const tiempo::duration& trad_freq, 
                               double P_current) const;

    trad_order get_order_end(const tiempo::datetime& date, const signal_type& siq, 
                             const TraderParams& p, const tiempo::duration& trad_freq, 
                             double quantity, const tiempo::datetime& date0, double price0, 
                             double P_current) const;

};
    



#endif
