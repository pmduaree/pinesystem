#ifndef __trader_profitpreserver__
#define __trader_profitpreserver__

#include "asset.hpp"
#include "trend.hpp"


class ProfitPreserver
{
  public:
    bool operator()(const tiempo::duration& dfreq, 
                    double trend_long, double trend_short, 
                    double profit_threshold, 
                    long quantity, 
                    const tiempo::datetime& date0, double price0, 
                    const tiempo::datetime& date, double price) const;
    
};


#endif
