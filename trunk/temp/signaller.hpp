#ifndef signaller_signaller
#define signaller_signaller

#include "Assets/asset.hpp"
#include "Tiempo/time_series.hpp"

#include "Strategy/strategy.hpp"


#include "signals.hpp"

class Signaller
{
    std::shared_ptr<sql_connection> conn;
    std::shared_ptr<Strategy> strategy;
    std::shared_ptr<Asset> asset;
  public:
    Signaller(std::shared_ptr<sql_connection>, std::shared_ptr<Strategy>, std::shared_ptr<Asset>);

    ~Signaller();
    
    void load_signals();

    
    double operator()(const tiempo::datetime& date);
    
    std::shared_ptr<Asset> get_asset() { return asset; }
    std::shared_ptr<Strategy> get_strategy() { return strategy; }
    
  private:
    void insert_signal(const tiempo::datetime& date, const std::tuple<double,std::string>& x);

    time_series<std::tuple<double,std::string> > _cache;
};



#endif

