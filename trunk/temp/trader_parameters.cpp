#include "trader_parameters.hpp"
#include <cppconn/statement.h>
#include <cppconn/resultset.h>

bool TraderParameters::load(int trp_id, TraderParams& trp)
{
    auto ptr = load(trp_id);
    if (ptr) {
        trp = *ptr;
        return true;
    } else return false;
}

std::shared_ptr<TraderParams> TraderParameters::load(int trp_id)
{
    std::shared_ptr<TraderParams> retval;
    std::string q = (boost::format("SELECT `param` FROM `Main`.`TraderParameters` WHERE `id`=%d")%trp_id).str();
    conn->lock();
    auto con = conn->connection();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
    std::string trp_str = "";
    if (res->rowsCount() > 0) {
        res->first();
        trp_str = res->getString("param");
    }
    conn->unlock();
    if (trp_str.empty()) return retval;
    retval = std::make_shared<TraderParams>(trp_str);
    return retval;
}


int TraderParameters::save(const TraderParams& trp)
{
    int myid = 0;
    std::string S = trp.to_string();

    if (true) {
        std::string q0 = (boost::format("SELECT `id` FROM `Main`.`TraderParameters` WHERE `param`=\'%s\'")%S).str();
        conn->lock();
        auto con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q0));
        if (res->rowsCount() > 0) {
            res->first();
            myid = res->getInt("id");
        }
        conn->unlock();
    }
    if (myid) return myid;
    
    if (true) {
        conn->lock();
        auto con = conn->connection();
        // try to find it.
        std::string q = (boost::format("INSERT INTO `Main`.`TraderParameters` (`param`) VALUES (\'%s\')") % S).str();
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        stmt->execute(q);
        stmt.reset(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery("SELECT LAST_INSERT_ID() AS myid"));
        if (res->rowsCount() > 0) {
            res->first();
            myid = res->getInt("myid");
        }
        conn->unlock();
    }
    return myid;

}

