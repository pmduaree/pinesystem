#include <cmath>
#include "signal_measure.hpp"

double SignalMeasure::operator()(const time_series<signal_type>& signals) const
{
    //std::cout << "WE ARE IN" << std::endl;
    auto date_1 = signals.begin()->first;

    const auto& tix = asset->get_ticks();

    auto it = tix.begin();
    while(it != tix.end() && it->first < date_1) ++it;
    if (it == tix.end() ) throw std::runtime_error("NO DATA IN Measure");

    double total_mean_profit = 0.0;
    for(const auto& z: signals) {
        //std::cout << "Processing date: " << z.first << std::endl;
        const auto& date = z.first;
        auto sig = z.second;
        
        while(it != tix.end() && it->first < date) ++it;
        if (it == tix.end()) {
            std::cout << "End of data reached" << std::endl;
            break;
        }
        
        auto it_now = it;
        double p_open = it->second.p;
        
        tiempo::datetime next_date = date + sigdur;
        double p_avg = 0.0;
        size_t n_avg = 0;
        size_t v_tot = 0;
        while(++it != tix.end() && it->first < next_date) {
            p_avg += it->second.p;
            v_tot += it->second.v;
            ++n_avg;
        }
        it = it_now;
        // no prices:
        if (!n_avg) continue;
        // no volume:
        if (!v_tot) continue;
        
        // average price:
        p_avg *= 1.0 / n_avg;

        // average return
        double p_return = (p_avg - p_open) / fabs(p_open);
        double mean_profit = 0.0;
        //if (sig != signal_type::Wait) std::cout << date.to_string() << " : " << signal_to_string(sig) << std::endl;
        
        // realized profit:
        if (sig == signal_type::Buy)
            mean_profit = p_return;
        else if (sig == signal_type::Sell)
            mean_profit += -p_return;
        //std::cout << "mean profit: " << mean_profit << std::endl;
        // add the mean profit of this period to the cumulative profit
        total_mean_profit += mean_profit;
    }
    double measure = total_mean_profit;
    //std::cout << "OBTAINED MEASURE: " << measure << std::endl;
    return measure;
}


