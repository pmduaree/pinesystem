#include "signaller.hpp"
#include <cppconn/statement.h>
#include <cppconn/resultset.h>

Signaller::Signaller(std::shared_ptr<sql_connection> conn_, 
                     std::shared_ptr<Strategy> strategy_, 
                     std::shared_ptr<Asset> asset_)
    : conn(conn_), strategy(strategy_), asset(asset_)
{
    load_signals();
}

Signaller::~Signaller()
{
}

void Signaller::load_signals()
{
    conn->lock();

    std::shared_ptr<sql::Connection> con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`Signals` WHERE `strategy_id`=%d AND `asset_id`=%d") %
                      strategy->get_id() % asset->get_id() ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    //std::cout << "set count: " << res->rowsCount() << std::endl;
    while(res->next()) {
        double p = res->getDouble("Ppos");
        std::string r = res->getString("Rpar");
        tiempo::datetime date(res->getString("date"));
        //std::cout << "Feeding signal on date " << date << std::endl;
        _cache[date] = std::make_tuple(p, r);
    }
    conn->unlock();
}

void Signaller::insert_signal(const tiempo::datetime& date, const std::tuple<double,std::string>& x)
{
    if (std::get<0>(x) < 0) {
        //std::cout << "This signal is rubbish." << std::endl;
        return;
    }
    conn->lock();
    // check if we have this signal already.
    std::shared_ptr<sql::Connection> con = conn->connection();
    int strategy_id = strategy->get_id();
    int asset_id = asset->get_id();
    auto date_str = date.to_string("%Y-%m-%d %H:%M:%S");
    std::string query0 = (boost::format("SELECT Ppos,Rpar FROM `Main`.`Signals` WHERE `strategy_id`=%d AND `asset_id`=%d AND `date`=\'%s\'")
                            % strategy_id % asset_id % date_str ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query0));
    if (res->rowsCount() == 0) {
        //std::cout << "Inserting signal to the databse...." << std::endl;
        std::string query = (boost::format("INSERT INTO `Main`.`Signals` (`strategy_id`,`asset_id`,`date`,`Ppos`,`Rpar`,`detail_id`) VALUES (%d,%d,\'%s\',%.6f,\'%s\',%d)") %
                          strategy_id % asset_id % date_str % std::get<0>(x) % std::get<1>(x) % 0).str();
        stmt.reset(con->createStatement());
        stmt->execute(query);
    } 
    conn->unlock();
}

double Signaller::operator()(const tiempo::datetime& date)
{
    //std::cout << boost::format("Request for a signal on date %s") % date.to_string() << std::endl;
    auto it = _cache.find(date);
    if (it == _cache.end()) {
        //std::cout << "Calling aclculate_signal..." << std::endl;
        std::tuple<double,std::string> res = strategy->calculate_signal(asset, date);
        _cache[date] = res;
        insert_signal(date, res);
        it = _cache.find(date);
    } 
    const auto& z = it->second;
    return std::get<0>(z);
}



