#include "trader_request.hpp"

trad_order TradeRequest::get_order_start(const tiempo::datetime& date, const signal_type& siq, 
                           const TraderParams& p, const tiempo::duration& trad_freq, 
                           double P_current) const
{
    const tiempo::duration trade_day_safe = std::chrono::minutes(10);
    const tiempo::duration close_time_safe = std::chrono::minutes(30);
    const tiempo::duration short_signal_length = std::chrono::minutes(p.shorttime ? p.shorttime : 4);
    
    // No signal, no position, do nothing.
    if (siq == signal_type::None || siq == signal_type::Wait)
        return trad_order::Wait;

    const auto& exchange = iinfo->get_exchange();
    
    // check the trading times
    if (!exchange.is_market_open(date))
        return trad_order::Wait;
    
    // check the trading day.
    if (!exchange.is_in_trading_day(date)) 
        return trad_order::Wait;
    
    auto time_to_end_day = exchange.time_to_end_trading_day(date);
    auto time_to_close = exchange.time_to_market_close(date);
    
    // don't start trading just before the trading day stops.
    if (time_to_end_day < (trade_day_safe + trad_freq) || 
        time_to_close < (close_time_safe + trad_freq) )
        return trad_order::Wait;
    
    if (siq == signal_type::None) return trad_order::Wait;
    
    ShortTimeSignal short_timer(iinfo, local_trend);
    signal_type st_siq = short_timer(date, short_signal_length);

    assert( siq == signal_type::Buy || siq == signal_type::Sell);

    if (st_siq != signal_type::Wait && st_siq != siq)
        return trad_order::Postpone;

    if (st_siq == siq || st_siq == signal_type::Wait) {
        if (siq == signal_type::Buy) return trad_order::Buy;
        if (siq == signal_type::Sell) return trad_order::Sell;
        std::cerr << "ERROR> SHOULDNT BE HERE\n";
        return trad_order::Wait;
    }
    std::cerr << "Strange, we should not have come here.\n";
    return trad_order::Wait;
}

trad_order TradeRequest::get_order_end(const tiempo::datetime& date, const signal_type& siq, 
                         const TraderParams& p, const tiempo::duration& trad_freq, 
                         double quantity, const tiempo::datetime& date0, double price0, 
                         double P_current)  const
{
    const tiempo::duration trade_day_safe = std::chrono::minutes(10);
    const tiempo::duration close_time_safe = std::chrono::minutes(30);
    const tiempo::duration short_signal_length = std::chrono::minutes(p.shorttime ? p.shorttime : 4);
    const tiempo::duration trend_time_short = std::chrono::minutes(p.trendshort ? p.trendshort : 3);
    const tiempo::duration trend_time_long = std::chrono::minutes(p.trendlong ? p.trendlong : 10);
    double prof_thres = p.profthres ? p.profthres : 0.002;
    
    // check the stop loss immediately.
    if (zoe(p, trad_freq, quantity, date0, price0, date, P_current)) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << boost::format("[%s] Stop loss signal triggered at price %.3f.\n")
                        % date.to_string() % P_current ;
        #endif
        #ifdef QUASITRADER
        std::cout << boost::format("[%s] Stop loss signal triggered at price %.3f.\n")
                        % date.to_string() % P_current ;
        #endif
        */
        return trad_order::Close;
    }
    const auto& exchange = iinfo->get_exchange();
    // check the trading times
    if (!exchange.is_market_open(date)) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << "Holding because the market is not open" << std::endl;
        #endif
        */
        return trad_order::Hold;
    }

    bool soft_close = false;
    
    // check the trading day.
    if (!exchange.is_in_trading_day(date)) 
        return trad_order::Close;

    auto time_to_end_day = exchange.time_to_end_trading_day(date);
    auto time_to_close = exchange.time_to_market_close(date);
    
    // is the signal consistent with the current position?
    if ((quantity>0 && siq==signal_type::Buy) || (quantity<0 && siq==signal_type::Sell) ||
        (siq==signal_type::Wait &&p.HP) ) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << "Holding because the signal is consistent with the current position." << std::endl;
        #endif
        */
        return trad_order::Hold;
    }

    // close the position before the trading day stops.
    if ( time_to_end_day <= trade_day_safe  || time_to_close <= close_time_safe ) {
        soft_close = true;
    }
    
    // if the signal is opposite, then try to close it.
    if ( (quantity>0 && siq==signal_type::Sell) || (quantity<0&&siq==signal_type::Buy) ) {
        soft_close = true;
    }
    
    if (siq==signal_type::Wait && !p.HP) {
        /*
        #ifdef QUASITRADER
        std::cout << boost::format("[%s] soft_close=true because WAIT and not HP.\n") % date.to_string() <<std::endl;
        #endif
        */
        soft_close = true;
    }
    
    // No real signal, just checking. Which means that we must have a position.
    //if (siq == signal_type::None) 
    //    return trad_order::Hold;
    
    if (siq == signal_type::Wait && p.HP)
        return trad_order::Hold;
    
    // how are we with money. Are we losing it?
    // If so, then don't close the position at all.
    double profit = (quantity * (P_current - price0))  / fabs(quantity*price0);
    
    // Calculate the short time signal.
    
    // now check if it's going in the right direction.
    ShortTimeSignal short_timer(iinfo, local_trend);
    signal_type st_siq = short_timer(date, short_signal_length);

    // if it's going in the right direction, just keep it for some time.
    if ((st_siq == signal_type::Buy && quantity>0) || 
        (st_siq == signal_type::Sell&& quantity<0) ) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << "It's going in the right direction on the short time scale. Postponing." << std::endl;
        #endif
        */
        return trad_order::Postpone;
    }
    
    #if 0
    if ((st_siq==signal_type::Buy && quantity<0) ||
        (st_siq==signal_type::Sell&& quantity>0)) {
        #ifdef QUASITRADER
        std::cout << boost::format("[%s] soft_close=true because short time trend\n") % date.to_string() <<std::endl;
        #endif
        soft_close = true;
    }
    #endif
    
    if (profit > 0.0007 && soft_close) {
        /*
        #ifdef QUASITRADER
        std::cout << boost::format("[%s] Closing beause soft close and profit. \n") % date.to_string() <<std::endl;
        #endif
        */
        return trad_order::Close;
    }

#if 0
    if (profit > 0.01/100.0  && profit < 0.03 / 100.0 && 
        ( (st_siq == signal_type::Buy && quantity < 0) || (st_siq == signal_type::Sell && quantity>0) ) ) {
        return trad_order::CloseAndReset;
    }
#endif
    
    tiempo::duration position_age = date - date0;
    
    if (position_age.seconds() < 0.2 * trad_freq.seconds() && 
        ( !soft_close || profit < 0) ) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << boost::format("[%s] Postponing because it's too early.") % date.to_string() <<std::endl;
        #endif
        */
        return trad_order::Postpone;
    }

    double trend_short=0;
    (*local_trend)(date, trend_time_short, trend_short);
    double q_short = trend_short * quantity;
    
    double trend_long=0;
    (*local_trend)(date, trend_time_long, trend_long);
    double q_long = trend_long * quantity;
    
    ProfitPreserver profiter;
    if (profiter(trad_freq, trend_long, trend_short, prof_thres,
                 quantity, date0, price0, date, P_current)) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << boost::format("[%s] Profit Preserver says its a good time to close the position at %.3f.\n")
                        % date.to_string()  % P_current;
        #endif
        #ifdef QUASITRADER
        std::cout << boost::format("[%s] Profit Preserver says its a good time to close the position at %.3f.\n")
                        % date.to_string()  % P_current;
        #endif
        */
        return trad_order::Close;
    }
    
    
    if (q_short > 0 && q_long > 0) {
        /*
        #ifdef AUTOEXECUTION
        std::cout << "Postponing because it's going in the right direction." << std::endl;
        #endif
        */
        return trad_order::Postpone;
    }
#if 0
    if (q_short < 0) {
        std::cout << "CLOSING AND RESETTING" << std::endl;
        // going down. Just close it right now and reset the signal.
        return trad_order::CloseAndReset;
    }
#endif
    if (soft_close && q_short < 0)
        return trad_order::CloseAndReset;
/*
    if (soft_close && profit < 0 && position_age < 2 * trad_freq) {
        #ifdef AUTOEXECUTION
        std::cout << "Postponing because we are losing money and we better wait." << std::endl;
        #endif
        return trad_order::Postpone;
    }
*/
    
    if (soft_close && profit < 0) {
        if (position_age > 3 * trad_freq && q_long >0 && q_short < 0)
            return trad_order::Close;
        if (position_age > 4 * trad_freq && q_short < 0)
            return trad_order::Close;
    }
    
    if (!soft_close) return trad_order::Postpone;

    if (profit < 0 && position_age < trad_freq)
        return trad_order::Postpone;

#if 0
    if (profit > 0 && q_long > 0 && q_short < 0) {
        return trad_order::CloseAndReset;
    }
    if (profit > 0 && q_long < 0 && q_short < 0) {
        return trad_order::CloseAndReset;
    }
#endif

#if 0
    std::cout << boost::format("[%s] It's weird how we are still here: profit=%.2e, q_short:%.2e,q_long:%.2e.") 
              % date.to_string()  % profit % q_short % q_long << std::endl;
#endif
    return trad_order::Postpone;
}

trad_order TradeRequest::get_order(const tiempo::datetime& date, const signal_type& siq, 
                     const TraderParams& p, const tiempo::duration& trad_freq, 
                     int quantity, const tiempo::datetime& date0, double price0, double P_current) const
{
    if (!quantity)
        return get_order_start(date, siq, p, trad_freq, P_current);
    else
        return get_order_end(date, siq, p, trad_freq, quantity, date0, price0, P_current);

}


