#ifndef __historic_trader_trader__
#define __historic_trader_trader__

#include "tiempo.hpp"
#include "signal_interface.hpp"
#include "trader_position.hpp"
#include "trader_request.hpp"

class TradeStatus
{
    double money;
    TradePosition trp;
    
  public:
    TradeStatus(double money_=0.0) 
        : money(money_)
    {
    }
    
    TradeStatus(double money_, long items_, double price_, 
                const tiempo::datetime& date_)
        : money(money_), trp(items_, price_, date_)
    {
    }
    
    TradeStatus(const TradeStatus& p) 
        : money(p.money),
          trp(p.trp)
    {
    }
    
    void open_position(const tiempo::datetime& date, long xitems, double price)
    {
        trp.quantity = xitems;
        trp.price = price;
        trp.date = date;
        money -= xitems * fabs(price);
    }
    
    void close_position(const tiempo::datetime& date, double price)
    {
        if (trp.quantity==0.0) return;
        double cur_value = value(price);
        money += cur_value;
        trp.quantity = 0;
        trp.price = 0;
        trp.date = date;
    }
    
    const tiempo::datetime& date0() const { return trp.date; }
    
    double price0() const { return trp.price; }
    
    int sign() const { return (trp.quantity==0 ? 0 : (trp.quantity > 0 ? 1 : -1) ); }
    
    long quantity() const { return trp.quantity; }
    double investment() const  { return trp.quantity * fabs(trp.price); }
    double value(double current_price) const { return investment() + profit(current_price); }
    double profit(double current_price) const { return trp.quantity * (current_price - trp.price); }
    
    double current_value(double current_price) const { return value(current_price) + money; }
    double cash() const { return money; }
    double saldo() const { return money; }
    
    void clear()
    {
        trp.quantity = 0;
        trp.price = 0;
    }
    
    operator bool() const
    {
        return (trp.quantity != 0);
    }
};

//class TradeStatus;

struct trade_slice
{
    tiempo::datetime date;
    int signal;
    signal_type siq;
    TradeStatus tradestatus;
    trad_order order;
    double fees;
    double money;
    char action;
    double eff_money;
    double roi;
};


class HistoricTrader
{
    std::shared_ptr<Asset> iinfo;
    TradeRequest tradereq;
    
  public:
    HistoricTrader(std::shared_ptr<Asset> in_info)
        : iinfo(in_info), tradereq(in_info)
    { 
    }
    
    void operator()(std::shared_ptr<SignalInterface> sigint, 
                    const TraderParams& trp, const tiempo::datetime& date_from,
                    const tiempo::datetime& date_to, const tiempo::duration& step_freq, 
                    double money0, std::vector<trade_slice>& RV);
    
  private:
    void operator()(std::shared_ptr<SignalInterface> sigint, 
                    const TraderParams& trp, const tiempo::datetime& date_from,
                    const tiempo::datetime& date_to, const tiempo::duration& step_freq, 
                    TradeStatus& status, std::vector<trade_slice>& RV);
};

std::tuple<double,double,double> calculate_alpha(const std::vector<trade_slice>& RV);

#endif
