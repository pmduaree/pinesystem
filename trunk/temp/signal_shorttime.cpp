#include "signal_shorttime.hpp"

ShortTimeSignal::ShortTimeSignal(std::shared_ptr<VAsset> i_info, std::shared_ptr<Trend> trend_)
    : iinfo(i_info), local_trend(trend_)
{
    if (!local_trend) local_trend = std::make_shared<Trend>(i_info->get_ticks() );
}
    
signal_type ShortTimeSignal::operator()(const tiempo::datetime& date, const tiempo::duration& lookback)
{
    double threshold = 0.01  / 100 ;    // 0.01 % per hour
    double trend;
    if (!(*local_trend)(date, lookback, trend))
        return signal_type::Wait;
    if (trend > threshold) return signal_type::Buy;
    else if (trend < -threshold) return signal_type::Sell;
    else return signal_type::Wait;
}

