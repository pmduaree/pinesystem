#ifndef __trader_action__
#define __trader_action__


#include "tiempo.hpp"
#include "signals.hpp"
#include "trader_common.hpp"

struct TradeAction
{
    tiempo::datetime date;
    signal_type siq;
    trad_order order;
    double order_value;
    //
    TradeAction() 
        : siq(signal_type::None),
          order(trad_order::Wait),
          order_value(0.0) {}
};


#endif
