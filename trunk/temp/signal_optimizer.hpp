#ifndef signal_optimizer_hpp
#define signal_optimizer_hpp


#include <iostream>
#include <list>
#include "asset.hpp"
#include "strategy.hpp"
#include "strategy_engines.hpp"
#include "strategy_portfolio.hpp"
#include "signal_measure.hpp"

class SignalOptimizer
{
    std::shared_ptr<sql_connection> conn;
    std::shared_ptr<Asset> asset;
    int freq, offset;
    tiempo::datetime_type date_1,date_2;
    tiempo::duration dfreq;
    
  public:
    SignalOptimizer(std::shared_ptr<sql_connection> conn_,
                    std::shared_ptr<Asset> asset_, int freq_, int offset_,
                    const tiempo::datetime_type& date1_, const tiempo::datetime_type& date2_);
    
    size_t size() const { return strategies.size(); }
    bool make_signals(const std::vector<double>& Wj, time_series<signal_type>& signals);
    double operator()(const std::vector<double>& Wj);
    void save(int optparam_id, const std::vector<double>& Wj);
  
  private:
    void check_strategies();
    
    void load_strategies();
    
    std::shared_ptr<StrategyPortfolio> get_portfolio(const std::vector<double>& Wj);
    
    SignalMeasure sig_measure;//(asset, dfreq);
    std::vector<tiempo::datetime> trading_dates;
    std::vector<std::shared_ptr<Strategy> > strategies;
};


#endif

