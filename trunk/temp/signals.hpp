#ifndef signals_hpp
#define signals_hpp

#include <memory>
#include <string>
#include <vector>
#include <list>

#include "Tiempo/tiempo.hpp"

enum class signal_type
{
    None,
    Wait,
    Buy,
    Sell
};
std::string signal_to_string(signal_type sig);
signal_type signal_from_buy_probability(double p);


#endif
