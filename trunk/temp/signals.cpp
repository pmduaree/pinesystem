#include <string>
#include "signals.hpp"

std::string signal_to_string(signal_type sig)
{
    if (sig == signal_type::None)
        return "None";
    else if (sig == signal_type::Wait)
        return "Wait";
    else if (sig == signal_type::Buy)
        return "Buy";
    else if (sig == signal_type::Sell)
        return "Sell";
    else
        return "N/A";
}

signal_type signal_from_buy_probability(double p)
{
    const double of = 0.05;
    if (p > 0.5 + of) return signal_type::Buy;
    else if (p < 0.5 - of) return signal_type::Sell;
    else if (p >= 0.5 - of && p <= 0.5 + of) return signal_type::Wait;
    return signal_type::None;
}


