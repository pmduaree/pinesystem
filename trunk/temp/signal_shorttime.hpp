#ifndef __signal_shorttime_
#define __signal_shorttime_

#include "vasset.hpp"
#include "trend.hpp"
#include "signals.hpp"

class ShortTimeSignal
{
    std::shared_ptr<VAsset> iinfo;
    std::shared_ptr<Trend> local_trend;
  public :
    ShortTimeSignal(std::shared_ptr<VAsset> i_info, std::shared_ptr<Trend> trend_=nullptr);
    signal_type operator()(const tiempo::datetime& date, const tiempo::duration& lookback);
};


#endif
