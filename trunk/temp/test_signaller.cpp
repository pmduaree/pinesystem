#include "signaller.hpp"
#include "strategy_portfolio.hpp"
#include "signals.hpp"

int main()
{
    auto conn = std::make_shared<sql_connection>();
    auto asset = std::make_shared<Asset>(conn, "QON4");
    asset->init(std::chrono::hours(24 * 30));
    std::shared_ptr<StrategyPortfolio> engine;
    int freq = 0;
    int offset = 0;
    if (false) {
        freq = 15;
        offset = 0;
        auto strategy1 = std::make_shared<Strategy>(conn, "HMM", "hmmtype=sg,rndtype=gmm,M=3,N=3,T=20", 60);
        auto strategy2 = std::make_shared<Strategy>(conn, "HMM", "hmmtype=sg,rndtype=gmm,M=3,N=3,T=40", 10);
        engine = std::make_shared<StrategyPortfolio>(conn, asset, freq, offset);
        engine->add_signaller(0.5, std::make_shared<Signaller>(conn, strategy1, asset) );
        engine->add_signaller(0.5, std::make_shared<Signaller>(conn, strategy2, asset) );
        engine->save();
    } else {
        engine = std::make_shared<StrategyPortfolio>(conn, asset, 2);
        freq = engine->get_freq();
        offset = engine->get_offset();
    }
    
    auto now = tiempo::now();
    auto on_date = offset_date(now, freq, offset);
    std::cout << "time now: " << now << std::endl;
    auto sig = engine->operator()(on_date);
    std::cout << "Get signal: " << signal_to_string(std::get<0>(sig) ) << std::endl;
    
    return 0;
}
