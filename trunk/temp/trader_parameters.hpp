#include "stringparpars.hpp"
#include "sql_connection.hpp"
#include "trader_params.hpp"

class TraderParameters
{
    std::shared_ptr<sql_connection> conn;
  public:
    TraderParameters(std::shared_ptr<sql_connection> conn_)
        : conn(conn_) {}

    bool load(int trp_id, TraderParams& trp);
    std::shared_ptr<TraderParams> load(int trp_id);
    int save(const TraderParams& trp);
};

