#ifndef __trader_position__
#define __trader_position__


#include "tiempo.hpp"

struct TradePosition
{
    int quantity;
    double price;
    tiempo::datetime date;

    TradePosition()
        : quantity(0), price(0.0) {}

    TradePosition(int q, double p, const tiempo::datetime& d)
        : quantity(q), price(p), date(d) {}

    TradePosition(const TradePosition& x)
        : quantity(x.quantity), price(x.price), date(x.date) {}

};


#endif
