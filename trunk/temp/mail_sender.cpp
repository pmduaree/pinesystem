#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <string>
#include <cstdio>
#include <unistd.h>

#include "mail_sender.hpp"

void MailSender::send_message(const std::string& f_to, const std::string& f_subj, const std::string& f_body)
{
    char temporary_file[] = "/tmp/mailfile.XXXXXX";
    int fd = mkstemp(temporary_file);
    //std::cout << "Generated the file: " << temporary_file << std::endl;
    
    //FILE* pFile = fdopen(fd, "w+");
    //std::cout << "Temporary name: " << temporary_name << std::endl;
    
    std::stringstream s;
    s << "To: " << f_to << "<"<<f_to<<">\n";
    s << "From: Oleum Trading <quantgroup@oleum.com>\n";
    s << "Subject: " << f_subj << "\n";
    s << "MIME-Version: 1.0\n";
    s << "Content-Type: text/plain\n";
    s << "\n";
    s << f_body;
    s << "\n";
    std::string raw_message = s.str();
    //std::cout << "Raw messsage:\n" << raw_message << std::endl;
    
    write(fd, raw_message.c_str(), raw_message.size() );

    close(fd);

    std::string command = "cat " + std::string(temporary_file) + " | sendmail -i -t";
    //std::cout << "Command to run: " << command << std::endl;
    system(command.c_str());
    remove(temporary_file);
}

