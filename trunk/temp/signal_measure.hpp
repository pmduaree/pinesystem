#ifndef signal_measure_hpp
#define signal_measure_hpp

#include "tiempo.hpp"
#include "ticks.hpp"
#include "vasset.hpp"
#include "signals.hpp"

class SignalMeasure
{
    std::shared_ptr<VAsset> asset;
    tiempo::duration sigdur;

  public:
    SignalMeasure(std::shared_ptr<VAsset> asset_, const tiempo::duration& sigdur_)
        : asset(asset_), sigdur(sigdur_)
    {
    }

    double operator()(const time_series<signal_type>& signals) const;
};
    
    
#endif
