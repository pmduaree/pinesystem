#ifndef __AutoExecCQG__CQGTraderAPI__
#define __AutoExecCQG__CQGTraderAPI__
//
//

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <memory>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/statement.h>
#include <memory>


struct TradeDetail
{
    int quantity;
    double price_open, price_close;
    std::string date_open, date_close;
};


enum class OrderStatus
{
    None,
    Open,
    Processing,
    Close
};


class CQGTraderAPI
{
    sql::Driver *driver;
    std::string hostname, username, password;
    std::shared_ptr<sql::Connection> con;
  public:
    CQGTraderAPI();
    ~CQGTraderAPI();
    void Init();
    void Destroy();
    unsigned int GetTradingCode(unsigned int);
    unsigned int EnterTrade(unsigned int assetid, int quantity, const std::string& TS, double price);
    int ExitTrade(unsigned int, const std::string&, double );
    OrderStatus TradeStatus(unsigned int);
    bool TradeDetails(unsigned int, TradeDetail& detail);
};

//////////////////////////////////////////////////////////////////


#endif /* defined(__AutoExecCQG__CQGTraderAPI__) */

