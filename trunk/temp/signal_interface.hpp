#ifndef __signal_interface__
#define __signal_interface__

#include <tuple>
#include "Tiempo/tiempo.hpp"
#include "signals.hpp"

class SignalInterface
{
  public:
    SignalInterface() {}
    virtual ~SignalInterface() {}
    
    virtual std::tuple<signal_type,tiempo::datetime> operator()(const tiempo::datetime& d) const = 0;
};


#endif
