#include "strategy_portfolio.hpp"


StrategyPortfolio::StrategyPortfolio(std::shared_ptr<sql_connection> conn_,
                  std::shared_ptr<Asset> asset_,
                  int freq_, int offset_)
    : SignalInterface(), 
      strategyportfolio_id(0), conn(conn_), asset(asset_), freq(freq_), offset(offset_)
{
}

StrategyPortfolio::StrategyPortfolio(std::shared_ptr<sql_connection> conn_, 
                                     std::shared_ptr<Asset> asset_, 
                                     int p_id)
    : SignalInterface(), 
      strategyportfolio_id(0), conn(conn_), asset(asset_)
{
    init(p_id);
}

bool StrategyPortfolio::init(int p_id)
{
    conn->lock();
    auto con = conn->connection();
    // initialize everything from the database.
    std::string query = (boost::format(
        "SELECT SP.id AS portfolio_id, SP.freq AS freq, SP.offset AS offset,  \
                SPP.strategy_id AS strategy_id, SPP.weight AS weight \
            FROM `Main`.`StrategyPortfolios` AS SP \
            JOIN `Main`.`StrategyPortfolioParts` AS SPP ON SP.id=SPP.strategyportfolio_id \
            WHERE SP.id = %d") % p_id ).str();
    std::shared_ptr<sql::Statement> stmt(con->createStatement() );
    std::shared_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    std::vector<std::tuple<int,double> > Z;
    if (res->rowsCount() > 0) {
        res->first();
        // do some common stuff.
        freq = res->getInt("freq");
        offset = res->getInt("offset");
        do {
            Z.push_back( std::make_tuple(res->getInt("strategy_id"), res->getDouble("weight") ) );
        } while(res->next() );
    } else {
        std::cerr << "WARNING: STRATEGYPORTFOLIO NOT FOUND." << std::endl;
    }
    conn->unlock();
    for(const auto& z : Z) {
        int strategy_id = std::get<0>(z);
        auto w = std::get<1>(z);
        auto strategy = std::make_shared<Strategy>(conn, strategy_id);
        auto sig = std::make_shared<Signaller>(conn, strategy, asset);
        add_signaller(w, sig);
    }
    strategyportfolio_id = p_id;
    return true;

}
    
StrategyPortfolio::~StrategyPortfolio()
{
}






int StrategyPortfolio::save()
{
    if (strategyportfolio_id) {
        std::cout << boost::format("This portfolio (id=%d) exists already. Not saving")%
            strategyportfolio_id << std::endl;
        return strategyportfolio_id;
    }
    // first make a new entry in the database.
    conn->lock();
    auto note_str = get_info();
    std::string query_1 = (boost::format(
        "INSERT INTO `Main`.`StrategyPortfolios` (`freq`,`offset`,`note`) VALUES (%d,%d,\'%s\')")
         % freq % offset % note_str).str();
    auto con = conn->connection();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    stmt->execute(query_1);

    std::string query_2 = "SELECT LAST_INSERT_ID() AS portfolio_id";
    stmt.reset(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query_2));
    res->first();
    strategyportfolio_id = res->getInt("portfolio_id");
    //std::cout << "strategy portfolio id: " << strategyportfolio_id << std::endl;
    for(const auto& z : _sig) {
        auto w = z.first;
        auto sig = z.second;
        auto strategy = sig->get_strategy();
        std::string query = (boost::format(
            "INSERT INTO `Main`.`StrategyPortfolioParts` (`strategyportfolio_id`,`strategy_id`,`weight`) VALUES (%d,%d,%.5f)"
            ) % strategyportfolio_id % strategy->get_id() % w ).str();
        stmt.reset(con->createStatement());
        stmt->execute(query);
    }
    conn->unlock();
    return strategyportfolio_id;
}

void StrategyPortfolio::add_signaller(double w, std::shared_ptr<Signaller> mod)
{
    strategyportfolio_id = 0;   // reset the id because it's a different one.
    _sig.emplace_back(std::make_pair(w, mod) );
}

tiempo::datetime StrategyPortfolio::get_regularized_date(const tiempo::datetime& date) const
{
    return offset_date(date, freq, offset);
}

std::tuple<double,signal_type> StrategyPortfolio::calculate_signal(const tiempo::datetime& on_date) const
{
    double p = 0.0;
    double r = 0.0;
    size_t nsuc = 0;
    for(auto& S : _sig) {
        double rx = (*S.second)(on_date);
        double w = S.first;
        if (rx < 0) continue;
        r += w * rx;
        p += w;
        ++nsuc;
    }
    double prob_buy = -1.0;
    signal_type sig = signal_type::None;
    //std::cout << "Signal calculated: " << on_date << ", suc:" << nsuc << std::endl;
    if (nsuc) {
        prob_buy = r / p;
        sig = signal_from_buy_probability(prob_buy);
    }
    store_signal_to_db(on_date, prob_buy, sig);
    return std::make_tuple(prob_buy, sig);
}

std::tuple<signal_type,tiempo::datetime>
StrategyPortfolio::operator()(const tiempo::datetime& date) const
{
    auto on_date = offset_date(date, freq, offset);
    if (on_date > date) {
        std::cout << "OFFSET DATE WRONG" << std::endl;
        throw std::runtime_error("QWR");
    }
    //auto on_date = date;
    //assert( offset_date(date, freq, offset) == on_date );
    auto my_signal = get_signal_from_db(on_date);
    signal_type sig = signal_type::None;
    if (std::get<0>(my_signal)) {
        sig = std::get<2>(my_signal);
        //std::cout << "Fetch successful: " << signal_to_string(sig) << std::endl;
    } else {
        double prob_buy;
        std::tie(prob_buy,sig) = calculate_signal(on_date);
    }
    return std::make_tuple(sig, on_date);
}

void StrategyPortfolio::calculate_signals(const std::vector<tiempo::datetime>& dates, time_series<signal_type>&S ) const
{
    std::vector<std::thread> threads;
    for (const auto& date : dates) {
        auto on_date = offset_date(date, freq, offset);
        auto& s = S[on_date];
        auto my_signal = get_signal_from_db(on_date);
        if (std::get<0>(my_signal)) {
            s = std::get<2>(my_signal);
        } else {
            auto z = calculate_signal(on_date);
            s = std::get<1>(z);
        }
    }
}

std::tuple<bool,double,signal_type> StrategyPortfolio::get_signal_from_db(const tiempo::datetime& date) const
{
    _mux.lock();
    auto it = _signals.find(date);
    const bool has_signal = (it != _signals.end());
    _mux.unlock();
    if (has_signal) {
        return std::make_tuple(true, std::get<0>(it->second), std::get<1>(it->second) );
    }
    
    if (!strategyportfolio_id) 
        return std::make_tuple(false, 0.0, signal_type::None);
    conn->lock();
    //std::cout << "Fetching stuff from the SQL for date " << date.to_string() << " ..." << std::endl;
    auto con = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::string q = (boost::format("SELECT prob, sig FROM `Main`.`StrategyPortfolioSignals` WHERE `asset_id`=%d AND `strategyportfolio_id`=%d AND `date`=\'%s\'") %
                                  asset->get_id() % strategyportfolio_id % date.to_string("%Y-%m-%d %H:%M:%S") ).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
    double p=0;
    signal_type sig = signal_type::None;
    bool found = false;
    if (res->rowsCount() > 0) {
        found = true;
        res->first();
        p = res->getDouble("prob");
        int s = res->getInt("sig");
        sig = static_cast<signal_type>(s + static_cast<int>(signal_type::None) );
        // put it to the DB.
        _mux.lock();
        //std::cout << "Storing the signal " << date << std::endl;
        _signals[date] = std::make_tuple(p, sig);
        _mux.unlock();
    } else {
        //std::cout << "Didnt find it." << std::endl;
    }
    conn->unlock();
    return std::make_tuple(found, p, sig);
}

void StrategyPortfolio::store_signal_to_db(const tiempo::datetime& date, double p, signal_type sig) const
{
    _mux.lock();
    //std::cout << "Storing stuff to the internal DB " << date << std::endl;
    _signals[date] = std::make_tuple(p, sig);
    _mux.unlock();
    if (!strategyportfolio_id) return;
    if (p < 0) return;
    int isig = static_cast<int>(sig) - static_cast<int>(signal_type::None);
    auto date_str = date.to_string("%Y-%m-%d %H:%M:%S");
    auto date_str2 = date.to_string("%Y-%m-%d %H:%M:%S");
    if (date_str != date_str2) {
        throw std::runtime_error("DATE MISMATCH IN store_signal_to_db");
    }
    //std::cout << "Compare: " << date_str << " to " << date.to_string() << " to " << date_str2 << std::endl;
    auto asset_id = asset->get_id();
    conn->lock();
    auto con = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    auto q1 = (boost::format("SELECT `id` FROM `Main`.`StrategyPortfolioSignals` WHERE `asset_id`=%d AND `strategyportfolio_id`=%d AND `date`=\'%s\'")
                            % asset_id % strategyportfolio_id % date_str).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q1));
    if (res->rowsCount() == 0) {
        stmt.reset(con->createStatement());
        auto date_x = date;
        auto date_str_1 = date.to_string("%Y-%m-%d %H:%M:%S");
        if (date_str_1 != date_str) {
            std::cout << "INVALID DATE: " << date_str_1 << " vs " << date_str << std::endl;
            std::cout << "how about this: " << (date == date_x) << std::endl;
            throw std::runtime_error("qwe");
        }
        //std::cout << "Inserting signal for date " << date.to_string() << " : " << date_str << std::endl;
        std::string q = (boost::format("INSERT INTO `Main`.`StrategyPortfolioSignals` (`asset_id`,`strategyportfolio_id`,`date`,`prob`,`sig`) VALUES (%d,%d,\'%s\',%.8f,%d)") %
                                            asset_id % strategyportfolio_id % date_str % p % isig ).str();
        stmt->execute(q);
    }
    conn->unlock();
}

std::string StrategyPortfolio::get_info() const
{
    std::string strat_info = "";
    for(const auto& S : _sig) {
        auto z = (boost::format("{%.4f:%s}") % S.first % S.second->get_strategy()->get_info()).str();
        if (!strat_info.empty() ) strat_info += ",";
        strat_info += z;
    }
    std::string s = (boost::format("f=%d,offset=%d; STRAT={%s}") % freq % offset % strat_info ).str();
    return s;
}


