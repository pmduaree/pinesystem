#include "trader_params.hpp"

void TraderParams::from_string(const std::string& X)
{
    StringParamParser strp(X);
    HP = strp.get<int>("HP", 0);
    shorttime = strp.get<int>("shorttime", 0);
    trendshort = strp.get<int>("trendshort", 0);
    trendlong = strp.get<int>("trendlong", 0);
    profthres = strp.get<double>("profthres", 0.0);
    maxloss = strp.get<double>("maxloss", 0.0);
    proftgt = strp.get<double>("proftgt", 0.0);
    closehold = strp.get<int>("closehold", 0);
}

std::string TraderParams::to_string() const
{
    StringParamParser strp;
    strp.put("HP", HP);
    strp.put("shorttime",shorttime);
    strp.put("trendshort",trendshort);
    strp.put("trendlong",trendlong);
    strp.put("profthres",profthres);
    strp.put("maxloss",maxloss);
    strp.put("proftgt",proftgt);
    strp.put("closehold",closehold);
    std::string x = strp.to_string();
    return x;
}

bool operator==(const TraderParams& p1, const TraderParams& p2)
{
    return ( p1.HP == p2.HP && 
             p1.shorttime == p2.shorttime &&
             p1.trendshort == p2.trendshort &&
             p1.trendlong == p2.trendlong &&
             p1.profthres == p2.profthres &&
             p1.maxloss == p2.maxloss &&
             p1.proftgt == p2.proftgt &&
             p1.closehold == p2.closehold );
}

bool operator!=(const TraderParams& p1, const TraderParams& p2)
{
    return !operator==(p1,p2);
}

