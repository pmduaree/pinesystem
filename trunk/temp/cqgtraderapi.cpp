#include "cqgtraderapi.hpp"
#include <boost/format.hpp>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>

CQGTraderAPI::CQGTraderAPI() : driver(nullptr)
{
    Init(); 
}

CQGTraderAPI::~CQGTraderAPI()
{
    Destroy();
}

void CQGTraderAPI::Init()
{
    username = "cqgtrader";
    password = "ishallnevertellyouits666";
    hostname = "localhost";
    driver = get_driver_instance();
    
    try {
        con.reset(driver->connect(hostname, username, password));
        con->setSchema("Trading");
    } catch(sql::SQLException& e) {
        std::cerr << "Couldnt connect ot the database in CQGTraderAPI::Init()" << std::endl;
        std::cerr << "# ERR: " << e.what();
        std::cerr << " (MySQL error code: " << e.getErrorCode();
        std::cerr << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
}

void CQGTraderAPI::Destroy()
{
    if (con) {
        if (con->isClosed()) con->close();
        con.reset();
    }
}

unsigned int CQGTraderAPI::EnterTrade(unsigned int ID_Asset, int quantity,
                                      const std::string& TS_Pushed, double Price)
{
    unsigned int retval = 0;
    try{
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::string query = ( boost::format("CALL EnterTrade(%d,%d,\'%s\',%.4f,@OrdIX)")
                              % ID_Asset % quantity % TS_Pushed % Price ).str();
        std::cout << "Sending query: " << query << std::endl;
        stmt->execute(query);


        stmt.reset(con->createStatement());
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery("SELECT @OrdIX AS _Ticket"));
        if (res->rowsCount() > 0) {
            res->first();
            retval = res->getInt("_Ticket");
        }
    } catch (const sql::SQLException &e) {
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
        retval = 0;
    }
    return retval;
}


/////////////////////////////////////////////////////////////////////
int CQGTraderAPI::ExitTrade(unsigned int Ticket, const std::string& TS_Pull, double Price)
{
    try{
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::string query = (boost::format("CALL ExitTrade(%d,\'%s\',%.4f)")
                              % Ticket % TS_Pull % Price ).str();
        stmt->execute(query);
    } catch (const sql::SQLException &e) {
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
    return 0;
}
/////////////////////////////////////////////////////////////////////
OrderStatus CQGTraderAPI::TradeStatus(unsigned int Ticket)
{
    OrderStatus my_status = OrderStatus::None;
    try {
        std::shared_ptr<sql::Statement> stmt(con->createStatement());
        std::string query = (boost::format("SELECT `order_pending`,`order_processing`,`order_status` FROM `Trading`.`Orders` WHERE `id`=%d")
                                % Ticket).str();
        std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
        if (res->rowsCount() < 1) return OrderStatus::None;
        res->first();
        int tpending = res->getInt("order_pending");
        int tprocessing = res->getInt("order_processing");
        int tstatus = res->getInt("order_status");
        if (tpending != 0  || tprocessing != 0) return OrderStatus::Processing;

        if (tstatus == 4) my_status = OrderStatus::Close;
        else if (tstatus == 2) my_status = OrderStatus::Open;
        else if (tstatus == 1 || tstatus == 3) my_status = OrderStatus::Processing;
        else if (tstatus == 0) my_status = OrderStatus::None;
        else my_status = OrderStatus::None;
    } catch (const sql::SQLException &e) {
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
        my_status = OrderStatus::None;
    }
    return my_status;
}

bool CQGTraderAPI::TradeDetails(unsigned int Ticket, TradeDetail& detail)
{
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::string query = (boost::format("SELECT * FROM `Trading`.`Orders` WHERE `id`=%d")
                            % Ticket).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() < 1) {
        return false;
    }
    res->first();

    detail.quantity = res->getInt("quantity");
    detail.date_open = res->getString("TS_open");
    detail.price_open = res->getDouble("price_open");
    detail.date_close = res->getString("TS_close");
    detail.price_close = res->getDouble("price_close");
    return true;
}

unsigned int CQGTraderAPI::GetTradingCode(unsigned int tix_id)
{
    return tix_id;
}


