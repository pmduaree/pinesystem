#include <string>
#include "trader_common.hpp"

std::string order_to_string(trad_order order)
{
    std::string action = "N";
    if (order == trad_order::Buy) action ="LONG";
    if (order == trad_order::Sell) action ="SHORT";
    if (order == trad_order::Close) action ="CLOSE";
    if (order == trad_order::Wait) action ="WAIT";
    if (order == trad_order::Hold) action = "HOLD";
    if (order == trad_order::Postpone) action = "POSTPONE";
    if (order == trad_order::CloseAndReset) action = "CRESET";
    return action;
}

char get_action(trad_order order)
{
    char action = 'N';
    if (order == trad_order::Buy) action ='L';
    if (order == trad_order::Sell) action ='S';
    if (order == trad_order::Close) action ='C';
    if (order == trad_order::Wait) action ='N';
    if (order == trad_order::Hold) action ='H';
    if (order == trad_order::Postpone) action ='P';
    if (order == trad_order::CloseAndReset) action ='R';
    return action;
}

