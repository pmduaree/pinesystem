#ifndef __trader_common__
#define __trader_common__

#include <string>

enum class trad_order
{
    Wait,
    Hold,
    Sell,
    Buy,
    Close,
    Postpone,
    CloseAndReset
};

std::string order_to_string(trad_order order);
char get_action(trad_order order);


#endif
