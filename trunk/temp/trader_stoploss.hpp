#ifndef __trader_stoploss__
#define __trader_stoploss__

#include <iostream>
#include "tiempo.hpp"
#include "ticks.hpp"
#include "trader_params.hpp"

#include "statvar.hpp"
#include "trend.hpp"

class StopLossModule
{
    const tix_series& tix;
    std::shared_ptr<Trend> localtrend;
  public:
    StopLossModule(const tix_series& tix_, std::shared_ptr<Trend> trend_)
        : tix(tix_), localtrend(trend_)
    {}
    
    bool operator()(const TraderParams& p, const tiempo::duration& trad_freq, 
                double quantity, 
                const tiempo::datetime& date0, double price0, 
                const tiempo::datetime& date_, double price_) const;
};

#endif

