#include <cmath>
#include "tiempo.hpp"
#include "trader_profitpreserver.hpp"

bool ProfitPreserver::operator()(const tiempo::duration& dfreq, 
                                 double trend_long, double trend_short, 
                                 double profit_threshold, 
                                 long quantity, 
                                 const tiempo::datetime& date0, double price0, 
                                 const tiempo::datetime& date, double price) const
{
    std::string message = "";
    // no quantity, no profit
    if (!quantity) {
        message = "no quantity";
        return false;
    }
    tiempo::duration time_passed = date - date0;
    if (time_passed.seconds() < 0.15 * dfreq.seconds() || 
        time_passed < std::chrono::minutes(3) ) {
            message = "too early";
            return false;
        }
    // no profit, no close. We have stop loss for that.
    double profit = quantity * ( price - price0) / fabs( price0 * quantity );
    bool in_money = profit > profit_threshold;
    
    double r_short = trend_short * quantity;
    double r_long = trend_long * quantity;
    
    // if both trends go up and they do it even more, do nothing.
    if (r_long > 0 && r_short > r_long) {
        if (in_money) {
            message = (boost::format("[%s] Making more and more...") % date.to_string() ).str();
        }
        return false;
    }
    if (!in_money) return false;
    assert( in_money );
    
    // if we have started to lose money, hten just stop it for god sake.
    if (r_long <= 0 && r_short < 0) {
        message = (boost::format("[%s] In the money but we are started losing [C]...") % date.to_string()).str();
        return true;
    }
    
    // turning point, we just started losing money
    if (r_long > 0 && r_short < -2.0 * r_long) {
        message = (boost::format("[%s] In the money but turning point [C]...")%date.to_string() ).str();
        return true;
    }
    
    // it really goes down fast, just close it!
    //if (profit < 0 && r_long > 0 && r_short < -3.0 * r_long) return true;
    
    // started making money.
    if (r_short > 0 && r_short > 2.0 * fabs(r_long) ) {
        message = (boost::format("[%s] Started making probably?") % date.to_string() ).str();
        return false;
    }
    message = (boost::format("[%s] none of the conditions satisfied, %.3e, %.3e.") % date.to_string() % r_long % r_short).str();
    return false;
}


