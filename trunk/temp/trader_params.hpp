#ifndef __trader_params__
#define __trader_params__

#include <string>

#include "stringparpars.hpp"

struct TraderParams
{
    int HP;
    int shorttime;
    int trendshort;
    int trendlong;
    double profthres;
    double maxloss;
    double proftgt;
    int closehold;

    TraderParams() {} 
    TraderParams(const std::string& x) { from_string(x); }

    void from_string(const std::string& X);
    std::string to_string() const;

};



bool operator==(const TraderParams& p1, const TraderParams& p2);
bool operator!=(const TraderParams& p1, const TraderParams& p2);

#endif

