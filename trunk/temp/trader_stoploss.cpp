#include "trader_stoploss.hpp"

bool StopLossModule::operator()(const TraderParams& p, const tiempo::duration& trad_freq, 
                double quantity, 
                const tiempo::datetime& date0, double price0, 
                const tiempo::datetime& date_, double price_) const
{
    if (!quantity) return false;
    double max_loss_ratio = (p.maxloss ? p.maxloss : 0.007);
    double max_win_ratio = (p.proftgt ? p.proftgt : 0.007);
    double investment = fabs(price0 * quantity );
    double profit = quantity * (price_ - price0);
    double profit_rate = profit / investment;
    
    tiempo::duration hold_time = date_ - date0;
    
    if (p.closehold && hold_time > p.closehold * trad_freq) {
        StatVar statvar;
        double mean, var;
        if (!statvar(tix, date_, hold_time, mean, var)) {
            std::cout << "Something REALLY weird happened." << std::endl;
            mean = (price0 + price_)/2.0;
            var = (pow(price0-mean,2) + pow(price_-mean,2))/2.0;
        }
        // mean profit:
        double profitrm = quantity * (mean - price0) / fabs(price0*quantity);
        if (profitrm < 0.0) {
            // we have been losing money for too long. Just close the damn thing as soon it
            // turns down.
            tiempo::duration local_trend_time = std::chrono::minutes(5);
            double xtrend;
            if ((*localtrend)(date_, local_trend_time, xtrend) && xtrend <= 0.0)
                return true;
        }
    }
    
    if (profit_rate < -max_loss_ratio) return true;

    #if 1
    if (profit_rate >  max_win_ratio) return true;
    // don't hold for too long. just close it 
    //if (hold_time >= 3*trad_freq && profit_rate > 0.003 ) return true;
    if (hold_time >= 4*trad_freq && profit_rate > 0.0015 ) return true;
    if (hold_time >= 5*trad_freq && profit_rate > 0.0005 ) return true;
    #endif
    #if 0
    if (profit_rate > max_win_ratio) return true;
    #endif
    //if (date_ - date0 > tiempo::duration(3600 * 5) && profit_rate > 0.0005) return true;
    //if (date_ - date0 > tiempo::duration(3600 * 7) && profit_rate > 0.0002) return true;
    return false;
}


