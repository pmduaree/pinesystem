#ifndef strategy_portfolio
#define strategy_portfolio

#include <vector>
#include <memory>
#include <unordered_map>
#include "../Tiempo/tiempo.hpp"
#include "../Assets/asset.hpp"
#include "../signals.hpp"
#include "../signaller.hpp"
#include "../signal_interface.hpp"

class StrategyPortfolio: public SignalInterface
{
    int strategyportfolio_id;
    std::vector<std::pair<double,std::shared_ptr<Signaller> > > _sig;
    std::shared_ptr<sql_connection> conn;
    std::shared_ptr<Asset> asset;
    int freq;
    int offset;
  public:
    StrategyPortfolio(std::shared_ptr<sql_connection> conn_,
                      std::shared_ptr<Asset> asset_,
                      int freq_, int offset_);
    StrategyPortfolio(std::shared_ptr<sql_connection> conn_, std::shared_ptr<Asset> asset_, 
                      int p_id);
    ~StrategyPortfolio();
    
    std::tuple<signal_type,tiempo::datetime> operator()(const tiempo::datetime& date) const;
    void calculate_signals(const std::vector<tiempo::datetime>& dates, time_series<signal_type>&S ) const;

    bool init(int p_id);
    int save();
    tiempo::datetime get_regularized_date(const tiempo::datetime& date) const;
    void add_signaller(double w, std::shared_ptr<Signaller> mod);


    size_t size() const { return _sig.size(); }
    std::pair<double, std::shared_ptr<Signaller> > get_signaller(size_t i) { return _sig[i]; }

    int get_freq() const { return freq; }
    int get_offset() const { return offset; }
    std::shared_ptr<Asset> get_asset() { return asset; }

    int get_id() const { return strategyportfolio_id; }

    std::string get_info() const;

  private:
    std::tuple<bool,double,signal_type> get_signal_from_db(const tiempo::datetime& date) const;
    void store_signal_to_db(const tiempo::datetime& date, double p, signal_type sig) const;
    std::tuple<double,signal_type> calculate_signal(const tiempo::datetime& on_date) const;
    
    // cache:
    mutable std::mutex _mux;
    mutable std::unordered_map<tiempo::datetime, std::tuple<double,signal_type> > _signals;

};
    
    


#endif
