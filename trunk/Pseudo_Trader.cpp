//
//  Pseudo_Trader.cpp
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//
#include "Pseudo_Trader.hpp"


Pseudo_Trader::Pseudo_Trader(std::shared_ptr<sql_connection> _conn,int _StrategyID)
:conn(_conn),StrategyID(_StrategyID),Push_SignalsInto_DB(false),type("close")
{
    Load_Elements();
    Load_Portfolio();
}
void Pseudo_Trader::Change_Strat_Params(std::string Params,int freq)
{
    __Signaller->Set_Parameters(Params);
    __Signaller->Set_freq(freq);
}
void Pseudo_Trader::Load_Elements()
{
    __Signaller         = std::make_shared<SignalSeries> (conn,StrategyID);
    PortfolioID         = __Signaller->get_portfolio();
    freq                = __Signaller->get_freq();
    SignalerEngine_ID   = __Signaller->Get_SignalerID();
    
    __Encoder           = std::make_shared<Encoder>(conn,StrategyID);
     EncoderEngine_ID   = __Encoder->get_EncoderEngineId();
     AssetsID           = __Encoder->get_AssetsID();
}
void Pseudo_Trader::Load_Portfolio()
{
    for ( auto id: AssetsID  )
        __AssetManager.push_back( SingleAssetManager (conn,id)    );
}
std::string Pseudo_Trader::To_String()
{
    std::string out;
    out += "Assets=";
    out += std::to_string(AssetsID.size())+"\n";
    for (auto a:AssetsID)
        out += std::to_string( a )+ " \n";
    return  out;
}
tiempo::datetime  Pseudo_Trader:: get_last_transaction_date()
{
    conn->lock();
    //// 0 for enter, 1 for exit
    std::string query = (boost::format( "SELECT LastTransactionDate FROM `Main`.`Strategies` \
                                        WHERE `id`=  %d")
                                        % StrategyID ).str();
    auto con_ = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con_->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() > 0)
    {
        res->first();
      
        tiempo::datetime out(res->getString("LastTransactionDate"));
        std::cout << "   Last Transaction Fetched = " << out.to_string() <<
                        " at " << (tiempo::now()).to_string() << std::endl;
        //if brand new stablish new las date
        if (out < tiempo::datetime("2014-01-01 00:00:00") )
        {   std::cout << "Replacing Last Date value out, Out of bounds " << std::endl;
            query =  (boost::format("UPDATE `Main`.`Strategies` \
                                    SET `LastTransactionDate` = '%s' \
                                    WHERE `id` = %d ")
                                    % (tiempo::now()).to_string("%Y-%m-%d@%H:%M:00")
                                    % StrategyID ).str();
            stmt.reset(con_->createStatement());
            stmt->execute(query);
            conn->unlock();
            return tiempo::datetime(tiempo::now().to_string("%Y-%m-%d %H:%M:00") );
        }
        else{
            conn->unlock();
            return out;
        }
    }
    else{
        
        std::cout << "Replacing Last Date value out, Out of bounds 2" << std::endl;
        query =  (boost::format("UPDATE `Main`.`Strategies` \
                                SET `LastTransactionDate` = '%s' \
                                WHERE `id` = %d ")
                  % (tiempo::now()).to_string("%Y-%m-%d@%H:%M:00")
                  % StrategyID ).str();
        stmt.reset(con_->createStatement());
        stmt->execute(query);
        conn->unlock();
        return tiempo::datetime(tiempo::now().to_string("%Y-%m-%d %H:%M:00") );
        }
}

bool  Pseudo_Trader:: get_last_status()
{
    conn->lock();
    // 0 for enter, 1 for exit
    std::string query = (boost::format( "SELECT OnTrade FROM `Main`.`Strategies` \
                                       WHERE `id`=  %d") % StrategyID ).str();
    auto con_ = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con_->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));

    res->first();
    int Status = res->getInt("OnTrade");
    conn->unlock();
    std::cout << "Fetched last Status " << Status << std::endl;
    return (Status == 1 )? true : false;
}

void   Pseudo_Trader:: get_last_transaction_status()
{
    Last_transaction_date = get_last_transaction_date();
    std::cout << "Last Transacion Date " << Last_transaction_date.to_string() << std::endl;
    //Status.set_status(   S_Init );
    
    if ( get_last_status()){
        std::cout << "Last Status On Trade ON" << std::endl;
        Status.set_on_trade_on();
    }
    else{
        std::cout << "Last Status On Trade OFF " << std::endl;
        Status.set_on_trade_off();
    }
    
    if (Get_Enter_Signal_LIVE(Last_transaction_date))
    {   std::cout << "Restoring last state values" << std::endl;
        Enter_Trade(Last_transaction_date,false);
    }
}
void Pseudo_Trader::Init_Values()
{
   // Status.set_status(S_Init);
    Status.set_on_trade_off();
    Stats.Clear();
    __Signaller->setType(type);
    
  //either calculate or fetch
   if (CalculateOnTheFly)
        __Signaller->Init( from_date, to_date ,  CALCULATE_ON_THE_FLY  );
    else
        __Signaller->Init( from_date, to_date , LOAD_FROM_DB);
    
    rawSignals =  __Signaller->get_Signals();

}
bool Pseudo_Trader::Get_Enter_Signal(tiempo::datetime date)
{
    if (rawSignals.find(CurrentDate) == rawSignals.end() )
        return false;
    
    int Signal = std::get<0>(rawSignals[CurrentDate ]);
    if (Signal > 0)
        return true;
    else
        return  false;
}
bool Pseudo_Trader::Get_Enter_Signal_LIVE(tiempo::datetime date)
{
    conn->lock();
    // 0 for enter, 1 for exit
    std::string query = (boost::format( "SELECT * FROM `Main`.`RawSignals` \
                                       WHERE `date`= '%s' AND `portfolio_id`=%d  \
                                       AND `strategy_id`=%d AND `LIVE` = 1 ")
                                    % date.to_string("%Y-%m-%d@%H:%M:%S")
                                    %  PortfolioID
                                    %  StrategyID
                                    ).str();
    auto con_ = conn->connection();
    std::unique_ptr<sql::Statement> stmt(con_->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    
    if (res->rowsCount()<=0 )
    {
        conn->unlock();
        std::cout << "Get Empty signal on date " << date.to_string() << std::endl;
        rawSignals[date] = std::make_tuple(0,0.0,"");
        return false;
    }
    
    res->first();
    int Signal  = res->getInt("SignalType");
    double Prob = res->getDouble("Ppos");
    std::string comment_ = res->getString("Rpar");
    rawSignals[date] = std::make_tuple(Signal,Prob,comment_);
    
    conn->unlock();
    std::cout << (boost::format("Get %d Signal with %0.3f Prob and %s")% Signal % Prob % comment_ ).str() << std::endl;
    if (Signal > 0)
        return true;
    else
        return  false;
}
bool Pseudo_Trader::Get_Exit_Signal(tiempo::datetime date)
{
    if (rawSignals.find(CurrentDate) == rawSignals.end() )
        return false;
    
    int Signal = std::get<0>(rawSignals[CurrentDate ]);
    if (Signal < 0)
        return true;
    else
        return  false;
}
void Pseudo_Trader::Enter_Trade(tiempo::datetime date,bool skip_DB_interact)
{
    std::string comment("EnterTrade");
    
    std::vector<std::tuple<int,int>> Purchase_Order =
    __Encoder->Encode_Signal(date, InvestingMoney, rawSignals[date]);
    
    //check if order is valid
    bool Ok_to_go=true;
    for ( auto order: Purchase_Order)
    {   //stock column
        if (std::get<Stock_Colum>(order)  == 0)
        {    Ok_to_go= false;
            comment+="|Avoid_Trade_OnePositionZero";
        }
    }
    for (auto SingleAsset : __AssetManager)
    {
        if(!SingleAsset.is_it_open_to_trade(date) )
        {
          Ok_to_go= false;
            comment+= "|Not_tradeable_Time";
        }
    }
    if (Ok_to_go)
    { //std::cout << "Ok to go to enter trade" << std::endl;
        Status.set_on_trade_on();
        
        if (WorkingAsLiveTrader & skip_DB_interact )
            Push_Status_toDB(ENTER_TRADE,date);
        
        for ( auto order: Purchase_Order)
        {   int position=std::get<Stock_Colum>(order);
            int id      =std::get<Asset_Colum>(order);
            std::cout << "id= " << id  <<  " pos=" << position <<std::endl;
            //for (auto SingleAsset : __AssetManager)
            for(unsigned int i = 0; i < __AssetManager.size(); i++)
            {
                auto SingleAsset = __AssetManager[i];
                if( SingleAsset.get_id_asset() == id)
                {
                    if( position>0)
                    {  __AssetManager[i].Long_position(date,(unsigned int )abs(position));
                        Push_To_PortfolioRisk(ENTER_TRADE ,std::get<3>( __AssetManager[i].fetchValue(date) )   , LONG,__AssetManager[i].get_id_asset() );
                        
                        comment+="|success";
                        if(Push_SignalsInto_DB & skip_DB_interact)
                            Push_Trade_To_DB(order,date,DueEnter);
                       
                    }
                    if( position<0)
                    {  __AssetManager[i].Short_position(date,(unsigned int )abs(position));
                        Push_To_PortfolioRisk(ENTER_TRADE , std::get<3>( __AssetManager[i].fetchValue(date) ), SHORT,__AssetManager[i].get_id_asset() );
                        
                        comment+="|success";
                        if(Push_SignalsInto_DB & skip_DB_interact)
                            Push_Trade_To_DB(order,date,DueEnter);
                    }
                }
            }
        }
    }
    if ( skip_DB_interact)
        ExecutedSignals[date] = std::make_tuple(ENTER_TRADE,Get_profit(),comment) ;
}
void Pseudo_Trader::Exit_Trade(tiempo::datetime date,bool skip_DB_interact)
{
    bool Ok_to_go=true;
     std::string comment("ExitTrade");
    
    for (auto SingleAsset : __AssetManager)
    {
        if(!SingleAsset.is_it_open_to_trade(date) )
        {
            Ok_to_go= false;
            comment+= "|Not_tradeable_Time";
        }
    }
    if(Ok_to_go)
    {
        Status.set_on_trade_off();
        
        if (WorkingAsLiveTrader & skip_DB_interact)
            Push_Status_toDB(EXIT_TRADE,date);
        
        for(unsigned int i = 0; i < __AssetManager.size(); i++)
        {
            int position = __AssetManager[i].get_postion();
            if( position>0)
            {
                __AssetManager[i].Short_position(date,(unsigned int )abs(position));
                Push_To_PortfolioRisk(EXIT_TRADE , __AssetManager[i].get_last_Price(), LONG,__AssetManager[i].get_id_asset() );
                
                comment+="|success";
                Process_Exit_Trade();
                if(Push_SignalsInto_DB & skip_DB_interact)
                   Push_Trade_To_DB(std::make_tuple(__AssetManager[i].get_id_asset(),position),date,DueExit);
            }
            if( position<0)
            {
                __AssetManager[i].Long_position(date,(unsigned int )abs(position));
                Push_To_PortfolioRisk(EXIT_TRADE , __AssetManager[i].get_last_Price(), SHORT,__AssetManager[i].get_id_asset() );
                comment+="|success";
                Process_Exit_Trade();
                if(Push_SignalsInto_DB & skip_DB_interact)
                    Push_Trade_To_DB(std::make_tuple(__AssetManager[i].get_id_asset(),position),date,DueExit);
            }
        }
    
    }
    if ( skip_DB_interact)
         ExecutedSignals[date] = std::make_tuple(EXIT_TRADE,Get_profit(),comment) ;
}

void Pseudo_Trader::WithDrawLastEnterTrade()
{
    
    std::string comment("ExitTrade");
    for(unsigned int i = 0; i < __AssetManager.size(); i++)
    {
        int position = __AssetManager[i].get_postion();
        tiempo::datetime date = __AssetManager[i].get_LastTrade();
        if( position>0)
        {
            
            __AssetManager[i].Short_position(date,(unsigned int )abs(position));
            comment+="|successExitDue end of simulation";
            std::cout << "Forced Exit due exit position=" << position << " Go short " << (unsigned int )abs(position) << " went to=" << __AssetManager[i].get_postion() << std::endl;
            Status.set_on_trade_off();
            
            
        }
        if( position<0)
        {
            __AssetManager[i].Long_position(date,(unsigned int )abs(position));
            std::cout << "Forced Exit due exit position=" << position << " Go Long " << (unsigned int )abs(position) << " went to=" << __AssetManager[i].get_postion() << std::endl;
            comment+="|successExitDue end of simulation";
            Status.set_on_trade_off();
        }
    }
}
void Pseudo_Trader::run(tiempo::datetime from_ , tiempo::datetime to_,double money, bool calculate)
{
    from_date   = tiempo::datetime(from_.to_string("%Y-%m-%d@%H:%M:00"));
    to_date     =to_;
    
    CalculateOnTheFly = calculate;
    InvestingMoney = money;
    
    // main for
    CurrentDate = from_date;
    Init_Values();
   
    std::cout << "Start To calculate Trader " << std::endl;


    for(  ; CurrentDate < to_date ; CurrentDate+= std::chrono::minutes(freq) )
    { 
        //std::cout << "[" <<  CurrentDate.to_string() << "] " << std::endl;
        //if no trading search eagerly to jum into one
        if(!Status.is_it_on_trade())
        {
                if (Get_Enter_Signal(CurrentDate))
                {
                    Enter_Trade(CurrentDate,true);
                    std::cout << "<result>[" << CurrentDate.to_string() <<"]\tENTER\t\t" <<
                    Print_Portfolio_Value() << "</result>" << std::endl;
                }
        } //end if status is not on trade
           else //meaning on Going Trade

                {
                    if (Get_Exit_Signal(CurrentDate))
                    {
                     Exit_Trade(CurrentDate,true);
                     std::cout << "<result>[" << CurrentDate.to_string() <<"]\tEXIT \t\t" <<
                     Print_Portfolio_Value() <<"</result>" << std::endl;
                        
                    }
                }//end else on going trade
    }// end main for
    
    if(Status.is_it_on_trade())
        WithDrawLastEnterTrade();
    
    for(unsigned int i = 0; i < __AssetManager.size(); i++)
        std::cout << __AssetManager[i].toString() << std::endl;
    std::cout << "Run Done \n" <<std::endl;
    
    PortfolioRisk p(PortfolioID, conn, from_date.to_string(), to_date.to_string() ,t);
	p.printAll();
    std::cout << Print_Portfolio_Value() << std::endl;
}
void Pseudo_Trader::RunLive( tiempo::datetime to_,double money)
{
    std::cout << (boost::format("Initalize values for Strategy ID %d with Investing Money = %0.2f")
                  %StrategyID %InvestingMoney ).str() << std::endl;
    CalculateOnTheFly = false;
    
    Stats.Clear();
    InvestingMoney = money;
    get_last_transaction_status();
    
    to_date     =to_;
    from_date   =Last_transaction_date;
    CurrentDate = from_date;
    
    //Load Previous Status
    __Signaller->Init( from_date, to_date , LOAD_FROM_DB_LIVE);
    rawSignals =  __Signaller->get_Signals();
   
    // main for
    std::cout << "Start To calculate Trader LIVE  at las Date of  " <<
    Last_transaction_date.to_string() << " TradeStatus = " <<  Status.is_it_on_trade() <<  std::endl;
    
//if there is something to look for ?  go ahead
if (( CurrentDate+= std::chrono::minutes(freq) ) < to_date )
    for(  ; CurrentDate < to_date ; CurrentDate+= std::chrono::minutes(freq) )
    { std::cout << "[" <<  CurrentDate.to_string() << "] " << std::endl;
        //if no trading search eagerly to jum into one
        if(!Status.is_it_on_trade())
        {    std::cout << "\t\t\tCurrently NO Trades " << std::endl;
            if (Get_Enter_Signal(CurrentDate))
            {
                Enter_Trade(CurrentDate,true);
                std::cout << "<result>[" << CurrentDate.to_string() <<"]\tENTER\t\t" <<
                Print_Portfolio_Value() << "</result>" << std::endl;
                break;
            }
        } //end if status is not on trade
        else //meaning on Going Trade
        {   std::cout << "\t\t\tCurrently ONGOING Trade " << std::endl;
            if (Get_Exit_Signal(CurrentDate))
            {
                Exit_Trade(CurrentDate,true);
                std::cout << "<result>[" << CurrentDate.to_string() <<"]\tEXIT \t\t" <<
                Print_Portfolio_Value() <<"</result>" << std::endl;
                break;
            }
        }//end else on going trade
    }// end main for
    
    std::cout << " \n Run Done  \n" << std::endl;
    
    for(unsigned int i = 0; i < __AssetManager.size(); i++)
        std::cout << __AssetManager[i].toString() << std::endl;

    std::cout << "Portfolio values \n" << Print_Portfolio_Value() << std::endl;
}
std::string Pseudo_Trader::Print_Portfolio_Value()
{
    std::string out;
    out += (boost::format("%f\t")% Get_profit() ).str();
    for(unsigned int i = 0; i < __AssetManager.size(); i++)
        out += (boost::format("%d=%f\t")%__AssetManager[i].get_id_asset() % __AssetManager[i].get_Money()  ).str();
    return out;
}
void Pseudo_Trader::PrintToFile(std::string FileName)
{
    std::ofstream my;
    my.open (FileName.c_str());
    my << "date," ;
    
    for (auto SingleAsset:__AssetManager){
        my << SingleAsset.get_name() << "_O;" ;
        my << SingleAsset.get_name() << "_H;" ;
        my << SingleAsset.get_name() << "_L;" ;
        my << SingleAsset.get_name() << "_C;" ;
        my << SingleAsset.get_name() << "_Position;" ;
        my << SingleAsset.get_name() << "_Money;" ;
        my << SingleAsset.get_name() << "_Comission;" ;
        my << SingleAsset.get_name() << "History_NAsset;";
        my << SingleAsset.get_name() << "History_Value;";
        my << SingleAsset.get_name() << "History_Str;";
    }
    my << "ExecSignal_D;ExecSignal_Str;Signal_D;Signal_Str"<<std::endl;
 
    tiempo::datetime Date =  from_date;
    for(  ; Date < to_date ; Date+= std::chrono::minutes(freq) )
    {
       my << Date.to_string() << ";";
        
        for (auto SingleAsset:__AssetManager)
        {
            std::tuple<double,double,double,double> value =SingleAsset.fetchValue( Date );
             my << std::get<0>(value) << ";" ;
             my << std::get<0>(value) << ";" ;
             my << std::get<0>(value) << ";" ;
             my << std::get<0>(value) << ";" ;
             my << SingleAsset.get_postion() << ";";
             my << SingleAsset.get_Money() << ";";
             my << SingleAsset.get_AcumComission() << ";";
             my << SingleAsset.TimeHistory_Assets(Date) << ";";
             my << SingleAsset.TimeHistory_Value(Date) << ";";
             my << SingleAsset.TimeHistory_Str(Date) << ";";
        }
        my<< std::get<0>(ExecutedSignals[Date])<< ";";
        my<< std::get<1>(ExecutedSignals[Date])<< ";";
    
        my<< std::get<0>(rawSignals[Date]) << ";";
        my<< std::get<1>(rawSignals[Date]) << std::endl;
    }
    my.close();
}
double Pseudo_Trader:: Get_profit()
{
 Stats.currentProfit=0;
 Stats.comissions=0;
    for(unsigned int i = 0; i < __AssetManager.size(); i++)
    {
         Stats.currentProfit        +=   __AssetManager[i].get_Money();
          Stats.comissions    += __AssetManager[i].get_AcumComission()  ;
    }
    return Stats.currentProfit - Stats.comissions;
}
void Pseudo_Trader::Process_Exit_Trade()
{
    Status.set_on_trade_off();
    Stats.number_of_trades++;
    
    double money = Get_profit();
    
    if (money > 0)
    {
        Stats.succes_trades++;
        Stats.wins+= money;
        
        Stats.IsItBetterWin(money);
    }
    if (money <= 0)
    {
        Stats.failed_trades++;
        Stats.losses+= money;
        
        Stats.IsItBetterLoss(money);
    }
   // Stats.PrintValues();
}
void Pseudo_Trader::Push_Trade_To_DB(std::tuple<int,int> TradingSignals, tiempo::datetime date ,int TradeType )
{
    std::cout << "Pushing to DB" << std::endl;
  conn->lock();
        int id_asset = std::get<Asset_Colum>(TradingSignals);
        int amount   = std::get<Stock_Colum>(TradingSignals);
        int side=0;
        side = (amount >0 )? 1:side;
        side = (amount <0 )?-1:side;
        amount = (int) abs(amount);
    
        int limit_price = 0;
        int order_price = 0;
    
    ///Live Status
    // 0 to push and take them as live signals
    // -1 ignored by the Tradebook , backtesting only
    
        int LiveStatus = -1;
        if (WorkingAsLiveTrader)
            LiveStatus = 0;
   // 0 for enter, 1 for exit
        std::string query = (boost::format( "INSERT INTO `Main`.`Signal` \
                                           (side,quant,date,\
                                           limit_price, order_price, processed, \
                                           asset_id,portfolio_id,position) \
                                            VALUES (%d,%d,\'%s\',  \
                                                    %d,%d,%d, \
                                                    %d,%d,%d )")
                                         % side % amount % date.to_string("%Y-%m-%d@%H:%M:%S")
                                         % limit_price % order_price % LiveStatus
                                         % id_asset % PortfolioID % TradeType
                                          ).str();
    //std::cout << query << std::endl;
    try {
        auto _con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(_con->createStatement());
        stmt->execute(query);
    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << "Duplicate Signal in DataBase" << std::endl;
    }
  conn->unlock();
}
void Pseudo_Trader::Push_Status_toDB(int Stat,tiempo::datetime date )
{
    conn->lock();
    //// 0 for enter, 1 for exit
    std::string query = (boost::format( "UPDATE  `Main`.`Strategies` SET  \
                                       `OnTrade`= %d , `LastTransactionDate`='%s' WHERE `id`= %d")
                                         % Stat
                                         % date.to_string("%Y-%m-%d@%H:%M:%S")
                                         % StrategyID
                                                 ).str();
        auto _con = conn->connection();
        std::shared_ptr<sql::Statement> stmt(_con->createStatement());
        stmt->execute(query);
    conn->unlock();
}
void Pseudo_Trader::Push_To_PortfolioRisk(int TypeOfTransaction,double price,int position , int asset_id_ )
{
             Transaction a;
            if (position == LONG  )
            {
                a.side = (TypeOfTransaction == ENTER_TRADE)? 1 : -1;
                a.position = 1;
                a.asset_id = asset_id_;
                a.price = price;
            }else
            {   a.side =  (TypeOfTransaction == EXIT_TRADE)? 1 : -1;
                a.position = -1;
                a.asset_id = asset_id_;
                a.price = price;
            }
            std::cout << ( boost::format("\n[ Asset %d Price %f side %d position %d ]\n") % asset_id_  % price % TypeOfTransaction % position ).str() <<std::endl;
            t.emplace_back(a);
}
