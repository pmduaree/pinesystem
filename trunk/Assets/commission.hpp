#ifndef __asset_commission__
#define __asset_commission__

#include <string>
class Commission
{
    double percent;
    double maxusd;
  public:
    Commission(const std::string& com="");
    Commission(const Commission& c);
    
    void init(const std::string& com);
    void clear();
    double operator()(double trade_value) const;
};




#endif
