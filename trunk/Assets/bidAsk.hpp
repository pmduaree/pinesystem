#ifndef __bdsk
#define __bdsk

//std::unordered_map<std::string,size_t>	aF={{"bid",0},{"ask",1},{"mid",2},{"close",3}};
// std::unordered_map<std::string,int>		aS={{"CLOSE_LONG",-1},{"CLOSE_SHORT",1},{"OPEN_LONG",1},{"OPEN_SHORT",-1},{"WAIT",0}};
// std::unordered_map<std::string,int>		aP={{"SHORT",-1},{"NONE",0},{"LONG",1}};
//std::unordered_map<std::string,int>		aA={{"CANCEL",-1},{"ORDER",0},{"APPROVE",1}};


//full asset data
template<class DT>
class dataContainer{
public:
	DT bid;	
	DT ask;	
	DT mid;
	size_t trainSize=0;
	
	std::vector<double> spr;
	double sprMean=0;
	double sprStDev=0;

	dataContainer();


	dataContainer(std::shared_ptr<sql_connection> conn,  int instrument_id, tiempo::datetime date)
	{

	    conn->lock();
	    auto con = conn->connection();

	    tiempo::datetime date1 = date - std::chrono::minutes(1);
	    tiempo::datetime date2 = date1 - std::chrono::minutes(60*24);

	    std::string query = (boost::format(
	    		"SELECT T.date, T.bid, T.ask, T.midprice FROM Main.Tick  AS T\
				WHERE T.instrument_id = %d\
				AND T.date BETWEEN '%s' AND '%s'\
				ORDER BY T.date ASC")
	                         % instrument_id
	                         % date2.to_string("%Y-%m-%d %H:%M:%S")
	                         % date1.to_string("%Y-%m-%d %H:%M:%S")).str();

	    std::unique_ptr<sql::Statement> stmt(con->createStatement());
	    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));

	    while(res->next()) {
	        tiempo::datetime t(res->getString("date"));
	       // std::cout << "   Time raw " << t.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
	        //if (!X.empty() && X.rbegin()->first >= t) continue;   // duplicate
	        bid[t].p = res->getDouble("bid");
	        ask[t].p =  res->getDouble("ask");
	        mid[t].p = res->getDouble("midprice");

	    }
	    conn->unlock();
	    
	    if(bid.size() == 0) return;
	    train(date1, date2);
	};

	//spread, std and mean 
	void train(tiempo::datetime_type beg, tiempo::datetime_type end){		
		size_t cnt=0;

		auto it1=bid.rbegin();
		auto it2=ask.rbegin();
		auto it3=mid.rbegin();

		while (it1 != bid.rend()){
			if(it1->first<=end && it1->first>=beg){
				++cnt;
				spr.push_back(log10(it2->second.p)-log10(it1->second.p));
				//printOut;
				//std::cout<<it1->first.to_string()<<"|"
				//	<<it1->second.p<<"|"<<it2->second.p<<"|"<<it3->second.p<<"|"<<spr.back()<<"\n";
			}
			++it1;++it2;++it3;
		}
		if(cnt<5) return;//throw std::runtime_error("ERROR::check train dates or asset files");

		double sum = std::accumulate(spr.begin(), spr.end(), 0.0);
		sprMean = sum / spr.size();

		double sq_sum = std::inner_product(spr.begin(), spr.end(), spr.begin(), 0.0);
		sprStDev = std::sqrt(sq_sum / spr.size() - sprMean * sprMean);
		
		std::cout<<"|records="<<cnt<<"|mSpr="<<sprMean <<"|StdSpr="<<sprStDev<<std::endl;
		
		trainSize=cnt;
	};
	
	int getBidAskSignal(tiempo::datetime_type date, int parentSignal,double a1,double a2,
						int offSet=0,int offSetSize=0
						)
	{
		if(trainSize < 5)
			return parentSignal;

		double th=(sprMean+a1*sprStDev)/2+offSet*offSetSize;
		double pn=(sprMean+a2*sprStDev)/2;
		//std::cout<<offSet<<":"<<i<<"-"<<th<<"\n";
				
		double askP=log10(ask.get_price(date));
		double bidP=log10(bid.get_price(date));
		double midP=log10(mid.get_price(date));
		//std::cout<<a1<<" "<<a2<<" "<<tr<<"\n"; 
		

		//determine if we want to sell or buy instrument >> depends on parentSignal and Portfolio weights. If we are selling portfolio, but the weight for given asset is negative, then we are buying it!	
		//int action=static_cast<int>(bestP->w[i]/abs(bestP->w[i]))*parentSignal;

		if(parentSignal==1){ //buying
			if((askP-midP)<=th) 	return parentSignal;//return(aA["APPROVE"]);
			if(th>pn) 				return 0;//return(aA["CANCEL"]);
			if((askP-midP)<=pn) 	return parentSignal;//return(aA["ORDER"]);
			if((askP-midP)> pn) 	return 0;//return(aA["CANCEL"]);
		}
		if(parentSignal==-1){ //selling
			if((bidP-midP)>=-th) 	return parentSignal;//return(aA["APPROVE"]);
			if(th>pn) 				return 0;//return(aA["CANCEL"]);
			if((bidP-midP)>=-pn) 	return parentSignal;//return(aA["ORDER"]);
			if((bidP-midP)< -pn) 	return 0;//;return(aA["CANCEL"]);
		}
		//throw std::runtime_error("asset level should always return something... check getBidAskSignal");
		return 0;
	}
};

// 	///support function
// 	//loads all files into vector<tix_collection>
// 	void loadContainersData(std::vector<std::string> inputFiles,
// 							std::vector<dataContainer<tix_series>>& data){
// 		std::cout<<"clearing bid/Ask Containers!\n";
// 		data.clear();
// 		for( auto& c:inputFiles ){
// 			std::stringstream sstr;
// 			sstr<<"inputs/"<<c;
// 			dataContainer<tix_series> tmp(sstr.str());
// 			data.push_back(tmp);
// 		}
// }

// 	//train all supplied series
// 	void trainBidAskAll(tiempo::datetime_type beg,tiempo::datetime_type end,
// 						std::vector<dataContainer<tix_series>>& data){		
// 		std::cout<< "\npreparing spread stat for ";
// 		std::cout<<"--"<<  beg.to_string() << "--" << end.to_string()<<"\n";
// 		if(data.size()==0) throw std::runtime_error("ERROR::check asset files");
// 		for( auto& c:data ){
// 			c.train(beg,end);
// 		}
// }

// ///signaller engine
// class signalEngine{	    
// public:
//     double offSetSize=0.000001;	//this is not ok ... static numbers are always problematic... it should be normalized somehow; we should discuss that!
//     size_t minTstep=10;			//in seconds
    
// 	int position;
// 	int signal;
// 	double openStdTresh;
// 	double closeStdTresh;
// 	double investment;

// 	std::vector<dataContainer<tix_series>> assetData;
// 	double a1,a2; //bidAsk strategy paramaters

// 	portfolio<assets>* bestP;

// 	signalEngine(){};
// 	signalEngine(portfolio<assets>* bP,const std::vector<dataContainer<tix_series>>& D,
// 					double openStdTresh,double closeStdTresh,double a1_,double a2_){
		
// 		position=aP["NONE"];
// 		signal=aS["WAIT"];
// 		bestP=bP;
// 		assetData=D;
// 		this->closeStdTresh = closeStdTresh; //
// 		this->openStdTresh = openStdTresh; //
// 		a1=a1_;
// 		a2=a2_;
// 	}
    
//     signalEngine& operator=(const signalEngine& other) = default;

// 	//support function for determinePortfolioSignal
// 	std::tuple<double,double> computePortfolioStat(tiempo::datetime_type date) const{
// 		double PPrice=0.0;
// 		//computes portfolio close price
// 		for(auto i=0;i<bestP->w.size();++i)	PPrice+=bestP->w[i]*assetData[i].close.get_price(date);
		
// 		double CDF = 0.5 + 0.5 * erf((PPrice-bestP->mean )/(bestP->stdDev * sqrt(2)));
// 		return std::make_tuple(PPrice, CDF);
// 	}

// 	std::tuple<int,double> determinePortfolioSignal(tiempo::datetime_type date){ //
		
// 		double PPrice, CDF;
// 		std::tie(PPrice, CDF)=computePortfolioStat(date);
        	   
// 		///decision tree
// 		if (CDF < 0 || CDF > 1) {
//             std::cerr << " >> invalid Probability" << std::endl;
// 			return std::make_tuple(aS["WAIT"],PPrice);
// 		}
	  
// 		if (position ==	aP["NONE"]){ 
// 			if(CDF < openStdTresh)			return std::make_tuple(aS["OPEN_LONG"],PPrice);
// 			if(CDF > (1.0-openStdTresh))	return std::make_tuple(aS["OPEN_SHORT"],PPrice);}

//         // MD; mean reverting: after the price returns to mean, close the open position
//         if (position ==	aP["LONG"])
// 			if(CDF > (1.0-closeStdTresh))	return std::make_tuple(aS["CLOSE_LONG"],PPrice); 
         
// 		if (position ==	aP["SHORT"])
// 			if(CDF < closeStdTresh)			return std::make_tuple(aS["CLOSE_SHORT"],PPrice); 
        
//         /*
//         //  this code trades at extremes only (open position at one extreme, close at the opposite extreme)
// 		if(position ==	aP["LONG"])
// 			if(CDF>(1.0-stdTresh))			return std::make_tuple(aS["CLOSE_LONG"],PPrice); 
         
// 		if (position ==	aP["SHORT"])
// 			if(CDF<(stdTresh))		return std::make_tuple(aS["CLOSE_SHORT"],PPrice); 
//         */

// 		return std::make_tuple(aS["WAIT"],PPrice);
// 	}
	
//     // calculate the price of portfolio (single unit), depending on the signal (either opening long or short position)
// 	double BidAskPortPrice(tiempo::datetime_type date,int signal) const{
// 		double price; 
// 		if(signal==aS["OPEN_LONG"]) // == CLOSE_SHORT
// 		{
// 			for(auto i=0;i<bestP->w.size();++i){
// 				if(bestP->w[i]>0) price+=bestP->w[i]*assetData[i].ask.get_price(date);
// 				if(bestP->w[i]<0) price+=bestP->w[i]*assetData[i].bid.get_price(date);
// 			}
// 		}
// 		if(signal==aS["OPEN_SHORT"]) // == CLOSE_LONG
// 		{
// 			for(auto i=0;i<bestP->w.size();++i){
// 				if(bestP->w[i]>0) price-=bestP->w[i]*assetData[i].bid.get_price(date);
// 				if(bestP->w[i]<0) price-=bestP->w[i]*assetData[i].ask.get_price(date);
// 			}
// 		}
// 		return price;
// 	}
	
// 	//check the logic here:: right now it cannot go directly from long to short or from short to long, it always goes through position=NONE; is that ok?
// 	//output [price, profit] and accumulate profit in trader
// 	std::tuple<double,double> setPosition(int signal,tiempo::datetime_type date){
// 		//based on current position and signal changes position and collects profit
// 		double price=0;
// 		double profit=0;
		
//         //  test to open a new position
//         // to enable going from long to short and vice versa directly, just put this if after the next two ifs
// 		if(position==aP["NONE"]){
// 			if(signal==aS["OPEN_LONG"]){
// 				price=BidAskPortPrice(date,aS["OPEN_LONG"]);
// 				position=aP["LONG"];
// 				investment=price;
// 				profit=0;
// 			}
// 			if(signal==aS["OPEN_SHORT"]){
// 				price=BidAskPortPrice(date,aS["OPEN_SHORT"]);
// 				position=aP["SHORT"];
// 				investment=price;
// 				profit=0;
// 			}
// 		}
//         // test to close an existing position
// 		if(position==aP["LONG"]){
// 			if(signal==aS["CLOSE_LONG"]){
// 				price=BidAskPortPrice(date,aS["CLOSE_LONG"]);
// 				position=aP["NONE"];
// 				profit=investment+price;	
// 				investment=0;
// 			}	
// 		}
// 		if(position==aP["SHORT"]){
// 			if(signal==aS["CLOSE_SHORT"]){
// 				price=BidAskPortPrice(date,aS["CLOSE_SHORT"]);
// 				position=aP["NONE"];
// 				profit=investment+price; //check signs here!
// 				investment=0;
// 			}
// 		}
// 		return std::make_tuple(price,-profit);
// 	}

// 	std::vector<int> assetLevelSignaler(int parentSignal,tiempo::datetime_type idate,int offSet){
// 	//check all assets and returns vector<int>, for each asset one action::
// 		//if all actions are APPROVE, you change position according the signal
// 		//if one action is CANCEL you do nothing with position, ignore SIGNAL
// 		//you increase all ORDER ticks by 0.01 and proceed in time
// 		std::vector<int> SIGNALS(bestP->w.size());

// 		tiempo::datetime_type date=idate+std::chrono::seconds(minTstep*offSet);

// 		for(auto i=0;i<bestP->w.size();++i){
// 			//determine if we want to sell or buy instrument >> depends on parentSignal and Portfolio weights. If we are selling portfolio, but the weight for given asset is negative, then we are buying it!	
// 			int action=static_cast<int>(bestP->w[i]/abs(bestP->w[i]))*parentSignal;
// 			SIGNALS[i]=assetData[i].getBidAskSignal(date,action,a1,a2,offSet,offSetSize);

// 		}
// 		return SIGNALS;
// 	}

// 	///support
// 	vector<double> prepOutData(tiempo::datetime_type date){
// 		vector<double> temp;

// 		for(auto i=0;i<bestP->w.size();++i){
// 			temp.push_back((assetData[i].ask.get_price(date)));
// 			temp.push_back((assetData[i].bid.get_price(date)));
// 			temp.push_back((assetData[i].mid.get_price(date)));
// 			temp.push_back((assetData[i].close.get_price(date)));
// 			temp.push_back((assetData[i].sprMean+a1*assetData[i].sprStDev)/2);
// 			temp.push_back((assetData[i].sprMean+a2*assetData[i].sprStDev)/2);
// 		}
// 		return temp;
// 	}
// };

// template<class T>
// class tradingEngine{
// 	public:
// 	tiempo::datetime_type TradeFrom;
// 	tiempo::datetime_type TradeTo;
//     size_t numTrades = 0;
// 	size_t tradeFreq;
// 	T sigEngine;

// 	tradingEngine(const T& sGe){
// 		sigEngine=sGe;
// 	}
// 	void setTradingDates(tiempo::datetime_type from, tiempo::datetime_type to){
// 		TradeFrom=from;
// 		TradeTo=to;
// 	}
// 	double operator()(size_t frq){
//         numTrades = 0;
// 		if(sigEngine.assetData.size()==0) throw std::runtime_error("no data in signaler!!!");

// 		tiempo::datetime_type now=TradeFrom;
// 		tiempo::datetime_type internalTime;
// 		//std::ofstream outFile ("outputs/out.csv", std::ios::out);
// 		double PPrice,TPrice, profit; int PSignal;
// 		double accProfit=0;
// 		//simulator
// 		while (now<TradeTo){
// 			std::vector<int> bidaskSignals(sigEngine.bestP->w.size());

// 			std::tie(PSignal,PPrice)=sigEngine.determinePortfolioSignal(now);
			
// 			///decide what to do 
// 			//we try to get a approve signal as long as we have an order signal from assetSignaler
// 			int offSet=0; //deteremines the offset from original trade attempt
// 			if(PSignal==aS["WAIT"]) std::tie(TPrice, profit)=sigEngine.setPosition(aS["WAIT"],now);

// 			else{
// 				while(std::chrono::minutes(frq)>std::chrono::seconds(sigEngine.minTstep*offSet)) //as long as we do not cross next trading time
// 				{
// 					internalTime=now+std::chrono::seconds(sigEngine.minTstep*offSet);
// 					bidaskSignals=sigEngine.assetLevelSignaler(PSignal,now,offSet);	//checks bidask criteria
					
// 					if(std::accumulate(bidaskSignals.begin(), bidaskSignals.end(), 0.0) ==
// 						bidaskSignals.size()) {
// 							std::tie(TPrice, profit)=sigEngine.setPosition(PSignal,internalTime);break;} //all approved go with the action vector correction suggested
					
// 					if(std::find(bidaskSignals.begin(), bidaskSignals.end(), aA["CANCEL"])!=
// 						bidaskSignals.end()) {
// 							std::tie(TPrice, profit)=sigEngine.setPosition(aS["WAIT"],internalTime);break;} //if one asset signal is CANCEL proceed with WAIT::do nothing 
// 					++offSet;
// 				}
// 			}
// 			//OUTPUTS
// 			/*outFile<<now.to_string()<<
// 				";"<<sigEngine.prepOutData(now)<<
// 				";"<<bidaskSignals<<
// 				";"<<sigEngine.bestP->mean<<
// 				";"<<sigEngine.bestP->stdDev*sigEngine.stdTresh<<
// 				";"<<PSignal<<
// 				";"<<PPrice<<
// 				";"<<profit<<
// 				";"<<"\n";
// 			*/

// 			/*std::cout	<< now.to_string()<< " " << internalTime.to_string()<<"|PSig " <<PSignal 
// 						<<"|BAs "<< bidaskSignals<<"|Off "<<offSet<<"|CLPrice "<< PPrice
// 						<<"|BAPrice "<< TPrice <<"|Profit "<<profit
// 						<<"|Pos "<<sigEngine.position
// 						<<"\n";
			
// 			*/
// 			now += std::chrono::minutes(frq);
// 			accProfit+=profit;
//             if (profit != 0) ++numTrades;
			
// 		}	
// 		//outFile.close();
// 		//prn(accProfit);
// 		return accProfit;
// 	}
//  };

#endif
