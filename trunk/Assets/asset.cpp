#include "asset.hpp"
#include "../SQL/sql_connection.hpp"

Asset::Asset(std::shared_ptr<sql_connection> conn_, int instrumentid)
    : VAsset(), conn(conn_), asset_id(instrumentid), asset_name(""), type("close")
{
    update_asset_info();
}


Asset::Asset(std::shared_ptr<sql_connection> conn_, const std::string& asset_name)
    : VAsset(), conn(conn_), asset_id(0), asset_name(asset_name), type("close")
{
    update_asset_info();
}

Asset::~Asset()
{
}
void Asset::update_asset_info()
{
    // do it.
    std::unique_ptr<sql_instruments> instruments;
    sql_instrument xinstr;

    conn->lock();
    instruments.reset(new sql_instruments(conn->connection()) );
    bool has_i = false;
    if (asset_id) {
        has_i = instruments->pull_data(asset_id, xinstr);
    } else if (!asset_name.empty()) {
        has_i = instruments->pull_data(asset_name, xinstr);
    }
    conn->unlock();
    if (!has_i) {
        throw std::runtime_error("Instrument not found");
    }
    exchange.init(xinstr.open_market, xinstr.trading_day);
    _commission = Commission(xinstr.commission);
    asset_id = xinstr.id;
    asset_name = xinstr.name;
}

std::tuple<double,double,double,double> Asset::fetchValue(tiempo::datetime date)
{
    conn->lock();
    double open,high,low,close;
    open=high=low=close=(double)-1.0;
    std::cout <<"try org search"<< std::endl;
    std::shared_ptr<sql::Connection> con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`Candles` WHERE `instrument_id` = %d AND date='%s'") %
                         get_id() %
                         date.to_string("SQL")).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    //In case no results, try again broadening the search
    if (res->rowsCount() < 1)
    {   std::cout <<"try broad search"<< std::endl;
        tiempo::datetime from = date-std::chrono::seconds(SECONDS_SPAN_FETCH_QUERY);
        tiempo::datetime to   = date+std::chrono::seconds(SECONDS_SPAN_FETCH_QUERY);
        
        query = (boost::format("SELECT * FROM `Main`.`Candles` WHERE `instrument_id` = %d AND date BETWEEN '%s' AND '%s'") %
                 get_id() %
                 from.to_string() %
                 to.to_string() ).str();
        std::unique_ptr<sql::Statement> stmt2(con->createStatement());
        std::unique_ptr<sql::ResultSet> res2(stmt->executeQuery(query));
        if (res2->rowsCount() == 1)
        {
            std::cout <<" broad search worked"<< std::endl;
            res2->first();
            open = res2->getDouble("open");
            high = res2->getDouble("high");
            low = res2->getDouble("low");
            close = res2->getDouble("close");
            conn->unlock();
            return std::make_tuple(open,high,low,close);
        }
        else //if that does not work either, go Kalman
        {
            
            std::cout <<"try Kalman search"<< std::endl;
            
            query = (boost::format("SELECT * FROM `Main`.`CandlesKalman` WHERE `instrument_id` = %d AND date='%s'") %
                     get_id() %
                     date.to_string("SQL")).str();
            std::unique_ptr<sql::Statement> stmt3(con->createStatement());
            std::unique_ptr<sql::ResultSet> res3(stmt->executeQuery(query));
            
            if (res3->rowsCount() < 1){
                
                std::cout <<"try broad Kalman broad search"<< std::endl;
                tiempo::datetime from = date-std::chrono::seconds(SECONDS_SPAN_FETCH_QUERY);
                tiempo::datetime to   = date+std::chrono::seconds(SECONDS_SPAN_FETCH_QUERY);
                
                query = (boost::format("SELECT * FROM `Main`.`CandlesKalman` WHERE `instrument_id` = %d AND date BETWEEN '%s' AND '%s'") %
                         get_id() %
                         from.to_string() %
                         to.to_string() ).str();
                std::unique_ptr<sql::Statement> stmt4(con->createStatement());
                std::unique_ptr<sql::ResultSet> res4(stmt->executeQuery(query));
                if (res4->rowsCount() == 1)
                {
                    std::cout <<"Kalman broad worked"<< std::endl;
                    res4->first();
                    open = 0;//res4->getDouble("open");
                    high = 0;//res4->getDouble("high");
                    low = 0;//res4->getDouble("low");
                    close = res4->getDouble("close");
                    conn->unlock();
                    return std::make_tuple(open,high,low,close);
                }
                else
                {// return what you have
                    std::cout <<"Nothing worked"<< std::endl;
                    conn->unlock();
                    return std::make_tuple(open,high,low,close);
                }
            
            }else
            {
                std::cout <<"Kalman worked"<< std::endl;
                res3->first();
                open = 0;//res3->getDouble("open");
                high = 0;//res3->getDouble("high");
                low = 0;//res3->getDouble("low");
                close = res3->getDouble("close");
                conn->unlock();
                return std::make_tuple(open,high,low,close);
            
            }
        }
    }
    //in case everything went swell
    else
    {   std::cout <<"Org worked"<< std::endl;
        res->first();
        open = res->getDouble("open");
        high = res->getDouble("high");
        low = res->getDouble("low");
        close = res->getDouble("close");
        conn->unlock();
        return std::make_tuple(open,high,low,close);
    }
    conn->unlock();
    return std::make_tuple(open,high,low,close);
}


void Asset::setInputType(std::string type)
{
    this->type = type;
}


void Asset::init(const tiempo::duration& how_far_back)
{
    auto time_now = tiempo::now();
    std::cout << boost::format("[%s] Initializing Instrument %s.")
        % time_now.to_string() % asset_name << std::endl;

    tiempo::datetime_type date_2 = time_now;
    tiempo::datetime_type date_1 = date_2 - how_far_back;
   
    conn->lock();
    
    if (!rawdata) {
        rawdata = std::make_shared<sql_rawdata>(conn->connection());
    }
    if(type.compare("spread") == 0)
        rawdata->load_data_spread(asset_id, date_1, date_2, tix);
    else if(type.compare("mid") == 0)
        rawdata->load_data_midprice(asset_id, date_1, date_2, tix);
    else
        rawdata->load_data(asset_id, date_1, date_2, tix);



    conn->unlock();
    std::cout << boost::format("[%s] Instrument %s has been initialized.") 
        % time_now.to_string() % asset_name << std::endl;
}

void Asset::init(const tiempo::duration& how_far_back,tiempo::datetime  time_now)
{
    //auto time_now = tiempo::now();
    std::cout << boost::format("[%s] Initializing Instrument %s.") % time_now.to_string() % asset_name << std::endl;
    
    tiempo::datetime_type date_2 = time_now;
    tiempo::datetime_type date_1 = date_2 - how_far_back;
     std::cout << boost::format("date from %s to %s") % date_1.to_string() % date_2.to_string() << std::endl;
    conn->lock();
    
    if (!rawdata) {
        //std::cout << "NO HASTIX !!!!!!!!!!!!!!" << std::endl;
        rawdata = std::make_shared<sql_rawdata>(conn->connection());
    }
    if(type.compare("spread") == 0)
        rawdata->load_data_spread(asset_id, date_1, date_2, tix);
    else if(type.compare("mid") == 0)
        rawdata->load_data_midprice(asset_id, date_1, date_2, tix);
    else
        rawdata->load_data(asset_id, date_1, date_2, tix);
    conn->unlock();
    std::cout << boost::format("[%s] Instrument %s has been initialized.")
    % time_now.to_string() % asset_name << std::endl;
}

void Asset::update_asset_data()
{
    conn->lock();
    // update the instrument information.
    auto time_now = tiempo::now();
    
    if (!rawdata) {
        std::cout << "Reseting the rawdata database" << std::endl;
        rawdata = std::make_shared<sql_rawdata>(conn->connection());
    }
    auto date_tix = tix.rbegin()->first;
    //std::cout << "Should be updating from teh date " << date_tix << std::endl;
    tix_series tix_add;
    rawdata->load_data(asset_id, date_tix, time_now, tix_add);
    //std::cout << "New data: " << tix_add.size() << std::endl;
    for(const auto& t : tix_add) tix[t.first] = t.second;
    
#ifdef HASTIX
    //std::cout << "Updating the ticks..." << std::endl;
    // tick by tick
    if (!ticksdb) {
        std::cout << "Reseting the ticks database" << std::endl;
        ticksdb = std::make_shared<sql_ticks>();
    }
    if (!tbt.empty() ) {
        auto date_tbt = tbt.back().d;
        TickByTick tbt_add;
        ticksdb->load_ticks(asset_id, date_tbt, tbt_add);
        if (!tbt_add.empty()) {
            while(!tbt.empty() && tbt.back().d > tbt_add.front().d)
                tbt.pop_back();
            for(const auto& z : tbt_add) tbt.push_back(z);
        }
    }
#endif
    conn->unlock();


}

void Asset::update()
{
    update_asset_info();
    update_asset_data();
}


void Asset::clear()
{
    VAsset::clear();
    exchange.clear();
    _commission.clear();
}
    
