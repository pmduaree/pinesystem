#ifndef __transaction_struct__
#define __transaction_struct__

//STRUCT OF TRANSACTION
struct Transaction
{
	int side; //1 for buy, -1 for sell
	int position; //1 for long, -1 for short
	int asset_id; //id of the asset
	//std::string date; //date of the transaction
	double price; //price at which the transaction was made
};

#endif 