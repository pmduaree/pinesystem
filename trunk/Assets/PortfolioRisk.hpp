#ifndef __portfolio_risk__
#define __portfolio_risk__

#include <iostream>
#include <chrono>
#include <algorithm>
#include <boost/format.hpp>
#include <armadillo>

#include "../SQL/sql_connection.hpp"
#include "Transaction.hpp"

using namespace arma;

class PortfolioRisk
{
	std::vector<int> assets_ids; //assets id fetched from the database 
	std::vector<std::string> assets_names; //names of the assets 
	mat P; //portfolio
	vec weight; //weight of the portfolio
	vec SD_col; //colum containing the SD of every asset.
    std::shared_ptr<sql_connection> conn; //sql connector
    std::string dateFrom, dateTo; // dates to calculate all the risks
    std::vector<Transaction> T; //vector of transactions;

public:
	PortfolioRisk(int portfolioid, std::shared_ptr<sql_connection> conn_, std::string dateFrom_, std::string dateTo_, std::vector<Transaction> T_);

	double getPortfolioVolatility();
	void printPortfolioVolatility();

	std::vector<double> getRealizedRisk();
	void printRealizedRisk();

	double getBetaExposure();
	void printBetaExposure();

	std::vector<double> getDollarExposure();
	void printDollarExposure();

	void printAll();


private:
	mat fetchPortfolioData();
	std::vector<int> fetchPortfolioIds(int portfolio_id);
	vec calculateWeightColumn();
	mat calculateCovarianceMatrix();
	int getIndexOfVector(int id);
	double calculateSD(std::vector<double> X);

};

#endif
