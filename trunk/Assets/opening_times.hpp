#ifndef __opening_times__
#define __opening_times__

#include <set>
#include <vector>
#include <string>

#include "../Tiempo/tiempo.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>

class OpeningTimes
{
  public:
    OpeningTimes();
    OpeningTimes(const std::string& text);
    OpeningTimes(const OpeningTimes& z);
    OpeningTimes& operator=(const OpeningTimes& z);

    void clear();
    void init(const std::string& open_str);
    bool is_open(const tiempo::datetime& date) const;
    tiempo::duration time_to_open(const tiempo::datetime& date) const;
    tiempo::duration time_to_close(const tiempo::datetime& date) const;
    
  private:
    void insert_opening_period(const std::string& opx);
    size_t get_from_sun(const std::string& opx) const;
    tiempo::datetime next_close(const tiempo::datetime& date) const;
    tiempo::datetime next_open(const tiempo::datetime& date) const;
    tiempo::datetime prev_open(const tiempo::datetime& date) const;
    tiempo::datetime prev_close(const tiempo::datetime& date) const;
    
  private:
    std::set<size_t> time_close;
    std::set<size_t> time_open;
};



#endif
