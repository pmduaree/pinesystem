#ifndef virtual_asset_hpp
#define virtual_asset_hpp

#include <memory>
#include <string>

#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"

#undef HASTIX

class VAsset
{
  public:
    VAsset() {}
    virtual ~VAsset() {}
    
    // initialize the asset taking prices 'how_far_back' using the connection to the 
    virtual void init(const tiempo::duration& how_far_back) = 0;
    virtual void init(const tiempo::duration& how_far_back,tiempo::datetime offset_date) = 0;
    virtual void update() = 0;
    
    // reset all class members
    virtual void clear();
    
    //-----------------------------------------------------------------------
    // Informative functions
    //-----------------------------------------------------------------------
    // get a container containing all ticks
    const tix_series& get_ticks() const { return tix; }
    
    // get price at datetime 'date' (i.e. the last trade at t <= date)
    double get_price(const tiempo::datetime_type& date) { return tix.get_price(date); }
    
    // get realized volume in the interval [date - back, date]
    size_t get_cum_volume(const tiempo::datetime_type& date, const tiempo::duration& back) const;
    
    // get the last price.
    double get_last_price() const { return tix.get_last_price(); }
    
    // get the last date.
    const tiempo::datetime_type& get_last_date() const { return tix.get_last_date(); }
    
  protected:
    tix_series tix;
#ifdef HASTIX
    TickByTick tbt;
#endif

};


#endif
