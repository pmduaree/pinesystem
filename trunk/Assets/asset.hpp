#ifndef __asset_hpp_
#define __asset_hpp_

#include <memory>
#include <string>
#include <boost/format.hpp>

#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"
#include "../SQL/sql_connection.hpp"
#include "../SQL/sql_instruments.hpp"
#include "../SQL/sql_rawdata.hpp"
#include "commission.hpp"
#include "exchange.hpp"
#include "vasset.hpp"


#define SECONDS_SPAN_FETCH_QUERY 5

#undef HASTIX

class Asset: public VAsset
{
    std::shared_ptr<sql_connection> conn;
    int asset_id;
    std::string asset_name;
    std::string type; // close, midprice or spread
    

  public:
    Asset(std::shared_ptr<sql_connection> conn_, int instrumentid);
    Asset(std::shared_ptr<sql_connection> conn_, const std::string& ticker);
    ~Asset();
    
    // initialize the asset taking prices 'how_far_back' using the connection to the 
    void init(const tiempo::duration& how_far_back);
    void init(const tiempo::duration& how_far_back,tiempo::datetime date_offset);
    void update();
    void setInputType(std::string type);
    
    // reset all class members
    void clear();
    std::tuple<double,double,double,double> fetchValue(tiempo::datetime );

    
    //-----------------------------------------------------------------------
    // Informative functions
    //-----------------------------------------------------------------------
    const std::string& get_name() const { return asset_name; }
    int get_id() const { return asset_id; }
    double get_commission(double trade_value) const { return _commission(trade_value); }
    
    const Exchange& get_exchange() const { return exchange; }
    
  private:
    Exchange exchange;
    Commission _commission;
    std::shared_ptr<sql_rawdata> rawdata;
#ifdef HASTIX
    std::shared_ptr<sql_ticks> ticksdb;
#endif

    void update_asset_info();
    void update_asset_data();

};




#endif

