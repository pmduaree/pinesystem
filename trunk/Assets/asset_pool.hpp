#ifndef __asset_pool__
#define __asset_pool__

#include <vector>
#include <memory>
#include "../SQL/sql_connection.hpp"
#include "asset.hpp"

#include <unordered_map>

class AssetPool
{
  public:
    AssetPool(std::shared_ptr<sql_connection> conn_);
    void add_asset(const std::string& asset_name);
    void add_asset(int asset_id);
    void init(const tiempo::duration& how_far);
    void update();
    std::shared_ptr<Asset> get_asset(const std::string& asset_name);
    std::shared_ptr<Asset> get_asset(int asset_id);

  private:
    std::shared_ptr<sql_connection> conn;
    std::unordered_map<std::string, std::shared_ptr<Asset> > _pool;
};

#endif

