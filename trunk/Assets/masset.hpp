#ifndef __Masset_hpp_
#define __Masset_hpp_

#include <memory>
#include <string>

#include <boost/format.hpp>
#include <initializer_list>

#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"
#include "../SQL/sql_connection.hpp"
#include "../SQL/sql_instruments.hpp"
#include "../SQL/sql_rawdata.hpp"
#include "commission.hpp"
#include "exchange.hpp"
#include "vasset.hpp"
#include "asset.hpp"

#undef HASTIX

class MAsset: public VAsset
{
    std::shared_ptr<sql_connection> conn;
    std::vector<int> asset_id;
    std::vector<std::string> asset_name;
    
public:
   // MAsset(){};
    //std::initializer_list<int> ticker
    MAsset(std::shared_ptr<sql_connection> conn_, std::initializer_list<int> ticker);
    MAsset(std::shared_ptr<sql_connection> conn_, std::initializer_list<const char*> ticker);
    MAsset(std::shared_ptr<sql_connection> conn_, int portfolio_id);
    ~MAsset();
    
    // initialize the asset taking prices 'how_far_back' using the connection to the
    void init(const tiempo::duration& how_far_back);
    void init(const tiempo::duration& how_far_back,tiempo::datetime time_offset);
    void update();
    
    // reset all class members
    void clear();
    
    void setInputType(std::string type);

    
    //-----------------------------------------------------------------------
    // Informative functions
    //-----------------------------------------------------------------------
    //const std::string& get_name() const { return  std::string("Multiple assets no single name");/*asset_name;*/ } 
    std::vector<std::string> get_names(){return asset_name;}
    int get_id() const { return 0;/*asset_id;*/ }
    std::vector<int> get_ids(){return asset_id;}
    double get_commission(double trade_value) const { return _commission(trade_value); }
    
    const Exchange& get_exchange() const { return exchange; }
    tix_series get_ONE_Asset_ticks(int id )
          {for(auto x:_Assets) if ( x->get_id() == id   )return x->get_ticks();
           return  tix_series();}
    
    
    void update_ONE_Asset_ticks(int id )
    {for(auto x:_Assets) if ( x->get_id() == id   )return x->update();}
private:
    int N;
    Exchange exchange;
    Commission _commission;
    std::shared_ptr<sql_rawdata> rawdata;
public:    
std::vector<std::shared_ptr<Asset> > _Assets ;
    
private:
#ifdef HASTIX
    std::shared_ptr<sql_ticks> ticksdb;
#endif
public:    
    void Merge_Assets();
private:
    void Merge_ComissionsAndExchanges();
    double CalculateMixTix(std::vector<double> prices);
    std::vector<int> Fetch_PortofolioID(int ticker);
    
};


#endif

