#include "asset_pool.hpp"
AssetPool::AssetPool(std::shared_ptr<sql_connection> conn_)
    : conn(conn_) {}

void AssetPool::add_asset(const std::string& asset_name)
{
    auto asset = std::make_shared<Asset>(conn, asset_name);
    _pool.emplace(asset_name, asset);
}

void AssetPool::add_asset(int asset_id)
{
    auto asset = std::make_shared<Asset>(conn, asset_id);
    if (!_pool.count(asset->get_name())) {
        _pool.emplace(asset->get_name(), asset);
    }
}

void AssetPool::init(const tiempo::duration& how_far)
{
    for(auto& z : _pool) z.second->init(how_far);
}

void AssetPool::update()
{
    for(auto& z : _pool) z.second->update();
}

std::shared_ptr<Asset> AssetPool::get_asset(const std::string& asset_name)
{
    auto it = _pool.find(asset_name);
    return (it == _pool.end() ? std::shared_ptr<Asset>() : it->second);
}

std::shared_ptr<Asset> AssetPool::get_asset(int asset_id)
{
    auto it = _pool.begin();
    while(it != _pool.end() && it->second->get_id() != asset_id) ++it;
    return (it == _pool.end() ? std::shared_ptr<Asset>() : it->second);
}

