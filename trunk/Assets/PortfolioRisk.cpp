#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include "PortfolioRisk.hpp"

//FUNCTIONS THAT FETCHES THE DATA
PortfolioRisk::PortfolioRisk(int portfolioid, std::shared_ptr<sql_connection> conn_,
 std::string dateFrom_, std::string dateTo_, std::vector<Transaction> T_)
 : conn(conn_), dateFrom(dateFrom_), dateTo(dateTo_), T(T_)
{
	assets_ids = fetchPortfolioIds(portfolioid);
	P = fetchPortfolioData();
	//std::cout << T.size() << std::endl;
	//P.print();
}

std::vector<int> PortfolioRisk::fetchPortfolioIds(int portfolio_id)
{
	std::vector<int> ids;
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("SELECT * FROM Main.Portfolio JOIN Main.Instruments ON portfolio_id=%d AND Main.Instruments.id = Main.Portfolio.instrument_id") % portfolio_id).str();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() > 0) {
    	while(res->next())
    	{
	        ids.push_back(res->getInt("instrument_id"));
	    	assets_names.push_back(res->getString("name"));
	    }
    }
    conn->unlock();
    return ids;
}
mat PortfolioRisk::fetchPortfolioData()
{
    conn->lock();
    auto con = conn->connection();
    //P.set_size(100, assets_ids.size());
    std::vector<std::vector<double>> aux_mat;

    //fetch the data from DB
	for(unsigned int i = 0; i < assets_ids.size(); i++)
	{
	    std::string query = (boost::format("SELECT close FROM Main.Candles"
	    	" WHERE instrument_id=%d AND date BETWEEN \"%s\" AND \"%s\" ORDER BY instrument_id, date DESC")
	    	 % assets_ids[i] % dateFrom % dateTo).str();

	    std::shared_ptr<sql::Statement> stmt(con->createStatement());
	    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
	    if (res->rowsCount() > 0) {
	        std::vector<double> aux_vec;
	        while(res->next())
	        	aux_vec.push_back(res->getDouble("close"));
	        aux_mat.push_back(aux_vec);
    	}
	}
    conn->unlock();

	//assings the data from the DB to armadillo libs
	int unsigned min = 9999999;
	for(unsigned int i = 0; i < aux_mat.size(); i++)
		if(aux_mat[i].size() < min)
			min = aux_mat[i].size();
	mat X(aux_mat.size(), min);
	//X.print();
	for(unsigned int i = 0; i < aux_mat.size(); i++)
		for(unsigned int j = 0; j < min; j++)
			X(i,j) = aux_mat[i][j];
    return X;
}

//CALCULATORS OF EVERYTHING

//Calculate the weight of every asset. It is done by the money invested in each asset
vec PortfolioRisk::calculateWeightColumn()
{
	vec X(assets_ids.size());
	//X.fill(1/assets_ids.size());
	if(T.size() <= 0)
		return X;
	std::vector<double> prices(assets_ids.size(),0); //[id]: invested
	double total = 0;

	for(unsigned int i = 0; i < T.size(); i++)
	{
		int index = getIndexOfVector(T[i].asset_id);
		prices[index] += T[i].price;
		total += T[i].price;
	}
	for(unsigned int i = 0; i < X.n_rows; i++)
		X(i) = prices[i]/total;
	return X;
}
mat PortfolioRisk::calculateCovarianceMatrix()
{
	return cov(trans(P));
}

//HANDLERS 
double PortfolioRisk::getPortfolioVolatility()
{
	if(weight.is_empty())
		weight = calculateWeightColumn();

	if(weight.is_empty() || T.size() == 0)
		return 0;
	mat covariance = calculateCovarianceMatrix();
	mat A = trans(weight)*covariance*weight;
	//A.print();
	//std::cout << A(0,0) << std::endl;
	return A(0,0);
}
std::vector<double> PortfolioRisk::getRealizedRisk()
{
	if(T.size() == 0)
	{
		std::vector<double> meh;
		for(unsigned int i = 0; i < assets_ids.size() ; i++)
			meh.push_back(0);
		return meh;
	}
	else
	{
		std::vector<double> meh(1,0);
		std::vector<std::vector<double>> aux(assets_ids.size(), meh); //contains all the p&l info
		std::map<int, double> savePosition;
		for(unsigned int i = 0; i < T.size(); i++)
		{
			if(T[i].position == 1) //if its long
			{
				if(T[i].side == 1) //buy, open the position
					savePosition[T[i].asset_id] = T[i].price;
				else if(T[i].side == -1) //sell, close the position
					aux[getIndexOfVector(T[i].asset_id)].push_back(savePosition[T[i].asset_id] - T[i].price);
			}
			else if(T[i].position == -1)
			{
				if(T[i].side == -1) //sell, open the position
					savePosition[T[i].asset_id] = T[i].price;
				else if(T[i].side == 1) //buy, close the position
					aux[getIndexOfVector(T[i].asset_id)].push_back(T[i].price - savePosition[T[i].asset_id]);
			}
		}
		std::vector<double> m;
		for(unsigned int i = 0; i < aux.size(); i++)
		{
			vec X(aux[i]);
			//X.print();
			//std::cout << P(i,0) << endl;
			m.push_back(stddev(X)/P(i,0));
		}
		return m;
	}
}
double PortfolioRisk::getBetaExposure()
{
	if(weight.is_empty() || T.size() == 0)
		return 0;
	return stddev(weight);
}
std::vector<double> PortfolioRisk::getDollarExposure()
{
	if(T.size() == 0)
	{
		std::vector<double> meh;
		for(unsigned int i = 0; i < assets_ids.size() ; i++)
			meh.push_back(0);
		return meh;
	}
	else
	{
		std::vector<double> longProfit(assets_ids.size(), 0);
		std::vector<double> shortProfit(assets_ids.size(), 0);
		for(unsigned int i = 0; i < T.size(); i++)
		{
			if(T[i].position == 1) //if its long
			{
				if(T[i].side == 1) //buy
					longProfit[getIndexOfVector(T[i].asset_id)] += T[i].price;
				else if(T[i].side == -1) //sell
					longProfit[getIndexOfVector(T[i].asset_id)] -= T[i].price;
			}
			else if(T[i].position == -1)
			{
				if(T[i].side == -1) //sell
					shortProfit[getIndexOfVector(T[i].asset_id)] -= T[i].price;
				else if(T[i].side == 1) //buy
					shortProfit[getIndexOfVector(T[i].asset_id)] += T[i].price;	
			}
		}
		std::vector<double> exposure;
		for(unsigned int i = 0; i < longProfit.size(); i++)
			exposure.push_back((longProfit[i] - shortProfit[i])/P(i,0));
		return exposure;
	}
}
//PRINTERS
void PortfolioRisk::printBetaExposure()
{
	printf("<result>Beta Exposure: %f </result>\n", getBetaExposure());
}
void PortfolioRisk::printRealizedRisk()
{
	printf("<result>Realized Risk: ");
	std::vector<double> aux = getRealizedRisk();
	for(unsigned int i = 0; i < aux.size(); i++)
		printf("%s=%f ", assets_names[i].c_str(), aux[i]);
	printf("</result>\n");

}
void PortfolioRisk::printDollarExposure()
{
	printf("<result>Dollar Exposure: ");
	std::vector<double> aux = getDollarExposure();
	for(unsigned int i = 0; i < aux.size(); i++)
		printf("%s=%f ", assets_names[i].c_str(), aux[i]);
	printf("</result>\n");
}
void PortfolioRisk::printPortfolioVolatility()
{
	printf("<result>Portfolio Volatility: %f </result>\n", getPortfolioVolatility());
}
void PortfolioRisk::printAll()
{
	printPortfolioVolatility();
	printBetaExposure();
	printRealizedRisk();
	printDollarExposure();
}
//AUX 
int PortfolioRisk::getIndexOfVector(int id)
{
	std::vector<int>::iterator it;
	it = std::find(assets_ids.begin(), assets_ids.end(), id);
	return it - assets_ids.begin();
}
double PortfolioRisk::calculateSD(std::vector<double> X)
{
	vec aux(X);
	return stddev(aux); 
}
