#include "vasset.hpp"

void VAsset::clear()
{
    tix.clear();
#ifdef HASTIX
    tbt.clear();
#endif
}

size_t VAsset::get_cum_volume(const tiempo::datetime_type& date, const tiempo::duration& back) const
{
    auto date_1 = date - back;
    size_t v = 0;
    auto it = tix.rbegin();
    while( it != tix.rend() && it->first > date) ++it;
    while( it != tix.rend() && it->first >= date_1) {
        v += it->second.v;
        ++it;
    }
    return v;
}

