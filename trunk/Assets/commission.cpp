#include "commission.hpp"
#include "../Utils/stringparpars.hpp"

Commission::Commission(const std::string& com)
    : percent(0.0), maxusd(0.0)
{
    init(com);
}

Commission::Commission(const Commission& c)
    : percent(c.percent), maxusd(c.maxusd)
{
}

void Commission::init(const std::string& com)
{
    StringParamParser spp(com);
    percent = spp.get<double>("percent", 0.25);
    maxusd = spp.get<double>("maxusd", 5.0);
}

void Commission::clear() 
{
    init("");
}

double Commission::operator()(double trade_value) const
{
    return std::min<double>(percent *  0.01 * fabs(trade_value), maxusd);
}

