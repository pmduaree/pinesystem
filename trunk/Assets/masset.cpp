

#include "masset.hpp"
#include "../SQL/sql_connection.hpp"


MAsset::MAsset(std::shared_ptr<sql_connection> conn_, std::initializer_list<int> Tickers)
: VAsset(),conn(conn_),N(Tickers.size())
{
    N= Tickers.size();
    // std::cout << "attempt to get init assets" << std::endl;
    _Assets = std::vector<std::shared_ptr<Asset> >(N);
    int i=0;
    for (auto it=Tickers.begin(); it!=Tickers.end(); ++it,i++)
    {
        std::cout << "attempt to get asset " << *it << "...    ";
        
        _Assets[i]=std::make_shared<Asset>(conn,*it);
        asset_id.push_back(_Assets[i]->get_id());
        asset_name.push_back(_Assets[i]->get_name());
        std::cout << " Fetched     id=" << _Assets[i]->get_id() << " name=" << _Assets[i]->get_name()<< std::endl;
    }
    setInputType("close");
    
}
std::vector<int> MAsset::Fetch_PortofolioID(int id)
{
    conn->lock();
    std::shared_ptr<sql::Connection> con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`Portfolio` WHERE `portfolio_id`=%d") % id ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    std::vector<int> Tickers;
    while(res->next()) {
        
        Tickers.push_back(  res->getInt("instrument_id")  );
    }
    conn->unlock();
    return Tickers;
}

MAsset::MAsset(std::shared_ptr<sql_connection> conn_, int portfolio_id)
: VAsset(),conn(conn_)
{
    std::vector<int> Tickers = Fetch_PortofolioID(portfolio_id);
    N= Tickers.size();
    // std::cout << "attempt to get init assets" << std::endl;
    _Assets = std::vector<std::shared_ptr<Asset> >(N);
    int i=0;
    for (auto it=Tickers.begin(); it!=Tickers.end(); ++it,i++)
    {
        std::cout << "attempt to get asset " << *it << "...    ";
        
        _Assets[i]=std::make_shared<Asset>(conn,*it);
        asset_id.push_back(_Assets[i]->get_id());
        asset_name.push_back(_Assets[i]->get_name());
        std::cout << " Fetched     id=" << _Assets[i]->get_id() << " name=" << _Assets[i]->get_name()<< std::endl;
    }
}
MAsset::MAsset(std::shared_ptr<sql_connection> conn_, std::initializer_list<const char*> Tickers )
: VAsset(), conn(conn_),N(Tickers.size())
{
    // std::cout << "attempt to get init assets" << std::endl;
    _Assets = std::vector<std::shared_ptr<Asset> >(N);
    int i=0;
    for (auto it=Tickers.begin(); it!=Tickers.end(); ++it,i++)
    {
        std::cout << "attempt to get asset " << *it << "...    ";
        
        _Assets[i]=std::make_shared<Asset>(conn,*it);
        asset_id.push_back(_Assets[i]->get_id());
        asset_name.push_back(_Assets[i]->get_name());
        std::cout << " Fetched     id=" << _Assets[i]->get_id() << " name=" << _Assets[i]->get_name()<< std::endl;
    }
}

void MAsset::setInputType(std::string type)
{
    for (int i =0 ; i!=N ; i++)
        _Assets[i]->setInputType(type);
}

MAsset::~MAsset()
{
}
void MAsset::init(const tiempo::duration& how_far_back)
{
    for (int i =0 ; i!=N ; i++)
        _Assets[i]->init(how_far_back);
   
    try {
      //   Merge_Assets();
    } catch (...) {
        std::cerr  << "error Merging MAsset \n" << std::endl;
    }
   
}
void MAsset::init(const tiempo::duration& how_far_back,tiempo::datetime date)
{
    for (int i =0 ; i!=N ; i++){
        _Assets[i]->init(how_far_back,date);
       // std::cout  << i << std::endl;
    }
    std::cout << "Attempt to merge " << std::endl;
  //  Merge_Assets();
}
void MAsset::update()
{
    for (int i =0 ; i!=N ; i++)
        _Assets[i]->update();
//    Merge_Assets();
}
double MAsset::CalculateMixTix(std::vector<double> prices)
{
    if (prices.size() == 2 )
        return prices[0] - prices[1];
    else return 0;
}
void MAsset::Merge_Assets()
{
    std::cout << "Merging assets.... " ;
    
    // tix_series  Global;
    tix.clear();
    auto TIX  = _Assets[0]->get_ticks();
    for (auto it = TIX.rbegin(); it != TIX.rend() ; it++)
    {
        std::vector<double> price;
        for (int i=0; i!=N ; i++)
        {
            try {
                price.push_back( _Assets[i]->get_price(it->first)  );
                 //std::cout << "   Asset " << asset_name[i] << " -> " << _Assets[i]->get_price(it->first) << std::endl ;
            } catch (...) {
                // throw std::runtime_error("Unable to get price form Asset, merge Asset not possiblel!");
                 std::cerr  << "error Merging MAsset \n" << std::endl;
            }
        }
        auto& x= tix[it->first];
        x.p= CalculateMixTix(price);
       //  std::cout << "Merged " <<  it->first << " --> " << x.p << std::endl << std::endl;
    }
    
    std::cout << "Merged from " << TIX.begin()->first << " to " << TIX.rbegin()->first << std::endl;
    
    //tix.clear();
    //tix = Global;
    /*
     for (int i =0 ; i!=N ; i ++)
     {
     std::cout << "Asset " << asset_name[i] << " last date " ;
     auto tix = _Assets[i]->get_ticks();
     auto date = _Assets[i]->get_last_date();
     std::cout << date << std::endl;
     
     std::cout << "Tix " << std::endl;
     std::cout << "end of ticks " << std::endl;
     */
    std::cout << " Done!" << std::endl;
    
}
void MAsset::clear()
{
    for (int i =0 ; i!=N ; i++)
    {
        _Assets[i]->VAsset::clear();
    }
    exchange.clear();
    _commission.clear();
}


