#ifndef __exchange_hpp_
#define __exchange_hpp_


#include "opening_times.hpp"

class Exchange
{
  public:
    void init(const std::string& s_om, const std::string& s_td)
    {
        open_market = OpeningTimes(s_om);
        trading_day = OpeningTimes(s_td);
    }
    
    // trading time queries
    bool is_market_open(const tiempo::datetime& date) const 
        { return open_market.is_open(date); }

    bool is_in_trading_day(const tiempo::datetime& date) const 
        { return trading_day.is_open(date); }

    tiempo::duration time_to_open_trading_day(const tiempo::datetime& date) const
        { return trading_day.time_to_open(date); }

    tiempo::duration time_to_end_trading_day(const tiempo::datetime& date) const
        { return trading_day.time_to_close(date); }
    tiempo::duration time_to_market_close(const tiempo::datetime& date) const
        { return open_market.time_to_close(date); }

    void clear()
    {
        open_market.clear();
        trading_day.clear();
    }

  private:
    OpeningTimes open_market;
    OpeningTimes trading_day;
};


#endif
