#include "opening_times.hpp"

#include <set>
#include <vector>
#include <string>

#include "../Tiempo/tiempo.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>


OpeningTimes::OpeningTimes()
{
    clear();
}

OpeningTimes::OpeningTimes(const std::string& text)
{
    init(text);
}

OpeningTimes::OpeningTimes(const OpeningTimes& z)
    : time_close(z.time_close),
      time_open(z.time_open)
{
}

OpeningTimes& OpeningTimes::operator=(const OpeningTimes& z)
{
    time_close = z.time_close;
    time_open = z.time_open;
    return *this;
}



void OpeningTimes::clear()
{
    init("24/7");
}

void OpeningTimes::init(const std::string& open_str)
{
    time_open.clear();
    time_close.clear();
    
    if (open_str == "24/7" || open_str.empty()) {
        // no opening and closing time of the week.
        // means that we will close the position on sunday 0:00 and open it on sunday 1:00
        time_open.insert(3600);
        time_close.insert(0);
        return;
    }
    // do the opening times.
    // first split over the commas, if there's any
    if (open_str.find(",") != std::string::npos) {
        std::vector<std::string> items;
        boost::split(items, open_str, boost::is_any_of(","));
        for(auto& itm : items) {
            boost::trim(itm);
            insert_opening_period(itm);
        }
    } else {
        insert_opening_period(open_str);
    }
    // Mon@03:00:00->Sun@03:00:00,
    //std::cout << "Initializing the opening times with " << open_str << std::endl;
}

bool OpeningTimes::is_open(const tiempo::datetime& date) const
{
    return (prev_close(date) < prev_open(date)) && next_close(date) > date;
}

tiempo::duration OpeningTimes::time_to_open(const tiempo::datetime& date) const
{
    tiempo::duration cdif = next_open(date) - date;
    return cdif;
}

tiempo::duration OpeningTimes::time_to_close(const tiempo::datetime& date) const
{
    tiempo::duration cdif = next_close(date) - date;
    return cdif;
}


size_t OpeningTimes::get_from_sun(const std::string& opx) const
{
    // Day@TIME
    auto p1 = opx.find('@');
    if (p1 == std::string::npos) throw std::runtime_error("wrong date in zzz");
    auto day = opx.substr(0,p1);
    auto tme = opx.substr(p1+1);

    // parse days.
    const std::vector<std::string> days = {{ "Sun","Mon","Tue","Wed","Thu","Fri","Sat"}};

    auto it = days.begin();
    while(it != days.end() && it->compare(day) ) ++it;
    if (it == days.end()) {
        std::cerr << "Strange day: " << day << std::endl;
        throw std::runtime_error("invalid day in zzz");
    }

    size_t ndays = it - days.begin();
    size_t dff = ndays * 24 * 3600;
    // now also parse the time.
    if (tme.find(":") != std::string::npos) {
        std::vector<std::string> hms;
        boost::split(hms, tme, boost::is_any_of(":"));
        dff += boost::lexical_cast<size_t>(hms[0]) * 3600;
        if (hms.size() >= 2) dff += boost::lexical_cast<size_t>(hms[1]) * 60;
        if (hms.size() >= 3) dff += boost::lexical_cast<size_t>(hms[2]);
    } else {
        // it's just hours
        dff += boost::lexical_cast<size_t>(tme) * 3600;
    }
    return dff;
}

void OpeningTimes::insert_opening_period(const std::string& opx)
{
    // it must be of the form 'Day@TIME->Day@TIME'
    auto z1 = opx.find("->");
    if (z1 == std::string::npos)
        throw std::runtime_error("invalid opening time poerido");
    size_t dif_1 = get_from_sun(opx.substr(0, z1));
    size_t dif_2 = get_from_sun(opx.substr(z1+2));
    time_close.insert(dif_2);
    time_open.insert(dif_1);
}

tiempo::datetime OpeningTimes::next_close(const tiempo::datetime& date) const
{
    assert( !time_close.empty());
    auto from_sun = date.from_sunday();
    size_t from_sun_s = from_sun.seconds();
    auto it = time_close.begin();
    while(it != time_close.end() && *it < from_sun_s) ++it;
    
    tiempo::datetime rv = date - from_sun;
    if (it == time_close.end())
        rv += std::chrono::seconds(*time_close.begin()) + std::chrono::hours(24*7);
    else
        rv += std::chrono::seconds(*it);
    return rv;
}

tiempo::datetime OpeningTimes::next_open(const tiempo::datetime& date) const
{
    assert( !time_open.empty());
    auto from_sun = date.from_sunday();
    size_t from_sun_s = from_sun.seconds();
    auto it = time_open.begin();
    while(it != time_open.end() && *it < from_sun_s) ++it;

    tiempo::datetime rv = date - from_sun;
    if (it == time_open.end())
        rv += std::chrono::seconds(*time_open.begin()) + std::chrono::hours(24*7);
    else
        rv += std::chrono::seconds(*it);
    return rv;
}


tiempo::datetime OpeningTimes::prev_open(const tiempo::datetime& date) const
{
    assert( !time_open.empty());
    auto from_sun = date.from_sunday();
    size_t from_sun_s = from_sun.seconds();
    auto it = time_open.rbegin();
    while( it != time_open.rend() && *it > from_sun_s) ++it;
    tiempo::datetime rv = date - from_sun;
    if (it == time_open.rend())
        rv += std::chrono::seconds(*time_open.rbegin()) - std::chrono::hours(24*7);
    else
        rv += std::chrono::seconds(*it);
    return rv;
}


tiempo::datetime OpeningTimes::prev_close(const tiempo::datetime& date) const
{
    assert( !time_close.empty());
    auto from_sun = date.from_sunday();
    size_t from_sun_s = from_sun.seconds();
    auto it = time_close.rbegin();
    while( it != time_close.rend() && *it > from_sun_s) ++it;
    tiempo::datetime rv = date - from_sun;
    if (it == time_close.rend())
        rv += std::chrono::seconds(*time_close.rbegin()) - std::chrono::hours(24*7);
    else
        rv += std::chrono::seconds(*it);
    return rv;
}


