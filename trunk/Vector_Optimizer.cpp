//
//  Vector_Optimizer.cpp
//  
//
//  Created by David de la Rosa on 7/7/14.
//
//
#include <cstdlib>
#include <thread>
#include <atomic>
#include <unordered_map>

#include "Vector_Optimizer.hpp"

Vector_Optimizer::Vector_Optimizer(std::shared_ptr<sql_connection> __conn,int __StrategyID):
conn(__conn),Strategy_ID(__StrategyID)
{ std::cout << "loading" << std::endl;

    auto  __QT = std::make_shared<Pseudo_Trader>(conn,Strategy_ID);
    SignalerEngine_ID = __QT->get_SignalerID();
    EncoderEngine_ID  = __QT->get_EncoderID();
    Load_Parameters();
}
void Vector_Optimizer::Load_Parameters()
{
    conn->lock();
    auto _con = conn->connection();
    std::string query = (boost::format("SELECT SE.Name as Name, Minval,\
                                       MaxVal,PT.Name as Type \
                                       FROM `Main`.`SignalerEnginesParam` SE\
                                       INNER JOIN `Main`.`ParameterTypes` PT ON\
                                       SE.type = PT.id\
                                       WHERE `SignalerType_id` =%d") % SignalerEngine_ID ).str();
    std::shared_ptr<sql::Statement> stmt(_con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    
    std::cout << "Parameters on BD= " << res->rowsCount() << std::endl;
    
    if (res->rowsCount() > 0)
      while(res->next())
      {
          
            std::string type = res->getString("Type");
            std::string name = res->getString("Name");
            
            std::cout << (boost::format("name=%s type=%s \n")% name %type ).str();
            if (type == "Int")
            {
                int min_int     = res->getInt("MinVal");
                int max_int     = res->getInt("MaxVal");
                int step_int    = 1;
                 Int_Variables_Param.push_back(  std::make_tuple(name,min_int,max_int,step_int,0) );
            }
            if (type == "Double")
            {
                double min_double   = res->getDouble("MinVal");
                double max_double   = res->getDouble("MaxVal");
                double step_double  = 1.0;
                Doub_Variables_Param.push_back(  std::make_tuple(name, min_double, max_double,step_double,0.0));
            }
     }
    
    conn->unlock();
}
void Vector_Optimizer::Populate_DB(int scale)
{
    if (scale < 1 ) scale = 1;
    if (scale > 10) scale = 10;
    
    // calculate scale step
    for (int unsigned i =0 ; i!=Int_Variables_Param.size(); i++ )
    {
        int span = std::get<Max_Col>(Int_Variables_Param[i]) - std::get<Min_Col>(Int_Variables_Param[i]);
        int scale_local = (int) floor( span / (scale+2) );
        scale_local = (scale_local<1 )? 1 : scale_local;
        std::cout << "scaleInt[" << i << "]=" << scale_local << std::endl;
        std::get<Step_Col >(Int_Variables_Param[i]) = scale_local;
        std::get<Range_Col>(Int_Variables_Param[i]) = span;
    }
    
    for (int unsigned i =0 ; i!=Doub_Variables_Param.size(); i++ )
    {
        double span = std::get<Max_Col>(Doub_Variables_Param[i]) - std::get<Min_Col>(Doub_Variables_Param[i]);
        double scale_local = span / (scale+2);
        scale_local = (scale_local<0.01 )? 0.01 : scale_local;
        std::cout << "scaleDoub[" << i << "]=" << scale_local << std::endl;
        std::get<Step_Col >(Doub_Variables_Param[i]) = scale_local;
        std::get<Range_Col>(Doub_Variables_Param[i]) = span;
    }
   
    
    
    
    
    int F[]={1,5,10,15,20,25};
   /* for(int f=0; f<1 ; f++)
        for (int n=std::get<Min_Col>(Int_Variables_Param[0]); n <  std::get<Max_Col>(Int_Variables_Param[0]); n+= std::get<Step_Col>(Int_Variables_Param[0]))
            for (int m=std::get<Min_Col>(Int_Variables_Param[1]);  m <  std::get<Max_Col>(Int_Variables_Param[1]); m+= std::get<Step_Col>(Int_Variables_Param[1]))
                for (double S =std::get<Min_Col>(Doub_Variables_Param[0]);  S <  std::get<Max_Col>(Doub_Variables_Param[0]); S+= std::get<Step_Col>(Doub_Variables_Param[0]))
                {
                std::cout << " -> " << (boost::format("n=%d,m=%d,Sigma=%0.2f")% n % m % S ).str() << " F=" << F[f] << std::endl;
                push_Param_toDB( (boost::format("n=%d,m=%d,Sigma=%0.2f")% n % m % S  ).str() ,F[f]  );
                    loop++;
                    if (loop > 5 ) break;
                }
    */
    for(int f=0; f<6; f++)
    {
        unsigned long int loop= Loop_Size((unsigned long int) scale);
        
        std::cout << "loop=" << loop << std::endl;
        
        
                    do {
                        std::string Param__="";
                        for (int unsigned i =0 ; i!=Int_Variables_Param.size(); i++ )
                        {
                           if(i>0)
                               Param__ += ",";
                            

                            Param__ +=  std::get<Name_Col>(Int_Variables_Param[i]) ;
                            Param__ += (boost::format("=%d")% RandomInt(std::get<Min_Col>(Int_Variables_Param[i]) , std::get<Max_Col>(Int_Variables_Param[i]))).str();

                        
                        }
                        if (Param__ != "")
                            Param__ += ",";
                            
                        for (int unsigned i =0 ; i!=Doub_Variables_Param.size(); i++ )
                        {
                            if(i>0  )
                                Param__ += ",";
                            
                            Param__ += std::get<Name_Col>(Doub_Variables_Param[i]) ;
                            Param__ += (boost::format("=%0.2f")% RandomDouble(std::get<Min_Col>(Doub_Variables_Param[i]) , std::get<Max_Col>(Doub_Variables_Param[i]))).str();
                        
                        }
                            loop--;
                        
                           std::cout << Param__     <<  std::endl;
                        
                       if ( No_double_Param(Param__, F[f])==1  )
                              push_Param_toDB( Param__ ,F[f]  );
                        
                         }while (  loop> 1    ) ;
        
            }
}
int Vector_Optimizer:: No_double_Param(std::string Param__, int freq)
{
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`OptimizationParamTuples` \
                                       WHERE OptOrder_id = %d AND Params = '%s' AND freq = %d")
                         % OptimizationID
                         % Param__
                         % freq
                          ).str();
    
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::shared_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    
    //std::cout << (boost::format("[ %d ] found form order[%d] Params[%s] freq[%d] \n") % res->rowsCount() % OptimizationID  %Param__ % freq ).str();
    
    if (res->rowsCount() == 0 )
    {
        conn->unlock();
        return 1;
    }
    
    conn->unlock();
    return 0;

}
unsigned long int Vector_Optimizer:: Loop_Size(unsigned long int scale)
{
    
    scale ++ ;

    unsigned long int loop = 1;
    
    for (int unsigned i =0 ; i!=Int_Variables_Param.size(); i++ )
        loop *= (unsigned long int )( scale  * std::get<Range_Col>(Int_Variables_Param[i]) )/2 ;
    
    for (int unsigned i =0 ; i!=Doub_Variables_Param.size(); i++ )
    {
        
        double DoubScale = std::get<Range_Col>(Doub_Variables_Param[i]);
        if (DoubScale <= 1.0 )
            DoubScale  = 1.0;
        loop *= (unsigned long int )( scale  * DoubScale ) ;
    }
    
    
    loop/=8;
    return 5;//loop;
}

int Vector_Optimizer::RandomInt(int low, int high)
{
   // std::cout << (boost::format("Int low[%d] high[%d] \n")% low % high).str();
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> distribution(low,high);
    return  distribution(generator);
}
double Vector_Optimizer::RandomDouble(double low, double high)
{
   //  std::cout << (boost::format("Double low[%f] high[%f] \n")% low % high).str();
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_real_distribution<double> distribution(low,high);
    return  distribution(generator);
}

void Vector_Optimizer:: push_Param_toDB( std::string  Param,int freq)
{
    conn->lock();
    std::string query = (boost::format("INSERT INTO `Main`.`OptimizationParamTuples` \
                                       (OptOrder_id,Params,freq,Status) \
                                       VALUES (%d,\'%s\',%d,0)")
                                        %OptimizationID
                                        % Param % freq ).str();
                        
         auto con = conn->connection();
         std::shared_ptr<sql::Statement> stmt(con->createStatement());
         stmt->execute(query);
    conn->unlock();
}

void Vector_Optimizer:: get_OptipmizationID()
{
    conn->lock();
    OptimizationID = -1;
    auto con = conn->connection();
    std::string query = (boost::format("INSERT INTO `Main`.`OptimizationOrders`\
                                       (Strategy_id,date_from,date_to,money,Status)\
                                       VALUES (%d,\'%s\',\'%s\',%f,0)")
                                        % Strategy_ID
                                        % from_date.to_string("%Y-%m-%d %H:%M:%S")
                                        % to_date.to_string("%Y-%m-%d %H:%M:%S")
                                        %  InvestingMoney  ).str();
    
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    try {
      stmt->executeQuery(query);
    } catch (...) {
        
    }

    stmt.reset(con->createStatement());
    std::shared_ptr<sql::ResultSet>  res(stmt->executeQuery("SELECT LAST_INSERT_ID() AS ID"));

   if (res->rowsCount() > 0)
   {
        res->first();
       std::cout << "res" << std::endl;
        
        OptimizationID = res->getInt("ID");
        if (OptimizationID == 0 )
        {
        conn->unlock();
           throw std::logic_error("Unable to Get OptimizationID due pushing to the DB error!");
        
        }
       std::cout << "ID inserted=" << OptimizationID << std::endl;
    }
    
    conn->unlock();
}
std::string Vector_Optimizer::to_string()
{
    std::string out;
    
    out += (boost::format("Stategy=%d\nSignalerID=%d\nEncoderID=%d\n")%Strategy_ID % SignalerEngine_ID % EncoderEngine_ID ).str();
    out += (boost::format("Int Param size= %d \n")% Int_Variables_Param.size() ).str() ;
    
    for (int unsigned i =0 ; i!=Int_Variables_Param.size(); i++ )
        out += (boost::format("\tName=%s\tMin=%d\tMax=%d\tStep=%d \n")
                %  std::get<Name_Col>(Int_Variables_Param[i])
                %  std::get<Min_Col>(Int_Variables_Param[i])
                %  std::get<Max_Col>(Int_Variables_Param[i])
                %  std::get<Step_Col>(Int_Variables_Param[i])).str();

    out +=  (boost::format("Double Param size= %d\n")% Doub_Variables_Param.size() ).str() ;
     for (int unsigned i =0 ; i!=Doub_Variables_Param.size(); i++ )
     out += (boost::format("\tName=%s\tMin=%d\tMax=%d\tStep=%d \n")
             %  std::get<Name_Col>(Doub_Variables_Param[i])
             %  std::get<Min_Col>(Doub_Variables_Param[i])
             %  std::get<Max_Col>(Doub_Variables_Param[i])
             %  std::get<Step_Col>(Doub_Variables_Param[i])).str();
    return out;
}
void Vector_Optimizer:: SetUpOptmization()
{
    get_OptipmizationID();
    if (OptimizationID <0 )
            throw std::logic_error("Unable to Get OptimizationID due pushing to the DB error!");
    Populate_DB(Optimization_Scale);
    //OptimizationID = 22;

}

std::tuple<int,std::string,int>
Vector_Optimizer:: fetch_Params()
{   std::cout << "fetchParams " << std::endl;
    conn->lock();
    auto _con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`OptimizationParamTuples`\
                                       WHERE Status = 0 AND OptOrder_id = %d  Limit 3")
                                    %OptimizationID ).str();
    
    auto con = conn->connection();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    
    if (res->rowsCount() > 0){
        res->first();
        
        std::string Params = res->getString("Params");
        int freq =    res->getInt("freq");
        int id   =    res->getInt("id");
        query = (boost::format("UPDATE `Main`.`OptimizationParamTuples`\
                                Status= 1 WHERE id= %d ") %  id  ).str();
        std::shared_ptr<sql::Statement> stmt2(con->createStatement());
        try {
            stmt2->executeQuery(query);
        } catch (...) {}
        conn->unlock();
        return std::make_tuple(id,Params,freq);
    }
    std::cout << "Not id fetched" << std::endl;
    conn->unlock();
    return std::make_tuple(-1,std::string(""),0);
}

void Vector_Optimizer::Push_SolutionToDB(std::tuple<double,int,double> outcome , int ID_params)
{
    conn->lock();
    double Profit   = std::get<0>(outcome);
    int    N_Trades = std::get<1>(outcome);
    double Rate     = std::get<2>(outcome);
    std::cout << "pushing " << Profit << " into ID " << ID_params << std::endl;
    auto con = conn->connection();
    std::string query = (boost::format("UPDATE `Main`.`OptimizationParamTuples`\
                                       SET Profit= %f , Status= 2 , NTrades= %d ,\
                                       S_Rate= %f WHERE id= %d ")
                         %  Profit
                         %  N_Trades
                         %  Rate
                         %  ID_params
                         ).str();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    try {
        stmt->executeQuery(query);
        } catch (...) {}
    conn->unlock();
}
std::tuple<double,int,double>
Vector_Optimizer::SingleRun(std::string Param,int freq)
{
    
    auto conn___  = std::make_shared<sql_connection>();
     auto  __QT = std::make_shared<Pseudo_Trader>(conn___,Strategy_ID);
    __QT->Change_Strat_Params(Param,freq);
    
    __QT->Set_Push_IntoDB(false); //Not to save to DB
    __QT->Set_LiveTradeStatus(false); //It is not live
    __QT->run( from_date ,to_date, InvestingMoney, true); // Calculate on the fly
    double profit   =  __QT->Get_profit();
    int    N_Trades =  __QT->Get_Amount_Of_Trades();
    double Rate     =  __QT->Get_Success_Rate();
    
    std::cout << boost::format("\n<thread> [%d] Runing with Param=%s and freq=%d Profit= %f Amount of Trades = %d Success Rate = %d  </thread> ")
                %std::this_thread::get_id()  %Param % freq % profit % N_Trades % Rate << std::endl;
    return std::make_tuple(profit,N_Trades,Rate);
}
void  Vector_Optimizer:: Change_Order_Status(int S )
{
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("UPDATE `Main`.`OptimizationOrders`\
                                       SET Status= %d WHERE id= %d ")
                         %  S
                         %  OptimizationID ).str();
    //std::cout << query << std::endl ;
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    try {
        stmt->executeQuery(query);
    } catch (...) {}
    conn->unlock();
}

bool Vector_Optimizer::SamplesNeedToRunn()
{
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`OptimizationParamTuples`\
                                       WHERE Status = 0 AND OptOrder_id = %d  Limit 3 ")
                         %  OptimizationID ).str();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    
    if (res->rowsCount() > 0)
    {
        conn->unlock();
        return true;
    }
    conn->unlock();
    return false;
}
void Vector_Optimizer::MiniRun()
{
    auto myParams= fetch_Params();
    int ID = std::get<0>(myParams);
    if (ID < 0 )
        return;
    std::cout <<  boost::format("\n<thread>id pushDB =%d on Thread[%d] </thread> ")
                                % ID % std::this_thread::get_id()    <<  std::endl;
    Push_SolutionToDB( SingleRun(std::get<1>(myParams), std::get<2>(myParams)) , ID );
}
void Vector_Optimizer::Run (tiempo::datetime from_, tiempo::datetime to_,double Money,int Scale )
{
    from_date   = tiempo::datetime(from_.to_string("%Y-%m-%d@%H:%M:00"));
    to_date     =to_;
    
    InvestingMoney = Money;
    Optimization_Scale = Scale;
    SetUpOptmization();
    
    //Main
   
    std::cout << boost::format("\n<thread>[%s] Trader Optimization started.</thread>")%tiempo::now().to_string() << std::endl;
    size_t nthreads = std::thread::hardware_concurrency();
    //size_t max_allowed_threads = 5;
    size_t max_allowed_threads = 1;
    nthreads = std::min<size_t>(max_allowed_threads, nthreads);
    size_t tasks_per_thread = 100 / nthreads;
    std::cout << boost::format("\n<thread>[%s] run_optimization started on %d threads. Task/Thread = %d </thread>")
                % tiempo::now().to_string() % nthreads % tasks_per_thread << std::endl;

   //std::this_thread::sleep_for(std::chrono::milliseconds(2000) );
    
   Change_Order_Status(_OPTIMIZATION_PROCESS_STARTED);
    
    
   do{
        std::vector<std::thread> ManageThd ;
        
            for(int unsigned tt=0; tt<nthreads; tt++)
            {
                ManageThd.push_back(std::thread (&Vector_Optimizer::MiniRun,this)) ;
                std::cout << "<thread> next thread </thread>" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(10) );
            }
            for(int unsigned tt=0; tt<ManageThd.size(); tt++)
                ManageThd[tt].join();
        
           std::cout << boost::format("<thread> reload  %d </thread>") % ManageThd.size() << std::endl;
           std::this_thread::sleep_for(std::chrono::seconds(5) );
        }while(  SamplesNeedToRunn()  );
    
    Change_Order_Status(_OPTIMIZATION_PROCESS_ENDED);
    
}



