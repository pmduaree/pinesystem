//
//  SingleAsseetManager.h
//  
//
//  Created by David de la Rosa on 6/24/14.
//
//

#ifndef ____SingleAssetManager__
#define ____SingleAssetManager__

#include <iostream>
#include <math.h>
#include <memory>

#include "Assets/asset.hpp"
#include "SQL/sql_connection.hpp"
#include "Tiempo/tiempo.hpp"
#include "SQL/sql_connection.hpp"
#include "SQL/sql_rawdata.hpp"

#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>


#define SECONDS_SPAN_FETCH_QUERY 5

#define OPEN    0
#define HIGH    1
#define LOW     2
#define CLOSE   3

class  SingleAssetManager
{
public:
    SingleAssetManager(std::shared_ptr<sql_connection> _conn,int id_asset);
    ~SingleAssetManager(){};
    
    double  Long_position(tiempo::datetime date,double money);
    void    Long_position(tiempo::datetime date, unsigned int AmountOfAssets);
    
    double  Short_position(tiempo::datetime date, double money);
    void    Short_position(tiempo::datetime date, unsigned int AmountOfAssets);
    int     get_id_asset(){return asset->get_id();}
    int     get_postion(){return Positions;}
    std::string    get_name(){return asset->get_name();}
    double get_Money(){return CurrentMoney;}
    double get_AcumComission(){return AccumCommission;}
    bool   is_it_open_to_trade(tiempo::datetime date){ return  asset->get_exchange().is_market_open(date);}
    
    std::string toString();
    void   cleanStart(){  CurrentMoney=0.0;AccumCommission=0.0;Positions=0;  }
    int         TimeHistory_Assets(tiempo::datetime date) { return std::get<0>(TimeHistory[date]); }
    double      TimeHistory_Value(tiempo::datetime date)  { return std::get<1>(TimeHistory[date]); }
    std::string TimeHistory_Str(tiempo::datetime date)    { return std::get<2>(TimeHistory[date]); }
    double      get_last_Price(){return Last_Price_Value;}
    
    tiempo::datetime
                get_LastTrade(){return LastTrade; }
    
    
    std::tuple<double,double,double,double> fetchValue(tiempo::datetime date) {  return asset->fetchValue(date);  }
    std::map<tiempo::datetime,std::tuple<int,double,std::string>>  TimeHistory_All(){return TimeHistory;}
    
private:
    std::shared_ptr<sql_connection> conn;
    std::map<tiempo::datetime,std::tuple<int,double,std::string>> TimeHistory;
    std::shared_ptr<Asset> asset;
    double CurrentMoney;
    double AccumCommission;
    tiempo::datetime LastTrade;
    double Last_Price_Value;
    
    int id_asset;
    int Positions;
};

#endif /* defined(____SingleAsseetManager__) */
