#ifndef __TIME_SERIES__
#define __TIME_SERIES__

#include <map>
#include <cassert>
#include "tiempo.hpp"


template<class T>
class time_series: public std::map<tiempo::datetime, T>
{
    typedef std::map<tiempo::datetime,T> MAP;
    typedef time_series<T> TS;
    typedef tiempo::datetime DATE;
  public:
    time_series(): MAP() {}
    time_series(const TS& ts): MAP(ts) {}
    time_series(const MAP& ts): MAP(ts) {}
    
    TS& operator=(const TS& ts)
    {
        MAP::operator=(ts);
        return *this;
    }
    
    TS& operator=(const MAP& ts)
    {
        MAP::operator=(ts);
        return *this;
    }
    
    typedef typename MAP::iterator iterator;
    typedef typename MAP::const_iterator const_iterator;
    typedef typename MAP::reverse_iterator reverse_iterator;
    typedef typename MAP::const_reverse_iterator const_reverse_iterator;
    
    const T& operator()(const DATE& d)
    {
        return MAP::find(d)->second;
    }
    
    reverse_iterator rfind(const DATE& d)
    {
        if (MAP::empty()) return MAP::rend();
        #if 1
        auto it1 = MAP::rbegin();
        auto it2 = MAP::rbegin() + (MAP::size() - 1);
        if (d > it1->first) return MAP::rend();
        else if (d == it1->first) return it1;
        if (d < it2->first) return MAP::rend();
        else if (d == it2->first) return it2;
        // if it exists, then it's between these two.
        if (it2 - it1 <= 1) return MAP::rend();
        assert(it1->first > d && d > it2->first);
        while(it2 - it1 > 1) {
            size_t inc = 0.51 * (it2-it1);
            auto it3 = it1 + inc;
            if (it3->first > d) it1 = it3;
            else if (it3->first < d) it2 = it3;
            else return it3;
        }
        return MAP::rend();
        #else
        auto it = MAP::rbegin();
        while(it != MAP::rend() &&  it->first != d) ++it;
        return it;
        #endif
    }
    
    const_reverse_iterator rfind(const DATE& d) const
    {
        if (MAP::empty()) return MAP::rend();
        #if 1
        auto it1 = MAP::rbegin();
        auto it2 = MAP::rbegin() + (MAP::size() - 1);
        if (d > it1->first) return MAP::rend();
        else if (d == it1->first) return it1;
        if (d < it2->first) return MAP::rend();
        else if (d == it2->first) return it2;
        // if it exists, then it's between these two.
        if (it2 - it1 <= 1) return MAP::rend();
        assert(it1->first > d && d > it2->first);
        while(it2 - it1 > 1) {
            size_t inc = 0.51 * (it2-it1);
            auto it3 = it1 + inc;
            if (it3->first > d) it1 = it3;
            else if (it3->first < d) it2 = it3;
            else return it3;
        }
        return MAP::rend();
        #else
        auto it = MAP::rbegin();
        while(it != MAP::rend() &&  it->first != d) ++it;
        return it;
        #endif
    }
    
    tiempo::duration get_period() const
    {
        // get the elementary time step
        tiempo::duration minfreq(3600 * 24 * 30*365);   // very long...
        auto it = MAP::begin();
        while(it != MAP::end()) {
            auto prev_date = it->first;
            ++it;
            if (it == MAP::end()) break;
            auto next_date = it->first;
            auto mydur = tiempo::duration(next_date - prev_date);
            if (mydur < minfreq) minfreq = mydur;
        }
        return minfreq;
    }
    
    tiempo::duration frequency() const __attribute__((deprecated))
    {
        return get_period();
    }

};


#endif
