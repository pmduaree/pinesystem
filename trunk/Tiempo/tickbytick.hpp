#ifndef tickbytick_hpp
#define tickbytick_hpp


#include <vector>
#include "Assets/prices.hpp"

class TickByTick: public std::vector<TickType>
{
    typedef std::vector<TickType> V;
  public:
    TickByTick(): V() {}
    TickByTick(const TickByTick& tbt) : V(tbt) {}

    V::const_iterator find(const tiempo::datetime& d) const
    {
        if (V::empty()) return V::end();
        long j = V::size() - 1;
        while( j>=0 && V::operator[](j).d > d) --j;
        if (j < 0) return V::end();
        return V::begin() + j;
    }

    const TickType& operator[](const tiempo::datetime& d) const
    {
        auto it = TickByTick::find(d);
        if (it == TickByTick::end()) {
            throw std::runtime_error("PRICE NOT OFUND");
        }
        return *it;
    }
    
    double price(const tiempo::datetime& d) const
    {
        auto it = find(d);
        return it == V::end() ? 0.0 : it->p;
    }
    
    size_t volume(const tiempo::datetime& d) const
    {
        auto it = find(d);
        return it==V::end() ? 0 : it->v;
    }
    
    double last_price() const
    {
        if (V::empty()) throw std::runtime_error("no prices found");
        return V::back().p;
    }
};

void make_candles(const TickByTick& tix, int freq, time_series<CandleType>& candles, int offset=0)
{
    candles.clear();
    if (tix.empty())  return;
    for(const auto& tick : tix) {
        // find the time window
        auto d_of = offset_date(tick.d, freq, offset);
        auto& c = candles[d_of];
        if (c.v) {
            c.v += tick.v;
            c.p_c = tick.p;
            c.p_h = std::max(c.p_h, tick.p);
            c.p_l = std::max(c.p_l, tick.p);
        } else {
            c.v = tick.v;
            c.p_c = c.p_o = tick.p;
            c.p_h = c.p_l = tick.p;
        }
    }
}


#endif

