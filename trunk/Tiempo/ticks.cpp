#include "time_series.hpp"
#include "ticks.hpp"
#include <cmath>



TixSeries::const_reverse_iterator TixSeries::get_iterator(const tiempo::datetime& d) const
{
    if (TS::empty()) return TS::rend();
    auto it = TS::rbegin();
    while(it != TS::rend() && it->first > d) ++it;
    return it;
}
    
double TixSeries::get_price(const tiempo::datetime& d) const
{
    auto it1 = TS::find(d);
    if (it1 != TS::end()) return it1->second.p;
    auto it = get_iterator(d);
    if (it != TS::rend()) return it->second.p;
    else {
        std::cerr << "Bad iterator (date="<<d<<"), range:"<<TS::begin()->first << " to " << TS::rbegin()->first << ".\n";
        throw std::runtime_error("blabla");
    }
    return 0.0;
}

double TixSeries::get_mean_price(const tiempo::datetime& d1, const tiempo::datetime& d2) const
{
    double p = 0.0;
    size_t c = 0;
    auto it = TS::rbegin();
    while(it != TS::rend() && it->first > d2) ++it;
    while(it != TS::rend() && it->first >= d1) {
        p += it->second.p;
        ++c;
        ++it;
    }
    if (it == TS::rend()) throw std::runtime_error("INSUFFICEINT DATA");
    if (c == 0) {
        p = it->second.p;
    } else {
        p *= 1.0 / c;
    }
    return p;
}

const tiempo::datetime& TixSeries::get_last_date() const
{
    return TS::rbegin()->first; 
}

double TixSeries::last() const 
{ 
    return get_last_price();
}

double TixSeries::get_last_price() const 
{ 
    return TS::rbegin()->second.p; 
}



CandleType make_candle(const TixSeries& tix, const tiempo::datetime& date, 
                       const tiempo::duration& dur)
{
    CandleType c;
    c.v = 0;
    auto it = tix.begin();
    auto d_1 = date;
    auto d_2 = date + dur;
    while(it != tix.end() && it->first < d_1) ++it;
    while(it != tix.end() && it->first < d_2) {
        const auto& t = it->second;
        if (c.v==0) {
            c.p_o = t.p;
            c.p_h = t.p;
            c.p_l = t.p;
        }
        c.p_c = t.p;
        c.p_h = std::max(c.p_h, t.p);
        c.p_l = std::min(c.p_l, t.p);
        c.v += t.v;
        ++it;
    }
    return std::move(c);
}

PriceSeries make_log_returns(const PriceSeries& P)
{
    PriceSeries dlogp;
    auto it = P.begin();
    while( it != P.end()) {
        double p_prev = it->second;
        ++it;
        if (it == P.end()) break;
        double p_next = it->second;
        double logret = 0.0;
        if (p_next >0 && p_next > 0)
            logret = log(p_next / p_prev);
        else {
            logret= (p_next - p_prev) / fabs(p_prev);
        }
        dlogp[it->first] = logret;
    }
    return dlogp;
}

double get_current_price(const PriceSeries& P, const tiempo::datetime& date)
{
    auto it = P.rbegin();
    while (it != P.rend() && it->first > date) ++it;
    if (it == P.rend()) throw std::runtime_error("doesnt exist this price whatsoever");
    return it->second;
}



void make_candles(const TixSeries& tt, int freq, int offset, CandleSeries& P)
{
    P.clear();
    for(auto it = tt.begin() ; it != tt.end() ; ++it) {
        auto d = offset_date(it->first, freq, offset);
        size_t v = it->second.v;
        double p = it->second.p;
        auto itp = P.find(d);
        if (itp == P.end() ) {
            auto& c = P[d];
            c.v = v;
            c.p_o = p;
            c.p_h = p;
            c.p_l = p;
            c.p_c = p;
        } else {
            auto& c = itp->second;
            c.v += v;
            c.p_c = p;
            c.p_h = std::max(c.p_h, p);
            c.p_l = std::min(c.p_l, p);
        }
    }
}

void make_candles(const TixSeries& tt, int freq, int offset, const tiempo::datetime& date, 
                  size_t howmany, CandleSeries& P)
{ 
    P.clear();
    auto it = tt.rbegin();
    while(it != tt.rend() && P.size() < howmany) {
        auto d = offset_date(it->first, freq, offset);
        if (d > date) {
            ++it;
            continue;
        }
        auto v = it->second.v;
        auto p = it->second.p;
        auto itp = P.find(d);
        if (itp == P.end() ) {
            auto& c = P[d];
            c.v = v;
            c.p_o = p;
            c.p_h = p;
            c.p_l = p;
            c.p_c = p;
        } else {
            auto& c = itp->second;
            c.v += v;
            c.p_c = p;
            c.p_h = std::max(c.p_h, p);
            c.p_l = std::min(c.p_l, p);
        }
        ++it;
    }
}



void make_candles(const TixSeries& tt, int freq, const tiempo::datetime& date, 
                  size_t howmany, CandleSeries& P)
{
    // calculate the current offset.
    tiempo::duration my_offset = date - offset_date(date, freq, 0);
    int offset = my_offset.seconds() / 60;
    make_candles(tt, freq, offset, date, howmany, P);
}






