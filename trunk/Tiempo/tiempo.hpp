#ifndef tiempo_hpp
#define tiempo_hpp
#include <string>
#include <tuple>
#include <chrono>
#include <iostream>

namespace tiempo {
    
std::tuple<int,int,int> parse_date(std::string text);
std::tuple<int,int,int> parse_time(std::string anytime);

class duration : public std::chrono::duration<long>
{
    typedef std::chrono::duration<long> DUR;
  public:
    duration() : DUR() {}
    duration(const duration& d) : DUR(d) {}
    duration(const DUR& d) : DUR(d) {}
    duration(long seconds) : DUR(seconds) {}
    
    template<class D1, class D2>
    duration(const std::chrono::duration<D1,D2>& d)
        : DUR(std::chrono::duration_cast<std::chrono::seconds>(d)) {}

    duration& operator=(const duration& d);
    
    template<class D1,class D2>
    duration& operator=(const std::chrono::duration<D1,D2>& d);

    std::string to_string() const;

    long seconds() const;
};

class datetime: public std::chrono::system_clock::time_point
{
    typedef std::chrono::system_clock::time_point TP;
  public:
    datetime() : TP() {}
    datetime(const TP& z) : TP(z) {}
    datetime(const datetime& z) : TP(z) {}
    datetime(const std::string& strdat) : TP() { operator=(strdat); } 
    
    
    datetime& operator=(const TP& z);
    datetime& operator=(const std::string& strd);

    tiempo::duration from_sunday() const;
    tiempo::duration time_of_day() const;

    bool is_div(const tiempo::duration& freq) const;
    tiempo::duration remainder(const tiempo::duration& freq) const;
    size_t seconds_since_epoch() const;

    std::string to_string(const std::string& format="%Y-%m-%d@%H:%M:%S") const;
    void from_string(const std::string& strdate);

  private:
};


/// A SPECIAL CLASS:: NOW 
//
class now: public datetime
{
  public:
    now();
};


tiempo::datetime offset_date(const tiempo::datetime& d, int freq, int offset=0);



typedef datetime datetime_type;

}    // end of namespace tiempo


namespace std {
    template <> struct hash<tiempo::datetime>
    {
        size_t operator()(const tiempo::datetime& d) const
        {
            return d.time_since_epoch().count();
        }

    };
}

std::ostream& operator<<(std::ostream& s, const tiempo::duration& dur);
std::ostream& operator<<(std::ostream& s, const tiempo::datetime& t);





#endif
