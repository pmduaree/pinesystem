#ifndef ticks_hpp
#define ticks_hpp

#include <cstdlib>
#include "time_series.hpp"
#include "tiempo.hpp"

struct TickType
{
    tiempo::datetime d;
    double p;
    size_t v;
    TickType() {}
    TickType(double p_, size_t v_) : p(p_), v(v_) {}
};

struct CandleType
{
    double p_o, p_c, p_h, p_l;
    size_t v;
    CandleType() : p_o(0.0), p_c(0.0), p_h(0.0), p_l(0.0), v(0) {}
    double close() const { return p_c; }
    double open() const { return p_o; }
    double high() const { return p_h; }
    double low() const { return p_l; }
    size_t volume() const { return v; }
};

class TixSeries: public time_series<TickType>
{
    typedef time_series<TickType> TS;
  public:
    TixSeries(): TS() {}
    TixSeries(const TixSeries& ts): TS(ts) {}
    typedef TS::const_reverse_iterator const_reverse_iterator;
    double get_price(const tiempo::datetime& d) const;
    double get_mean_price(const tiempo::datetime& d1, const tiempo::datetime& d2) const;
    const tiempo::datetime& get_last_date() const;
    double get_last_price() const;
    double last() const __attribute__((deprecated));
  private:
    const_reverse_iterator get_iterator(const tiempo::datetime& d) const;
};

typedef TixSeries tix_series;

typedef time_series<CandleType> CandleSeries;
typedef time_series<CandleType> price_series;
typedef time_series<double> PriceSeries;


CandleType make_candle(const TixSeries& tix, const tiempo::datetime& date, 
                       const tiempo::duration& dur);

PriceSeries make_log_returns(const PriceSeries& P);

double get_current_price(const PriceSeries& P, const tiempo::datetime& date);

void make_candles(const TixSeries& tt, int freq, int offset, CandleSeries& P);
void make_candles(const TixSeries& tt, int freq, int offset, const tiempo::datetime& date, 
                  size_t howmany, CandleSeries& P);
void make_candles(const TixSeries& tt, int freq, const tiempo::datetime& date, 
                  size_t howmany, CandleSeries& P);

#endif
