#include "tiempo.hpp"

#include <vector>
#include <string>
#include <tuple>
#include <stdexcept>
#include <sstream>
#include <mutex>

#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/format.hpp>

namespace tiempo {

std::tuple<int,int,int> parse_date(std::string text)
{
    boost::trim(text);
    if (text.empty()) return std::make_tuple(1980,1,1);
    int Y,M,D;
    if (std::count(text.begin(), text.end(), '/') == 2) {
        auto p1 = text.find('/');
        auto p2 = text.find('/',p1+1);
        if (p1 == 4 && p2 == 7) {
            Y = std::stoi(text.substr(0,p1));
            M = std::stoi(text.substr(p1+1, p2-p1-1));
            D = std::stoi(text.substr(p2+1));
        } else {
            M = std::stoi(text.substr(0, p1));
            D = std::stoi(text.substr(p1+1,p2-p1-1));
            Y = std::stoi(text.substr(p2+1));
            if (50 < Y&& Y < 100) Y += 1900;
            else if (Y < 50) Y += 2000;
        }
    } else if (std::count(text.begin(),text.end(), '-')==2) {
        auto p1 = text.find('-');
        auto p2 = text.find('-',p1+1);
        Y = std::stoi(text.substr(0, p1));
        M = std::stoi(text.substr(p1+1,p2-p1-1));
        D = std::stoi(text.substr(p2+1));
        if (50 < Y&& Y < 100) Y += 1900;
        else if (Y < 50) Y += 2000;
    } else {
        std::cerr << "INVALID DATE: " << text << std::endl;
        throw std::runtime_error("invalid date");
    }
    return std::make_tuple(Y,M,D);
}

std::tuple<int,int,int> parse_time(std::string anytime)
{
    boost::trim(anytime);
    int h=0, m=0, s=0;
    bool pm = false, am=false;
    if (anytime.find("AM") != std::string::npos) {
        anytime = anytime.substr(0, anytime.find("AM"));
        boost::trim(anytime);
        am = true;
    }
    if (anytime.find("PM") != std::string::npos) {
        anytime = anytime.substr(0, anytime.find("PM"));
        boost::trim(anytime);
        pm = true;
    }
    std::vector<std::string> flds;
    boost::split(flds, anytime, boost::is_any_of(":-"));
    if (flds.size() == 3) {
        h = std::stoi(flds[0]);
        m = std::stoi(flds[1]);
        s = std::stoi(flds[2]);
    } else if (flds.size() == 2) {
        h = std::stoi(flds[0]);
        m = std::stoi(flds[1]);
    } else {
        throw std::runtime_error("nothgin here");
    }
    if (h<12 && pm) h += 12;
    else if (h==12 && am) h -= 12;
    return std::make_tuple(h,m,s);
}


duration& duration::operator=(const duration& d)
{
    DUR::operator=(d);
    return *this;
}
    
template<class D1,class D2>
duration& duration::operator=(const std::chrono::duration<D1,D2>& d)
{
    DUR::operator=(std::chrono::duration_cast<std::chrono::seconds>(d));
    //DUR::operator=(d);
    return *this;
}

duration operator-(const duration& d1, const duration& d2)
{
    return duration( operator-(d1,d2) );
}

long duration::seconds() const
{
    return DUR::count();
}


std::string duration::to_string() const
{
    long m = 0, s = 0, h = 0, d=0;
    s = DUR::count();
    if (s==0) return std::string("0 seconds");
    std::string rv = "";
    if (s < 0) { rv += "-"; s = -s; };
    m = s / 60;
    s -= m * 60;
    h = m / 60;
    m -= 60 * h;
    d = h / 24;
    h -= d * 24;
    if (d > 1) 
        rv += (boost::format("%d days ") % d).str();
    else if (d == 1)
        rv += (boost::format("%d day ") % d).str();
    if (h > 1) 
        rv += (boost::format("%d hours ") % h).str();
    else if (h == 1)
        rv += (boost::format("%d hour ") % h).str();
    if (m > 1)
        rv += (boost::format("%d minutes ") % m).str();
    else if (m == 1)
        rv += (boost::format("%d minute ") % m).str();
    if (s > 1)
        rv += (boost::format("%d seconds ") % s).str();
    else if (s == 1)
        rv += (boost::format("%d second ") % s).str();
    boost::trim(rv);
    return rv;
}


datetime& datetime::operator=(const TP& z)
{
    TP::operator=(z);
    return *this;
}

datetime& datetime::operator=(const std::string& strd)
{
    from_string(strd);
    return *this;
}

std::tm tp_to_tm(const std::chrono::system_clock::time_point& pt)
{
    std::time_t t = std::chrono::system_clock::to_time_t(pt);
    std::tm rv;
    #if 0
    static std::mutex _zz;
    _zz.lock();
    auto p_tm = std::gmtime(&t);
    memcpy(&rv, p_tm, sizeof(std::tm));
    _zz.unlock();
    #else
    gmtime_r(&t, &rv);  // thread safe version -- avoid using locks.
    #endif
    return std::move(rv);
}
    
std::chrono::system_clock::time_point tm_to_tp(const std::tm& t)
{
    static std::mutex _ww;
    _ww.lock();
    std::tm tm_copy(t);
    std::time_t tt = timegm(&tm_copy);
    std::chrono::system_clock::time_point tp = std::chrono::system_clock::from_time_t(tt);
    _ww.unlock();
    return std::move(tp);
}
    
tiempo::duration datetime::from_sunday() const
{
    const TP& tp = *this;
    auto gmt = tp_to_tm(tp);
    size_t seconds = gmt.tm_sec + 60 * (gmt.tm_min + 60 * (gmt.tm_hour + 24 * gmt.tm_wday) );
    return tiempo::duration(seconds);
}


tiempo::duration datetime::time_of_day() const
{
    const TP& tp = *this;
    auto gmt = tp_to_tm(tp);
    size_t seconds = gmt.tm_sec  + 60 * (gmt.tm_min + 60 * gmt.tm_hour);
    return tiempo::duration(seconds);
}

bool datetime::is_div(const tiempo::duration& freq) const
{
    auto d = this->time_of_day();
    bool is_div = (d.seconds() % freq.seconds() ) == 0;
    return is_div;
}
    
tiempo::duration datetime::remainder(const tiempo::duration& freq) const
{
    auto d = this->time_of_day();
    tiempo::duration rem(d.seconds() % freq.seconds());
    return rem;
}
    
size_t datetime::seconds_since_epoch() const
{
    return TP::time_since_epoch().count();
}

std::string make_date_string(const std::chrono::system_clock::time_point& tx, const std::string& format) {
    //std::time_t t = std::chrono::system_clock::to_time_t(tx);
    //std::cout << "Q: " << tx.time_since_epoch().count() << " : " << reinterpret_cast<long>(t) << std::endl;
    auto gmt = tp_to_tm(tx);
    std::string timeStr(1000, '\1'); // fill with nonsense
    std::strftime(&timeStr[0], 999, format.c_str(), &gmt);
    timeStr.erase(timeStr.find_first_of("\1")-1); // trim the nonsense and the trailing EOF (byte 0) character inserted by strftime
    //std::cout << "W: " << tx.time_since_epoch().count() << " : " << timeStr << std::endl;
    return timeStr;
}

std::string datetime::to_string(const std::string& format) const
{
    //std::cout << "to_string B["<<reinterpret_cast<size_t>(this) << "]: " << time_since_epoch().count() << std::endl;
    std::string retval = make_date_string(*this, format);
    std::string retval2 = make_date_string(*this, format);
    //std::cout << "to_string C["<<reinterpret_cast<size_t>(this)<<"]: "<<time_since_epoch().count() << std::endl;
    if (retval != retval2) {
        std::cout << "ERROR: " << retval << " != " << retval2 << std::endl;
    }
    assert( retval == retval2 );
    return retval;
}



void datetime::from_string(const std::string& strdate)
{
    if (strdate == "now") {
        auto z_now = now();
        (*this) = z_now;
        return;
    } else if (strdate == "yesterday") {
        throw std::runtime_error("not supported");
    }
    //std::cout << "FROM STRING CALLED." << std::endl;
    std::string text = strdate;
    boost::trim(text);
    int Y=0,M=0,D=0,h=0,m=0,s=0;
    auto p_at = text.find('@');
    auto p_space = text.find_first_of(' ');
    auto p_T = text.find_first_of('T');
    auto p_Z = text.find_first_of('Z');
    if (p_at != std::string::npos) {
        // {DATE}@{TIME}
        std::tie(Y,M,D) = parse_date(text.substr(0, p_at));
        std::tie(h,m,s) = parse_time(text.substr(p_at+1));
    } else if (p_T != std::string::npos && p_Z == text.length()-1 && p_T < p_Z) {
        // {DATE}T{TIME}Z
        std::tie(Y,M,D) = parse_date(text.substr(0, p_T));
        std::tie(h,m,s) = parse_time(text.substr(p_T+1, p_Z-p_T-1));
    } else if (p_space != std::string::npos) {
        // {DATE} {TIME}
        std::tie(Y,M,D) = parse_date(text.substr(0, p_space));
        std::tie(h,m,s) = parse_time(text.substr(p_space+1));
    } else if (text.find(':') == std::string::npos) {
        // {DATE}
        std::tie(Y,M,D) = parse_date(text);
        h=m=s = 0;
    } else {
        throw std::runtime_error("invalid date");
    }
    std::tm t;
    t.tm_sec = s;
    t.tm_min = m;
    t.tm_hour = h;
    t.tm_mon = M-1;
    t.tm_year = Y - 1900;
    t.tm_mday = D;
    t.tm_isdst = -1;
    auto tp = tm_to_tp(std::move(t));
    TP::operator=(tp);
    TP::operator=(TP(std::chrono::duration_cast<std::chrono::seconds>(tp.time_since_epoch() ) ) );
}


tiempo::datetime offset_date(const tiempo::datetime& d, int freq, int offset)
{
    if (freq > 60 || freq < 1) {
        throw std::runtime_error("Thats not supported");
    }
    auto dy = d;
    size_t z1 = d.seconds_since_epoch() - 60 * offset;
    long offset_seconds = z1 % (freq * 60);
    if (offset_seconds) dy -= std::chrono::seconds(offset_seconds);
    return dy;
}


now::now()
    : datetime()
{
    auto sec = std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now().time_since_epoch()
               );
    //std::cout << "NOW IN SECONDS: " << sec.count() << std::endl;
    auto new_point = std::chrono::system_clock::time_point(sec);
    //std::cout << "Got the new point." << std::endl;
    datetime::operator=(new_point);
}



}   // end of namespace tiempo


std::ostream& operator<<(std::ostream& s, const tiempo::duration& dur)
{
    s << dur.seconds();
    return s;
}


std::ostream& operator<<(std::ostream& s, const tiempo::datetime& t)
{
    s << t.to_string();
    return s;
}
