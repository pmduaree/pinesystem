//
//  Signal_Encoder_hmm.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Encoder_HMM__
#define ____Encoder_HMM__

#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <numeric>
#include "Encoder_Engine.hpp"
#include "../Utils/stringparpars.hpp"

class Encoder_Engine_HMM : public Encoder_Engine
{

public:
    Encoder_Engine_HMM(std::vector<int> Assets): Encoder_Engine(Assets){}
    virtual  ~Encoder_Engine_HMM(){}
    virtual
    std::vector<std::tuple<int,int> >
    Encode_Signal(const tiempo::datetime& date,
                  float Money,
                  std::string Param,
                  std::tuple<int,double,std::string> Signal,
                  std::vector<double> Prices)
    {
        std::cout << "Encode HMM Signal" << std::endl;
        const double of = 0.05;
        
        int SignalType     =  std::get<SIGNALTYPE_COLUMN>(Signal);
        double SignalValue = std::get<SIGNALVALUE_COLUMN>(Signal);
        std::string Comment_ = std::get<COMMENT_COLUMN>(Signal);
        //StringParamParser paramparser(Comment_);
        
        std::cout << (boost::format("Encoding Signal %d \t %0.3f  \t %s \n ") % SignalType % SignalValue % Comment_ ).str() << std::endl;
        // std::string Action = paramparser.get<std::string>("ACTION","");
        std::vector<int> Stock(Prices.size());
    
        
        //Exit -1
        if (SignalType == _EXIT_TRADE_SIGNAL ||  SignalType == _WAIT_TRADE_SIGNAL )
            Stock[0]= 0;

        //Enter  +1
        if(SignalType == _ENTER_TRADE_SIGNAL)
            //SELL
            if(SignalValue <= 0.5 - of)
                Stock[0] = -round( Money / Prices[0]);
            //BUY
            if(SignalValue >= 0.5 + of)
                Stock[0] = round( Money / Prices[0]);
        
        std::vector<std::tuple<int, int>> out;
        for (int unsigned i =0; i!= Prices.size();i++){
            out.push_back( std::make_tuple( Id_Assets[i], Stock[i]  )   );
            std::cout << "AssetID=" << Id_Assets[i] << " Stock=" << Stock[i] << std::endl;
        }
        
        return out;
    }
};


#endif /* defined(____Signal_Encoder_Pair__) */
