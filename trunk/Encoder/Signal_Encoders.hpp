//
//  Signal_Encoders..h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Signal_Encoders___
#define ____Signal_Encoders___

#include <iostream>
#include <string>
#include "Encoder.hpp"

std::shared_ptr<SignalEncoder> create_Encoder(std::string type);

#endif /* defined(____Signal_Encoders___) */
