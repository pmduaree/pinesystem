//
//  Encoder_Engine.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Encoder_Engine__
#define ____Encoder_Engine__

#include <iostream>

#include <string>
#include <memory>
#include <tuple>
#include <vector>
#include "../Tiempo/tiempo.hpp"


#define SIGNALTYPE_COLUMN    0
#define SIGNALVALUE_COLUMN     1
#define COMMENT_COLUMN   2


#define _ENTER_TRADE_SIGNAL 1
#define _EXIT_TRADE_SIGNAL  -1
#define _WAIT_TRADE_SIGNAL  0


class Encoder_Engine
{
public:
    Encoder_Engine(std::vector<int> Assets): Id_Assets(Assets)
    { }
    virtual ~Encoder_Engine() {};
    virtual std::vector<std::tuple<int,int> >
    Encode_Signal(const tiempo::datetime& date,float money,std::string Param, std::tuple<int,double,std::string> Signal,std::vector<double> Prices)=0;
    
    std::tuple<int,double,std::string> err_result;
    std::vector<int> Id_Assets;
};
#endif /* defined(____Encoder_Engine__) */
