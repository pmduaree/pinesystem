//
//  Encoder.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Encoder__
#define ____Encoder__

#include <iostream>

#include <string>
#include <memory>
#include <vector>
#include <tuple>
#include "../Tiempo/tiempo.hpp"
#include "../SQL/sql_connection.hpp"
#include "../Encoder/Encoder_Engine.hpp"
#include "../Encoder/Encoder_Engines.hpp"

#define SECONDS_SPAN_FETCH_QUERY 5

class Encoder
{
public:
    Encoder(std::shared_ptr<sql_connection> _conn,int strategy);
    ~Encoder() {};
    
    std::vector<std::tuple<int,int>>
    Encode_Signal(const tiempo::datetime& date,float money, std::tuple<int,double,std::string> Signal);
    std::tuple<int,double,std::string> err_result; ///check iztok usage
    std::string get_EngineType(){return EngineName;}
	std::vector<int> get_AssetsID(){return id_Assets;}
    int get_StrategyId(){return strategy_id;}
    int get_EncoderEngineId(){return EncoderEngine_id;}
private:
    std::shared_ptr<sql_connection> conn;
    std::vector<int> id_Assets;
    std::vector<double> Prices;
    std::string Param;
    int strategy_id;
    int EncoderEngine_id;
    std::string EngineName;
    std::shared_ptr<Encoder_Engine> _engine;
    
    void Load_instruments();
    void Load_engine();
    std::tuple<double,double,double,double>
      Load_Price( tiempo::datetime date,int );
    void Load_Prices (const tiempo::datetime& date);

};
#endif /* defined(____Encoder__) */
