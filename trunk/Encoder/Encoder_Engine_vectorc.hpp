//
//  Signal_Encoder_Pair.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Encoder_Vector__
#define ____Encoder_Vector__

#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <numeric>
#include "Encoder_Engine.hpp"
#include "../Utils/stringparpars.hpp"

class Encoder_Engine_VectorC : public Encoder_Engine
{

public:
    Encoder_Engine_VectorC(std::vector<int> Assets): Encoder_Engine(Assets){}
    virtual  ~Encoder_Engine_VectorC(){}
    virtual
    std::vector<std::tuple<int,int> >
    Encode_Signal(const tiempo::datetime& date,
                  float Money,
                  std::string Param,
                  std::tuple<int,double,std::string> Signal,
                  std::vector<double> Prices)
    {
        std::cout << "Encode Vector Correction  Signal" << std::endl;
        
        int SignalType     =  std::get<SIGNALTYPE_COLUMN>(Signal);
        double SignalValue = std::get<SIGNALVALUE_COLUMN>(Signal);
        std::string Comment_ = std::get<COMMENT_COLUMN>(Signal);
        StringParamParser paramparser(Comment_);
        
        std::cout << (boost::format("Encoding Signal %d \t %0.3f  \t %s \n ") % SignalType % SignalValue % Comment_ ).str() << std::endl;
        std::string binsize_S = paramparser.get<std::string>("binsize","");
        std::string bestComIndx_S = paramparser.get<std::string>("bestComIndx","");
        std::string weights_S = paramparser.get<std::string>("weights","");
        
        std::cout << (boost::format("binsize[%s] bestComIndx[%s] weights[%s]")
                                % binsize_S
                                % bestComIndx_S
                                % weights_S ).str() << std::endl;
        
        
        int binsize =  std::stoi(binsize_S);
        std::vector<double> bestComIndx;

        ///////////////////
        std::string delimiter = "-";
        size_t pos = 0;
        std::string token;
        while ((pos = bestComIndx_S.find(delimiter)) != std::string::npos) {
            token = bestComIndx_S.substr(0, pos);
            std::cout << token << std::endl;
            bestComIndx.push_back(std::stoi( token  ));
            bestComIndx_S.erase(0, pos + delimiter.length());
        }
        std::cout << bestComIndx_S << std::endl;
        bestComIndx.push_back(std::stoi( bestComIndx_S  ));
        
        
        ///////////////////
        std::vector<double> weights;
        delimiter = "|";
        pos = 0;
        token="";
        while ((pos = weights_S.find(delimiter)) != std::string::npos) {
            token = weights_S.substr(0, pos);
            std::cout << token << std::endl;
            weights.push_back(std::stod( token  ));
            weights_S.erase(0, pos + delimiter.length());
        }
        std::cout << weights_S << std::endl;
        weights.push_back(std::stod( weights_S  ));
        
        /////////////////////
        
        std::vector<int> Stock(Prices.size());
        
        //Exit -1
        if (SignalValue == _EXIT_TRADE_SIGNAL ||  SignalValue == _WAIT_TRADE_SIGNAL )
        {
            for (unsigned int i =0 ; i!= Prices.size() ; i++)
                Stock[i]=0;
        }

        //Enter  +1
        if(SignalValue == _ENTER_TRADE_SIGNAL)
        {   std::cout << "Encoding Enter trade  " << std::endl;
            
            for (auto P: Prices )
                std::cout << "Price " << P << std::endl;
            
            
            double Sum=0.0;
            for (auto w: weights )
                    Sum+= fabs(w);
            
            for (int i =0 ; i!= binsize ; i++ )
            {
                
                double amount = (fabs( weights[i]  )*Money)/ Sum;
                
                
               // if (weights[i] < 0 )
               // {
               //     Stock[ bestComIndx[i]   ]= -1 * amount/Prices[ bestComIndx[i] ];
               //     std::cout << (boost::format(" STOCK[ %d ] %i   w[%f]  P[%f]\n") %bestComIndx[i] % i % weights[i] % Prices[i] ).str();
                //}
               // else{
                    Stock[ bestComIndx[i]   ]= amount/Prices[ bestComIndx[i] ];
                
                    std::cout << (boost::format(" STOCK[ %d ] %i   w[%f] \t P[%0.3f]\t amount[%f ] \t M[%f] \t Sum[%f] Stk[ %f ]\n")
                                  %bestComIndx[i] % i % weights[i] % Prices[bestComIndx[i]] % amount  % Money % Sum  %  (amount/Prices[ bestComIndx[i]]) ).str();
                //}
                
            }
            
            
        }
        
        std::vector<std::tuple<int, int>> out;
        for (int unsigned i =0; i!= Prices.size();i++){
            out.push_back( std::make_tuple( Id_Assets[i], Stock[i]  )   );
        
            std::cout << "AssetID=" << Id_Assets[i] << " Stock=" << Stock[i] << std::endl;
        }
        
        return out;
    }
};


#endif /* defined(____Signal_Encoder_Pair__) */
