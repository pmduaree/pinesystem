    //
//  Encoder_Engines.cpp
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//
#include <iostream>
#include <boost/algorithm/string/case_conv.hpp>
#include "Encoder_Engine.hpp"
#include "Encoder_Engine_Pair.hpp"
#include "Encoder_Engine_hmm.hpp"
#include "Encoder_Engine_vectorc.hpp"

std::shared_ptr<Encoder_Engine>
create_Encoder_engine(std::string type,std::vector<int> Assets )
{
    boost::to_upper(type);
    std::shared_ptr<Encoder_Engine> _Encoder;
    if (type == "PAIR") {
        //std::cout << "Encoder Engine type =PAIR " << std::endl;
        _Encoder = std::make_shared<Encoder_Engine_Pair>(Assets);
    } else if (type == "HMM") {
        //std::cout << "Encoder Engine type =HMM" << std::endl;
        _Encoder = std::make_shared<Encoder_Engine_HMM>(Assets);
    }
    else if (type == "VECTORC") {
        //std::cout << "Encoder Engine type =VectorC" << std::endl;
        _Encoder = std::make_shared<Encoder_Engine_VectorC>(Assets);
    }

    else {
        throw std::runtime_error((boost::format("Encoder Engine [%d] Not Found!!!!")% type ).str());
    }
    return _Encoder;
}

