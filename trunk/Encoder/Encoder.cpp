//
//  Encoder.cpp
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#include "Encoder.hpp"
#include "boost/format.hpp"
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


#include "../Assets/asset.hpp"
#include "Encoder_Engine.hpp"
#include "Encoder_Engines.hpp"

#include "../Tiempo/tiempo.hpp"
#include <chrono>

Encoder::Encoder(std::shared_ptr<sql_connection> _conn,int id_strategy)
: conn(_conn),strategy_id(id_strategy)
{
    Load_instruments();
    Load_engine();
    _engine = create_Encoder_engine(EngineName,id_Assets);
    //std::cout << "Encoder generated" << std::endl;
}
void Encoder::Load_instruments()
{
    conn->lock();
    std::shared_ptr<sql::Connection> con = conn->connection();
    std::string query = (boost::format("SELECT * \
                                       FROM `Main`.`Portfolio` \
                                       WHERE portfolio_id = \
                                       (SELECT portfolio_id FROM `Main`.`Strategies` WHERE `id` = %d )\
                                       ORDER BY  `order` ASC") % strategy_id ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    //In case no results, No portfolio found
    if (res->rowsCount() < 1)
    {
        conn->unlock();
        throw std::runtime_error("Portfolio Not Found !!!");
    }
    //in case everything went swell
    if (res->rowsCount() >= 1)
    {
        while (res->next()) {
            id_Assets.push_back(res->getInt("instrument_id"));
            
        }
    }
    conn->unlock();
}
void Encoder::Load_engine()
{
    conn->lock();
    std::shared_ptr<sql::Connection> con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`Strategies` S\
                                       INNER JOIN `Main`.`EncoderEngines` EE\
                                       ON `S`.`EncoderEngine_id` = `EE`.`id`\
                                       INNER JOIN `Main`.`EncoderType` ET ON \
				                       `EE`.`type` = `ET`.`id` \
                                       WHERE `S`.`id`=%d") % strategy_id ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    //In case no results, No portfolio found
    if (res->rowsCount() < 1)
    {
        conn->unlock();
        throw std::runtime_error((boost::format("Encoder id  [%d] Not Found!!!!") % strategy_id ).str() );
    }
    //in case everything went swell
    if (res->rowsCount() == 1)
    {
        res->first();
        EngineName          = res->getString("name");
        EncoderEngine_id    = res->getInt("EncoderEngine_id");
        Param               = res->getString("CurrentParam");
    }
    conn->unlock();
}
std::tuple<double,double,double,double>
Encoder::Load_Price( tiempo::datetime date, int instrument_id)
{

   auto  asset = std::make_shared<Asset>(conn, instrument_id);
    
    return asset->fetchValue(date);
}
void Encoder::Load_Prices(const tiempo::datetime& date )
{
    Prices.clear();
    Prices = std::vector<double>(id_Assets.size());
    for (int unsigned i = 0 ; i!= id_Assets.size(); i++ )
    {   std::tuple<double,double,double,double> SinglePrice =  Load_Price ( date, id_Assets[i] ) ;
        Prices[i] = std::get<3>(SinglePrice);
    }
}

std::vector<std::tuple<int,int>>
Encoder::Encode_Signal( const tiempo::datetime& date,float money, std::tuple<int,double,std::string> Signal)
{

    if (!_engine) {
        std::cerr << "The engine is null." << std::endl;
        throw std::runtime_error("null Encoder engine");
    }
    Load_Prices(date);
    //std::cout<< "Prices Loaded P=" << Prices.size() << std::endl;
    
    for (auto p :Prices)
        std::cout << p <<std::endl;
    //std::cout << " Assets="  << id_Assets.size()<< std::endl;
    //for (auto a :id_Assets)
    //    std::cout << a <<std::endl;
    std::cout << " Encode signal with money " << money << std::endl;
    auto s = _engine->Encode_Signal(date, money,Param,Signal,Prices);
    //std::vector<std::tuple<int,int >> a ;
    //a.push_back(std::make_tuple(1,1));
    return s;
    
}

