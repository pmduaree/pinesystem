//
//  Signal_Encoder_Pair.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//

#ifndef ____Encoder_Pair__
#define ____Encoder_Pair__

#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <numeric>
#include "Encoder_Engine.hpp"
#include "../Utils/stringparpars.hpp"

class Encoder_Engine_Pair : public Encoder_Engine
{

public:
    Encoder_Engine_Pair(std::vector<int> Assets): Encoder_Engine(Assets){}
    virtual  ~Encoder_Engine_Pair(){}
    virtual
    std::vector<std::tuple<int,int> >
    Encode_Signal(const tiempo::datetime& date,
                  float Money,
                  std::string Param,
                  std::tuple<int,double,std::string> Signal,
                  std::vector<double> Prices)
    {
        std::cout << "Encode Pair Signal" << std::endl;
        
        int SignalType     =  std::get<SIGNALTYPE_COLUMN>(Signal);
        double SignalValue = std::get<SIGNALVALUE_COLUMN>(Signal);
        std::string Comment_ = std::get<COMMENT_COLUMN>(Signal);
        StringParamParser paramparser(Comment_);
        
        std::cout << (boost::format("Encoding Signal %d \t %0.3f  \t %s \n ") % SignalType % SignalValue % Comment_ ).str() << std::endl;
        std::string Action = paramparser.get<std::string>("ACTION","");
        std::vector<int> Stock(Prices.size());
        
       /* std::cout << "SignalVal=" << SignalValue << std::endl;
        std::cout<<" Action="  << Action << " ->" << std::get<1>(Signal) <<  std::endl;
        std::cout << " Prices="  << Prices.size() << std::endl;
        for (auto p :Prices)
            std::cout << p <<std::endl;
        std::cout << " Assets="  << Id_Assets.size()<< std::endl;
        for (auto a :Id_Assets)
            std::cout << a <<std::endl;
        */
        
        
        //Exit -1
        if (SignalValue == _EXIT_TRADE_SIGNAL ||  SignalValue == _WAIT_TRADE_SIGNAL )
        {
            Stock[0]=Stock[1]= 0;
        }

        //Enter  +1
        if(SignalValue == _ENTER_TRADE_SIGNAL)
        {   std::cout << "Encoding Enter trade " << std::endl;
            //BUY
            if(Action == "UP")
            {
                 Stock[1] =  round(Money /(2*Prices[1]));
                 Stock[0] = -round(Stock[1] * Prices[0]);

            }
            //SELL
            if(Action == "DOWN")
            {
                 Stock[1] = -round( Money /(2*Prices[1]));
                 Stock[0] =  round(-Stock[1] * Prices[0]);
            }
        }
        
        std::vector<std::tuple<int, int>> out;
        for (int unsigned i =0; i!= Prices.size();i++){
            out.push_back( std::make_tuple( Id_Assets[i], Stock[i]  )   );
        
            std::cout << "AssetID=" << Id_Assets[i] << " Stock=" << Stock[i] << std::endl;
        }
        
        return out;
    }
};


#endif /* defined(____Signal_Encoder_Pair__) */
