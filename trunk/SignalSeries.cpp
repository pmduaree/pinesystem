 //
//  SignalSeries.cpp
//  
//
//  Created by David de la Rosa on 6/23/14.
//
//
#include "SignalSeries.hpp"
#define SECONDS_SPAN_SELECTION 5

SignalSeries::SignalSeries(std::shared_ptr<sql_connection> _conn,int strategy)
:conn(_conn),id_strategy(0),onTrade(0)
{
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("SELECT `S`.`id`, `SE`.`CurrentParam`, `S`.`freq`, `S`.`portfolio_id`, \
                                       `SE`.`id` as SignalerID ,`ST`.`assetType`,  \
                                       `ST`.`Name`, `ST`.`Hrs_FarBack` FROM `Main`.`Strategies` S \
                                       INNER JOIN `Main`.`SignalerEngines` SE\
                                       ON `S`.`SignalerEngine_id` = `SE`.`id`  \
                                       INNER JOIN `Main`.`SignalerType` ST \
                                       ON `SE`.`Type_id` = `ST`.`id` \
                                       WHERE `S`.`id`= %d ") % strategy ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() > 0) {
        res->first();
        id_strategy = res->getInt("id");
        freq = res->getInt("freq");
        id_portfolio = res->getInt("portfolio_id");
        Hrs_Back     = res->getInt("Hrs_FarBack");
        StratParam  = res->getString("CurrentParam");
        SignalerID  = res->getInt("SignalerID");
        int Type    = res->getInt("assetType");
        TypeOfAssetMultiple = (Type == MULTIPLE_ASSET)? true: false;
        if(!TypeOfAssetMultiple)
        {
              std::string query = (boost::format("SELECT instrument_id FROM `Main`.`Portfolio` \
                                       WHERE `portfolio_id`=%d")
                         % id_portfolio).str();
            std::unique_ptr<sql::Statement> stmt(con->createStatement());
            std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
            if(res->rowsCount() != 0)
            {
                res->first();
                id_asset = res->getInt("instrument_id");
            }
        }
    }
    conn->unlock();
}

void SignalSeries::setType(std::string type)
{
    this->type = type;
}

std::tuple<int, double,std::string>
SignalSeries::Load_ONE_Signal_formCalculations(tiempo::datetime date)
{
    
    
    if (TypeOfAssetMultiple) {
        
        std::shared_ptr<MAsset> Multiple_asset = std::make_shared<MAsset>(conn, id_portfolio);
        Multiple_asset->setInputType(type);
        Multiple_asset->init(std::chrono::hours(Hrs_Back),date);
        //std::cout << "Calculate with CurrentParam " << StratParam <<  " As Param" << std::endl;
        //int offset = 0;
        auto strategy = std::make_shared<Strategy>(conn, id_strategy);
        //auto on_date = offset_date(date, freq, offset);
        //std::cout << "Regularized date: " << on_date << std::endl;
        //std::cout << "Calculate with " << strategy->get_param() << " As Param" << std::endl;
        std::tuple<int,double,std::string> res = strategy->calculate_signal(Multiple_asset, date,false);
        return res;
    }else{
        
        std::shared_ptr<Asset> Single_asset = std::make_shared<Asset>(conn, id_asset);
        Single_asset->setInputType(type);

        Single_asset->init(std::chrono::hours(Hrs_Back),date);
        //std::cout << "Calculate with CurrentParam " << StratParam <<  " As Param" << std::endl;
        //int offset = 0;
        auto strategy = std::make_shared<Strategy>(conn, id_strategy);
        //auto on_date = offset_date(date, freq, offset);
        //std::cout << "Regularized date: " << on_date << std::endl;
        //std::cout << "Calculate with " << strategy->get_param() << " As Param" << std::endl;
        bool trade = (onTrade == 0) ? false : true;
        std::tuple<int,double,std::string> res = strategy->calculate_signal(Single_asset, date, trade);
        dataContainer<tix_series> cont(conn, strategy->get_id(), date);
        int newSig = cont.getBidAskSignal(date, std::get<0>(res), 1, 1, 0,0);
        if(newSig != 0 && onTrade != newSig)
            onTrade = newSig;
        res = std::make_tuple(newSig, std::get<1>(res), std::get<2>(res));
        return res;
        }
}
void SignalSeries::LoadSignals_formCalculations(tiempo::datetime from,tiempo::datetime to)
{
    tiempo::datetime FROM = tiempo::datetime( from.to_string("%Y-%m-%d %H:%M:00") );
    for (;FROM < to; FROM +=  std::chrono::minutes(freq) )
    {
        Signals[FROM] = Load_ONE_Signal_formCalculations(FROM);
        std::cout << "[" << FROM.to_string() << "]  " <<  std::get<0>(Signals[FROM])  << " "  << std::get<1>(Signals[FROM])  << std::endl;
    }
}
void SignalSeries::Load_ONE_Signal_formDB(tiempo::datetime from,tiempo::datetime to,int type)
{
    conn->lock();
    auto con = conn->connection();
    std::string query = (boost::format("SELECT * FROM `Main`.`RawSignals` \
                                       WHERE `strategy_id`=%d AND date BETWEEN \
                                       '%s' AND '%s' AND Live= %d")
                         % id_strategy
                         % from.to_string("%Y-%m-%d %H:%M:%S")
                         % to.to_string("%Y-%m-%d %H:%M:%S")
                         % type ).str();
    std::unique_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    while(res->next())
    {
        tiempo::datetime date   =   tiempo::datetime(  res->getString("date") );
        int SignalType          =   res->getDouble("SignalType");
        double Ppos             =   res->getDouble("Ppos");
        std::string comment     =   res->getString("Rpar");
        std::cout << "find " << Ppos << " -> " << comment << std::endl;
        Signals[date] = std::make_tuple(SignalType, Ppos, comment);
    }
    conn->unlock();
}
void SignalSeries::LoadSignals_formDB(tiempo::datetime from,tiempo::datetime to,int type  )
{
    tiempo::datetime FROM = tiempo::datetime( from.to_string("%Y-%m-%d %H:%M:00") );
   std::cout << " Init Signall from " << FROM.to_string("%Y-%m-%d %H:%M:%S")  <<
    " to " << to.to_string("%Y-%m-%d %H:%M:%S")    << std::endl;
    
    for (;FROM < to; FROM +=  std::chrono::minutes(freq) )
    {
        Load_ONE_Signal_formDB(FROM-std::chrono::seconds(SECONDS_SPAN_SELECTION),
                               FROM+std::chrono::seconds(SECONDS_SPAN_SELECTION) , type );
    }
}
void SignalSeries::Init(tiempo::datetime from,tiempo::datetime to,int Type  )
{
    switch (Type) {
        case LOAD_FROM_DB:
            std::cout << "Calculate From the DB NON LIVE SIGNALS" << std::endl;
            LoadSignals_formDB(from,to, NON_LIVE_SIGNALS);
            break;
        case LOAD_FROM_DB_LIVE:
            std::cout << "Calculate From the DB LIVE SIGNALS" << std::endl;
            LoadSignals_formDB(from,to, LIVE_SIGNALS);
            break;
        case CALCULATE_ON_THE_FLY:
            std::cout<< "Calculate on the Fly "<< std::endl;
            LoadSignals_formCalculations(from,to);
            break;
        default :
            LoadSignals_formDB(from,to,NON_LIVE_SIGNALS);
            break;
    }
}
std::string SignalSeries::toString()
{
    std::string out;
    for (auto it=Signals.begin(); it!=Signals.end(); ++it )
    {
        out += " ";
        out += it->first.to_string("%Y-%m-%d %H:%M:%S");
        out += " ->  <";
        int SignalType = std::get<0>(it->second);
        float Ppos = std::get<1>(it->second);
        std::string comment = std::get<2>(it->second);
        out += std::to_string(SignalType);
        out += " , ";
        out += std::to_string(Ppos);
        out += " , ";
        out += comment;
        out += "> \n";
    }
    return out;
}
