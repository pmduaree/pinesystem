#ifndef __xxx_trend__
#define __xxx_trend__

#include <cstdlib>
#include <tuple>
#include <mutex>
#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"
#include "linalg.hpp"

class Trend
{
    const tix_series& tix;
  public:
    Trend(const tix_series& tix_);
    
    bool operator()(const tiempo::datetime& date_1, 
                    const tiempo::datetime& date_2, double& trend) const;
    bool operator()(const tiempo::datetime& date_2, 
                    const tiempo::duration& dur, double& trend) const;
    
  private:
    bool calculate_trend(const tiempo::datetime& date_1, 
                         const tiempo::datetime& date_2, double& trend) const;
    // cache table.
    typedef std::map<tiempo::duration, std::tuple<bool, double> > MapX;
    mutable std::map<tiempo::datetime, MapX> _cache;
    mutable std::mutex _mux;
};
    

#endif

