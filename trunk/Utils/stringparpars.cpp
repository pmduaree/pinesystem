
#include <map>
#include <vector>
#include <string>
#include <tuple>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/format.hpp>
#include "stringparpars.hpp"


#if 1


template<>
struct Empty<std::string>
{
    constexpr static const char* value = "";
};

template<>
struct Empty<int>
{
    constexpr static int value = 0;
};

template<class T>
struct ToStringer
{
    std::string operator()(const T& z)
    {
        return boost::lexical_cast<std::string>(z);
    }
};

template<>
struct ToStringer<double>
{
    std::string operator()(const double& z)
    {
        // what's the range?
        if (z==0.0) return "0";
        double za = fabs(z);
        
        std::string rv = "";
        if (za < 1e2 && za > 1e-5) {
            int nafter = 6 - (log(za) / log(10.0));
            std::string xt = std::string("%") + (boost::format(".%df") % nafter).str();
            rv = (boost::format(xt) % z).str();
        } else {
            rv = boost::lexical_cast<std::string>(z);
        }
        return rv;
    }

};


#endif

StringParamParser::StringParamParser()
{
}

StringParamParser::StringParamParser(const std::string& z)
{
    init(z);
}

StringParamParser::StringParamParser(const StringParamParser& z)
    : data(z.data)
{
}
    
void StringParamParser::init(const std::string& z)
{
    data = parse_stringparam(z);
}

template<typename T>
T StringParamParser::get(const std::string& name, const T& defval) const
{
    auto it = data.find(name);
    if (it != data.end())
        return boost::lexical_cast<T>(it->second);
    //std::cout << "Using the default value for " << name << ": " << defval << std::endl;
    return defval;
    //return (it != data.end() ? boost::lexical_cast<T>(it->second) : defval );
}

template<typename T>
void StringParamParser::put(const std::string& name, const T& val)
{
    ToStringer<T> tostr;
    data[name] = tostr(val);
    //data[name] = boost::lexical_cast<std::string>(val);
}

void StringParamParser::from_string(const std::string& x)
{ 
    data = parse_stringparam(x);
}
    
std::string StringParamParser::to_string() const
{
    std::string s;
    for(const auto& z : data) {
        std::string one_fld = z.first  + "=" + z.second;
        if (s.empty()) s = one_fld ; else s += "," + one_fld;
    }
    return s;
}

std::map<std::string,std::string> StringParamParser::parse_stringparam(const std::string& z) const
{
    //std::cout << "Parsing stringparam: " << z << std::endl;
    std::map<std::string,std::string> Z;
    std::vector<std::string> fields;
    boost::split(fields, z, boost::is_any_of(","));
    for(const auto& f : fields) {
        std::string z1,z2;
        std::tie(z1,z2) = parse_one_param_string(f);
        //std::cout << boost::format("Obtained: [%s=%s]\n")%z1%z2;
        Z[z1] = z2;
    }
    return Z;
}

std::tuple<std::string,std::string> StringParamParser::parse_one_param_string(const std::string& z) const
{
    auto p = z.find("=");
    std::string z1 = z.substr(0, p);
    std::string z2 = z.substr(p+1);
    return std::make_tuple(z1,z2);
}


// template instantiation

template void StringParamParser::put<std::string>(const std::string& name, const std::string& val);
template void StringParamParser::put<int>(const std::string& name, const int& val);
template void StringParamParser::put<size_t>(const std::string& name, const size_t& val);
template void StringParamParser::put<double>(const std::string& name, const double& val);


template int StringParamParser::get<int>(const std::string& name, const int& defval) const;
template size_t StringParamParser::get<size_t>(const std::string& name, const size_t& defval) const;
template double StringParamParser::get<double>(const std::string& name, const double& defval) const;
template std::string StringParamParser::get<std::string>(const std::string& name, const std::string& defval) const;




