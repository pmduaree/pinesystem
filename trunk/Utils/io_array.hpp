#ifndef __IO_ARRAY__
#define __IO_ARRAY__
//////////////////////////////////////////////////////////////////////////////
/// \file io_array.hpp
/// \brief std::array I/O support
///
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  October 2013
///
//////////////////////////////////////////////////////////////////////////////
#include <array>
#include <cassert>
#include <stdexcept>

#include "io.hpp"

namespace morana {

template<size_t N, class T>
struct io_module<std::array<T,N> >
{
    void read(std::array<T,N>& v, std::ifstream& fp)
    {
        uint32_t beg_flag, end_flag, obj_id, version, size_of_sizet, size_of_T;
        morana::read(beg_flag, fp);
        if (beg_flag != static_cast<uint32_t>(io_constants::magic_begin))
            throw std::runtime_error("Read_error");
        morana::read(obj_id, fp);
        if (obj_id != static_cast<uint32_t>(io_constants::id_array))
            throw std::runtime_error("Read_error");
        morana::read(version, fp);
        if (version != 1) {
            //TODO
            throw std::runtime_error("Read_error");
        }
        morana::read(size_of_T, fp);
        if (size_of_T != sizeof(T) )
            throw std::runtime_error("cross architecture not supported yet.");
        uint32_t n;
        morana::read(n, fp);
        if (n != N) throw std::runtime_error("Read_error");
        fp.read(reinterpret_cast<char*>(v.data()), n * size_of_T);
        morana::read(end_flag, fp);
        if (end_flag != static_cast<uint32_t>(io_constants::magic_end))
            throw std::runtime_error("Read_error");
    }
    
    void write(const std::array<T,N>& v, std::ofstream& fp)
    {
        uint32_t beg_flag = static_cast<uint32_t>(io_constants::magic_begin);
        uint32_t end_flag = static_cast<uint32_t>(io_constants::magic_end);
        uint32_t object_identifier = static_cast<uint32_t>(io_constants::id_array);
        uint32_t version = 1;
        uint32_t size_of_T = sizeof(T);
        uint32_t n = static_cast<uint32_t>(N);
        
        morana::write(beg_flag, fp);
        morana::write(object_identifier, fp);
        morana::write(version, fp);
        morana::write(size_of_T, fp);
        morana::write(n, fp);
        fp.write(reinterpret_cast<const char*>(v.data()), size_of_T * n);
        morana::write(end_flag, fp);
    }
};
}

// << ARRAY 
template<class T, size_t N>
std::ostream& operator<<(std::ostream& xx, const std::array<T,N>& arr)
{
    std::stringstream str;
    // do it like the matlab does it.
    str << "[";
    for(size_t i=0;i<N;++i) {
        str << arr[i] ;
		if(i<N-1) str << ",";
    }
    str << "]";
    xx << str.str();
    return xx;
}


#endif

