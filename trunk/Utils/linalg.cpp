#include "linalg.hpp"

#include <tuple>
#include <vector>

// \sum_i [ (y_i - (c + alpha * x_i) ]^2 = min
// boils down to
//  alpha = \sum_i (x_i y_i) / \sum_i (x_i^2)
// and 
//  \sum_i (y_i - c - alpha * x_i) ] = 0 --> 
//   c = (1/n) \sum_i (y_i - alpha * x_i)
std::tuple<double,double> 
linear_fit(const std::vector<double>& X, const std::vector<double>& Y)
{
    size_t n = X.size();
    double m_x = 0, m_y = 0;
    for(const auto& x: X) m_x += x;
    m_x *= 1.0 / n;

    for(const auto& y: Y) m_y += y;
    m_y *= 1.0 / n;

    double cov = 0.0, var = 0.0;
    for(size_t i=0;i<n;++i) {
        cov += (X[i] - m_x) * (Y[i] - m_y);
        var += (X[i] - m_x) * (X[i] - m_x);
    }
    double alpha = cov / var;
    double c = m_y - alpha * m_x;
    return std::make_tuple(c, alpha);
}

std::tuple<double,double> alpha_fit_linear(const std::vector<double>& X, 
                                           const std::vector<double>& Y)
{
    size_t n = X.size();
    if (n < 2) return std::make_tuple(0.0, 1.0);
    double xy = 0.0, xx = 0.0, m_x=0, m_y=0;
    for(size_t i=0;i<n;++i) {
        xy += X[i] * Y[i];
        xx += X[i] * X[i];
        m_x += X[i];
        m_y += Y[i];
    }
    m_x *= 1.0/n;
    m_y *= 1.0/n;
    double alpha = xy / xx;
    double var = 0.0;
    for(size_t i=0;i<n;++i)
        var += (Y[i] - alpha * X[i])*(Y[i] - alpha * X[i]);
    return std::make_tuple(alpha,var);
}


