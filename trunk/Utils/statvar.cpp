#include "statvar.hpp"

bool StatVar::operator()(const tix_series& tix, const tiempo::datetime& date_1, 
                const tiempo::datetime& date_2, 
                double& mean, double& var) const
{
    mean = 0.0;
    var = 0.0;
    
    //-------------------------------------------------------------------
    // find iterator range
    //-------------------------------------------------------------------
    auto it_1 = tix.begin();
    while( it_1 != tix.end() && it_1->first < date_1) ++it_1;
    if (it_1 == tix.end()) return false;
    auto it_2 = it_1;
    while( it_2 != tix.end() && it_2->first <= date_2) ++it_2;
    if (it_2 == it_1) return false;
    
    //-------------------------------------------------------------------
    // calculate the mean value
    //-------------------------------------------------------------------
    double mx = 0.0;
    size_t cnt = 0;
    for(auto it=it_1 ; it != it_2; ++it) {
        mx += it->second.p;
        ++cnt;
    }
    if (cnt < 2) return false;
    mx /= cnt;

    
    //-------------------------------------------------------------------
    // calculate the variance
    //-------------------------------------------------------------------
    double m2 = 0.0;
    for(auto it=it_1 ; it != it_2; ++it) {
        double v = it->second.p - mx;
        m2 += v * v;
    }
    m2 /= (cnt-1);

    mean = mx;
    var = m2;
    return true;
}


