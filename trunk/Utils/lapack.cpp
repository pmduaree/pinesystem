#include "lapack.hpp"

namespace lapack 
{

extern "C" {
void dgesvd_(const char*, const char*, const int*, const int*, 
             double*, const int*, double*, 
             double*, const int*, double*, const int*,
             double*, const int*,int*);
}


void gesvd(const char* jobu, const char* jobvt, int m, int n, double* a, 
           int lda, double* s, double* u, int ldu, double* vt, int ldvt, 
           double* work, int lwork, int* info)
{
    dgesvd_(jobu,jobvt, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work,&lwork, info);
}

}

