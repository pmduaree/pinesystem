#ifndef __TENSOR_HPP__
#define __TENSOR_HPP__
//////////////////////////////////////////////////////////////////////////////
/// \file tensor.hpp
/// \brief A tensor container implementation
///
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  September 2013
///
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <cstdlib>
#include <type_traits>
#include <stdexcept>



namespace morana {

// these functions hsuold be moved some place else.
template<class T,size_t N>
T prod(const std::array<T,N>& X)
{
    T z = static_cast<T>(1);
    for(const auto& x: X) z *= x;
    return z;
}

template<class T,size_t N>
T sum(const std::array<T,N>& X)
{
    T z = static_cast<T>(0);
    for(const auto& x: X) z += x;
    return z;
}

////////////////////////////////////////////////////////////////////////////
/// \class tensor
/// \brief Tensor container
///
/// This class implements a tensor container which is based on the 
/// std::vector class with the so called Fortran ordering.
/// That makes the container directly compatible with all BLAS/LAPACK 
/// routines.
/// 
/// Example:
///     An element (i1,i2,i3) for a m1 x m2 x m3 tensor is converted to
///     the element i1 + m1 * (i2 + m2 * i3) of the underlying vector.
/// 
/// Matrices:
///     A matrix is a rank-2 tensor tensor<double,2>
///     An element (i,j) , that is the element in the i-th row and j-th column,
///     is given by A(i,j) or A({i,j}) or A(key) where 
///        key=std::array<size_t,2>(); key[0]=i; key[1]=j;
///     
///     The elements are stored in the memory in the same way as in Fortran,
///     that means for a m x n matrix, first 'm' elements are the first column,
///     the elements m+1, m+2, ..., m+m is the second column and so on.
/// 
///  Implementation Logic:
///     The tensor class has two members, a statically allocated array 
///     and a dynamically allocated vector.
///     The array holds the extents of the tensor (e.g. matrix dimensions)
///     and the vector holds the values. 
///     The size of the vector should at all times be equal to the product of
///     the elements in the array.
///    
/// Author:
///     Iztok Pizorn <pizorn@mail.ru> 
///     All rights reserved.
////////////////////////////////////////////////////////////////////////////
template<typename T, size_t N>
class tensor
{
    static_assert(std::is_arithmetic<T>::value, 
                  "Only tensors of arithmetic types are supported");
    static_assert(N>0, "Theres no such thing as zero rank tensors");
    static_assert(N>1, "Why tensor if it's a vector???");
    static_assert(N<=8, "No, N>8 is too much. Fix the implementation");
    
    typedef std::array<size_t,N> Array;
    typedef std::vector<T> Vector;
    
    Array _M;
    Vector _V;
  public:
    //-----------------------------------------------------------------------
    // Constructors
    //-----------------------------------------------------------------------
    tensor(): _V() {_M.fill(static_cast<size_t>(0)); }
    tensor(const Array& shape): _M(shape), _V(prod(shape)) {}
    tensor(const Array& shape, const Vector& v): _M(shape), _V(v)
    {
        if (prod(_M) != _V.size())  
            throw std::runtime_error("incompatible input");
    }
    tensor(const Array& shape, const T& v): _M(shape), _V(prod(shape), v) {}
    tensor(const tensor<T,N>& x): _M(x._M), _V(x._V) {}
    
    tensor<T,N>& operator=(const tensor<T,N>& x)
    {
        _M = x._M;
        _V = x._V;
        return *this;
    }

    //copy  to 1D tensor
	tensor<T,1>& operator=(const std::vector<T>& x)
    {
        //_M = x._M;
        _V = x;
        return *this;
    }

	//-----------------------------------------------------------------------
    // Iterators
    //-----------------------------------------------------------------------
    typedef typename Vector::iterator iterator;
    typedef typename Vector::const_iterator const_iterator;
    const_iterator begin() const { return _V.begin(); }
    const_iterator end() const { return _V.end(); }
    iterator begin() { return _V.begin(); }
    iterator end() { return _V.end(); }
    const_iterator cbegin() const { return _V.begin(); }
    const_iterator cend() const { return _V.end(); }
    
    //-----------------------------------------------------------------------
    // Size/shape inquiry and manipulation
    //-----------------------------------------------------------------------
    const Array& shape() const { return _M; }	//returns no of [rows-cols]
    size_t M(size_t j) const { return _M[j];}
    
    size_t size() const { return _V.size(); }
    void resize(const Array& shape)
    {
        _M = shape;
        _V.resize(prod(_M));
    }

    void reshape(const Array& shape)
    {
        if (prod(shape) != prod(_M))
            throw std::runtime_error("Total size mismatch");
        _M = shape;
    }
    
    bool empty() const {return _V.empty(); }
    const Vector& vector() const { return _V; }
    Vector& vector() { return _V; }
    void reserve(const Array& shape)
    {
        _V.reserve(prod(_M));
    }
    
    //-----------------------------------------------------------------------
    // raw memory access
    //-----------------------------------------------------------------------
    T* data() { return _V.data(); }
    const T* data() const { return _V.data(); }

    //-----------------------------------------------------------------------
    // element access operators, read-only and read/write
    //-----------------------------------------------------------------------
    const T& operator[](size_t i) const { return _V[i]; }
    T& operator[](size_t i) { return _V[i]; }


    const T& operator()(const Array& idx) const
    {
        return operator[](arr2idx(idx));
    }
    
    T& operator()(const Array& idx)
    {
        return operator[](arr2idx(idx));
    }

    const T& operator()(size_t i0, size_t i1=0, size_t i2=0, size_t i3=0,
                  size_t i4=0, size_t i5=0, size_t i6=0, size_t i7=0) const
    {
        return operator[](pos2idx(i0,i1,i2,i3,i4,i5,i6,i7));
    }
    
    T& operator()(size_t i0, size_t i1=0, size_t i2=0, size_t i3=0,
                  size_t i4=0, size_t i5=0, size_t i6=0, size_t i7=0)
    {
        return operator[](pos2idx(i0,i1,i2,i3,i4,i5,i6,i7));
    }
    
    //-----------------------------------------------------------------------
    // Conversion between tensor and linear vector layout
    //-----------------------------------------------------------------------
    size_t pos2idx(size_t i0, size_t i1=0, size_t i2=0, size_t i3=0,
                   size_t i4=0, size_t i5=0, size_t i6=0, size_t i7=0) const
    {
        std::initializer_list<size_t> x = {i0,i1,i2,i3,i4,i5,i6,i7};
        Array idx;
        std::copy(x.begin(), x.begin() + N, idx.begin());
        return arr2idx(idx);
    }
    
    size_t arr2idx(const Array& idx) const
    {
        size_t z = 1;
        size_t w = 0;
        for(size_t j=0;j<N;++j) {
            w += idx[j] * z;
            z *= _M[j];
        }
        return w;
    }

    void idx2arr(size_t ii, Array& idx) const
    {
        for(size_t j=0;j<N;++j) {
            idx[j] = ii % _M[j];
            ii /= _M[j];
        }
    }
    
    Array idx2arr(size_t ii) const
    {
        Array idx;
        idx2arr(ii,idx);
        return idx;
    }
};

//GETROW
template<class T,size_t N>
void getRow(const morana::tensor<T,N>& input, size_t row, 
            std::vector<T>& output)
{
    output.clear();
    for(auto i=0;i<input.M(1);++i) output.push_back(input(row,i)); 
};


}   // end of namespace morana

// we don't really need that but anyway, let's just have it, for now.








#endif
