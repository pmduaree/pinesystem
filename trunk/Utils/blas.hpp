#ifndef __BLAS_HPP__
#define __BLAS_HPP__


namespace blas {

void axpy(int n, double a, double const* x, int incx, double* y, int incy);
double dot(int n, const double* x, int incx, const double* y, int incy);
double nrm2(int n, const double* x, int incx);
void scal(int n, double a, double* x, int incx);
void gemv(const char* trans, int m, int n, double alpha, const double* A, 
          int lda, const double* x, int incx, double beta, double* y, int incy);
void gemm(const char* transa, const char* transb, int m, int n, int k, 
          double alpha, const double* A, int lda, const double* B, int ldb,
          double beta, double* C, int ldc);
void copy(int n, const double* x, int incx, double* y, int incy);

}   // end of namespace blas

#endif
