#ifndef __TENSOR_ALGEBRA__
#define __TENSOR_ALGEBRA__

#include <array>
#include <cassert>
#include "tensor.hpp"
#include "array_algebra.hpp"

namespace morana {

template<typename T,size_t N>
void inverse_permutation(const std::array<T,N>& P, std::array<T,N>& iP)
{
    for(size_t i=0;i<N;++i) iP[P[i]] = i;
}

template<typename T,size_t N>
std::array<T,N> inverse_permutation(const std::array<T,N>& P)
{
    std::array<T,N> iP;
    inverse_permutation(P, iP);
    return iP;
}


// yeah well this should be written for a more generic case by first reducing
// the permutatino to the irreducible permutation and then permuting the
// irreducible array.....
// i don't have time now but i have the code already from before...
template<size_t N,typename T>
void transpose(const tensor<T,N>& A, const std::array<size_t,N>& P,
               tensor<T,N>& B)
{
    if (P == range<N>()) {
        // no transpose needed, just copy...
        B = A;
        return;
    }
    
    static_assert( N == 2, "sorry, only N=2 for now, manana, manana... ");
    assert( (P == make_array<size_t,2>(1,0))  );
    B.resize(transpose(A.shape(), P) );
    size_t m = A.M(0), n = A.M(1);
    for(size_t j=0;j<n;++j)
        for(int i=0;i<m;++i) B(j,i) = A(i,j);

}




} // end of namespace morana


#endif
