#ifndef _statvar__x_
#define _statvar__x_

#include <cstdlib>
#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"

class StatVar
{
  public:
    StatVar() {}
    
    bool operator()(const tix_series& tix, const tiempo::datetime& date_1, 
                    const tiempo::datetime& date_2, 
                    double& mean, double& var) const;
    
    bool operator()(const tix_series& tix, const tiempo::datetime& date, 
                   const tiempo::duration& dur, double& vol, double& rel_vol) const
    {
        return operator()(tix,date-dur,date,vol,rel_vol);
    }
};


#endif
