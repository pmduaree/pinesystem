#ifndef __RANDOM_HPP__
#define __RANDOM_HPP__

#include <random>
#include <array>
#include <chrono>

std::default_random_engine& randomGenerator();
void make_random_seed();
void randomize_vector(std::vector<double>& v,double rate);
void make_stochastic_vector(std::vector<double>& v);
int random_sign();
void make_stochastic_scalar(double& v, double lo=0,double hi=1);
 
#endif
