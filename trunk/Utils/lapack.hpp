#ifndef __LAPACK_HPP__
#define __LAPACK_HPP__

namespace lapack 
{

void gesvd(const char* jobu, const char* jobvt, int m, int n, double* a, 
           int lda, double* s, double* u, int ldu, double* vt, int ldvt, 
           double* work, int lwork, int* info);
}


#endif
