#include "blas.hpp"


namespace blas
{

extern "C" {
void dcopy_(const int*, const double*, const int*, double*, const int*);
void dscal_(const int*, const double*, double*, const int*);
void daxpy_(const int*, const double*, const double*, const int*, 
            double*, const int*);

void dgemv_(const char*, const int*, const int*, 
            const double*, const double*, const int*, 
            const double*, const int*, const double*, double*, const int*);

void dgemm_(const char*, const char*, const int*, const int*, const int*,
            const double*, const double*, const int*, const double*, const int*,
            const double*, double*, const int*);


double dnrm2_(const int*, const double*, const int*);
double ddot_(const int*, const double*, const int*, const double*, const int*);
}


// in future, replace these functions by calls to BLAS
void axpy(int n, double a, double const* x, int incx, double* y, int incy)
{
#if 1
    daxpy_(&n, &a, x, &incx, y, &incy);
#else
    for(int i=0;i<n;++i) {
        y[i*incy] += a * x[i*incx];
    }
#endif
}

double dot(int n, const double* x, int incx, const double* y, int incy)
{
#if 0
    static_assert( std::is_integral<I1>::value, "dot , Integer");
    static_assert( std::is_integral<I2>::value, "dot , Integer");
    static_assert( std::is_integral<I3>::value, "dot , Integer");
    double z = 0;
    for(I1 i=0;i<n;++i)
        z += x[incx*i] * y[incy*i];
    return z;
#else
    return ddot_(&n, x, &incx, y, &incy);
#endif
}

double nrm2(int n, const double* x, int incx)
{
    #if 1
    return dnrm2_(&n, x, &incx);
    #else
    return sqrt(dot(n,x,incx,x,incx));
    #endif
}

void scal(int n, double a, double* x, int incx)
{
#if 0
    for(int i=0;i<n;++i)
        x[i*incx] *= a;
#else
    dscal_(&n, &a, x, &incx);
#endif
}

void gemv(const char* trans, int m, int n, double alpha, const double* A, 
          int lda, const double* x, int incx, double beta, double* y, int incy)
{
    #if 0
    if (trans[0] == 'N') {
        for(int i=0;i<m;++i) {
            y[incy*i] *= beta;
            for(int j=0;j<n;++j)
                y[incy*i] += alpha * A[i + lda * j] * x[incx*j];
        }
    } else {
        for(int j=0;j<n;++j) {
            y[incy*j] *= beta;
            for(int i=0;i<m;++i)
                y[incy*j] += alpha * A[i + lda * j] * x[incx*i];
        }
    }
    #else
    dgemv_(trans, &m, &n, &alpha, A, &lda, x, &incx, &beta, y, &incy);
    #endif
}



void gemm(const char* transa, const char* transb, int m, int n, int k, 
          double alpha, const double* A, int lda, const double* B, int ldb,
          double beta, double* C, int ldc)
{
    dgemm_(transa,transb,&m,&n,&k,&alpha,A,&lda,B,&ldb,&beta,C,&ldc);
}


void copy(int n, const double* x, int incx, double* y, int incy)
{
#if 0
    for(int i=0;i<n;++i)
        y[i*incy] = x[i*incx];
#else
    dcopy_(&n, x, &incx, y, &incy);
#endif
}




}   // end of namespace blas

