#ifndef __mail_sender__
#define __mail_sender__

#include <string>

class MailSender
{
  public:
    void send_message(const std::string& f_to, const std::string& f_subj, 
                      const std::string& f_body);
};


#endif

