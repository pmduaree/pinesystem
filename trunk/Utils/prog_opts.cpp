#include "prog_opts.hpp"

#include <type_traits>
#include <vector>
#include <string>
#include <cstring>
#include <stdexcept>
#include <iostream> 

#include <boost/lexical_cast.hpp>


// do something about it. there's this strict aliasing issue... ]
// it's not really that important but anyway....


template<class T>
void ValueType::set(const T& x)
{
    memcpy(sv, &x, sizeof(T));
}

template<class T>
T ValueType::get() const
{
    T foo;
    memcpy(&foo, sv, sizeof(T));
    return foo;
}

template<>
std::string ValueType::get<std::string>() const
{
    std::string v(sv);
    return v;
}

template<>
void ValueType::set<std::string>(const std::string& v)
{
    strcpy(sv, v.c_str());
}





template<class T, class Enable=void>
struct ModeTrans
{
    typedef std::string type;
};

template<class T>
struct ModeTrans<T,
    typename std::enable_if<std::is_integral<T>::value && !std::is_same<T,bool>::value>::type>
{
    typedef long type;
};

template<class T>
struct ModeTrans<T,
    typename std::enable_if<std::is_floating_point<T>::value>::type>
{
    typedef double type;
};


template<class T>
struct ModeTrans<T,
    typename std::enable_if<std::is_same<bool,T>::value>::type >
{
    typedef bool type;
};




prog_opts::prog_opts()
{
    // we always have the help option.
    add_option<bool>("--help", OptionType::Boolean, "help", false);
}


template<class T>
T prog_opts::option(const std::string& name) const
{
    auto it = options.begin();
    while (it != options.end() && it->name.compare(name)) ++it;
    if (it == options.end()) throw std::runtime_error("invalid option naem");
    return it->value.get<typename ModeTrans<T>::type>();
}

template<class T>
T prog_opts::argument(size_t idx) const
{
    if (idx >= arguments.size()) throw std::runtime_error("invalid arg");
    const std::string& arg = arguments[idx];
    return boost::lexical_cast<T>(arg);
}

template<class T>
void prog_opts::add_option(const std::string& short_tag, 
                           const std::string& long_tag,
                           OptionType type, 
                           const std::string& name,
                           const T& defval)
{
    OptionItem item;
    item.name = name;
    item.long_tag = long_tag;
    item.short_tag = short_tag;
    item.type = type;
    item.value.set<typename ModeTrans<T>::type>(defval);
    options.push_back(item);
}



template<class T>
void prog_opts::add_option(const std::string& tag, 
                           OptionType type, 
                           const std::string& name, const T& defval)
{
    if (tag.find("--")==0)
        add_option("",tag, type, name, defval);
    else if (tag.find_first_of('-')==0)
        add_option(tag,"",type, name, defval);
    else
        throw std::runtime_error("invalid tag");
}
void prog_opts::associate_value(std::vector<OptionItem>::iterator it, 
                                const std::string& v)
{
    if (it->type == OptionType::Integer)
        it->value.set(boost::lexical_cast<long>(v));
    else if (it->type == OptionType::Float)
        it->value.set(boost::lexical_cast<double>(v));
    else if (it->type == OptionType::String)
        it->value.set(v);
    else
        throw std::runtime_error("associate value failed");
}

std::string prog_opts::help() const
{
    std::string h;
    std::string cmd;
    if (program_name.rfind("/") != std::string::npos)
        cmd = program_name.substr(program_name.rfind("/")+1);
    else
        cmd = program_name;
    h += cmd;
    for (const auto& o: options) {
        std::string me = "";
        me += "[";
        if (!o.short_tag.empty() && !o.long_tag.empty()) 
            me += o.short_tag + " | " + o.long_tag;
        else if (!o.short_tag.empty())
            me += o.short_tag;
        else if (!o.long_tag.empty())
            me += o.long_tag;
        me +=  "]";
        // how far are we from the beginning of the line
        size_t how_far = h.size();
        if (h.rfind("\n")!= std::string::npos)
            how_far -= h.rfind("\n");
        if (how_far + me.size() + 1 <= 80)
            h +=  " " + me;
        else {
            h += "\n";
            for(size_t i=0;i<=cmd.size();++i)
                h += " ";
            h += me;
        }
    }
    return h;
}


bool prog_opts::parse_args(int argc, char** argv)
{
    program_name = argv[0];
    int c = 1;
    while(c < argc) {
        std::string s(argv[c]);
        if (s.find("--")==0) {
            // it's the long one
            size_t w = s.find_first_of('=');
            std::string long_tag = (w != std::string::npos ? s.substr(0,w) : s );
            //std::cout << "parsing long_tag: " << long_tag << std::endl;
            auto it = options.begin();
            while (it != options.end() && it->long_tag.compare(long_tag)) ++it;
            if (it == options.end()) {
                std::cerr << "Option " << s << " not allowed." << std::endl;
                return false;
                //throw std::runtime_error("not allowed option");
            }
            if (it->type == OptionType::Boolean) {
                // then just set it and that's it.
                it->value.set(true);
            } else {
                // get the value.
                std::string v = s.substr(w+1);
                associate_value(it, v);
            }
        } else if (s.find("-")==0) {
            // find the name alright.
            std::string short_tag = s;
            auto it = options.begin();
            while(it != options.end() && it->short_tag.compare(short_tag)) {
                ++it;
            }
            if (it == options.end()) {
                std::cerr << "Option " << s << " not allowed." << std::endl;
                return false;
            }
            if (it->type == OptionType::Boolean) {
                it->value.set(true);
            } else {
                if (c == argc-1) throw std::runtime_error("missing ");
                std::string v = std::string(argv[++c]);
                associate_value(it, v);
            }
        } else {
            // it's an argument
            arguments.push_back(s);
        }
        ++c;
    }
    return true;
}

// explicit instantiation
template void prog_opts::add_option<int>(const std::string&,const std::string&, OptionType, 
                                    const std::string&,  const int&);
template void prog_opts::add_option<double>(const std::string&,const std::string&, OptionType, 
                                       const std::string&,  const double&);
template void prog_opts::add_option<bool>(const std::string&,const std::string&, OptionType, 
                                       const std::string&,  const bool&);
template void prog_opts::add_option<size_t>(const std::string&,const std::string&, OptionType, 
                                       const std::string&,  const size_t&);
template void prog_opts::add_option<std::string>(const std::string&,const std::string&, OptionType, 
                                       const std::string&,  const std::string&);

template void prog_opts::add_option<int>(const std::string&, OptionType, const std::string& , 
                                         const int& defval);
template void prog_opts::add_option<size_t>(const std::string&, OptionType, const std::string& , 
                                            const size_t& defval);
template void prog_opts::add_option<double>(const std::string&, OptionType, const std::string& , 
                                            const double& defval);
template void prog_opts::add_option<std::string>(const std::string&, OptionType, const std::string& , 
                                                 const std::string& defval);
template void prog_opts::add_option<bool>(const std::string&, OptionType, const std::string& , 
                                          const bool& defval);
template int prog_opts::option<int>(const std::string& name) const;
template size_t prog_opts::option<size_t>(const std::string& name) const;
template bool prog_opts::option<bool>(const std::string& name) const;
template double prog_opts::option<double>(const std::string& name) const;
template std::string prog_opts::option<std::string>(const std::string& name) const;


template bool prog_opts::argument<bool>(size_t idx) const;
template int prog_opts::argument<int>(size_t idx) const;
template size_t prog_opts::argument<size_t>(size_t idx) const;
template long prog_opts::argument<long>(size_t idx) const;
template double prog_opts::argument<double>(size_t idx) const;
template std::string prog_opts::argument<std::string>(size_t idx) const;
