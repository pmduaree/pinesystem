#ifndef __stringparparse__
#define __stringparparse__

#include <map>
#include <vector>
#include <string>
#include <tuple>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/format.hpp>


template<typename T>
struct Empty
{
    constexpr const static T value = 0;
};




class StringParamParser
{
    std::map<std::string,std::string> data;
  public:
    StringParamParser();
    StringParamParser(const std::string& z);
    StringParamParser(const StringParamParser& z);
    void init(const std::string& z);
    template<typename T> T get(const std::string& name, const T& defval = Empty<T>::value) const;
    template<typename T> void put(const std::string& name, const T& val);
    void from_string(const std::string& x);
    std::string to_string() const;

  private:
    std::map<std::string,std::string> parse_stringparam(const std::string& z) const;
    std::tuple<std::string,std::string> parse_one_param_string(const std::string& z) const;


};

#endif

