#ifndef __LINALG__
#define __LINALG__

#include <tuple>
#include <vector>

// \sum_i [ (y_i - (c + alpha * x_i) ]^2 = min
// boils down to
//  alpha = \sum_i (x_i y_i) / \sum_i (x_i^2)
// and 
//  \sum_i (y_i - c - alpha * x_i) ] = 0 --> 
//   c = (1/n) \sum_i (y_i - alpha * x_i)
std::tuple<double,double> 
linear_fit(const std::vector<double>& X, const std::vector<double>& Y);


std::tuple<double,double> 
alpha_fit_linear(const std::vector<double>& X, const std::vector<double>& Y);



#endif

