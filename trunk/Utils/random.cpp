#include "random.hpp"

#include <random>
#include <array>
#include <chrono>
#include <vector>

static std::default_random_engine random_generator;

std::default_random_engine& randomGenerator() {	return random_generator; }

void make_random_seed()
{
    auto now = std::chrono::system_clock::now();
    auto zz = now.time_since_epoch().count();
    random_generator.seed(zz);
}

void randomize_vector(std::vector<double>& v,double rate)
{
	if (v.empty()) return;
	std::uniform_real_distribution<double> distribution(1-rate/2,1+rate/2);
	for(double& a: v) a = a*distribution(randomGenerator());
}

void make_stochastic_vector(std::vector<double>& v)
{
    if (v.empty()) return;
	std::uniform_real_distribution<double> distribution(0,1.0);
	for(double& a: v) a = distribution(randomGenerator());
    double mysum = 0.0;
    for(const auto& z : v) mysum += z;
    for(auto& z : v) z *= 1.0 / mysum;
}

int random_sign()
{
	std::discrete_distribution<int> distribution {1,1};
	double v;
	if(distribution(randomGenerator())==0) v=-1; else v=1;
	return v;
}

void make_stochastic_scalar(double& v, double lo,double hi)
{
	std::uniform_real_distribution<double> distribution(lo,hi);
	v = distribution(randomGenerator());
}

 
