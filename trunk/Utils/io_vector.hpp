#ifndef __IO_VECTOR__
#define __IO_VECTOR__
//////////////////////////////////////////////////////////////////////////////
/// \file io_vector.hpp
/// \brief std::vector I/O support
///
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  October 2013
///
//////////////////////////////////////////////////////////////////////////////
#include <vector>
#include <cassert>
#include <stdexcept>

#include "io.hpp"
namespace morana {

template<class T>
struct io_module<std::vector<T>,
    typename std::enable_if<std::is_scalar<T>::value >::type >
{
    void read(std::vector<T>& v, std::ifstream& fp)
    {
        uint32_t beg_flag, end_flag, obj_id, version, size_of_sizet, size_of_T;
        morana::read(beg_flag, fp);
        if (beg_flag != static_cast<uint32_t>(io_constants::magic_begin))
            throw std::runtime_error("Read_error");
        morana::read(obj_id, fp);
        if (obj_id != static_cast<uint32_t>(io_constants::id_vector))
            throw std::runtime_error("Read_error");
        morana::read(version, fp);
        if (version != 1) {
            //TODO
            throw std::runtime_error("Read_error");
        }
        morana::read(size_of_sizet, fp);
        morana::read(size_of_T, fp);
        
        if (size_of_sizet != sizeof(size_t) || size_of_T != sizeof(T) )
            throw std::runtime_error("cross architecture not supported yet.");
        
        size_t n;
        morana::read(n, fp);
        v.resize(n);
        fp.read(reinterpret_cast<char*>(v.data()), n * size_of_T);
        morana::read(end_flag, fp);
        if (end_flag != static_cast<uint32_t>(io_constants::magic_end))
            throw std::runtime_error("Read_error");
    }
    
    void write(const std::vector<T>& v, std::ofstream& fp)
    {
        uint32_t beg_flag = static_cast<uint32_t>(io_constants::magic_begin);
        uint32_t end_flag = static_cast<uint32_t>(io_constants::magic_end);
        uint32_t object_identifier = static_cast<uint32_t>(io_constants::id_vector);
        uint32_t version = 1;
        uint32_t size_of_sizet = sizeof(size_t);
        uint32_t size_of_T = sizeof(T);
        
        size_t n = v.size();
        
        morana::write(beg_flag, fp);
        morana::write(object_identifier, fp);
        morana::write(version, fp);
        morana::write(size_of_sizet, fp);
        morana::write(size_of_T, fp);
        morana::write(n, fp);
        fp.write(reinterpret_cast<const char*>(v.data()), size_of_T * n);
        morana::write(end_flag, fp);
    }
};
}

// << VECTOR 
template<class T>
std::ostream& operator<<(std::ostream& xx, const std::vector<T>& arr)
{
	std::stringstream str;
    // do it like the matlab does it.
    str << "[";
    
	for(size_t i=0;i<arr.size();++i) {
        //if (i%4) str << " ";
        str << arr[i];
		if(i<arr.size()-1) {
            str <<",";
        }
    }
	
    str << "]" ;//<< std::endl;
    xx << str.str();
    return xx;
}




#endif
