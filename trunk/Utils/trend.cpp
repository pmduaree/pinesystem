#include "trend.hpp"
#include <cmath>

Trend::Trend(const tix_series& tix_)
    : tix(tix_) {}
    
bool Trend::operator()(const tiempo::datetime& date_1, 
                const tiempo::datetime& date_2, double& trend) const
{
    tiempo::duration dur = date_2 - date_1;
    return operator()(date_2, dur, trend);
}

bool Trend::operator()(const tiempo::datetime& date_2, 
                const tiempo::duration& dur, double& trend) const
{
    bool retval = false;
    bool has_in_db = false;
    _mux.lock();
    auto it_1 = _cache.find(date_2);
    if (it_1 != _cache.end() ) {
        const auto& z = it_1->second;
        auto it_2 = z.find(dur);
        if (it_2 != z.end() ) {
            trend = std::get<1>(it_2->second);
            retval = std::get<0>(it_2->second);
            has_in_db = true;
        }
    }
    _mux.unlock();
    if (!has_in_db) {
        retval = calculate_trend(date_2 - dur, date_2, trend);
        _mux.lock();
        _cache[date_2][dur] = std::make_tuple(retval, trend);
        _mux.unlock();
    }
    return retval;
}

bool Trend::calculate_trend(const tiempo::datetime& date_1, 
                     const tiempo::datetime& date_2, double& trend) const
{
    //-------------------------------------------------------------------
    // find iterator range
    //-------------------------------------------------------------------
    auto tix_end = tix.end();
    auto it_1 = tix.begin();
    while( it_1 != tix_end && it_1->first < date_1) ++it_1;
    if (it_1 == tix.end()) return false;
    auto it_2 = it_1;
    while( it_2 != tix_end && it_2->first <= date_2) ++it_2;
    if (it_2 == it_1) return false;
    
    auto t0 = it_1->first;
    auto v0 = it_1->second.p;
    
    size_t n = 0;
    double m_x = 0, m_y = 0;
    for(auto it = it_1 ; it != it_2; ++it) {
        double t = tiempo::duration(it->first - t0).seconds() / 3600.;
        double v = (it->second.p - v0) / fabs(v0);
        m_x += t;
        m_y += v;
        ++n;
    }
    if (n < 2) return false;
    m_x *= 1.0 / n;
    m_y *= 1.0 / n;
    double cov = 0.0, var = 0.0;
    for(auto it = it_1 ; it != it_2; ++it) {
        double t = tiempo::duration(it->first - t0).seconds() / 3600.;
        double v = (it->second.p - v0) / fabs(v0);
        cov += (t - m_x) * (v - m_y);
        var += (t - m_x) * (t - m_x);
    }
    trend = cov / var;
    return true;
}


