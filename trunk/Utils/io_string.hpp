#ifndef __IO_STRING__
#define __IO_STRING__
//////////////////////////////////////////////////////////////////////////////
/// \file io_string.hpp
/// \brief std::string I/O support
///
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  October 2013
///
//////////////////////////////////////////////////////////////////////////////
#include <string>
#include <cassert>
#include <stdexcept>

#include "io.hpp"
namespace morana {

template<>
struct io_module<std::string>
{
    void read(std::string& s, std::ifstream& fp)
    {
        uint32_t beg_flag, end_flag, obj_id;
        morana::read(beg_flag, fp);
        if (beg_flag != static_cast<uint32_t>(io_constants::magic_begin))
            throw std::runtime_error("Read_error");
        morana::read(obj_id, fp);
        if (obj_id != static_cast<uint32_t>(io_constants::id_string))
            throw std::runtime_error("Read_error");
        
        uint32_t n;
        morana::read(n, fp);
        assert( n>0);
        char *mybuf = new char[n+1];
        mybuf[n] = 0;
        fp.read(mybuf, n);
        s = mybuf;
        delete [] mybuf;
        morana::read(end_flag, fp);
        if (end_flag != static_cast<uint32_t>(io_constants::magic_end))
            throw std::runtime_error("Read_error");
    }
    
    void write(const std::string& s, std::ofstream& fp)
    {
        uint32_t beg_flag = static_cast<uint32_t>(io_constants::magic_begin);
        uint32_t end_flag = static_cast<uint32_t>(io_constants::magic_end);
        uint32_t obj_id = static_cast<uint32_t>(io_constants::id_string);
        
        uint32_t n = s.size();
        
        morana::write(beg_flag, fp);
        morana::write(obj_id, fp);
        morana::write(n, fp);
        fp.write(s.data(), n);
        morana::write(end_flag, fp);
    }
};


}



#endif

