//
//  KalmanFiller.cpp
//  
//
//  Created by David de la Rosa on 7/27/14.
//
//

#include "KalmanFiller.hpp"
#include <armadillo>

using namespace arma;
arma::mat kalmanfilter::summat(const int &I, const int &E, const arma::cube &V, const arma::mat &X1, int lag)
{
    int N = X1.n_cols;
    arma::mat out(N, N);
    out.zeros();
       for (int i = I; i < E; i++){
        out += V.slice(i) + X1.row(i).t() * X1.row(i - lag);
    }
    return out;
}
// filtering initialization
void kalmanfilter::init_QR(const arma::mat &data, arma::mat &Q, arma::mat &R, arma::mat &colmean){
    int M = data.n_rows;
    int N = data.n_cols;
    arma::umat Counter   = (data != 0);                          // Count none 0 in data
    colmean    = sum(data, 0)/sum(Counter , 0);        // Column mean exclude 0
    arma::mat colmeanmat = arma::ones<mat>(M, 1) * colmean;            // MxN column mean matrix
    arma::mat difdata    = data - colmeanmat % Counter;          // Difference with repect to column mean
    Q = difdata.t()*difdata / (Counter.t()*Counter - 1);       // Covariance matrix ignoring 0
    R = dt*arma::eye(N, N);
}
// kalman filter
void kalmanfilter::kalman(const arma::mat &x_k, const arma::mat &P_k, const arma::mat &y, const arma::mat &Q, const arma::mat &R,
            arma::mat &xPred, arma::mat &PPred, arma::mat &xfilt, arma::mat &Pfilt)
{
    int N = y.n_cols;
    arma::mat A = arma::eye(N, N), C = arma::eye(N, N);
    
    // Missing data handle
    C = C % ((y.t() != 0) * arma::ones<mat>(1, N));
    //si hay datos pone un uno en el elemento diagonal correspondiente sino cero,
    //esto se conoce como la matriz de medición H, la cuál cambia en el tiempo,
    //aunque uno podría dejarla fija.
    
    // Prediction
    xPred = A * x_k.t();
    PPred = A * P_k * A.t() + Q;
    
    // En las partes donde aparece C, matriz de medición sólo influyen los datos
    // parciales para las actualizaciones
    // Inovation
    arma::mat yr = y.t() - C * xPred;
    arma::mat RC = C * PPred * C.t() + R;
    // Update with Kalman gain
    arma::mat Kgain = PPred * C.t() * inv(RC);
    xfilt = xPred + Kgain * yr;
    Pfilt = (arma::eye(N, N) - Kgain * C) * PPred;
}

// Kalman smoother processing time i+1 to time i

void kalmanfilter::kalmansmoother(const arma::mat &xfilt, const arma::cube &Pfilt, const arma::cube &PPred, arma::mat &xsmo,
                    arma::cube &Psmo, arma::cube &L, arma::cube &Pauto, const arma::mat &A, const arma::mat &C)
{
    for (int  i = xfilt.n_rows - 2; i >= 0; i--){
        L.slice(i) = Pfilt.slice(i) * A.t() * inv(PPred.slice(i + 1));
        xsmo.row(i) = xfilt.row(i) + (xsmo.row(i + 1) - xfilt.row(i)) * (L.slice(i)).t();
        Psmo.slice(i) = Pfilt.slice(i) + L.slice(i) * (Psmo.slice(i + 1) - PPred.slice(i + 1)) * (L.slice(i)).t();
        int tmp = xfilt.n_rows -2 ; 
        if ( i < tmp)
            Pauto.slice(i + 1) = Pfilt.slice(i + 1) * L.slice(i).t() +
                                    L.slice(i) * (Pauto.slice(i + 2) -
                                    Pfilt.slice(i + 1))* L.slice(i).t();
    }
}
void kalmanfilter::EMextimator(const arma::mat &data, const arma::mat &xsmo, const arma::cube &Psmo, const arma::cube &L,
                 arma::mat &Qaj, arma::mat &Raj, const arma::mat &A, const arma::mat &C){
    int M = xsmo.n_rows;
    arma::mat xsmodif, ydif;
    
    // Estimate Q
    for (int i = 0; i < M - 2; i++){
        xsmodif = xsmo.row(i + 1) - xsmo.row(i) * A.t();
        Qaj += xsmodif.t()*xsmodif + A * Psmo.slice(i) * A.t() + Psmo.slice(i +1) -
                Psmo.slice(i+1) * L.slice(i).t() * A.t() - A * L.slice(i) * Psmo.slice(i + 1);
    }
    Qaj /= (M - 1);
    
    // Estimate R
    for (int j = 0; j < M - 1; j++){
        ydif = data.row(j) - xsmo.row(j) * C.t();
        Raj += ydif.t() * ydif + C * Psmo.slice(j) * C.t();
    }
    Raj /= M;
}

void kalmanfilter:: EMextimator_m(const arma::mat &data, const arma::mat &xsmo, const arma::cube &Psmo, const arma::cube &L,
                   arma::mat &Qaj, arma::mat &Raj, const arma::mat &A, const arma::mat &C, const arma::cube &Pauto)
{
    int M = data.n_rows;
    int N = data.n_cols;
    Qaj = summat(1, M, Psmo, xsmo) -
            summat(1, M, Psmo, xsmo, 1) * inv(summat(0, M - 1, Psmo, xsmo)) * summat(1, M, Pauto, xsmo, 1).t();
    Qaj /= (M - 1);
    
    arma::mat R(N,N ); R.zeros();
    
    for (int i = 0; i < M; i++){
        arma::mat yi = data.row(i);
        arma::umat ob = find(yi.row(0)), mi = find(yi.row(0) == 0);
        arma::mat D(N, N);D.zeros();
        
        if (mi.is_empty() | ob.is_empty()) D = arma::eye(N,N);
        else
        {
            arma::umat O = join_cols(ob, mi); //estaba rows
            for (int j = 0; j < N; j++) D(O(j), j) = 1;
        }
        
        arma::mat Po = Psmo.slice(i);
        Po = Po.rows(ob);
        Po = Po.cols(ob);
        arma::mat Rm = Raj;
        Rm = Rm.rows(mi);
        Rm = Rm.cols(mi);
        
        int PM = Po.n_rows, PN = Po.n_cols, RM = Rm.n_rows, RN = Rm.n_cols;
        arma::mat middle = join_rows(join_cols(Po, zeros<mat>(RM, PN)), join_cols(zeros<mat>(PM, RN), Rm));
        
        R += D * middle * D.t();
    }
    Raj = R / M;
}

kalmanfilter::Results kalmanfilter::run(arma::mat values){
    int M = values.n_rows;
    int N = values.n_cols;
    
    // Initialize Q, R, x_0, P_0
    arma::mat Q, R,colmean;
    init_QR(values, Q, R, colmean);
    arma::mat xfilt(M, N), xPred(M, N), xsmo(M, N);
    arma::cube P(N, N, M), PPred(N,N,M);
    
    
    xfilt.row(0) = values.row(0);
    xPred.row(0) = values.row(0);
    P.slice(0) = diagmat(R);
    
    for (int k = 0; k < 10; k++){
        // Kalman forward filtering
        for (int i = 1; i < M; i++){
            arma::mat xPredtemp, Pfilttemp, xfilttemp, PPredtemp;
            kalman(xfilt.row(i - 1), P.slice(i - 1), values.row(i), Q, R, xPredtemp, PPredtemp, xfilttemp, Pfilttemp);
            xPred.row(i) = xPredtemp.t();
            xfilt.row(i) = xfilttemp.t();
            PPred.slice(i) = PPredtemp;
            P.slice(i) = Pfilttemp;
        } // i
        
        // Kalman backward smoothing
        arma::cube Psmo(N, N, M), L(N, N, M), Pauto(N,N,M);
        
        xsmo.row(M - 1) = values.row(M - 1);
        xsmo.row(M - 1) += xfilt.row(M - 1) % (values.row(M - 1) == 0);
        Psmo.slice(M - 1) = diagmat(R);
        kalmansmoother(xfilt, P, PPred, xsmo, Psmo, L, Pauto, arma::eye(N, N), arma::eye(N, N));
        
        // EM algorithm estimating Q R
        if (sum(sum(values == 0)) == 0)	EMextimator(values, xsmo, Psmo, L, Q, R, arma::eye(N, N), arma::eye(N, N));
        else EMextimator_m(values, xsmo, Psmo, L, Q, R, arma::eye(N, N), arma::eye(N, N), Pauto);
        //EMextimator(values, xsmo, Psmo, L, Q, R, arma::eye(N, N), arma::eye(N, N));
        R = diagmat(R);
    }   //k
    Results temp;
    temp.xsmo=xsmo;
    temp.xPred=xPred;
    temp.xfilt=xfilt;
    return temp;
}

