#ifndef __PROGOPT__
#define __PROGOPT__
////////////////////////////////////////////////////////////////////////////
/// \file prog_opts.hpp
/// 
/// Implementation of the argument parsing functionality
///
/// Author: Iztok Pizorn, 2013.
////////////////////////////////////////////////////////////////////////////

#include <type_traits>
#include <vector>
#include <string>
#include <cstring>
#include <stdexcept>
#include <iostream> 

enum class OptionType
{
    Error=0,
    Float=1,
    Integer=2,
    String=3,
    Boolean=4,
};


struct ValueType
{
    char sv[256];
    template<class T> void set(const T& x);
    template<class T=std::string> T get() const;
};


struct OptionItem
{
    std::string name;
    std::string long_tag;
    std::string short_tag;
    OptionType type;
    ValueType value;
};


class prog_opts
{
  public:
    prog_opts();

    template<class T>
    void add_option(const std::string& short_tag, const std::string& long_tag,
                    OptionType type, const std::string& name, const T& defval);
    template<class T>
    void add_option(const std::string& tag, OptionType type, 
                    const std::string& name, const T& defval);
    bool parse_args(int argc, char** argv);

    template<class T=std::string> T option(const std::string& name) const;
    
    template<class T=std::string> T argument(size_t idx) const;
    size_t num_arguments() const { return arguments.size(); }

    std::string help() const;


  private:
    void associate_value(std::vector<OptionItem>::iterator it, 
                         const std::string& v);


    std::vector<OptionItem> options;
    std::vector<std::string> arguments;
    std::string program_name;
};




#endif

