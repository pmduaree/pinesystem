#ifndef __IO__HPP__
#define __IO__HPP__
//////////////////////////////////////////////////////////////////////////////
/// \file io.hpp
/// \brief I/O support for writing and reading binary files.
///
///  Any object can be written into a buffer by
///     write(object, fp);
///  where fp is a std::ofstream& and similarly for 
///     read(object, fp);
///  Custom classes must provide instructions how to write their members to
///  buffers by specializing the structure io_module, see e.g.
///     io_tensor.hpp for an example how that's done for the tensor class.
///  Custom classes may also add an identifier to io_constants...
/// 
///  The io_module is already defined for scalar types (floats, integers).
///
///  An object can also be read from a file by 
///     from_file(object, filename);
///  and written as 
///     to_file(object, filename);
///  
/// Currently we don't check if the file exists or anything.
/// 
/// Future directions:
///     Probably there should be some reorganizing here, be making a new folder
///     io which would contain all io_tensor.hpp, io_vector.hpp, ... 
///     Then one just includes the thing that's wanted, in a similar fashion
///     as done in Boost with the serialization module.
/// 
/// TODO:
///     the io_module should be defined here not just for all scalar but for
///     all classes that are trivially copiable. Unfortunately my G++ doesn't
///     support that C++11 feature yet... 
/// 
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  October 2013
///
//////////////////////////////////////////////////////////////////////////////
#include <type_traits>
#include <iostream>
#include <fstream>
#include <string>

namespace morana {

enum class io_constants
{
    magic_begin = 666,
    magic_end = 777,
    id_vector = 401,
    id_tensor = 402,
    id_array = 403,
    id_string = 404, 
};

// This is the main object which has to be specialized for any custom made
// class we may want to write to a file.
template<class A, class Enabled=void>
struct io_module
{
    void read(A& a, std::ifstream& fp);
    void write(const A& a, std::ofstream& fp);
};

template<class A>
struct io_module<A, typename std::enable_if<std::is_scalar<A>::value>::type>
{
    void read(A& a, std::ifstream& fp)
    {
        fp.read(reinterpret_cast<char*>(&a), sizeof(a) );
    }
    
    void write(const A& a, std::ofstream& fp)
    {
        fp.write(reinterpret_cast<const char*>(&a), sizeof(a) );
    }
};


template<class A>
void read(A& a, std::ifstream& fp)
{
    io_module<A> iomod;
    iomod.read(a, fp);
}


template<class A>
void write(const A& a, std::ofstream& fp)
{
    io_module<A> iomod;
    iomod.write(a, fp);
}

template<class A>
void from_file(A& a, const std::string& fname)
{
    std::ifstream fp(fname, std::ios::binary);
    read(a, fp);
    fp.close();
}

template<class A>
void to_file(const A& a, const std::string& fname)
{
    std::ofstream fp(fname, std::ios::binary);
    write(a, fp);
    fp.close();
}



}   // end  of namespace morana


#endif
