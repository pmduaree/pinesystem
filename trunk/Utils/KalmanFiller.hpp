//
//  KalmanFiller.h
//  
//
//  Created by David de la Rosa on 7/27/14.
//
//

#ifndef ____KalmanFiller__
#define ____KalmanFiller__

#include <iostream>
#include <armadillo>

const double dt = 1.0;
class kalmanfilter
{
protected:
    arma::mat summat(const int &I, const int &E, const arma::cube &V,
                        const arma::mat &X1, int lag = 0);
    
    // filtering initialization
    
    void init_QR(const arma::mat &data, arma::mat &Q, arma::mat &R, arma::mat &colmean);
    // kalman filter
    void kalman(const arma::mat &x_k, const arma::mat &P_k, const arma::mat &y,
                const arma::mat &Q, const arma::mat &R, arma::mat &xPred,
                arma::mat &PPred, arma::mat &xfilt, arma::mat &Pfilt);
    // Kalman smoother processing time i+1 to time i
    
    void kalmansmoother(const arma::mat &xfilt, const arma::cube &Pfilt, const arma::cube &PPred,
                        arma::mat &xsmo, arma::cube &Psmo, arma::cube &L, arma::cube &Pauto,
                        const arma::mat &A, const arma::mat &C);
    void EMextimator(const arma::mat &data, const arma::mat &xsmo, const arma::cube &Psmo,
                     const arma::cube &L, arma::mat &Qaj, arma::mat &Raj, const arma::mat &A,
                     const arma::mat &C);
    
    void EMextimator_m(const arma::mat &data, const arma::mat &xsmo, const arma::cube &Psmo,
                       const arma::cube &L, arma::mat &Qaj, arma::mat &Raj, const arma::mat &A,
                       const arma::mat &C, const arma::cube &Pauto);
public:
    struct Results{
        arma::mat xsmo;
        arma::mat xPred;
        arma::mat xfilt;
    };
    Results run(arma::mat values);
    inline Results operator()(arma::mat values){return run(values);}
    
};
#endif /* defined(____KalmanFiller__) */
