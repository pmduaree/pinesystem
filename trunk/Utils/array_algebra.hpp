#ifndef __ARRAY_ALGEBRA__
#define __ARRAY_ALGEBRA__



#include <array>

namespace morana {


template<typename T,size_t N>
struct make_array: public std::array<T,N>
{
    static_assert( N <= 4, "do it later...");

    make_array(std::initializer_list<T> x)
    {
        std::copy(x.begin(), x.begin() + N, std::array<T,N>::data());
    }

    make_array(T i0,T i1=0,T i2=0,T i3=0)
    {
        std::initializer_list<T> x = {i0,i1,i2,i3};
        std::copy(x.begin(), x.begin() + N, std::array<T,N>::data());
    }
};


template<typename T, size_t N1,size_t N2>
void concatenate(const std::array<T,N1>& a, 
                 const std::array<T,N2>& b,
                 std::array<T,N1+N2>& c)
{
    std::copy(a.begin(), a.end(), c.begin());
    std::copy(b.begin(), b.end(), c.begin() + N1);
}

template<typename T, size_t N1,size_t N2>
std::array<T,N1+N2> concatenate(const std::array<T,N1>& a, 
                                const std::array<T,N2>& b)
{
    std::array<T,N1+N2> c;
    concatenate(a,b,c);
    return c;
}

template<typename T,size_t N, class Itype>
void transpose(const std::array<T,N>& a, const std::array<Itype,N>& k, 
               std::array<T,N>& b)
{
    static_assert( std::is_integral<Itype>::value, "must be integral");
    for(size_t i=0;i<N;++i) b[i] = a[k[i]];
}


template<typename T,size_t N, class Itype>
std::array<T,N> transpose(const std::array<T,N>& a, const std::array<Itype,N>& k)
{
    std::array<T,N> b;
    transpose(a,k,b);
    return b;
}


template<typename T, size_t N1,size_t N2>
void complement(const std::array<T,N1>& a, const std::array<T,N2>& b,
                std::array<T,N1-N2>& c)
{
    const size_t N3 = N1 - N2;
    size_t i=0;
    size_t ia = 0;
    
    auto is_in_b = [&b](const T& x)
    {
        bool rval = false;
        for(size_t j=0;j<N2;++j) if (b[j]==x) {rval = true; break;}
        return rval;
    };
    
    while( i < N3) {
        while (is_in_b(a[ia])) ++ia;
        c[i++] = a[ia++];
    }
}

template<typename T, size_t N1,size_t N2>
std::array<T,N1-N2> complement(const std::array<T,N1>& a, 
                               const std::array<T,N2>& b)
{
    std::array<T,N1-N2> c;
    complement(a,b,c);
    return c;
}


template<size_t N, typename T=size_t>
class range: public std::array<T,N>
{
  public:
    range(T i0=0)
    {
        for(size_t i=0;i<N;++i)
            std::array<T,N>::operator[](i) = i0 + i;
    }
};




}   // end of namespace morana


#endif
