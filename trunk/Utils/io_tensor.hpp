#ifndef __TENSOR_IO_HPP__
#define __TENSOR_IO_HPP__
//////////////////////////////////////////////////////////////////////////////
/// \file io_tensor.hpp
/// \brief Tensor I/O support
///
/// Author:
///  Iztok Pizorn <pizorn@mail.ru>
///  October 2013
///
//////////////////////////////////////////////////////////////////////////////


#include <vector>
#include <cassert>
#include <stdexcept>

#include "io.hpp"
#include "io_vector.hpp"
#include "io_array.hpp"
#include "tensor.hpp"

namespace morana {

template<size_t N, typename T>
struct io_module<tensor<T,N> >
{
    void read(tensor<T,N>& v, std::ifstream& fp)
    {
        uint32_t beg_flag, end_flag, obj_id, version;
        morana::read(beg_flag, fp);
        if (beg_flag != static_cast<uint32_t>(io_constants::magic_begin))
            throw std::runtime_error("Read_error");
        morana::read(obj_id, fp);
        if (obj_id != static_cast<uint32_t>(io_constants::id_tensor))
            throw std::runtime_error("Read_error");
        morana::read(version, fp);
        if (version != 1) {
            //TODO
            throw std::runtime_error("Read_error");
        }
        std::array<size_t,N> shape;
        morana::read(shape, fp);
        v.resize(shape);
        morana::read(v.vector(), fp);
        morana::read(end_flag, fp);
        if (end_flag != static_cast<uint32_t>(io_constants::magic_end))
            throw std::runtime_error("Read_error");
    }
    
    void write(const tensor<T,N>& v, std::ofstream& fp)
    {
        uint32_t beg_flag = static_cast<uint32_t>(io_constants::magic_begin);
        uint32_t end_flag = static_cast<uint32_t>(io_constants::magic_end);
        uint32_t obj_id = static_cast<uint32_t>(io_constants::id_tensor);
        uint32_t version = 1;
        
        morana::write(beg_flag, fp);
        morana::write(obj_id, fp);
        morana::write(version, fp);
        morana::write(v.shape(), fp);
        morana::write(v.vector(), fp);
        morana::write(end_flag, fp);
    }
};


}


// << TENSOR 
template<class T,size_t N>
std::ostream& operator<<(std::ostream& xx, const morana::tensor<T,N>& A)
{
	//malab firendly
	std::stringstream str;
    str<<"[";
	
	//matrix NON TRANSPOSED output
	if(N==2){
		for(auto j=0;j<A.M(0);j++){  //loop over rows
			for(auto i=0;i<A.M(1);i++){	 //loop over cols
				str<<A(j,i);
				if(i<A.M(1)-1) str<<",";
			}
			if(j<A.M(0)-1) str<<";";
		}
	}
	//general output for high order tensors - warning TRANSPOSED OUT!
	if(N>2){
	for(size_t i=0;i<A.size();++i) {		
        std::array<size_t,N> arr;
        A.idx2arr(i, arr);
		if(i!=0) str << (arr[0] ? ',' : ';');
        str <<  A[i];
		}
	}
	str<<"]";
    xx << str.str();
    return xx;
}





#endif
