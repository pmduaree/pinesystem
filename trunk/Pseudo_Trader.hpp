 //
//  Pseudo_Trader.h
//  
//
//  Created by David de la Rosa on 6/26/14.
//
//
#ifndef ____Pseudo_Trader__
#define ____Pseudo_Trader__

#include <iostream>
#include <fstream>
#include <memory>
#include <atomic>
#include <string>
#include <boost/format.hpp>

#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


#include "Tiempo/tiempo.hpp"
#include "SQL/sql_connection.hpp"
#include "Encoder/Encoder.hpp"
#include "SingleAssetManager.hpp"
#include "SignalSeries.hpp"
#include "Assets/PortfolioRisk.hpp"


#define S_Init  0

#define Stock_Colum 1
#define Asset_Colum 0
#define Top_elements    5

#define DueEnter    0
#define DueExit     1

#define LONG    1
#define SHORT   2

#define ENTER_TRADE 1
#define EXIT_TRADE  0

class TraderHistory
{
public:
   
    TraderHistory(){top_losses = std::vector<double>(Top_elements);top_wins = std::vector<double>(Top_elements);}
    ~TraderHistory(){}
    void IsItBetterWin(double money)
    {
      if (  money > top_wins[Top_elements-1]  )
        {   top_wins[Top_elements-1] = money;
              std::sort(top_wins.rbegin(),top_wins.rend());
        }
    }
    void IsItBetterLoss(double money)
    {
      if(  money  < top_losses[Top_elements-1])
        {  top_losses[Top_elements-1] =  money;
             std::sort(top_losses.begin(),top_losses.end());
        }
    }
    void Clear()
    {
        currentProfit = losses = comissions = 0;
        number_of_trades = succes_trades  = failed_trades =0 ;
    }
    void PrintValues()
    {
        std::cout << " Wins=" << std::endl;
        for (auto i :top_wins)
           std::cout << i <<std::endl;
        
        std::cout << " Losses=" << std::endl;
        for (auto i :top_losses)
            std::cout << i <<std::endl;
    }
    //double Get_profit(){return currentProfit;}
    double currentProfit;
    double losses;
    double wins;
    double comissions;
    int number_of_trades;
    int succes_trades;
    int failed_trades;
    std::vector<double> top_losses;
    std::vector<double> top_wins;
    
};
class TraderStatus
{
public:
    TraderStatus(){}
    ~TraderStatus(){}
    //int get_status(){return status;}
    bool is_it_on_trade(){return OnTrade;}
   // void set_status(int S){status = S;}
    void set_on_trade_on(){OnTrade=true;}
    void set_on_trade_off(){OnTrade=false;}
    
private:
    //int status;
    bool OnTrade;
};


class Pseudo_Trader
{
public:
    Pseudo_Trader(std::shared_ptr<sql_connection> __conn,int __StrategyID);
    ~Pseudo_Trader(){}
    void run(tiempo::datetime from_ , tiempo::datetime to_,double, bool);
    void PrintToFile(std::string FileName);
    std::string Print_Portfolio_Value();
    std::string To_String();
    
    double Get_profit();
    int    Get_Amount_Of_Trades(){return Stats.number_of_trades;}
    double Get_Success_Rate(){if (Stats.number_of_trades>0 ) return ((double)Stats.succes_trades/((double)Stats.number_of_trades)); else return 0.0;}
    
    int get_SignalerID(){return SignalerEngine_ID;}
    int get_EncoderID(){return EncoderEngine_ID;}
    
    void Change_Strat_Params(std::string , int);
    void Set_Push_IntoDB(bool Set){Push_SignalsInto_DB=Set;}
    void Set_LiveTradeStatus(bool Status) {WorkingAsLiveTrader =  Status;}
    void RunLive(tiempo::datetime  , double);
    
    void SetTypeInput(std::string type) {this->type = type;}
    
    
private:
    
    std::shared_ptr<sql_connection> conn;
    void Load_Elements();
    void Load_Portfolio();
    void Init_Values();
    
    int StrategyID;
    int PortfolioID;
    int freq;
    tiempo::datetime from_date;
    tiempo::datetime to_date;
    tiempo::datetime Last_transaction_date;
    std::vector<int> AssetsID;

    void Exit_Trade(tiempo::datetime date, bool skip_DB_interact);
    void Enter_Trade(tiempo::datetime date, bool skip_DB_interact);
    
    bool Get_Enter_Signal(tiempo::datetime);
    bool Get_Exit_Signal(tiempo::datetime);
    
    bool Get_Enter_Signal_LIVE(tiempo::datetime );
    
    void Push_Trade_To_DB(std::tuple<int,int>,tiempo::datetime,int);
    void Process_Exit_Trade();
    void WithDrawLastEnterTrade();
    
    void Push_To_PortfolioRisk(int,double,int, int);
    void Push_State_toDB(int );
    void Push_Status_toDB(int, tiempo::datetime);
    tiempo::datetime get_last_transaction_date();
    void get_last_transaction_status();
    bool get_last_status();
    
    double InvestingMoney;
    
    bool CalculateOnTheFly;
    bool Push_SignalsInto_DB;
    
    int SignalerEngine_ID;
    int EncoderEngine_ID;
    
    bool WorkingAsLiveTrader;

    std::string type;
    

    tiempo::datetime CurrentDate;
    std::vector<Transaction> t;
    TraderHistory Stats;
    SignalTix rawSignals;
    SignalTix ExecutedSignals;
    TraderStatus Status;
    std::shared_ptr<Encoder> __Encoder;
    std::shared_ptr<SignalSeries> __Signaller;
    std::vector<SingleAssetManager> __AssetManager;
};


#endif /* defined(____Pseudo_Trader__) */
