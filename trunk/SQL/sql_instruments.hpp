#ifndef __sql_instruments__
#define __sql_instruments__

#include <vector>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>

#include <cppconn/driver.h>
#include <mysql_driver.h>

#include <memory>

#include "sql_connection.hpp"

struct sql_instrument
{
    int id;
    std::string name;
    std::string exchange;
    std::string CQGticker;
    std::string GVticker;
    std::string description;
    std::string source;
    int type;
    int disabled;
    std::string open_market;
    std::string commission;
    int lotsize;
    double margin;
    std::string trading_day;
    
    sql_instrument()
    {
        id = 0;
        type = 0;
        disabled = 0;
        open_market = "";
        name = "";
        exchange = "";
        description = "";
        source = "";
        commission = "";
        lotsize = 0;
        margin = 0.0;
        trading_day = "";
    }
};

class sql_instruments
{
  public:
    sql_instruments(std::shared_ptr<sql::Connection> con_);
    ~sql_instruments();

    void get_IDs(std::vector<int>& IDs);
    bool pull_data(int instrument_id, sql_instrument& x);
    bool pull_data(const std::string& name, sql_instrument& x);
    int get_id(const std::string& instrument_name);
    
  private:
    std::shared_ptr<sql::Connection> con;
};
    
    
#endif
