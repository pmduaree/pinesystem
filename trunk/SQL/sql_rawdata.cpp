#include "sql_rawdata.hpp"


#include <boost/format.hpp>
#include <cppconn/resultset.h>


sql_rawdata::sql_rawdata( std::shared_ptr<sql::Connection> con_)
    : con(con_ )
{
}
sql_rawdata::~sql_rawdata()
{
    if (S_pull_dd) S_pull_dd->close();
    if (S_last) S_last->close();
    if (S_first) S_first->close();
    if (S_pull1) S_pull1->close();
    if (S_pullwdate) S_pullwdate->close();
    if (S_push) S_push->close();
    if (S_update) S_update->close();
    if (S_delete) S_delete->close();
    if (S_pull) S_pull->close();
}
bool sql_rawdata::get_last(int instrument_id, int freq, raw_data_item& last)
{
    last.clear();
    if (!S_last) {
        S_last.reset(
            con->prepareStatement(
            "SELECT * FROM `Main`.`Candles` WHERE (instrument_id = ? AND freq = ?) \
                ORDER BY `date` DESC LIMIT 1"
            )
        );
    }
    auto pstmt = S_last;
    pstmt->setInt(1, instrument_id);
    pstmt->setInt(2, freq);
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    if (res->rowsCount() < 1) return false;
    res->first();
    last.id = res->getInt("id");
    last.instrument_id = res->getInt("instrument_id");
    last.freq = res->getInt("freq");
    last.high = res->getDouble("high");
    last.open = res->getDouble("open");
    last.close = res->getDouble("close");
    last.low = res->getDouble("low");
    last.volume = res->getInt("volume");
    last.date = tiempo::datetime(res->getString("date"));
    last.note = res->getString("note");
    return true;
}
bool sql_rawdata::get_first(int instrument_id, int freq, raw_data_item& last)
{
    if (!S_first) {
        S_first.reset(
            con->prepareStatement(
            "SELECT * FROM `Main`.`Candles` WHERE (instrument_id = ? AND freq = ?) \
                ORDER BY `date` ASC LIMIT 1"
            )
        );
    }
    auto pstmt = S_first;
    pstmt->setInt(1, instrument_id);
    pstmt->setInt(2, freq);
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    last.clear();
    if (res->rowsCount() < 1) return false;
    res->first();
    last.id = res->getInt("id");
    last.instrument_id = res->getInt("instrument_id");
    last.freq = res->getInt("freq");
    last.high = res->getDouble("high");
    last.open = res->getDouble("open");
    last.close = res->getDouble("close");
    last.low = res->getDouble("low");
    last.volume = res->getInt("volume");
    last.date = tiempo::datetime(res->getString("date"));
    last.note = res->getString("note");
    return true;
}


tiempo::datetime sql_rawdata::get_last_date(int instrument_id, int freq)
{
    raw_data_item last;
    tiempo::datetime my_date;
    if (get_last(instrument_id, freq, last)) my_date = last.date;
    return my_date;
}

tiempo::datetime sql_rawdata::get_first_date(int instrument_id, int freq)
{
    raw_data_item item;
    tiempo::datetime my_date;
    if (get_first(instrument_id, freq, item)) my_date = item.date;
    return my_date;
}


bool sql_rawdata::pull_data(int instrument_id, int freq, const tiempo::datetime& date,
                            raw_data_item& item)
{
    if (!S_pullwdate) {
        S_pullwdate.reset(
            con->prepareStatement(
            "SELECT * FROM `Main`.`Candles` \
                WHERE (instrument_id=? AND freq=? AND date='?') ORDER BY `id` DESC")
        );
    }
    auto pstmt = S_pullwdate;
    pstmt->setInt(1, instrument_id);
    pstmt->setInt(2, freq);
    pstmt->setString(3, date.to_string("%Y-%m-%d %H:%M:%S"));
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    if (!res->rowsCount()) return false;
    res->first();
    extract_one(res, item);
    return true;
}


bool sql_rawdata::pull_data(int id, raw_data_item& x)
{
    x.clear();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::string query = (boost::format(
        "SELECT * FROM `Main`.`Candles` WHERE id=%d")%id).str();
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(query));
    if (res->rowsCount() < 1) return false;
    if (res->rowsCount() != 1) {
        std::cout<< "WARNING: MULTIPLE ENTRIES ("<<id<<")!!!" << std::endl;
    }
    res->first();
    extract_one(res, x);
    return true;
}


void sql_rawdata::update_data(const raw_data_item& x)
{
    if (!S_update) {
        S_update.reset( con->prepareStatement(
        "UPDATE `Main`.`Candles` SET `high`=?,`open`=?,`close`=?,`volume`=?,`low`=? WHERE `id`= ?"
        )
        );
    }
    auto pstmt = S_update;
    pstmt->setDouble(1, x.high);
    pstmt->setDouble(2, x.open);
    pstmt->setDouble(3, x.close);
    pstmt->setInt(4, x.volume);
    pstmt->setDouble(5, x.low);
    pstmt->setInt(6, x.id);
    pstmt->executeUpdate();
}


void sql_rawdata::push_data(const raw_data_item& x)
{
    // S_push
    if (!S_push) {
        S_push.reset(con->prepareStatement(
            "INSERT INTO `Main`.`Candles` \
                (`instrument_id`,`freq`,`date`,`open`,`high`,`low`, `close`,`volume`,`note`) \
                VALUES (?,?,?,?,?,?,?,?,?)"
            )
        );
    }
    auto pstmt = S_push;
    pstmt->setInt(1, x.instrument_id);
    pstmt->setInt(2, x.freq);
    pstmt->setString(3, x.date.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setDouble(4, x.open);
    pstmt->setDouble(5, x.high);
    pstmt->setDouble(6, x.low);
    pstmt->setDouble(7, x.close);
    pstmt->setInt(8, x.volume);
    pstmt->setString(9, x.note);
    pstmt->executeUpdate();
}

void sql_rawdata::push_data(const std::vector<raw_data_item>& X)
{
    if (!S_push) {
        S_push.reset(con->prepareStatement(
            "INSERT INTO `Main`.`Candles` \
                (`instrument_id`,`freq`,`date`,`open`,`high`,`low`, `close`,`volume`,`note`) \
                VALUES (?,?,?,?,?,?,?,?,?)"
            )
        );
    }
    auto pstmt = S_push;
    for(const auto& x : X) {
        pstmt->setInt(1, x.instrument_id);
        pstmt->setInt(2, x.freq);
        pstmt->setString(3, x.date.to_string("%Y-%m-%d %H:%M:%S"));
        pstmt->setDouble(4, x.open);
        pstmt->setDouble(5, x.high);
        pstmt->setDouble(6, x.low);
        pstmt->setDouble(7, x.close);
        pstmt->setInt(8, x.volume);
        pstmt->setString(9, x.note);
        pstmt->executeUpdate();
    }
}




bool sql_rawdata::load_data(int instrument_id, const tiempo::datetime& date_from,
               const tiempo::datetime& date_to, tix_series& X)
               //__attribute__((deprecated))
{
    tiempo::datetime date1 = date_from - std::chrono::minutes(1);
    tiempo::datetime date2 = date_to - std::chrono::minutes(1);
    X.clear();
    if (!S_pull_dd) {
        S_pull_dd.reset(
            con->prepareStatement(
                "SELECT date, close, volume FROM \
                (\
                    SELECT date, close, volume FROM Main.Candles\
                        WHERE date BETWEEN ? AND ? \
                        AND freq = ? \
                        AND instrument_id = ?\
                            UNION\
                        SELECT date, close, 1 AS volume FROM Main.CandlesKalman\
                        WHERE date BETWEEN ? AND ?\
                        AND instrument_id = ?\
                ) AS C \
                GROUP BY date"
            )
        );
    }
    auto pstmt = S_pull_dd;

    pstmt->setString(1, date1.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setString(2, date2.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setInt(3, 1);
    pstmt->setInt(4, instrument_id);

    pstmt->setString(5, date1.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setString(6, date2.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setInt(7, instrument_id);

   // std::cout << "   sql raw from " << date1.to_string("%Y-%m-%d %H:%M:%S") << " to " << date2.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    if (res->rowsCount() == 0) return false;
    while(res->next()) {
        tiempo::datetime t(res->getString("date"));
        t += std::chrono::minutes(1);
       // std::cout << "   Time raw " << t.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
        if (!X.empty() && X.rbegin()->first >= t) continue;   // duplicate
        auto& x = X[t];
        x.v = res->getInt("volume");
        x.p = res->getDouble("close");
    }
    
    // KALMAN
    //THEN ADD THE KALMAN PARTS THAT ARE NOT WHERE TOO BUT DO NOT DUPPLLICATE
    //DATA
    
    return true;
}

bool sql_rawdata::load_data_midprice(int instrument_id, const tiempo::datetime& date_from,
               const tiempo::datetime& date_to, tix_series& X)
               //__attribute__((deprecated))
{
    tiempo::datetime date1 = date_from - std::chrono::minutes(1);
    tiempo::datetime date2 = date_to - std::chrono::minutes(1);
    X.clear();
    if (!S_pull_dd) {
        S_pull_dd.reset(
            con->prepareStatement(
            "SELECT date, midprice FROM `Main`.`Tick` \
                WHERE (instrument_id=? AND date>=? AND date<=?) \
                ORDER BY date ASC"
            )
        );
    }
    auto pstmt = S_pull_dd;
    pstmt->setInt(1, instrument_id);
    pstmt->setString(2, date1.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setString(3, date2.to_string("%Y-%m-%d %H:%M:%S"));
   // std::cout << "   sql raw from " << date1.to_string("%Y-%m-%d %H:%M:%S") << " to " << date2.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    if (res->rowsCount() == 0) return false;
    while(res->next()) {
        tiempo::datetime t(res->getString("date"));
        t += std::chrono::minutes(1);
       // std::cout << "   Time raw " << t.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
        if (!X.empty() && X.rbegin()->first >= t) continue;   // duplicate
        auto& x = X[t];
        x.v = 1;
        x.p = res->getDouble("midprice");
    }
    
    return true;
}

bool sql_rawdata::load_data_spread(int instrument_id, const tiempo::datetime& date_from,
               const tiempo::datetime& date_to, tix_series& X)
               //__attribute__((deprecated))
{
    tiempo::datetime date1 = date_from - std::chrono::minutes(1);
    tiempo::datetime date2 = date_to - std::chrono::minutes(1);
    X.clear();
    if (!S_pull_dd) {
        S_pull_dd.reset(
            con->prepareStatement(
            "SELECT date, ROUND(ask-bid,2) AS spread FROM `Main`.`Tick` \
                WHERE (instrument_id=? AND date>=? AND date<=?) \
                ORDER BY date ASC"
            )
        );
    }
    auto pstmt = S_pull_dd;
    pstmt->setInt(1, instrument_id);
    pstmt->setString(2, date1.to_string("%Y-%m-%d %H:%M:%S"));
    pstmt->setString(3, date2.to_string("%Y-%m-%d %H:%M:%S"));
   // std::cout << "   sql raw from " << date1.to_string("%Y-%m-%d %H:%M:%S") << " to " << date2.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
    if (res->rowsCount() == 0) return false;
    while(res->next()) {
        tiempo::datetime t(res->getString("date"));
        t += std::chrono::minutes(1);
       // std::cout << "   Time raw " << t.to_string("%Y-%m-%d %H:%M:%S") << std::endl;
        if (!X.empty() && X.rbegin()->first >= t) continue;   // duplicate
        auto& x = X[t];
        x.v = 1;
        x.p = res->getDouble("spread");
    }
    
    return true;
}

void sql_rawdata::extract_one(const std::unique_ptr<sql::ResultSet>& res, raw_data_item& x)
{
    x.id = res->getInt("id");
    x.instrument_id = res->getInt("instrument_id");
    x.freq = res->getInt("freq");
    x.open = res->getDouble("open");
    x.close = res->getDouble("close");
    x.high = res->getDouble("high");
    x.low = res->getDouble("low");
    x.volume = res->getInt("volume");
    x.date = tiempo::datetime(res->getString("date"));
    x.note = res->getString("note");
}

