#ifndef __sql_ticks_
#define __sql_ticks_

#include <string>

#include "sql_connection.hpp"
#include "date_time.hpp"
#include "prices.hpp"



struct sql_tick
{
    uint32_t id;
    uint32_t instrument_id;
    tiempo::datetime_type date;
    double price;
    uint32_t volume;
    
    void clear()
    {
        id=instrument_id=0;
        price=0;
        volume=0;
    }
};

class sql_ticks
{
    std::shared_ptr<sql::Connection> con;
    std::shared_ptr<sql::PreparedStatement> S_pull_ticks2, S_push;
  public:
    sql_ticks(std::shared_ptr<sql::Connection> con_ = sql_connection::get_connection() )
        : con(con_)
    {
    }
    
    ~sql_ticks()
    {
        if (S_pull_ticks2) S_pull_ticks2->close();
        if (S_push) S_push->close();
    }
    
    void push_data(const sql_tick& x)
    {
        if (!S_push) {
            S_push.reset(con->prepareStatement(
                "INSERT INTO `tix`.`ticks` (`instrument_id`,`date`,`price`,`volume`) VALUES (?,?,?,?)"
                )
            );
        }
        auto pstmt = S_push;
        pstmt->setInt(1, x.instrument_id);
        pstmt->setString(2, x.date.to_string("%Y-%m-%d %H:%M:%S"));
        pstmt->setInt(3, x.volume);
        pstmt->executeUpdate();
    }

    void push_data(const std::vector<sql_tick>& X)
    {
        for(const auto& x : X) push_data(x);
    }

    bool load_ticks(int instrument_id, const tiempo::datetime_type& date_from, TickByTick& X)
    {
        X.clear();
        if (!S_pull_ticks2) {
            S_pull_ticks2.reset(
                con->prepareStatement(
                "SELECT * FROM `tix`.`ticks` \
                    WHERE (instrument_id=? AND date>=?) ORDER BY `date` ASC, `id` ASC"
                )
            );
        }
        auto pstmt = S_pull_ticks2;
        pstmt->setInt(1, instrument_id);
        pstmt->setString(2, date_from.to_string("%Y-%m-%d %H:%M:%S"));
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        if (res->rowsCount() == 0) return false;
        while(res->next()) {
            TickType tick;
            tick.d = tiempo::datetime_type(res->getString("date"));
            tick.p = res->getDouble("price");
            tick.v = res->getInt("volume");
            X.push_back(tick);
        }
        return true;
    }

};






#endif

