#include "sql_instruments.hpp"
#include <boost/format.hpp>

#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>

void extract_one(const std::unique_ptr<sql::ResultSet>& res, sql_instrument& x)
{
    x.id = res->getInt("id");
    x.name = res->getString("name");
    x.CQGticker = res->getString("ticker");
    x.GVticker = res->getString("GVticker");
    x.exchange = res->getString("exchange");
    x.description = res->getString("description");
    x.source = res->getString("source");
    x.type = res->getInt("type");
    x.disabled = res->getInt("disabled");
    x.open_market = res->getString("open_market");
    x.commission = res->getString("commission");
    x.lotsize = res->getInt("lotsize");
    x.margin = res->getDouble("margin");
    x.trading_day = res->getString("trading_day");
}


sql_instruments::sql_instruments(std::shared_ptr<sql::Connection> con_)
    : con(con_)
{
}

sql_instruments::~sql_instruments()
{
}

void sql_instruments::get_IDs(std::vector<int>& IDs)
{
    IDs.clear();
    std::unique_ptr<sql::Statement> pstmt(con->createStatement() );
    std::unique_ptr<sql::ResultSet> res(
        pstmt->executeQuery("SELECT `id`, `name` FROM `Main`.`Instruments`")
    );
    if (res->rowsCount() > 0) {
        while(res->next()) {
            IDs.push_back(res->getInt("id"));
        }
    }
    pstmt->close();
    pstmt.reset();
}


bool sql_instruments::pull_data(int instrument_id, sql_instrument& x)
{
    std::string query = (boost::format("SELECT * FROM `Main`.`Instruments` WHERE `id` = %d")%instrument_id).str();
    std::shared_ptr<sql::Statement> pstmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery(query));
    if (res->rowsCount() != 1) return false;
    res->first();
    extract_one(res, x);
    return true;
}

bool sql_instruments::pull_data(const std::string& name, sql_instrument& x)
{
    std::string query = (boost::format("SELECT * FROM `Main`.`Instruments` WHERE `name` = \'%s\'")%name).str();
    std::shared_ptr<sql::Statement> pstmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery(query));
    if (res->rowsCount() != 1) return false;
    res->first();
    extract_one(res, x);
    return true;
}


int sql_instruments::get_id(const std::string& instrument_name)
{
    std::string query = (boost::format(
        "SELECT `id` FROM `Main`.`Instruments` WHERE `name` = \'%s\'") % instrument_name).str();
    std::shared_ptr<sql::Statement> pstmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery(query));
    if (!res->rowsCount()) return 0;
    res->first();
    int myid = res->getInt("id");
    return myid;
}



