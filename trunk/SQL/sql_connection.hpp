#ifndef __sql_connection__
#define __sql_connection__
#include <thread>
#include <memory>
#include <mutex>
#include <boost/utility.hpp>

#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>


class sql_connection: boost::noncopyable
{
  public:
    sql_connection();
    ~sql_connection();
    void lock();
    void unlock();
    std::shared_ptr<sql::Connection> connection() { return con; }
    std::shared_ptr<sql::Connection> get_connection() { return con; }

    void execute(const std::string&);
  private:
    sql::Driver* driver;
    std::shared_ptr<sql::Connection> con;
    std::mutex mtx_;
};


#endif

