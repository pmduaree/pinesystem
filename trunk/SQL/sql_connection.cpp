#include "sql_connection.hpp"
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

sql_connection::sql_connection()
    : driver(nullptr) 
{
    const std::string url = SQL_URL;
    const std::string user = SQL_USER;
    const std::string pass = SQL_PASS;
    // connect to the database.
    std::cout << "Connecting to the SQL..." << std::endl;

try
{  // std::cout << SQL_URL << " " << SQL_USER << " " << SQL_PASS << std::endl;
    driver = sql::mysql::get_driver_instance();
    //std::cout << "driver done" <<std::endl;
   // con.reset( driver->connect("tcp://127.0.0.1", user, pass) );
   con.reset(driver->connect(url, user, pass));
   // std::cout << "con reset done " << std::endl;


}	 catch (sql::SQLException &e)
    {
		/*
         The MySQL Connector/C++ throws three different exceptions:
         
         - sql::MethodNotImplementedException (derived from sql::SQLException)
         - sql::InvalidArgumentException (derived from sql::SQLException)
         - sql::SQLException (derived from std::runtime_error)
         */
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;

        throw std::runtime_error("Unable to connect to server and set up a SQL connection ");
	}



    std::cout << "Connection to the SQL established from thread " << std::this_thread::get_id() << std::endl;
}

sql_connection::~sql_connection()
{
    if (con) con->close();
}


void sql_connection::lock()
{
    mtx_.lock(); 
}
void sql_connection::unlock()
{
    mtx_.unlock();
}

void sql_connection::execute(const std::string& query)
{
    if (!con) throw std::runtime_error("There is no connection");
    lock();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    stmt->execute(query);
    unlock();
}
