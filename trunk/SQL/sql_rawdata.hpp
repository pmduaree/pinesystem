#ifndef __sql_rawdata__
#define __sql_rawdata__
#include <string>

#include <cppconn/exception.h>

#include "sql_instruments.hpp"
#include "../Tiempo/tiempo.hpp"
#include "../Tiempo/ticks.hpp"
#include "sql_connection.hpp"

#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

struct raw_data_item
{
    uint64_t id;
    uint32_t instrument_id;
    uint32_t freq;
    double high;
    double open;
    double close;
    uint32_t volume;
    double  low;
    tiempo::datetime date;
    std::string note;
    
    void clear()
    {
        id = instrument_id = 0;
        high= open = close = low = 0;
        volume = 0;
        note.clear();
    }
};

class sql_rawdata
{
  public:
    sql_rawdata( std::shared_ptr<sql::Connection>);
    ~sql_rawdata();
    
    bool get_last(int instrument_id, int freq, raw_data_item& last);
    bool get_first(int instrument_id, int freq, raw_data_item& last);
    tiempo::datetime get_last_date(int instrument_id, int freq);
    tiempo::datetime get_first_date(int instrument_id, int freq);
    
    bool pull_data(int instrument_id, int freq, const tiempo::datetime& date, raw_data_item& item);
    bool pull_data(int id, raw_data_item& x);
    void update_data(const raw_data_item& x);
    void push_data(const raw_data_item& x);
    void push_data(const std::vector<raw_data_item>& X);
    bool load_data(int instrument_id, const tiempo::datetime& date_from,
                   const tiempo::datetime& date_to, tix_series& X) ;
                   //__attribute__((deprecated))
    bool load_data_spread(int instrument_id, const tiempo::datetime& date_from,
                   const tiempo::datetime& date_to, tix_series& X) ;
    bool load_data_midprice(int instrument_id, const tiempo::datetime& date_from,
                   const tiempo::datetime& date_to, tix_series& X) ;

    
  private:
    std::shared_ptr<sql::Connection> con;
    std::shared_ptr<sql::PreparedStatement> S_last, S_pull1, S_pull_dd,
                                            S_first, S_pullwdate;
    std::shared_ptr<sql::PreparedStatement> S_push,S_update,S_delete,S_pull;
    void extract_one(const std::unique_ptr<sql::ResultSet>& res, raw_data_item& x);
};

#endif
