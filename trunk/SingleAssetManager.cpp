//
//  SingleAsseetManager.cpp
//  
//
//  Created by David de la Rosa on 6/24/14.
//
//

#include "SingleAssetManager.hpp"

SingleAssetManager::SingleAssetManager(std::shared_ptr<sql_connection> _conn,int _id_asset)
:conn(_conn),id_asset(_id_asset)
{
    asset = std::make_shared<Asset>(conn,_id_asset);
    cleanStart();
}
void SingleAssetManager::Long_position(tiempo::datetime date, unsigned int NAssets)
{
    if (asset->get_exchange().is_market_open(date)) {
        std::tuple<double,double,double,double> Prices;
        Prices=asset->fetchValue(date);
        double CurrentValue  = std::get<CLOSE>(Prices); Last_Price_Value = CurrentValue;
        Positions       += NAssets;
        CurrentMoney    -= NAssets*CurrentValue;
        AccumCommission += asset->get_commission( CurrentValue);
        std::cout << "Long position of " << NAssets << " assets of " << asset->get_name() << std::endl;
        if(TimeHistory.find(date) == TimeHistory.end()){
            TimeHistory[date] = std::make_tuple(Positions,CurrentValue,"Long_Position,BUY");
            LastTrade =date ;
        }
           else
               TimeHistory[date+std::chrono::seconds(1)] = std::make_tuple(Positions,CurrentValue,"Long_Position,BUY");
    }
    else {
        std::cout << "Market,not opened !!!!!,Unable to trade "<< asset->get_name() << " at date " << date.to_string() << std::endl;
    }
}
double SingleAssetManager::Long_position(tiempo::datetime date,double money)
{
    if (asset->get_exchange().is_market_open(date) && money>0 )
    {
        std::tuple<double,double,double,double> Prices=asset->fetchValue(date);
        double CurrentValue  = std::get<CLOSE>(Prices);
        int  NAssets = floor(money/CurrentValue);
        if (NAssets>0)
        {
            double NotUSed = money - NAssets*CurrentValue;
            Long_position(date,(unsigned int)NAssets);
            return NotUSed;
        }else{
           std::cout << "Market,not opened !!!!!,Unable to trade "<< asset->get_name() << " at date " << date.to_string() << std::endl;
        return money;
        }
    }
    return money;
}
void SingleAssetManager::Short_position(tiempo::datetime date,  unsigned int NAssets)
{
    if (asset->get_exchange().is_market_open(date))
    {
        std::tuple<double,double,double,double> Prices=asset->fetchValue(date);
        double CurrentValue  = std::get<CLOSE>(Prices); Last_Price_Value = CurrentValue;
        Positions       -= NAssets;
        CurrentMoney    += NAssets*CurrentValue;
        AccumCommission += asset->get_commission( CurrentValue);
        std::cout << "Short position of " << NAssets << " assets of " << asset->get_name() << std::endl;
         if(TimeHistory.find(date) == TimeHistory.end())
         {
             TimeHistory[date] = std::make_tuple(Positions,CurrentValue,"Short_Position,SELL");
             LastTrade = date ;
         }
         else
             TimeHistory[date+std::chrono::seconds(1)] = std::make_tuple(Positions,CurrentValue,"Short_Position,SELL");
        
    }
    else {
        std::cout << "Market,not opened !!!!!,Unable to trade "<< asset->get_name() << " at date " << date.to_string() << std::endl;
        }
}

double SingleAssetManager::Short_position(tiempo::datetime date, double money)
{
    if (asset->get_exchange().is_market_open(date) && money>0 )
    {
        std::tuple<double,double,double,double> Prices=  asset->fetchValue(date);
        double CurrentValue  = std::get<CLOSE>(Prices);
        int  NAssets = floor(money/CurrentValue);
        if (NAssets>0)
        {
            double NotUSed = money - NAssets*CurrentValue;
            Short_position(date,(unsigned int )NAssets);
            return NotUSed;
        }else{
            std::cout << "Market,not opened !!!!!,Unable to trade "<< asset->get_name() << " at date " << date.to_string() << std::endl;
            return money;
        }
    }
    return money;
}
std::string SingleAssetManager::toString()
{
    std::string cout;
    //std::cout << Positions << std::endl;
    cout += (boost::format("Asset id=%d Name=%s\n CurrentMoney=%f \t Commisions= %f \t Positions=%d \n")
            % asset->get_id() % asset->get_name() % CurrentMoney % AccumCommission % Positions ).str();
    for (auto x : TimeHistory)
        cout += (boost::format("  %s   -> Pos=%d  \t Val=%f  \t Comm=%s \n")
                            % x.first.to_string() % std::get<0>(x.second) % std::get<1>(x.second) % std::get<2>(x.second)   ).str();
    return cout;
}


