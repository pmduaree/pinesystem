//
//  SignalSeries.h
//  
//
//  Created by David de la Rosa on 6/23/14.
//
//

#ifndef ____SignalSeries__
#define ____SignalSeries__

#include <iostream>
#include <memory>
#include <atomic>
#include <string>
#include <boost/format.hpp>
#include "Tiempo/time_series.hpp"
#include "SQL/sql_connection.hpp"
#include "Assets/masset.hpp"
#include "Assets/bidAsk.hpp"

#include "Strategy/strategy.hpp"

#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#define LOAD_FROM_DB    1
#define CALCULATE_ON_THE_FLY 2
#define LOAD_FROM_DB_LIVE    3

#define LIVE_SIGNALS        1
#define NON_LIVE_SIGNALS    0

#define MULTIPLE_ASSET      0
#define SINGLE_ASSET        1


typedef std::map<tiempo::datetime, std::tuple<int,double,std::string>> SignalTix;

class SignalSeries
{
    
public:
    SignalSeries(std::shared_ptr<sql_connection> conn_,int  id_stategy);
    ~SignalSeries(){}
    
    void Init(tiempo::datetime from,tiempo::datetime to,int type );
    int get_portfolio(){ return id_portfolio;}
    int get_strategy(){return id_strategy;}
    int get_freq(){return freq;}
    SignalTix get_Signals(){return Signals;}
    std::tuple<int,double,std::string>
    get_One_Signal(tiempo::datetime date){ return Signals[date];  }
    void Set_Parameters(std::string param){StratParam  = param;}
    void Set_freq(int _freq ) {freq =  _freq;}
    int  Get_SignalerID(){return SignalerID;}
    void setType(std::string type);
    
    std::string toString();
private:
    void LoadSignals_formDB(tiempo::datetime from,tiempo::datetime to,int);
    void LoadSignals_formCalculations(tiempo::datetime from,tiempo::datetime to);
    void Load_ONE_Signal_formDB(tiempo::datetime ,tiempo::datetime ,int);
    std::tuple<int,double,std::string>
         Load_ONE_Signal_formCalculations(tiempo::datetime date);    
    std::shared_ptr<sql_connection> conn;
    SignalTix Signals;
    int id_portfolio;
    int id_asset;
    int id_strategy;
    int freq;
    int Hrs_Back;
    int SignalerID;
    bool TypeOfAssetMultiple;
    int onTrade;
    std::string StratParam;
    std::string type;
    
};

#endif /* defined(____SignalSeries__) */
