#ifndef __GLOBAL_VIEW__
#define __GLOBAL_VIEW__
//////////////////////////////////////////////////////////////////////////////
//
// Extracts data from Data View by firrst connecting to the web page,
// sending credentials and then fetching data in a csv form.
//
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <unordered_map>
#include <curl/curl.h>

#include "data_series.hpp"
#include "util/tiempo.hpp"
#include "globalview_parser.hpp"

#ifdef LINODE
#undef SINGO
#undef JUNIOR
#undef IZTOKMAC
#endif

#ifdef SINGO
#define GLOBALVIEW_FETCH "http://192.168.200.99/globalview.php"
#endif

#ifdef JUNIOR
#define GLOBALVIEW_FETCH "http://192.168.200.99/localdata.php"
#endif

#ifdef IZTOKMAC
#define GLOBALVIEW_FETCH "http://192.168.200.99/localdata.php"
#endif


#ifndef GLOBALVIEW_FETCH
#error "NEED TO DEFINE GLOBALVIEW_FETCH"
#endif

class html_extractor
{
  protected:
    CURL* curl;
  public:
    html_extractor()
    {
        curl_global_init(CURL_GLOBAL_ALL);
    }

    ~html_extractor()
    {
        curl_global_cleanup();
    }
    

    std::string get(const std::string& url)
    {
        std::cout << "Getting data from url: " << url << std::endl;
        std::string data;
        curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &html_extractor::writeCallback);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        return data;
    }

  protected:
    static size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
    { 
        std::string& my_data = *static_cast<std::string*>(up);
        for (size_t c = 0; c<size*nmemb; c++)
        {
            my_data.push_back(buf[c]);
        }
        return size*nmemb;
    }

};


class data_extractor: public html_extractor
{
    const char* url0= "http://li684-26.members.linode.com";
    // these credentials hsuold be stored in some other way but let's keep
    // it like this for now.
    // the hash_password is actually hash_password = md5(password).
    // not salted or anything but that's what it is
    const char* username = "admin";
    const char* password = "admin123";
    const char* hash_password = "0192023a7bbd73250516f069df18b500";
  public:
    data_extractor(): html_extractor() {}
    
    void login()
    {
        std::string post_fields = "user=" + std::string(username) + \
                                  "&pswd=" + std::string(password) + "&submit=submit";
        std::string url_index(url0);
        url_index += "/index.php";
        std::string data;
        // call CURL
        curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_URL, url_index.c_str());
        //curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookies.txt");
        //curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookies.txt");
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_fields.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &data_extractor::writeCallback);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    
    std::string get_data(const std::string& instrument,
                  const std::string& date_beg, const std::string& date_end,
                  int timestamp)
    {
        std::unordered_map<std::string,std::string> req_data;
        req_data["user"] = username;
        req_data["pswd"] = hash_password;
        req_data["instrument_id"] = instrument;
        req_data["start_date"] = date_beg;
        req_data["end_date"] = date_end;
        req_data["timestamp"] = std::to_string(timestamp);
        req_data["export"] = "yes";
        std::string url = url0;
        url += "/fetch_data.php?";
        std::string z;
        for(const auto& x : req_data) {
            if (!z.empty()) z += "&";
            z += x.first + "=" + x.second;
        }
        url += z;
        return html_extractor::get(url);
    }
};



class GlobalView
{
    GlobalViewParser gvparser;
  public:
    GlobalView()
    {
    }
    
    void fetch(const std::string& ticker, 
               tiempo::datetime_type date_from,
               tiempo::datetime_type date_to,
               int timestamp,
               data_series& data)
    {
        tiempo::datetime_type min_date = date_to - 60000 * tiempo::duration(60*timestamp);
        if (date_from < min_date) date_from = min_date;
        std::string buf;
#ifdef GLOBALVIEW_FETCH
        html_extractor extractor;
        auto date_format = "%Y-%m-%dT%H:%M:%SZ";
        std::string date_beg = date_from.to_string(date_format);
        std::string date_end = date_to.to_string(date_format);
        std::string url = GLOBALVIEW_FETCH;
        url += "?instrument_id=";
        url += ticker;
        url += "&start_date=";
        url += date_beg;
        url += "&end_date=";
        url += date_end;
        url += "&timestamp=";
        url += std::to_string(timestamp);
        buf = extractor.get(url);
#else
        data_extractor extractor;
        extractor.login();
        auto date_format = "%Y-%m-%d %H:%M";
        std::string date_beg = date_from.to_string(date_format);
        std::string date_end = date_to.to_string(date_format);
        auto process_date = [](std::string& d) {
            while( d.find_first_of(' ') != std::string::npos) {
                d.replace(d.find_first_of(' '), 1, "%20");
            }
        };
        process_date(date_end);
        process_date(date_beg);
        buf = extractor.get_data(ticker, date_beg, date_end, timestamp);
#endif
        gvparser(buf, data);
        if (data.items.empty() ) {
            std::cout << "NO data fetched. Return:\n";
            std::cout << buf << std::endl;
            std::cout << std::endl;

        }
    }

};



#endif

