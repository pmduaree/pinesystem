#ifndef __gv_parser__
#define __gv_parser__

#include <string>
#include <vector>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include "data_series.hpp"

class GlobalViewParser
{
  public:
    void operator()(const std::string& buf, data_series& data)
    {
        // now parse the buffer.
        auto p_beg = buf.find("[");
        //auto p_end = buf.rfind("]");
        auto p_cur = p_beg + 1;
        auto p = buf.find("[", p_cur);
        data.clear();
        if (buf.find("ERROR") != std::string::npos) return;
        while (std::string::npos != (p = buf.find("[", p_cur)) ) {
            auto p_end = buf.find("]", p);
            auto row = buf.substr(p+1, p_end-p-1);
            //std::cout << row << std::endl;
            boost::trim(row);
            if (!row.empty()) {
                std::vector<std::string> fields;
                boost::split(fields, row, boost::is_any_of(","));
                for(auto& x : fields) {
                    if (x.front() != '"' || x.back() != '"') {
                        std::cout << "ERROR: " << row << std::endl;
                    }
                    assert( x.front() == '"' && x.back() == '"');
                    x = x.substr(1, x.length() - 2);
                }
                if (data.labels.empty()) data.labels = fields;
                else data.items.push_back(fields);
            }
            p_cur = p_end;
        }
    }

};

#endif
