// Load instruments, select /BRN:60, and print raw data in a given period.
#include <iostream>

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <map>
#include <atomic>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include "sql/sql_connection.hpp"
#include "sql/sql_instruments.hpp"
#include "sql/sql_rawdata.hpp"
#include "util/tiempo.hpp"
#include "data_series.hpp"
#include "util/prog_opts.hpp"
#include "ticks/global_view.hpp"


#include <boost/format.hpp>


int main(int argc, char** argv)
{
    prog_opts parser;
    parser.add_option<std::string>("-a", "--asset", OptionType::String, "asset", "");
    parser.add_option<int>("-q", "--freq", OptionType::Integer, "freq", 0);
    parser.add_option<std::string>("--from", OptionType::String, "date_from", "2014-01-01");
    parser.add_option<std::string>("--to", OptionType::String, "date_to", "now");
    if (!parser.parse_args(argc, argv) || parser.option<bool>("help")) {
        std::cerr << parser.help() << std::endl;
        return 0;
    }
    

    std::string asset_name = parser.option<>("asset");
    int freq = parser.option<int>("freq");
    std::string date_from = parser.option<>("date_from");
    std::string date_to = parser.option<>("date_to");

    
    auto conn = std::make_shared<sql_connection>();


    std::string GVticker = "";
    int instrument_id = 0;

    // go to the database and get the GVticker.
    conn->lock();
    std::string q = (boost::format("SELECT id, GVticker FROM `Main`.`Instruments` WHERE `name`=\'%s\'") % asset_name
                     ).str();
    auto con = conn->connection();
    std::shared_ptr<sql::Statement> stmt(con->createStatement());
    std::unique_ptr<sql::ResultSet> res(stmt->executeQuery(q));
    if (res->rowsCount() > 0) {
        res->first();
        instrument_id = res->getInt("id");
        GVticker = res->getString("GVticker");
    }
    conn->unlock();

    if (GVticker == "" || !instrument_id) {
        std::cout << "No GV Ticker found for this asset. Quitting." << std::endl;
        return 0;
    }

    std::cout << "Processing GV Ticker " << GVticker << std::endl;

    GlobalView globalview;

    data_series data;
    globalview.fetch(GVticker, date_from, date_to, freq, data);


    sql_rawdata rawdata(conn->connection());
    
    int i_date = data.sub_index("TRADE_DATETIME");
    
    
    // go through all of them and push them.
    for(const auto& item : data.items) {
        tiempo::datetime_type on_date(item[i_date]);
        unsigned int volume = boost::lexical_cast<unsigned int>(item[data.sub_index("VOLUME")]);
        if (!volume) continue;  // nothing to do anyway.
        // maybe we have this one already.
        raw_data_item x;
        x.id = 0;
        bool has_item = rawdata.pull_data(instrument_id, freq, on_date, x);
        if (has_item && x.volume >= volume) continue;
        x.instrument_id = instrument_id;
        x.freq = freq;
        x.close = boost::lexical_cast<double>(item[data.sub_index("CLOSE")]);
        x.open = boost::lexical_cast<double>(item[data.sub_index("OPEN")]);
        x.high = boost::lexical_cast<double>(item[data.sub_index("HIGH")]);
        x.low = boost::lexical_cast<double>(item[data.sub_index("LOW")]);
        x.volume = boost::lexical_cast<int>(item[data.sub_index("VOLUME")]);
        x.date = on_date;
        
        if (has_item) {
            x.note = "updated by feed_data";
            rawdata.update_data(x);
        } else {
            x.note = "feed_data C++";
            rawdata.push_data(x);
        }
    }

    return 0;
}


