// usage:
//  ./gvparser < data.globalview > data.csv
#include "globalview_parser.hpp"
#include <iostream>
#include <string>

int main()
{
    std::string gvdata;
    std::string line;
    while (std::getline(std::cin, line))
    {
        gvdata += line;
    }

    data_series data;
    GlobalViewParser parser;
    parser(gvdata, data);
    for(int i=0;i<data.labels.size();++i) {
        if (i>0) std::cout <<",";
        std::cout << data.labels[i];
    }
    std::cout << std::endl;
    for(int k=0;k<data.items.size();++k) {
        const auto& Z = data.items[k];
        for(int i=0;i<Z.size();++i) {
            if (i>0) std::cout <<",";
            std::cout << Z[i];
        }
        std::cout << std::endl;
    }


    return 0;
}
