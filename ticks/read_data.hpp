#ifndef __READ_DATA__
#define __READ_DATA__

#include <chrono>
#include <string>
#include <iomanip>
#include "util/date_time.hpp"
#include "data_series.hpp"
#include "textfile_io.hpp"
#include "util/prices.hpp"

#include "global_view.hpp"


void read_data_from_internet(const std::string& ticker, const std::string& exchange, 
                             const std::string& field,
                             int frequency, 
                             const tiempo::datetime_type& date_from, 
                             const tiempo::datetime_type& date_to,
                             time_series<double>& ts)
{
    ts.clear();
    data_series data;
    GlobalView globalview;
    globalview.fetch(ticker, exchange, date_from, date_to, frequency, data);
    auto dates = data.sub("TRADE_DATETIME");
    auto prices = data.sub(field);
    for(size_t i=0;i<data.size();++i) {
        tiempo::datetime_type mydate(dates[i]);
        double p = boost::lexical_cast<double>(prices[i]);
        #if 0
        slice<double> s(mydate, p);
        ts.push_back(s);
        #else
        ts[mydate] = p;
        #endif
    }
}









#endif
