// Load instruments, select /BRN:60, and print raw data in a given period.
#include <iostream>

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <map>
#include <atomic>
#include "util/date_time.hpp"
#include "data_series.hpp"
#include "ticks/global_view.hpp"
#include <sstream>
#include <cstring>

#undef SINGO
#undef JUNIOR
#undef IZTOKMAC


extern "C" 
void fetch_globalview_data(const char* instrument, const char* exchange, 
                           int freq, const char* str_from, const char* str_to, 
                           unsigned int size_of_buffer, char* buffer)
{
    tiempo::datetime_type date_from(str_from);
    tiempo::datetime_type date_to(str_to);

    data_series data;
    GlobalView globalview;
    globalview.fetch(instrument, exchange, date_from, date_to, freq, data);

    std::stringstream str;
    str << "#";
    for(size_t i=0;i<data.labels.size();++i) {
        if (i) str << ",";
        str << data.labels[i];
    }
    str<<"\n";


    for(const auto& x : data.items) {
        for(size_t i=0;i<x.size();++i) {
            if (i) str << ",";
            str << x[i];
        }
        str << "\n";
    }

    std::string mystr = str.str();

    strncpy(buffer, mystr.c_str(), size_of_buffer);
}


