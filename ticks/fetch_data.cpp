// Load instruments, select /BRN:60, and print raw data in a given period.
#include <iostream>

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <map>
#include <atomic>
#include "util/date_time.hpp"
#include "util/prog_opts.hpp"
#include "data_series.hpp"
#include "ticks/global_view.hpp"



int main(int argc, char** argv)
{
    prog_opts parser;
    parser.add_option("-i", "--instrument", OptionType::String, "instrument", "");
    parser.add_option("-e", "--exchange", OptionType::String, "exchange", "");
    parser.add_option("-q", "--freq", OptionType::Integer, "freq", 0);
    parser.add_option("--from", OptionType::String, "date_from", "2014-03-01");
    parser.add_option("--to", OptionType::String, "date_to", "now");
    if (!parser.parse_args(argc, argv) || parser.option<bool>("help")) {
        std::cerr << parser.help() << std::endl;
        return 0;
    }
    
    std::string instrument = parser.option<>("instrument");
    std::string exchange = parser.option<>("exchange");
    int freq = parser.option<int>("freq");
    std::string date_from = parser.option<>("date_from");
    std::string date_to = parser.option<>("date_to");

    data_series data;
    GlobalView globalview;
    globalview.fetch(instrument, exchange, date_from, date_to, freq, data);

    for(const auto& x : data.items) {
        for(size_t i=0;i<x.size();++i) {
            if (i) std::cout << ",";
            std::cout << x[i];
        }
        std::cout << std::endl;
    }
    
    
    return 0;
}


