#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern 
void fetch_globalview_data(const char* instrument, const char* exchange,
                           int freq, const char* str_from, const char* str_to,
                           unsigned int size_of_buffer, char* buffer);

int main(int argc, char** argv)
{
    const char* instrument = argv[1];
    const char* exchange = argv[2];
    int freq = atoi(argv[3]);
    const char* date1 = argv[4];
    const char* date2 = argv[5];
    unsigned int bufsize = (1<<20);
    char* buf;
    
    buf = (char*)malloc(bufsize);
    fetch_globalview_data(instrument, exchange, freq, date1, date2, bufsize, buf);

    printf("%s", buf);


    free(buf);
    
    
    return 0;
}


