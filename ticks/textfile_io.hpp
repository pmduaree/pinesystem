/////////////////////////////////////////////////////////////////////////////
// textfile_io.hpp
//  Historical data file parser
//
// This header file provides routines for parsing data files in a TSV 
// (tab separated values), SSV (space separated values) and CSV
// (comma separted values). Note that in the TSV and SSV case, empty 
// fields are ignored (meaning, two tabs is the same as one tab)
//
// The main function is read_data_from_file(FNAME, FORMAT, OUTVECTOR)
// where FORMAT can be:
//  csv:5   : take 5th value in the CSV format
//  col:3   : the 3rd value in the TSV or SSV format. 
//
// new: now we also take into account the date. so an extended format 
// is:
//  csv:1:5 or 
//  csv:1-2:5
// with backward compatibility : col:3 translates to col:1:3 
//
// Author:
//  Iztok Pizorn <pizorn@mail.ru>
//  November 24, 2013
/////////////////////////////////////////////////////////////////////////////
#ifndef __TEXTFILE__IO__
#define __TEXTFILE__IO__

#include <string>
#include <list>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

#include "../trunk/Tiempo/tiempo.hpp"
#include "../trunk/Tiempo/time_series.hpp"

void split_line(const std::string& line, const std::string& format,
                std::vector<std::string>& fields)
{
    fields.clear();
    if (format.compare("col") == 0) {
        std::list<std::string> itemlist;
        boost::split(itemlist, line, boost::is_any_of(" \t"));
        for(const auto& x : itemlist) {
            if (!x.empty()) fields.push_back(x);
        }
    } else if (format.compare("csv") == 0) {
        boost::split(fields, line, boost::is_any_of(","));
    } else {
        throw std::runtime_error("unknown data format");
    }
}


template<typename T>
bool parse_line(const std::string& line, const std::string& format, 
                tiempo::datetime_type& dat, T& val)
{
    if (line.find('#') == 0) return false;
    
    std::vector<std::string> format_fields;
    boost::split(format_fields, format, boost::is_any_of(":"));
    std::string data_format = format_fields.front();
    std::string date_index = "1";
    if (format_fields.size() == 3) {
        date_index = format_fields[1];
    }
    int indx = boost::lexical_cast<int>(format_fields.back()) - 1;
    
    val = 0;
    std::vector<std::string> fields;
    split_line(line, data_format, fields);
    
    if (fields.size() < indx) return false;
    val = boost::lexical_cast<T>(fields[indx]);
    
    // and now get the date.
    std::vector<std::string> date_fields;
    boost::split(date_fields, date_index, boost::is_any_of("+"));
    std::string string_date;
    string_date = "";
    for(auto it = date_fields.begin(); it != date_fields.end(); ++it) {
        string_date += fields[boost::lexical_cast<int>(*it)-1];
        if (it != date_fields.end()) string_date += " ";
    }
    boost::trim(string_date);
    dat = tiempo::datetime_type(string_date);
    return true;
}


/////////////////////////////////////////////////////////////////////////////
// read data from a text file.
//
// formats:
//  csv:8       : read a csv file and take 8-th item (first is 1)
//  col:7       : read a tab or space separated data and take 7-th item.
/////////////////////////////////////////////////////////////////////////////
template<typename T>
void read_data_from_file(const std::string& fname, 
                         const std::string& format, 
                         std::vector<tiempo::datetime_type>& dates,
                         std::vector<T>& P)
{
    dates.clear();
    P.clear();
    std::ifstream fp(fname.c_str());
    if (!fp.is_open()) {
        std::cout << "Couldn't open file\n";
        throw std::runtime_error("failed raeding");
    }
    std::string line;
    while(std::getline(fp, line)) {
        boost::trim(line);
        if (line.empty()) continue;
        if (line.find("#") == 0) continue;
        T val;
        tiempo::datetime_type dat;
        try {
            if (parse_line(line, format, dat, val)) {
                dates.push_back(dat);
                P.push_back(val);
            }
        } catch (std::exception& e) {
            std::cout << "textfile I/O exception: " << line << std::endl;

        }
    }
    fp.close();
}

template<typename T>
void read_data_from_file(const std::string& fname, const std::string& format, 
                         time_series<T>& data)
{
    data.clear();
    std::ifstream fp(fname.c_str());
    if (!fp.is_open()) {
        std::cout << "Couldn't open file" << std::endl;
        throw std::runtime_error("reading failed");
    }
    std::string line;
    while(std::getline(fp, line)) {
        boost::trim(line);
        if (line.empty()) continue;
        if (line.find("#") == 0) continue;
        try {
            tiempo::datetime_type date;
            double val;
            if (parse_line(line, format, date, val)) {
                data[date] = val;
            }
        } catch (...) {
            std::cout << "textfile I/O exception: " << line << std::endl;
        }
    }
    fp.close();
}


#endif
