#ifndef __DATA_SERIES__
#define __DATA_SERIES__


#include <string>
#include <vector>

struct data_series
{
    std::vector<std::string> labels;
    std::vector<std::vector<std::string> > items;
    
    std::vector<std::string> sub(const std::string& label) const
    {
        size_t idx = sub_index(label);
        std::vector<std::string> rv;
        rv.reserve(items.size());
        for(const auto& item: items)
            rv.push_back( item[idx] );
        return rv;
    }

    size_t sub_index(const std::string& label) const
    {
        size_t idx = std::find(labels.begin(), labels.end(), label) - labels.begin();
        if (idx >= labels.size()) throw std::runtime_error("key not found");
        return idx;
    }
    
    size_t size() const { return items.size(); }
    
    void clear()
    {
        labels.clear();
        items.clear();
    }

};

#endif
