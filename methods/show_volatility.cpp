#include <iostream>

#include <chrono>
#include <boost/format.hpp> 
#include "sql/sql_rawdata.hpp"
#include "sql/sql_instruments.hpp"

#include "volatility.hpp"

#include "util/prog_opts.hpp"

int main(int argc, char** argv)
{
    prog_opts pars;
    pars.add_option("-i", "--instrument", OptionType::Integer, "instrumentid", 0);
    pars.add_option("-l", "--lag", OptionType::Integer, "lag", 10);
    pars.add_option("--to", OptionType::String, "date_to", "now");
    pars.add_option("--from", OptionType::String, "date_from", "yesterday");
    
    if (!pars.parse_args(argc, argv) || pars.option<bool>("help")) {
        std::cout << pars.help() << std::endl;
        return 0;
    }

    int instrument_id = pars.option<int>("instrumentid");
    int lag = pars.option<int>("lag");
    tiempo::datetime_type date_from(pars.option<>("date_from"));
    tiempo::datetime_type date_to(pars.option<>("date_to"));

    if (!instrument_id) {
        std::cout << boost::format("Invalid instrument id [=%d]\n") % instrument_id;
        return 0;
    }
    
    tiempo::duration vol_lag = std::chrono::minutes(lag);

    sql_db db;
    sql_conn conn(db, "tix");
    sql_rawdata rawdata(conn);

    //auto date_to = tiempo::datetime_type("now");
    //auto date_from = date_to - std::chrono::hours(5*24);
    
    tix_series tix;
    rawdata.load_data(instrument_id, date_from - 2* vol_lag , date_to, tix);

    
    Volatility volatility;

    auto it = tix.begin();
    while( it != tix.end() && it->first < date_from) ++it;

    auto it_beg = it;
    for(auto it = it_beg; it != tix.end(); ++it) {
        const auto& date = it->first;

        double vol, relvol;
        if (!volatility(tix, date, vol_lag, vol, relvol)) {
            continue;
        }

        std::cout << boost::format("%s %15.2f %15.6f %15.6f") % it->first.to_string() % 
                       it->second.p % vol % relvol << std::endl;
    }


    return 0;
}
