#ifndef __bollinger_bands_
#define __bollinger_bands_


#include "statvar.hpp"


class Bollinger: private StatVar
{
  public:
    Bollinger()
        : StatVar()
    {
    }
    bool operator()(const tix_series& tix, const tiempo::datetime_type& date, 
                    const tiempo::duration& lag, double f_up, double f_dn,
                    double& b_up, double& b_dn)
    {
        //-------------------------------------------------------------------
        // find iterator range
        //-------------------------------------------------------------------
        double mean, var;
        if (!StatVar::operator()(tix, date, lag, mean, var))
            return false;

        double stdev = sqrt(var);

        b_up = mean + f_up * stdev;
        b_dn = mean - f_dn * stdev;
        return true;
    }

};


#endif
