#ifndef __VOLATILIatR_HPP_
#define __VOLATILIatR_HPP_

#include <cstdlib>
#include "util/date_time.hpp"
#include "util/prices.hpp"

#include "statvar.hpp"


class Volatility: private StatVar
{
  public:
    Volatility(): StatVar() {}
    
    bool operator()(const tix_series& tix, const tiempo::datetime_type& date, 
                    const tiempo::duration& dur, double& vol, double& rel_vol) const
    {
        
        vol = 0.0;
        rel_vol = 0.0;
        double mean, var;
        if (!StatVar::operator()(tix, date, dur, mean, var)) return false;
        vol = sqrt(var);
        rel_vol = vol / fabs(mean);
        return true;
    }

};


#endif
