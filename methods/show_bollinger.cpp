#include <iostream>

#include <chrono>
#include <boost/format.hpp> 
#include "sql/sql_rawdata.hpp"
#include "sql/sql_instruments.hpp"

#include "bollinger.hpp"

#include "util/prog_opts.hpp"

int main(int argc, char** argv)
{
    prog_opts pars;
    pars.add_option("-i", "--instrument", OptionType::Integer, "instrumentid", 0);
    pars.add_option("-l", "--lag", OptionType::Integer, "lag", 10);
    pars.add_option("--to", OptionType::String, "date_to", "now");
    pars.add_option("--from", OptionType::String, "date_from", "yesterday");
    pars.add_option("-r", "--range", OptionType::Float, "range", 2.0);
    
    if (!pars.parse_args(argc, argv) || pars.option<bool>("help")) {
        std::cout << pars.help() << std::endl;
        return 0;
    }

    int instrument_id = pars.option<int>("instrumentid");
    int lag = pars.option<int>("lag");
    tiempo::datetime_type date_from(pars.option<>("date_from"));
    tiempo::datetime_type date_to(pars.option<>("date_to"));
    double bol_r = pars.option<double>("range");

    if (!instrument_id) {
        std::cout << boost::format("Invalid instrument id [=%d]\n") % instrument_id;
        return 0;
    }
    
    tiempo::duration bol_lag = std::chrono::minutes(lag);

    sql_db db;
    sql_conn conn(db, "tix");
    sql_rawdata rawdata(conn);

    //auto date_to = tiempo::datetime_type("now");
    //auto date_from = date_to - std::chrono::hours(5*24);
    
    tix_series tix;
    rawdata.load_data(instrument_id, date_from - 2* bol_lag , date_to, tix);

    
    Bollinger bollinger;

    auto it = tix.begin();
    while( it != tix.end() && it->first < date_from) ++it;

    auto it_beg = it;
    for(auto it = it_beg; it != tix.end(); ++it) {
        const auto& date = it->first;

        double b_up, b_dn;
        if (!bollinger(tix, date, bol_lag, bol_r, bol_r, b_up, b_dn)) {
            continue;
        }

        std::cout << boost::format("%s %15.2f %15.2f %15.2f") % it->first.to_string() % 
                       it->second.p % b_up % b_dn << std::endl;
    }


    return 0;
}
