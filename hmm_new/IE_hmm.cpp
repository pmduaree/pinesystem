#include <iostream>
#include <vector>
#include <list>
#include <boost/format.hpp>
#include "util/stringparpars.hpp"

#include "strategy_engine.hpp"

#include <iostream>
#include <tuple>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include "util/tiempo.hpp"
#include "util/linalg.hpp"
#include "hmm/hmm_simulator.hpp"

#include "IE_hmm.hpp"

SignalType
Strategy_hmm::calculate(const CandleSeries& C1min, const std::string& param, const tiempo::datetime& date)
{
    StringParamParser spp(param);
    int period = spp.get<size_t>("period", 60);
    // compose the data
    tiempo::duration dperiod = tiempo::minutes(period);
    CandleSeries candles;
    convert_candles(C1min, 1, 0, date-dperiod, period, 50, candles);
    if (candles.size() < 48) {
        //std::cout << "To little candles: " << candles.size() << "." << std::endl;
        //std::cerr << boost::format("Insufficient data for HMM [%s], on date %s") % param % date.to_string() << std::endl;
        return SignalType::error;
    }
    std::list<double> logps;
    auto rit = candles.rbegin();
    while(rit->first > date - dperiod) ++rit;
    auto rit_fut = rit;
    while(++rit != candles.rend() ) {
        auto logp = log( rit_fut->second.p_c / rit->second.p_c);
        logps.push_front(logp);
        rit_fut = rit;
    }
    std::vector<double> O_train(logps.begin(),logps.end() );
    
    //------------------------------------
    // set parameters
    //------------------------------------

    HMM::HMM_sim_param xparam;
    xparam.numMcRuns = 10;
    xparam.numModels = 70;
    xparam.eps = 1e-11;
    xparam.numBwSteps = 1000;
    xparam.bwConvergenceCheckLen = 30;
    xparam.bwConvergenceCheckEps = 1e-4;
    xparam.random_type = "gmm";
    xparam.N = spp.get<size_t>("N", 0);
    xparam.M = spp.get<size_t>("M", 0);
    xparam.T = spp.get<size_t>("T", 0);
    xparam.hmm_type = spp.get<std::string>("hmmtype", "sg");
    
    //std::cout << "time series has " << O_train.size() << " items." << std::endl;
    //std::cout << "Running the HMM predictor..." << std::endl;
    //std::cout << "DATA:\n" << O_train << std::endl;
    
    // run the predictor and store results in a slice (def in predictor.hpp)
    HMM::train_slice t_slice;
    bool suc = HMM::run_predictor(O_train, xparam, t_slice);
    if (!suc) return SignalType::error;
    
    // the return value is here.
    StringParamParser parser;
    parser.put("r_pos", t_slice.ratio_pos);
    parser.put("exval", t_slice.exval);
    parser.put("p_cumneg", t_slice.cum_prob_negative);
    std::string r = parser.to_string();
    
    // else, make it a result alright.
    double p1 = t_slice.ratio_pos;
    double p2 = 1.0 - t_slice.cum_prob_negative;
    double p = 0.5 * (p1 + p2);
    return make_signal(p,0.0, r);
}

