#ifndef discretization_engine_hpp
#define discretization_engine_hpp

/// discretization
/// takes fromType input (in current phase double) and returns disretized vector of toType type.
template <class fromType,class toType>
class discretizationEngine
{
public:
	std::vector<toType> dData;	//discretized data
	size_t NN;	//number of bins
	std::vector<fromType> bounds;
	
	///default variant uses uniform disretization
	discretizationEngine(const std::vector<fromType>& input, size_t iNN){
		NN=iNN;
        bounds.clear();
        std::vector<double> posv;
        for(const auto& x: input) posv.push_back(std::abs(x));
        std::sort(posv.begin(),posv.end());

        if (NN%2==0) {
            bounds.push_back(0);
            for(int i=0;i<NN/2-1;++i) {
                int zbnd = static_cast<int>(posv.size()*(i+1)*1.0/(NN/2));
                zbnd = std::min<int>(zbnd, posv.size());
                bounds.push_back(posv[zbnd-1]+1e-8);
                bounds.push_back(-posv[zbnd-1]+1e-8);
            }
            bounds.push_back(posv.back()+1e-8);
            bounds.push_back(-posv.back()-1e-8);
        } else {
            int z = std::min<int>(static_cast<int>(posv.size()*1.0/NN)+1, posv.size());
            bounds.push_back( posv[z-1]);
            bounds.push_back(-posv[z-1]);
            for(int i=0;i<NN/2-1;++i) {
                int z = std::min<int>(static_cast<int>(posv.size()*(1.0+2*i)/NN)+1, posv.size());
                bounds.push_back(posv[z-1]);
                bounds.push_back(-posv[z-1]);
            }
            bounds.push_back(posv.back()+1e-8);
            bounds.push_back(-posv.back()-1e-8);
        }
        std::sort(bounds.begin(), bounds.end());
        assert( bounds.size() == NN+1);
    #if 0
		//auto mm = std::minmax_element(input.begin(), input.end());
        //double max_span = std::max<int>(std::abs(mm.first),std::abs(mm.second));
		double min=*mm.first;
		double max=*mm.second;
		bounds.resize(NN+1);
		for( auto i=0;i<NN;++i ) 
            bounds[i]=(min + (max-min)/(double)(NN)*(double)i);
        bounds[NN]=max;
    #endif
        
		doIt(input);
	}

	///supply external bounds
	discretizationEngine(const std::vector<fromType>& input, 
                         const std::vector<fromType>& ibounds){
		bounds=ibounds;
		doIt(input);
	}
    
	///discretizator
	void doIt(const std::vector<fromType>& input)
    {
		dData.resize(input.size());
		for(auto i=0;i<input.size();i++){
			typename std::vector<fromType>::iterator	
				it=std::lower_bound(bounds.begin(), bounds.end(),input[i]);
			dData[i]=std::max((int)std::distance(bounds.begin(), it), 1)-1;
		}
		//prn(bounds);
	}
	
	std::vector<fromType> operator()(const std::vector<toType>& input){
		std::vector<fromType> out;
		out.resize(input.size());
		for(auto i=0;i<input.size();i++) out[i]=bounds[input[i]];
		return out;	
	}

};

#endif
