#ifndef _hmm_engine_header_
#define _hmm_engine_header_
/* 
 * Hidden Markov model engine
 */

#include <chrono>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>

#include <cstring>
#include <functional>

#include "hmm_statistics.hpp"

#include <boost/format.hpp>
#include "hmm_random.hpp"
#include "hmm_common.hpp"
//#include "kmean_engine.hpp"
#include "hmm_viterbi.hpp"
#include "hmm_baumwelch.hpp"



namespace HMM {
/////////////////////////////////////////////////////////////////////////////
/// \class HMM_engine
/// \brief Optimize Parameter definining the Hidden Markov Model
/// 
/// This is the main part of the hidden markov model algorithm.
/// It can generate an initial approximation for the HMM data structure,
/// as well as perform optimization with respect to the maximum likelihood.
///
/////////////////////////////////////////////////////////////////////////////
class HMM_engine
{
    BaumWelch BW;
   
	public:

    HMM_engine() {}
    /////////////////////////////////////////////////////////////////////////
    /// \brief Train HMM parameters to describe the observation sequence O 
    /////////////////////////////////////////////////////////////////////////
    template<class BJ, class TO>
    double train(HMM_data<BJ>& data, const std::vector<TO>& O, double eps,   
                 size_t nsteps, size_t conv_size )
    {
        // generate an initial approximation by k-mean clustering
        // A, (B_c,B_mu,B_sigma), Pi = self.make_init(O)
		Viterbi viterbi(data,O); //defines viterbi algorithms (alpha, beta, gamma)
		std::vector<double> PO_convergence;
		double P = calc_PO(viterbi); //likelihood
      
		std::list<double> Ps;
        Ps.push_back(P);
        
		size_t niter = 0;
		while (nsteps==0 || niter < nsteps) {
            ++niter;
            BW(data, viterbi, O);  //updates HMM model data 
			viterbi(data, O); //prepares new viterbi 
            P = calc_PO(viterbi); //likelihood
            //std::cout << P << std::endl;
            Ps.push_back(P);
			PO_convergence.push_back(P);	//stores convergence history
			while(Ps.size() > conv_size) Ps.pop_front();		//preveri ce zadeva se vedno kovergira
			if (Ps.size() == conv_size && fabs(Ps.back()/Ps.front() - 1.0)<eps) break;
			
        }
		//std::cout<< "alpha="<<viterbi.alpha_x()<<std::endl<<"end"<<std::endl;
		//prn(PO_convergence);
		
		return Ps.back();
    }
    /////////////////////////////////////////////////////////////////////////
    /// Make prediction for the observation immediately after the 
    /// training period.
    /// 
    /// Returns: the expected observation O_{T+1} [at time T + 1]
    /////////////////////////////////////////////////////////////////////////
    template<class BJ>
    auto predict(const HMM_data<BJ>& data, size_t q) const
         -> decltype(data.B.predict(0))
    {
        // Now predict the values for the day immediately after the training
        // period, that's the day (t + N_train)
        size_t N = data.getN();
        const auto& A = data.A;
        const auto& B = data.B;
        typedef decltype(B.predict(0)) TO;
        static_assert( std::is_same<TO,double>::value, "only for floats..");
        TO O_next = 0.0;
        for(size_t j=0;j<N;++j) {
            TO O_here = A(q,j) * B.predict(j);
            O_next += O_here;
        }
        return O_next;
    }
	
    /////////////////////////////////////////////////////////////////////////
    /// Calculate the probability that the next observation is >= yref
    /////////////////////////////////////////////////////////////////////////
    template<class BJ, class TO>
    double calcPref(const HMM_data<BJ>& data, size_t q, TO yref) const
    {
        // Now predict the values for the day immediately after the training
        // period, that's the day (t + N_train)
        size_t N = data.getN();
        const auto& A = data.A;
        const auto& B = data.B;
        std::vector<double> xx(N);
        double p = 0.0;
        for(size_t j=0;j<N;++j) {
            double px = A(q,j) * B.calcPref(j, yref);
            p += px;
        }
        return p;
    }

    /////////////////////////////////////////////////////////////////////////
    /// Calculate the probability that the next observation is positive
    /////////////////////////////////////////////////////////////////////////
    template<class BJ>
    double calcPpos(const HMM_data<BJ>& data, size_t q) const
    {
        typedef decltype(data.B.predict(0)) TO;
        return calcPref(data, q, static_cast<TO>(0) );
    }
};

/// //////////////////////////////////////////////////////////////////////////
/// HMM OPTIMIZATION FUNCTIONS
/// Optimization functions perform multiple runs of BW
/// //////////////////////////////////////////////////////////////////////////
/// monte carlo optimization
template<class BB, class dataType, class HmmRandomizer>
size_t trainAndTrialHMM(HMM_data<BB>& data,
	                    size_t N,size_t M,
                        const std::vector<dataType>& O,
                        double eps, size_t nsteps, 
                        size_t conv_size, size_t ntrials,
                        double& like,
                        HmmRandomizer bRand)
{
    std::vector<double> optimLog;
	optimLog.reserve(ntrials);
	like = -1e100;
    HMM_data<BB> hmm0;
    generateRandomHMM(N,M,hmm0);
	bRand(hmm0,O,N,M);
    size_t ncount = 0;
    //monte-carlo fashion trials
    for(size_t trial=0; trial < ntrials; ++trial) {     
        HMM_data<BB> hmm1;
        generateRandomHMM(N,M,hmm1);      
		bRand(hmm1,O,N,M);
		HMM_engine engine;
        double PO1 = 0.0;
        try {
            PO1 = engine.train(hmm1, O, eps, nsteps, conv_size);
        } catch (...) {
            continue;
        }
		if (PO1 > like) {
            like = PO1;
            hmm0 = hmm1;
        }
        optimLog.push_back(like);
        ++ncount;
    }
    data=hmm0;
    return ncount;
}

template<class Model>
struct Likelihooder
{
    template<class dType>
    double operator()(const Model& mod, const std::vector<dType>& O)
    {
        HMM::Viterbi viter(mod, O);
	    return calc_PO(viter);
    }
};

template<class Model>
struct Qcalcer
{
    template<class dType>
    const std::vector<double>& operator()(const Model& hmm, 
                                          const std::vector<dType>& O_train)
    {
        // calcualte the probability distribution for the last state q_T
        HMM::Viterbi viter(hmm, O_train);
        size_t N = hmm.getN();
        size_t T = O_train.size();
        alphaTT.clear();
        alphaTT.resize(N);
        for(auto i=0;i<N;++i) alphaTT[i]=viter.alpha_x(i,T-1);
        assert(abs(sum(alphaTT.begin(), alphaTT.end()) - 1.0) < 1e-13 );
	    //double like = calc_PO(viter);
        return alphaTT;
    }
    std::vector<double> alphaTT;
};



} // end of namespace HMM
#endif
