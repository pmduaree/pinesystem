#ifndef __HMM_DISCRETE__
#define __HMM_DISCRETE__
// this hsould be a representation for the discrete case
//  B_j(o) = B_{j,o}    where o are integers {0,1,..,M-1}

#include <iostream>
#include <cmath>
#include <cassert>
#include <vector>
#include <string>
#include "hmm_common.hpp"
#include "hmm_baumwelch.hpp"
#include "hmm_random.hpp"

namespace HMM {

class Bjdiscrete: public HMM_B
{
  public:
    morana::tensor<double,2> B; // matrix B(j,o)
    size_t N() const __attribute__((deprecated)) { return B.M(0); }
    size_t M() const __attribute__((deprecated)) { return B.M(1); }
    size_t getN() const { return B.M(0); }
    size_t getM() const { return B.M(1); }
	std::string bType ="'BjDiscrete::Dicrete'";
    
    Bjdiscrete(): HMM_B() {}

	//dummy constructor
	Bjdiscrete(size_t N,size_t M): HMM_B()
    {
        B.resize(morana::make_array<size_t,2>(N,M));
	}
    
    Bjdiscrete& operator=(const Bjdiscrete& x)
    {
        B = x.B;
        return *this;
    }

    /// Calculate the probability B_j(o)
    double operator()(size_t O, size_t j) const
    {
		assert(O>=0 && O<=getM());
		return B(j,O);
    }

    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(size_t O, double* Bo) const
    {
        size_t N = getN();
        for(size_t i=0;i<N;++i) {
            Bo[i] = operator()(O, i);
        }
    }
    
    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(size_t O, std::vector<double>& Bo)
    {
        Bo.resize(getN());
        operator()(O, Bo.data());
    }

    size_t predict(size_t q) const
    {
        // what's the mostly likely outcome?
        size_t w = 0;
        double p = B(q,0);
        size_t M = getM();
        for(size_t j=1;j<M;++j) {
            if (B(q,j) > p) {
                w = j;
                p = B(q,j);
            }
        }
        return w;
    }

    double calcPref(size_t q, size_t yref) const
    {
        size_t M = getM();
        double x = 0.0;
        // that it's larger than yref?
        for(size_t j=yref+1;j<M;++j)
            x += B(q,j);
        return x;
    }
};

template<>
struct BW_B<Bjdiscrete>
{
    void operator()(Bjdiscrete& B, const Viterbi& viterbi,
                    const std::vector<size_t>& O)
    {
        size_t N = B.getN();
        size_t M = B.getM();
        size_t T = O.size();

        auto NM = morana::make_array<size_t,2>(N,M);
        morana::tensor<double, 2> B_2(NM);
        std::vector<double> cumg(N);
        for(auto& b : B_2) b = 0.0;
        for(auto& b : cumg) b = 0.0;
        for(size_t t=0;t<T;++t) {
            blas::axpy(N, 1.0, &viterbi.gjt()(0,t), 1, &B_2(0,O[t]), 1);
            blas::axpy(N, 1.0, &viterbi.gjt()(0,t), 1, &cumg[0], 1);
        }
        for(size_t j=0;j<N;++j) {
            if (cumg[j] > 1e-10) {
                blas::scal(M, 1.0/cumg[j], &B_2(j,0), N);
            } else {
                // RESET!!!!
                std::vector<double> z(M);
				make_stochastic_vector(z);
                for(size_t m=0;m<M;++m)
                    B_2(j,m) = z[m];
            }
#ifndef NDEBUG
            // check if it's still stochastic
            double check = 0.0;
            for(size_t m=0;m<M;++m) check += B_2(j,m);
            assert( fabs(check - 1.0) < 1e-12);
#endif
        }
        B.B = B_2;
    }
};


template<>
class RandomHMM_B<Bjdiscrete>
{
  public:
   void operator()(size_t N, size_t M, Bjdiscrete& B)
    {
		B.B.resize(morana::make_array<size_t,2>(N,M));
        make_stochastic_tensor(B.B, 0);
    }
};

    void outPutHMM(const HMM_data<Bjdiscrete> A,std::string st=""){
}



void predict_discrete(const HMM_data<Bjdiscrete>& data, size_t q, 
                      std::vector<double>& Pm)
{
    size_t N = data.getN(), M = data.B.getM();
    // what's the most probable next one?
    Pm.resize(M);
    for(auto& p: Pm) p = 0.0;
    // P(m) = \sum_j A(q,j) B(j,m)
    for(size_t m=0;m<M;++m) {
        for(size_t j=0;j<N;++j)
            Pm[m] += data.A(q,j) * data.B.B(j,m);
    }
}

size_t predict_discrete(const HMM_data<Bjdiscrete>& data, size_t q)
{
    std::vector<double> Pm;
    predict_discrete(data, q, Pm);
    size_t O_next = std::max_element(Pm.begin(), Pm.end()) - Pm.begin();
    return O_next;
}

} // end of namespace HMM

#endif
