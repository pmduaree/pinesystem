#ifndef __HMM_SIMULATOR__
#define __HMM_SIMULATOR__
/////////////////////////////////////////////////////////////////////////////
// HMM SIMULATOR
//
//
// This is a wrapper class. It takes a time series, trains a coupled of HMMs
// on it, and uses that to predict future values.
/////////////////////////////////////////////////////////////////////////////
#include "util/tiempo.hpp"
#include "util/stringparpars.hpp"
#include "hmm_parameters.hpp"

namespace HMM {

/// add elements of v2 to v1 inline 
struct train_slice
{
    int signal;
    double ratio_pos;
    double exval;
    double cum_prob_negative;
    std::vector<double> qdist, gmmfactors;
    train_slice()
        : signal(0), ratio_pos(0.5), exval(0.0), cum_prob_negative(0.5)
    {}
};



// called from simulate.hpp and signallers.hpp
bool run_predictor(const std::vector<double>& oTrain, 
                   const HMM_sim_param& params, train_slice& slice);


}



#endif
