#ifndef __HMM_COMMON__
#define __HMM_COMMON__

#include <cstdlib>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include <type_traits>
#include <vector>
#include <cstdlib>
#include <array>

namespace HMM {


// this is an abstract class for B object in the HMM
class HMM_B {};


/// Check if x is not a number (iff x != x).
inline bool is_nan(double x)
{
    return x != x;
}

/// Check if any element in X is not a number
template<class C>
inline bool is_nan(const C& X)
{
    for(const auto& x : X)
        if (is_nan(x)) return true;
    return false;
}

/// Sum over a range from beg (inclusive) to end (exclusive)
template<class Iterator>
auto sum(const Iterator beg, const Iterator end) 
    -> typename std::remove_reference<decltype(*beg)>::type
{
    typedef typename std::remove_reference<decltype(*beg)>::type const_Type;
    typedef typename std::remove_const<const_Type>::type Type;
    static_assert(std::is_integral<Type>::value || std::is_floating_point<Type>::value, 
                   "must be either integral of floating point type");
    Type v = static_cast<Type>(0);
    for(Iterator it = beg; it != end; ++it) v += (*it);
    return v;
}


inline
double dec_round(double x, unsigned int n)
{
    return pow(0.1, n) * round(x * pow(10.0, n));
}

inline double gauss_log(double x, double mu, double var)
{
    return -0.5 * log(2*M_PI*var) - (x-mu)*(x-mu)/(2*var);
}

inline double gauss(double x, double mu, double var)
{
    return exp(gauss_log(x,mu,var));
}


}
#endif
