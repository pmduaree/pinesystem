#ifndef GMM_HPP
#define GMM_HPP


#include <vector>
#include <functional>


namespace HMM {

/**
    Assume: vector of values to fit on is discretized (finite resolution) : 
    minimum variance is defined as minimum distance between 
    non-equal points in x
**/
class ExpectationMaximizationGmm {
    // settings:
    size_t M; // number of gaussians
    
  public:
    // status:
    std::vector<double> logL; // log likelihood values - the performance measure
    
    // results:
    typedef std::function<double&(size_t index)> vectorAccess;
    vectorAccess mean, variance, alpha;
    
    ExpectationMaximizationGmm();
    
    void init(size_t nM, vectorAccess m, vectorAccess v, vectorAccess a);

    double logLikelihood(const std::vector<double>& x);
    void perform(const std::vector<double>& x, size_t maxNumSteps = 100, double likelihoodConvergenceFactor = 0.00001);
    void initialGuess(const std::vector<double>& x);
};
    

}

#endif //GMM_HPP
