#ifndef __HMM_SIMPLEGAUSS__
#define __HMM_SIMPLEGAUSS__
/////////////////////////////////////////////////////////////////////////////
/// this is similar to hmm_gmm but here we only have 'M' gaussians
/// and not N x M as in hmm_gmm. 
/////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include "hmm_statistics.hpp"
#include "hmm_common.hpp"
#include "hmm_baumwelch.hpp"
#include "hmm_random.hpp"

namespace HMM {

class Bjsg: public HMM_B
{ 
  public:
    std::vector<double> U;
    std::vector<double> mu;
    morana::tensor<double,2> C;
	std::string bType ="'Bjsg::simple gauss';";
	
    size_t getN() const { return C.M(0); }
    size_t getM() const { return C.M(1); }
    
    Bjsg(): HMM_B() {}

    Bjsg(size_t N, size_t M): HMM_B()
    {
        U.resize(M);
        mu.resize(M);
        C.resize(morana::make_array<size_t,2>(M,N));
    }

    Bjsg& operator=(const Bjsg& x)
    {
        C = x.C;
        mu = x.mu;
        U = x.U;
        return *this;
    }
    
    /// Calculate the partial probability density
    ///  $c_{j,k} P(O | mu_{k}, U_{k})$
    double operator()(double O, size_t j, size_t k) const
    {
        double c = C(j,k);
        double m = mu[k];
        double u = U[k];
        if (u < 1e-100) {
            throw std::runtime_error("hmm_simplegauss error: u ~ 0");
        }
        double v= c * gauss(O, m, u);
        if (is_nan(v)) {
            //std::cerr << "error in simplegauss: v=NaN; c=" << c << "; m=" << m << "; u=" << u << "\n";
            //throw std::runtime_error("error in simple gauss");
            return m == O ? c*1e100 : 0;
        }
        return v;
    }
    
    /// Calculate the probability density B_j(o)
    double operator()(double O, size_t j) const
    {
        size_t M = getM();
        double v = 0.0;
        for(size_t k=0;k<M;++k) {
            v += operator()(O, j, k);
        }
        return v;
    }
    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(double O, double* Bo) const
    {
        size_t N = getN();
        for(size_t i=0;i<N;++i) {
            Bo[i] = operator()(O, i);
        }
    }
    
    /// Calculate the array B_j(o) for j=1,2,...,N
    void operator()(double O, std::vector<double>& Bo)
    {
        Bo.resize(getN());
        operator()(O, Bo.data());
    }

    double predict(size_t q) const
    {
        size_t M = getM();
        double O_p = 0.0;
        for(size_t m=0;m<M;++m) {
            double o = C(q,m) * mu[m];
            O_p += o;
        }
        return O_p;
    }


    double calcPref(size_t q, double yref) const
    {
        size_t M = getM();
        double x = 0.0;
        for(size_t k=0;k<M;++k) {
            double p = (U[k]?  0.5 * std::erfc( (yref - mu[k])/(sqrt(2*U[k]))) :
                        (mu[k]> yref ? 1.0 : 0.0) );
            x += C(q,k) * p;
        }
        return x;
    }
	
	//sets B
     void setB(size_t N, size_t M, const std::vector<double>& iC,
              const std::vector<double>& imu, const std::vector<double>& iU)
    {
        auto NM = morana::make_array<size_t,2>(N,M);
        C.resize(NM);
        for(size_t k=0;k<M;++k) {
            std::fill(&C(0,k), &C(0,k)+N, iC[k]);
        }
		mu=imu;
        U=iU;
	}
};


template<>
struct BW_B<Bjsg>
{
    void operator()(Bjsg& B, const Viterbi& viterbi,
                    const std::vector<double>& O)
    {
        size_t N = B.getN();
        size_t M = B.getM();
        size_t T = O.size();
        // requires: gj, gjkt, O, eta
        assert( !is_nan(B.C) );
        assert( !is_nan(B.mu) );
        assert( !is_nan(B.U) );

        std::vector<double> eta(T);
        for(auto& e : eta) e = 1.0;
        
        // the gamma_{t}(j,k) are defined in a non-number equation following 
        // equation (54) in Rabiner.
        // we can write:
        //   gamma_t(j,k) = gamma_j(t) * W_t(j,k) / sum_m W_t(j,m)
        // where 
        //   W_t(j,k) = c_{j,k} g(Ot,mu_{jk}, U_{j,k}) 
        auto NMT = morana::make_array<size_t,3>(N,M,T);
        morana::tensor<double,3> gjkt(NMT);
        std::fill(gjkt.begin(), gjkt.end(), 0.0);
        
        for(size_t t=0;t<T;++t)
        for(size_t j=0;j<N;++j) {
            std::vector<double> Wjk(M);
            double vk = 0.0;
            for(size_t k=0;k<M;++k)  {
                Wjk[k] = B(O[t], j, k);
                vk += Wjk[k];
            }
            blas::scal(M, (vk>1e-16 ? 1.0/vk : 0.0), &Wjk[0], 1);
            blas::axpy(M, viterbi.gjt(j,t), &Wjk[0],1,&gjkt(j,0,t),N);
        }
        assert( !is_nan(gjkt));
        
        // g(j) = \sum_t g_t(j)
        std::vector<double> gj(N);
        blas::gemv("N", N, T, 1.0, viterbi.gjt().data(), N, &eta[0], 1, 
             0.0, &gj[0], 1);
        assert( !is_nan(gj));

        // c(j,k) = \sum_t \gamma_t(j,k) / \sum_t \sum_k \gamma_t(j,k)
        //        = \gamma(j,k) / gamma(j)
        for(size_t j=0;j<N;++j) {
            if (gj[j] < 1e-16) {
                for(size_t k=0;k<M;++k) B.C(j,k) = 0.0;
                continue;
            }
            
            double cs = 0.0;
            for(size_t k=0;k<M;++k) {
                double r = blas::dot(T, &gjkt(j,k,0), gjkt.M(0)*gjkt.M(1), &eta[0], 1);
                B.C(j,k) = (r<1e-16 ? 0.0 : r);
                cs += B.C(j,k);
            }
            blas::scal(M, (cs<1e-16?0.0:1.0/cs), &B.C(j,0), N);
        }
        // obtain a new 
        //      U_k = \sum_t \sum_j \gamma_t(j,k) (Ot-mu_k)^2 / 
        //            \sum_t \sum_j \gamma_t(j,k)
        // and then also 
        //      mu_k = \sum_j \sum_t \gamma_t(j,k) O_t / 
        //             \sum_j \sum_t \gamma_t(j,k)
        //double mean_O = mean(O);
        double var_O = variance(O);
        for(size_t k=0;k<M;++k) {
            double v1=0.0, v2=0.0, v3=0.0;
            for(size_t j=0;j<N;++j) {
            for(size_t t=0;t<T;++t) {
                double g = gjkt(j,k,t) * eta[t];
                v1 += g;
                v2 += g * O[t];
                v3 += g * (O[t] - B.mu[k])*(O[t] - B.mu[k]);
            }
            }
            if (v1 < 1e-16) {
                // this means that this component is irrelevant.
                // we better set it to something else.
                B.mu[k] = mean(O);
                B.U[k] = variance(O);
            } else {
                assert( v1 > 1e-16);
                B.U[k] = v3 / v1;
                B.mu[k] = v2 / v1;
            }
            B.U[k] = std::max(B.U[k], std::max(var_O,0.0001)/O.size());
            assert( B.U[k] > 0);
        }
    }
};

template<>
class RandomHMM_B<Bjsg>
{
  public:
    void operator()(size_t N, size_t M, Bjsg& B)
    {
        B.mu.resize(M);
        B.U.resize(M);
        B.C.resize(morana::make_array<size_t,2>(N,M));
        make_stochastic_tensor(B.C, 0);
		
        std::uniform_real_distribution<double> flatd(-1.0,1.0);
		std::normal_distribution<double> nD(2.0/M, 1.0/M);
		//std::uniform_real_distribution<double> nD(2.0/M,2.0);
		
        for(auto& z : B.mu) z = flatd(randomGenerator());
        for(auto& u : B.U) u = std::pow(nD(randomGenerator()),2.0);
		
        std::uniform_real_distribution<double> flatpos(0.0,1.0);
        std::uniform_real_distribution<double> flatneg(-1.0,0.0);
        B.mu.front() = flatpos(randomGenerator());
        B.mu.back() = flatneg(randomGenerator());
    }
};


} // end of HMM


#endif
