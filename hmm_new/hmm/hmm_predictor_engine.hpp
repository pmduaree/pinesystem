#ifndef __PREDICTOR_ENGINE__
#define __PREDICTOR_ENGINE__
#include <map>
#include <string>


#include "hmm_data.hpp"
#include "hmm_random.hpp"
#include "hmm_discrete.hpp"
#include "hmm_simplegauss.hpp"
#include "hmm_gmm.hpp"
#include "hmm_globalrestrictedgauss.hpp"

#include "hmm.hpp"
#include "hmm_discretengine.hpp"


namespace HMM {


enum class randomization_type
{
    invalid, null, flat, gmm, kmeans, fixed4, fixed4gmm 
};

class randomization_type_struct
{
  public:
    randomization_type_struct(const std::string& rtypeparam)
    {
        init_legend();
        // is there a parameter as well?
        if (rtypeparam.find('@') != std::string::npos) {
            size_t p = rtypeparam.find('@');
            rtype_str = rtypeparam.substr(0, p);
            param_str = rtypeparam.substr(p+1);
        } else {
            rtype_str = rtypeparam;
            param_str = "";
        }
    }
    randomization_type type() const
    {
        if (Legend.count(rtype_str))
            return Legend.find(rtype_str)->second;
        else
            return randomization_type::invalid;
    }

    std::string param() const { return param_str; }
  private:
    void init_legend()
    {
        Legend.clear();
        Legend["gmm"] = randomization_type::gmm;
        Legend["kmeans"] = randomization_type::kmeans;
        Legend["flat"] = randomization_type::flat;
        Legend["fixed4"] = randomization_type::fixed4;
        Legend["fixed4gmm"] = randomization_type::fixed4gmm;
        Legend["null"] = randomization_type::null;
    }
    std::string rtype_str;
    std::string param_str;
    std::map<std::string, randomization_type> Legend;
};




/**
 * The class to extend, providing uniform functions train and PDFCDF
 **/
struct PredEngine {
    PredEngine() 
    {
    }
    virtual ~PredEngine() 
    {
        //std::cout << "Calling the destrcutor [predengine"<<this<<"]..." << std::endl;
    }
    virtual void train(const HMM_sim_param& params, 
                       const std::vector<double>& O_train) = 0;
    virtual void calcPDFCDF(const std::vector<double>& qdist, 
                            const std::vector<double>& rangeO, 
                            std::vector<double>& PDF, 
                            std::vector<double>& CDF) = 0;
    virtual void calc_probs_you_MF(const std::vector<double>& pts, 
                                   std::vector<double>& PDF,
                                   std::vector<double>& CDF)=0;
    virtual PredEngine& operator=(const PredEngine& e)
    {
        //std::cout << "copy PredEngine: " << &e << "-> " << this << std::endl;
        alphaTT = e.alphaTT;
        return *this;
    }
    
    virtual double likelihoodEstimation() const = 0;
    
    virtual void getGmmFactors(std::vector<double>& factors) const = 0;

    std::vector<double> alphaTT;
};


template<class B>
class PredEngineHMM : public PredEngine 
{
    typedef HMM_data<B> Hmm;
    Hmm hmm;
    double likelihood;
    
  public:
    std::shared_ptr<discretizationEngine<double, size_t> > discretization;
    
  public:
    PredEngineHMM()
    {
    }

    PredEngineHMM(const PredEngineHMM& e)
    {
        //std::cout << "Copy constructor rpedenginehmm."<<std::endl;
        hmm = e.hmm;
        likelihood = e.likelihood;
    }
    
    ~PredEngineHMM()
    {
        //std::cout << "Calling the destrcutor [predengineHMM="<<this<<"]..." << std::endl;
    }
    
    HMM_data<B>& getHmm() { return hmm; }
    const HMM_data<B>& getHmm() const { return hmm; }
    
    virtual PredEngineHMM& operator=(const PredEngineHMM& e)
    {
        //std::cout << "Copy PredEngineHMM: " << &e << "->"<<this<<std::endl;
        alphaTT = e.alphaTT;
        hmm = e.hmm;
        likelihood = e.likelihood;
        discretization = e.discretization;
        return *this;
    }
    
    void train(const HMM_sim_param& params, const std::vector<double>& O_train)
    {
        std::function<void(HMM_data<B>&, const std::vector<double>&, 
                           size_t, size_t)> pbFunc;
        randomization_type_struct rand_mod(params.random_type);
        auto rmi = rand_mod.type();
        
		// This guy (opposed to other pb functions) must be an object since 
        // it has to remember two parameters
        pbFixed4<Hmm> pbFixed4Obj(rand_mod.param());
        
        if (rmi == randomization_type::gmm)
            pbFunc = &pbGmm<Hmm>;
        else if (rmi == randomization_type::null)
            pbFunc = &pbNull<Hmm>;
        else if (rmi == randomization_type::kmeans)
            pbFunc = &pbKmeans<Hmm>;
        else if (rmi == randomization_type::flat)
            pbFunc = &pbUniform<Hmm>;
        else if (rmi == randomization_type::fixed4)
            pbFunc = pbFixed4Obj;
        else if (rmi == randomization_type::fixed4gmm)
            pbFunc = &pbFixed4Gmm<Hmm>;
        else 
            throw std::runtime_error("Error in PredEngineHMM<...>::train: provided an invalid randomization method");
        size_t nsuc = trainAndTrialHMM(hmm, params.N, params.M, O_train,
                                      params.bwConvergenceCheckEps, 
                                      params.numBwSteps, 
                                      params.bwConvergenceCheckLen, 
                                      params.numMcRuns, likelihood, pbFunc);
        if (!nsuc) throw std::runtime_error("no data");
        // calcualte the probability distribution for the last state q_T
        HMM::Viterbi viter(hmm, O_train);
        size_t N = hmm.getN();
        size_t T = O_train.size();
        alphaTT.clear();
        alphaTT.resize(N);
        for(auto i=0;i<N;++i) alphaTT[i]=viter.alpha_x(i,T-1);
        assert(abs(sum(alphaTT.begin(), alphaTT.end()) - 1.0) < 1e-13 );
        //std::cout << "TRAIN["<<this<<","<<&hmm<<":"<<hmm.A << std::endl;
    }
    
    void calc_probs_you_MF(const std::vector<double>& pts, 
                           std::vector<double>& PDF, std::vector<double>& CDF)
    {
        pdfcdf(getHmm(), alphaTT, pts, PDF, CDF);
        assert( PDF.size()== pts.size() -1);
        assert( CDF.size()== pts.size());
    }
    
    void calcPDFCDF(const std::vector<double>& qdist, 
                    const std::vector<double>& rangeO, 
                    std::vector<double>& PDF, 
                    std::vector<double>& CDF)
    {
        if (std::is_same<Bjdiscrete,B>::value) {
            std::cout << "Cant use this with the discrete model." << std::endl;
            throw std::runtime_error("werrrr");
        }
        //std::cout << "Calling from " << this << " with " << &getHmm()<<std::endl;
        pdfcdf(getHmm(), qdist, rangeO, PDF, CDF);
    }
    
    double likelihoodEstimation() const { return likelihood; }
    
    void getGmmFactors(std::vector<double>& factors) const {
		factors.resize(hmm.getM(), -1);
		for (size_t jj = 0; jj<hmm.getM(); ++jj) {
			factors[jj] = 0;
			for (size_t ii = 0; ii<alphaTT.size(); ++ii) {
				factors[jj] += alphaTT[ii]*hmm.B.C(ii,jj);
			}
		}
		//prn(alphaTT);
		//prn(hmm.B.C);
		//prn(factors);
	}
    
  private:
    PDFCDF<HMM_data<B> > pdfcdf;
};
    

template<>
void PredEngineHMM<Bjdiscrete>::calc_probs_you_MF(
                                                const std::vector<double>& pts, 
                                                std::vector<double>& PDF, 
                                                std::vector<double>& CDF)
{
    assert( false );
}

template<>
void PredEngineHMM<Bjdiscrete>::getGmmFactors(std::vector<double>& factors) const {
	throw std::runtime_error("Can't get gauss factors for discrete model, chief.");
}
	
	
// specialized functions for the discrete gauss
template<>
void PredEngineHMM<Bjdiscrete>::train(const HMM_sim_param& params, 
                                      const std::vector<double>& O_train)
{
    discretization.reset(new discretizationEngine<double, size_t>(O_train, params.M));
    std::function<void(HMM_data<Bjdiscrete>&, const std::vector<size_t>&, size_t, size_t)> bRand;
    randomization_type_struct rand_mod(params.random_type);
    auto rmi = rand_mod.type();
    if (rmi == randomization_type::gmm) 
        bRand = &pbDiscreteNormal<HMM_data<Bjdiscrete>, size_t>;
    else if (rmi == randomization_type::null)
        bRand = &pbNull<HMM_data<Bjdiscrete>, size_t>;
    else
        throw std::runtime_error("Error in PredEngineHMM<Bjdiscrete>::train: provided an invalid randomization method");
    size_t nsuc = trainAndTrialHMM(hmm, params.N, params.M, 
                                    discretization->dData, 
                                    params.bwConvergenceCheckEps, 
                                    params.numBwSteps,
                                    params.bwConvergenceCheckLen, 
                                    params.numMcRuns, likelihood, bRand);
    if (!nsuc) {
        std::cerr << "ERROR: Nsuc = 0 in train...\n";
    }
    assert( nsuc);
    // calcualte the probability distribution for the last state q_T
    const auto& D_train = discretization->dData;
    HMM::Viterbi viter(hmm, D_train);
    size_t N = hmm.getN();
    size_t T = D_train.size();
    alphaTT.clear();
    alphaTT.resize(N);
    for(size_t i=0;i<N;++i) alphaTT[i]=viter.alpha_x(i,T-1);
    assert(abs(sum(alphaTT.begin(), alphaTT.end()) - 1.0) < 1e-13 );
}

}



#endif
