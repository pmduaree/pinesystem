#ifndef __KMEAN_ENGINE__
#define __KMEAN_ENGINE__

#include <vector>
#include "hmm_common.hpp"
#include "hmm_statistics.hpp"

namespace HMM {

static const size_t Kmean_trials = 2;

// 
// Cluster the data in X into K clusters such that the cluster distance is 
// minimized.
// The algorithm works as follows:
//     1. Partition data into 'K' clusters. If there exists some cluster
//        which is empty, then re-do the clustering.
//     2. Calculate means of clusters
//     3. Go through all elements and associated them to the nearest cluster,
//        that means to the cluster with a nearest mean.
//     4. Iterate.
// Finally we end up with a local minimum for the cluster distance.
// Therefore we do ten iterations to improve the chances of ending up 
// in the global minimum. But it doesn't matter that much really.
//

////////////////////////////////////////////////////////////////////////////
/// \class Kmean_engine
/// \brief Assign values to clusters minimizing the intracluster distance
/// 
/// This class implements the K-mean clustering method. 
/// A set of values {v_1,v_2,...,v_N} is assigned to K clusters, such that
/// the intracluster distance which is the distance to the cluster mean,
///
///    $d(C) = \sum_{c \in C} \sum_{x \in c} d(x, mean(c)) $
///
/// is minimized. For hte distance we typically take the euclid distance
///    $d(x,y) = (x-y)*(x-y)$
///
////////////////////////////////////////////////////////////////////////////
class Kmean_engine
{
  public:
    typedef std::vector<double> Vec;
    typedef std::vector<Vec> Cluster;
    
    /// \brief Make a random cluster partition.
    /// 
    /// Make a random parition in K clusters, no matter what the 
    /// intracluster distance is.
    void random_partition(size_t n, 		//!< Number of values
                          const double* X,  //!< Vector of values
                          size_t K,         //!< Number of cluster
                          Cluster& C        //!< Vector of clusters
                          )
    {
        C.clear();
        C.resize(K);
        for(auto it = C.begin(); it != C.end(); ++it) it->clear();
        for(size_t t=0;t<n;++t) {
            size_t c = lrand48() % K;
            assert( c >= 0 && c < K);
            C[c].push_back(X[t]);
        }
    }
    
    /// \brief Make a sorted cluster partition.
    /// 
    /// Sort values and make a partition into K sets
    void sorted_partition(size_t n, //!< Number of values
                          const double* X,  //!< Vector of values
                          size_t K,         //!< Number of cluster
                          Cluster& C   //!< Vector of clusters
                          )
    {
        std::vector<double> sortedX(X, X+n);
        std::sort(sortedX.begin(), sortedX.end() );
        
        C.clear();
        C.resize(K);
        
        auto lessthantwo = [](const Vec& s) { return s.size()<2; };
        int n_fail = 0;
        while (++n_fail < 100 && std::count_if(C.begin(), C.end(), lessthantwo)) {
            std::vector<double> vX(sortedX);
            for(auto it = C.begin(); it != C.end(); ++it) it->clear();
            // get cluster sizes.
            std::vector<size_t> sxzn(K);
            std::fill(sxzn.begin(), sxzn.end(), std::max<size_t>(0,n/K-1));
            while (sum(sxzn.begin(), sxzn.end()) < n) {
                // select one and increase it.
                size_t c = lrand48() % K;
                ++(sxzn[c]);
            }
            assert( sum(sxzn.begin(), sxzn.end()) == n );
            // now fill it.
            for(size_t k=0;k<K;++k) {
                size_t kk = K-1-k;
                for(size_t i=0;i<sxzn[kk];++i) {
                    C[kk].push_back(vX.back());
                    vX.pop_back();
                }
                assert( C[kk].size() == sxzn[kk] );
            }
        }
    }

    /// \brief Calculate the intra-cluster distance.
    ///
    /// The intracluster distance is defined as
    ///    d(C) = \sum_{c \in C} \sum_{x \in c} d(x, mean(c)) 
    double cluster_distance(const Cluster& C)
    {
        double d = 0.0;
        for(auto c = C.begin(); c != C.end(); ++c) {
            std::vector<double> x(*c);
            double m = mean(x);
            for(auto it = x.begin(); it != x.end(); ++it)
                d += ((*it)-m)*((*it)-m);
        }
        return sqrt(d);
    }
    
    void operator()(const std::vector<double>& X, //!< Array of values
                    size_t K,   //!< Number of clusters
                    std::vector<double>& p_k,   //!< Occupancy rates
                    std::vector<double>& m_k,   //!< Cluster mean values
                    std::vector<double>& U_k,   //!< Cluster variances
                    bool trivial=false          //!< Trivial distribution
                    )
    {
        operator()(X.size(), X.data(), K, p_k, m_k, U_k, trivial);
    }
    
    /// \brief Partition values to clusters and calculate cluster properties
    ///
    /// Distribute $n$ values in X into $K$ clusters such that intracluster
    /// distance is minimized. Once done, calculate the means and variances
    /// of the clusters, as well as the occupancy rate.
    void operator()(size_t n,   //!< Number of values
                    const double* X,    //!< Array of values
                    size_t K,   //!< Number of clusters
                    std::vector<double>& p_k,   //!< Occupancy rates- defines gauss coef
                    std::vector<double>& m_k,   //!< Cluster mean values - define gauss mean
                    std::vector<double>& U_k,   //!< Cluster variances - define gauss mi
                    bool trivial=false          //!< Trivial distribution
                    )
    {
        Cluster cluster;
        if (trivial)
            find_trivial(n, X, K, cluster);
        else
            find_cluster(n, X, K, cluster);
        //std::cout << "Clusterf ound." << std::endl;
        p_k.resize(K);
        m_k.resize(K);
        U_k.resize(K);
        for(size_t k=0;k<K;++k) {
            p_k[k] = cluster[k].size() * 1.0 / n;
            m_k[k] = mean(cluster[k]);
            U_k[k] = 0.0;
            for(auto it = cluster[k].begin(); it != cluster[k].end(); ++it) {
                U_k[k] += pow((*it)-m_k[k], 2);
            }
            U_k[k] *= 1.0 / cluster[k].size();
            assert( U_k[k] > 0);
        }
    }
    /// \brief Distribute values into K clusters, fill them in the order.
    ///
    /// We make sure that all the clusters are of similar size. And we 
    /// fill the values to the clusters as they come in the array.
    /// NO reshuffling is made.
    void find_trivial(size_t n, const double* X, size_t K, Cluster& cluster)
    {
        std::vector<double> vX(n);
        std::copy(X, X + n, vX.begin());
        cluster = Cluster(K);
        // determine cluster sizes:
        std::vector<size_t> sxzn(K);
        std::fill(sxzn.begin(), sxzn.end(), n/K);
        while (sum(sxzn.begin(), sxzn.end()) < n) {
            // select one and increase it.
            size_t c = lrand48() % K;
            ++(sxzn[c]);
        }
        assert( sum(sxzn.begin(), sxzn.end()) == n );
        size_t c = 0;
        for(auto it = vX.begin(); it != vX.end(); ++it) {
            while(cluster[c].size() >= sxzn[c]) ++c;
            assert( c < K );
            cluster[c].push_back(*it);
        }
    }

    void find_one_clustering(size_t n, const double* X, size_t K, Cluster& C)
    {
        C.clear();
        C.resize(K);
        auto lessthantwo = [](const Vec& s) { return s.size()<2; };
        std::vector<double> means(K);
        int nnn = 0;
        while (++nnn < 100 && std::count_if(C.begin(), C.end(), lessthantwo)) {
            random_partition(n, X, K, C);
        }
        if (nnn>=100) throw std::runtime_error("cant find partition");
        //print 'Clusters: ', map(len, C)
        size_t niters = 0;
        while(++niters < 1000) {
            // calculate the means:
            for(size_t i=0;i<K;++i) 
                means[i] = mean(C[i]);
            size_t n_changes = 0;
            Cluster C2(K);
            for(size_t c=0;c<K;++c) {
                const Vec& Cc = C[c];
                for(size_t i=0;i<Cc.size();++i) {
                    std::vector<double> dd(means);
                    for(auto it = dd.begin(); it != dd.end(); ++it)
                        *it = fabs( (*it) - Cc[i] );
                    // what's the minimal distance?
                    size_t new_pos = std::min_element(dd.begin(), dd.end()) - dd.begin();
                    assert( new_pos >= 0 && new_pos < K);
                    if (new_pos != c) ++n_changes;
                    C2[new_pos].push_back(Cc[i]);
                }
            }
            if (n_changes == 0) break;
            C.swap(C2);
            int nn = 0;
            while (++nn < 100 && std::count_if(C.begin(), C.end(), lessthantwo)) {
                random_partition(n, X, K, C);
            }
            if (nn>=100) throw std::runtime_error("cant find partition");
        }
    }


    
    /// \brief Partition values to clusters
    ///
    /// Distribute $n$ values in X into $K$ clusters such that intracluster
    /// distance is minimized. Rerun the algorithm several times to get 
    /// the best partitioning.
    double find_cluster(size_t n, const double* X, size_t K, Cluster& best_cluster)
    {
        size_t n_trials = Kmean_trials;
        sorted_partition(n, X, K, best_cluster);
        double my_distance = cluster_distance(best_cluster);
        for(size_t iiter = 0; iiter < n_trials; ++iiter) {
            Cluster C;
            find_one_clustering(n, X, K, C);
            double this_distance = cluster_distance(C);
            if (this_distance < my_distance) {
                my_distance = this_distance;
                best_cluster = C;
            }
        }
        return my_distance;
    }
};

template<class CC>
void kmeansInitHMM(CC& hmm1, const std::vector<double>& O, 
                   size_t N, size_t M)
{
    //static bool firstRun=true;
    
    std::vector<double> C, mu, U;
    Kmean_engine kmean;
    
    kmean(O, M, C, mu, U, false); //Occupancy rates,mean,var 
    //prn(mu);
    //randomize_vector(mu,R,generator);
    //randomize_vector(U,R,generator);
    //prn(mu);
    hmm1.B.setB(N, M, C, mu, U); 
    //firstRun=false;
}

}


#endif
