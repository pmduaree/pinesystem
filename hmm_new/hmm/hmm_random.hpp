#ifndef __HMM_RANDOM__
#define __HMM_RANDOM__

#include <string>
#include <boost/lexical_cast.hpp>

#include "util/random.hpp"

#include "hmm_data.hpp"
#include "kmean_engine.hpp"
#include "gmm.hpp"

#include "morana/tensor.hpp"
#include "morana/array_algebra.hpp"
#include "morana/tensor_algebra.hpp"


template<size_t N>
void make_stochastic_tensor(morana::tensor<double,N>& A, size_t leg)
{
    if (A.empty()) return;
    if (leg==N-1) {
        std::uniform_real_distribution<double> distribution(0,1.0);
        for(double& a: A) a = distribution(randomGenerator());
        size_t nj = A.M(N-1);
        size_t ldj = A.size() / nj;

        for(size_t j=0; j < nj ; ++j) {
            auto* zbeg = A.data() + j * ldj;
            double mysum = 0.0;
            for(auto it = zbeg ; it != zbeg + ldj ; ++it) mysum += *it;
            for(auto it = zbeg ; it != zbeg + ldj ; ++it) (*it) *= 1.0 / mysum;
        }
    } else {
        std::array<size_t,1> L; L[0] = leg;
        auto P = morana::concatenate(morana::complement(morana::range<N>(),L), L);
        auto P_shape = morana::transpose(A.shape(), P);
        auto P_inv = morana::inverse_permutation(P);
        morana::tensor<double,N> B(P_shape);
        make_stochastic_tensor(B, N-1);
        transpose(B, P_inv, A);
    }
}



namespace HMM {

/// /////////////////////////////////////////////////////////////////////////
/// RANDOMIZATION OF HMM
/// /////////////////////////////////////////////////////////////////////////
// specialize this function in hmm_bjgm and similar
template<class BB>
class RandomHMM_B
{
  public:
    void operator()(size_t N, size_t M, BB& B);
};

//randomization of all common attributes:: Pi and A
template<class BB>
class RandomHMM
{
  RandomHMM_B<BB> rndB;
  public:
    void operator()(size_t N, size_t M, HMM_data<BB>& data)
    {
        data.Pi.resize(N);
        data.A.resize(morana::make_array<size_t,2>(N,N));
        make_stochastic_vector(data.Pi);
        make_stochastic_tensor(data.A, 0);
        BB myB(N,M);
        rndB(N, M, myB);
        data.B = myB;
    }
};

template<class BB>
void generateRandomHMM(size_t N,size_t M, HMM_data<BB>& retHMM)
{
    RandomHMM<BB> randomizer;
    randomizer(N, M, retHMM);
}



/// ////////////////////////////////////////////
/// B altering functions, like kmeans ...
/// ////////////////////////////////////////////
//B randimozation altering, like Kmeans or some other similar algorithm
template<class HMMtype> 
void pbUniform(HMMtype& HMM, const std::vector<double>& O,size_t N,size_t M) 
{
	auto mm = std::minmax_element(O.begin(), O.end());
	double min=*mm.first + (*mm.second-*mm.first)*0.5/M;
	double max=*mm.second- (*mm.second-*mm.first)*0.5/M;
	std::vector<double> mu;
	std::vector<double> u;
	std::vector<double> C;
	mu.resize(M);
	u.resize(M);
	C.resize(M);

	for(auto i=0;i<M;++i){
		mu[i]=min + (max-min)/(double)(M-1)*(double)i;
		u[i]=0.5*(max-min)/M;
		C[i]=1.0/M;
	}
	HMM.B.setB(N, M, C, mu, u); 
}

template<class HMMtype> 
void pbKmeans(HMMtype& HMM, const std::vector<double>& O,size_t N,size_t M)
{
	kmeansInitHMM(HMM,O,N,M);
}

template<class HMMtype, class dType = double> 
void pbNull(HMMtype& HMM, const std::vector<dType>& O,size_t N,size_t M) {

}

template<class HMMtype, class dType> 
void pbDiscreteNormal(HMMtype& hmm, const std::vector<dType>& O, size_t N, size_t M) {
    hmm.B.B.resize(morana::make_array<size_t,2>(N,M));
    double sd = M* 1.3/N;
    for(size_t j=0;j<N;++j) {
        double mu = M*(0.5+j)/N;
        // it's centered around mu.
        double s = 0.0;
        for(size_t m=0;m<M;++m) {
            hmm.B.B(j,m) = exp(- (m-mu)*(m-mu)/(2*sd*sd) );
            s += hmm.B(j,m);
            //hmm.B.B(j,m) = 1.0/M;
        }
        s = 1.0 / s;
        for(size_t m=0;m<M;++m) hmm.B.B(j,m) *= s;
    }
}

template<class HMMtype> 
void pbGmm(HMMtype& predictor, const std::vector<double>& x, size_t N, size_t M) {
    // call estimation-maximization algorithm for gmm fitting
    ExpectationMaximizationGmm em;
    // ExpectationMaximizationGmm is a general class (knows nothing about HMMs) and requires 
    // the mode of access of mean vector/variance vector (will change into covariance matrix in 
    // the future)/coefficient vector to be defined
	std::vector<double> tempC(M, 0);
    em.init(M, 
        [&predictor](size_t i)->double& {return predictor.B.mu[i];},
        [&predictor](size_t i)->double& {return predictor.B.U[i];},
//        [&predictor](size_t i)->double& {return predictor.B.C(0, i);}
		[&tempC](size_t i)->double& {return tempC[i];}
        );
        
    em.initialGuess(x);
    em.perform(x, 50);
    
    for (size_t n = 0; n < N; ++n) {
		for (size_t m = 0; m < M; ++m) {
			predictor.B.C(n, m) *= tempC[m];
		}
	}
}

template<class HMMtype> 
struct pbFixed4 {
	double muOffs = 0.005;
	double uAll = 0.0025;
	
	pbFixed4() {}
	pbFixed4(double mu, double u) : muOffs(mu), uAll(u) {}
    pbFixed4(const std::string& p)
    {
        if (p.find(':') != std::string::npos) {
            muOffs = boost::lexical_cast<double>(p.substr(0,p.find(':')));
            uAll = boost::lexical_cast<double>(p.substr(p.find(':')+1));
        }
    }
	
	void operator() (HMMtype& HMM, const std::vector<double>& O,size_t N,size_t M) {
		if (M != 4 && M != 3) throw std::runtime_error("M != 4 and M != 3");
		std::vector<double> mu;
		std::vector<double> u;
		std::vector<double> C;
		mu.resize(M);
		u.resize(M);
		C.resize(M);
        
		mu[0]=-muOffs;
		u[0]=uAll;
		
		mu[1]=muOffs;
		u[1]=uAll;
		
		mu[2]=0;
		u[2]=uAll/2;
		
        if (M==4) {
            mu[3]=0;
            u[3]=muOffs+uAll;
        }
		
		for(auto i=0;i<M;++i) {
			C[i]=1.0/M;
		}
		
		HMM.B.setB(N, M, C, mu, u); 
	}
};

template<class HMMtype> 
void pbFixed4Gmm(HMMtype& HMM, const std::vector<double>& O,size_t N,size_t M) {
	if (M != 4) throw std::runtime_error("M != 4");
	
	// gaussian #3 is fitted on the data as a single function
	// there is a better way to do this, but ...; fit gmm(M=1), 
    // then push the fitted params to the 
	//   end of the vectors, making room for fitting gmm(M=3)
	pbGmm(HMM, O, N, 1);
	HMM.B.mu[3] = HMM.B.mu[0];
	HMM.B.U[3] = HMM.B.U[0];
	for (size_t n = 0; n < N; ++n) {
		HMM.B.C(n, 3) = HMM.B.C(n, 0);
	}
	
	// gaussians #0,#1,#2 are fitted on the data as a mixture
	pbGmm(HMM, O, N, M-1);
}
} // end of namespace HMM

#endif
