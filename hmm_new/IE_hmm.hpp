#ifndef hmm_strategy_engine
#define hmm_strategy_engine

#include "strategy_engine.hpp"

class Strategy_hmm : public StrategyEngine
{
  public:
    Strategy_hmm() : StrategyEngine() {}
    virtual ~Strategy_hmm() {}
    
    virtual SignalType
    calculate(const CandleSeries& C1min, const std::string& param, const tiempo::datetime& date);
};

#endif

