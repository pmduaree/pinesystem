
#ifndef optimOBJ
#define optimOBJ
#include "optimization_base_object.hpp"
#include "util/blas.hpp"

void normalize_vector(std::vector<double>& v)
{
    double mysum = sum(v.begin(), v.end());
    double f = 1.0 / mysum;
    for(auto& x : v) x *= f;
}


//Class for Iztok
template<class ext>
class sigOptim : public optimObjBase{
	typedef sigOptim TT; 
	ext* extClass;
    size_t n_params, n_nonzeros;
    std::set<size_t> nonzeros;
public:
	///PARAMETERs AND CRITERIA definition
	enum CriteriaName{R,S,endCrit};
	//enum ParamName{X,Y,endParam};
	sigOptim(ext& input_, size_t numParams, size_t numNonzeros)
        : n_params(numParams), n_nonzeros(numNonzeros)
    {
		extClass=&input_;
		params.resize(numParams);	//define number of parameters, in simple case we use only two;
		criteria.resize(endCrit);

        init();
	}

    void init()
    {
        nonzeros.clear();
        auto& gen = randomGenerator();
        std::uniform_int_distribution<size_t> distribution(0,n_params-1);
        while(nonzeros.size() < n_nonzeros) {
            size_t strategy_id = distribution(gen);
            if (strategy_id >= n_params) continue;
            nonzeros.insert(strategy_id);
        }
    }
    
	/// CRITERIA COMPUTATION
	void evaluate(){
		criteria[R]=extClass->operator()(params);
        //std::cout << "Evaluated criteria: " << criteria[R] << std::endl;
	}

    void make_random_W(std::vector<double>& Wj)
    {
        assert( !nonzeros.empty() );
        auto& gen = randomGenerator();
        std::uniform_real_distribution<> unigen(0,1);
        Wj.resize(n_params);
        std::fill(Wj.begin(), Wj.end(), 0.0);
        double wsum = 0.0;
        for(const auto& strategy_idx : nonzeros) {
            auto& w = Wj[strategy_idx];
            w = unigen(gen);
            wsum += w;
        }
        for(auto& z : Wj) z *= (1.0 / wsum);
    }


	/// RANDOMIZATION POLICY
	std::vector<double> newParameters(){
		std::vector<double> ret;
		ret.resize(params.size());
        make_random_W(ret);
		return ret;
	}
	///IS BETTER OPERATOR
	//is better operator A<B = true means B is dominant regarding A
	//define what means better!
	bool operator <(const TT& B){
		//maximization 
		if(criteria[R]<=B.criteria[R]) return true;
		return false;
	}
};

/// EXAMPLES
/// Simple test functions 
/// ///////////////////////////////////////////////////////////////////////////
class trivial2D : public optimObjBase{
	typedef trivial2D TT; 
public:
	///PARAMETERs AND CRITERIA definition
	enum CriteriaName{R1,endCrit};
	enum ParamName{X,Y,endParam};
	trivial2D(){
		params.resize(endParam);	//define number of parameters, in simple case we use only two;
		criteria.resize(endCrit);
	}
	/// CRITERIA COMPUTATION
	void evaluate(){
		criteria[R1]=(params[X]-0.5)*(params[X] -0.5) + (params[Y]-0.5)*(params[Y] -0.5) ; 
	}
	/// RANDOMIZATION POLICY
	std::vector<double> newParameters(){
		std::vector<double> ret;
		ret.resize(params.size());
		//make_stochastic_vector(ret); 
		make_stochastic_scalar(ret[X],0,2.5);
		make_stochastic_scalar(ret[Y],0,1.5);
		return ret;
	}
	///IS BETTER OPERATOR
	//is better operator A<B = true means B is dominant regarding A 
	bool operator <(const TT& B){
		if(criteria[R1]>B.criteria[R1]) return true; //minimzation example, we minimize R1
		return false;
	}
};

//standard optimization test : http://en.wikipedia.org/wiki/Test_functions_for_optimization
class CeXp : public optimObjBase{
	typedef CeXp TT; 
public:
	///PARAMETERs AND CRITERIA definition
	enum CriteriaName{R1,R2,endCrit};
	enum ParamName{X,Y,endParam};
	CeXp(){
		params.resize(endParam);	//define number of parameters, in simple case we use only two;
		criteria.resize(endCrit);
	}
	/// CRITERIA COMPUTATION
	void evaluate(){
		criteria[R1]=params[X];
		//criteria[R2]=(1+params[Y])*exp(-params[X]/(1+params[Y]));
		criteria[R2]=(1+params[Y])/(params[X]);
	}
	/// RANDOMIZATION POLICY :: including ranges
	bool parameterConstrainCheck(std::vector<double>& I){
		if((I[Y]+9*I[X])>=6 && (-I[Y]+9*I[X])>=1) return true;
		return false;
	}
	std::vector<double> newParameters(){
		std::vector<double> ret;
		ret.resize(params.size());
		//make_stochastic_vector(ret,0.4,0.5);
		while(!parameterConstrainCheck(ret)){
			make_stochastic_scalar(ret[X],0.1,1);
			make_stochastic_scalar(ret[Y],0.0,5);
		}
		return ret;
	}
	///IS BETTER OPERATOR
	//is better operator A<B = true means B is dominant regarding A 
	bool operator <(const TT& B){
		//> minimization //< maximization
		if(criteria[R1]>B.criteria[R1] && criteria[R2]>B.criteria[R2]) return true; //minimzation example, we minimize R1
		return false;
	}
};

#endif
