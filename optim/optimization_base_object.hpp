
#ifndef optimOBJBase
#define optimOBJBase
#include "util/random.hpp"
#include "util/common.hpp"

/// optimBase
/// ///////////////////////////////////////////////////////////////////////////
class optimObjBase{
protected:
	std::vector<double> params;
	std::vector<double> criteria;
	
    virtual void init() = 0;
	virtual void evaluate ()=0 ;		//evaluates optimObject :: computes cost functions
	virtual std::vector<double> newParameters ()=0;
	///IO functions
public:
	optimObjBase(){};
	///evaluate operator
	void operator ()(){					//evaluation 
		std::fill(criteria.begin(), criteria.end(), 0);
		std::fill(params.begin(), params.end(), 0);

        init();

		//TODO::here we could supply randomization engine as a parameter of base class
		params=newParameters();
		evaluate();
		dominant=true;
	}
	///move operator 
	//we need that for random walk local optimization :: again depends on randomization
	void move(double howFar){
		//move current solution towards a new randomly generated one
        auto newp = newParameters();
        for(size_t i=0;i<params.size();++i) {
            params[i] += (newp[i] - params[i]) * howFar;
        }
		//params=params+(newParameters()-params)*howFar;
		evaluate();

	}
	///IO
	std::vector<double> getParams() const {return params;}
	std::vector<double> getCriteria() const {return criteria;}
	bool dominant;
};
//template<class T>
std::ostream& operator<<(std::ostream& xx,const optimObjBase& arr){
    std::stringstream str;
    // do it like the matlab does it.
    str << "[{";
	str << arr.getParams();
	str << "};{";
	str << arr.getCriteria();
	str << "};";
	str << arr.dominant;
	str << "]";
    xx << str.str();
	return xx;
}


#endif
