#ifndef	optimEngin
#define optimEngin

#include "optimization_user_objects.hpp"

/// GLOBAL OPTIMIZATION ENGINE
//template<class localMinimizationEngine>
class optimizationEngine
{
  public:
    optimizationEngine(){};
    template<class objToOptim,class localOptimClass>
    void generatePopulation(localOptimClass localOptim,std::vector<objToOptim>& pF, 
                            objToOptim& P0, size_t num, size_t convCheck, bool storeHistory=false)
    {
        //pF optimization front, maxNum MC trials limit, convergence: after how many steps without better solution we stop MC		
        //convCheck how many global MC fails to perform before giveup 
		P0();
		pF.push_back(P0);
		size_t MCcheck=0; //MC fails counter
		for(auto i=0;i<num;++i){
            //std::cout << "Global optimization step " << i << std::endl;
			if(MCcheck>convCheck) {std::cout<<"No better solution after "<<MCcheck<<" trials,bailing out"<<std::endl; 
				return;}
			P0(); //evaluation, randomization ...
			MCcheck++;
            if (!is_dominant(P0, pF)) continue;
            //std::cout << "Local optimization..." << std::endl;
            localOptim.minimize(P0); //local optimization
            //std::cout << "End of local optimization" << std::endl;
			///checking if new guy is better :: goes through current front
			bool addP0 = check_if_add(P0, pF, false);
			if(addP0) {
				MCcheck=0;
				P0.dominant=true;
				pF.push_back(P0);
			}
		}
		std::cout<<"MC reached maximal allowed trials: "<<num<<", bailing out"<<std::endl;
	}	

    template<class T>
    bool is_dominant(T& P, std::vector<T>& pF)
    {
        bool bdominant = false;
        auto iter = pF.begin();
        while(iter!=pF.end()){
            //ignore peons in the population
            if(!(iter->dominant)) {++iter;continue;}
            if (P < *iter) {
                bdominant = false;
                break;
            } else if (*iter < P) {
                bdominant = true;
                break;
            } else if (!(*iter<P && P<*iter)) {
                bdominant = true;
                break;
            }
            ++iter;
        }
        return bdominant;

    }

    template<typename T>
    bool check_if_add(T& P, std::vector<T>& pF, bool readonly=true)
    {
        bool addP0=false;
        auto iter = pF.begin();
        while(iter!=pF.end()){
            //ignore peons in the population
            if(!(iter->dominant)) {++iter;continue;}		
            //if there is somebody dominant to a new guy, a new guy is peaon
            if(P<*iter) {addP0=false;break;}				
            //new guy dominates!
            if(*iter<P) {
                //std::cout << "New guy is better" << std::endl;
                addP0=true;
                if (!readonly) iter = pF.erase(iter);
                //if(storeHistory) iter->dominant=false; else iter=pF.erase(iter);
                continue;
            } 
            //new guy represent a new dominant solution in a pareto front
            if(!(*iter<P && P<*iter)) {addP0=true;++iter;}
        }
        return addP0;
    }

};

/// LOCAL OPTIMIZATION ENGINEs
//monte carlo markov chain
class MCMC{
	size_t numOfSteps;
	size_t numOfCoolings;
	double range;
public:
	MCMC(size_t NOT,size_t NOC,double RNG){
		numOfSteps=NOT;
		numOfCoolings=NOC;
		range=RNG;
		std::cout<<"using MCMC::simulated annealing class with "<<" numOfSteps: " 
			<< numOfSteps << " numOfCoolings: " << numOfCoolings<<std::endl;
	}
	template<class obj> 
    void minimize(obj& P){
        //std::ofstream outFile ("outputs/pop.m", std::ios::out);
        //std::vector<obj> out;
        obj newGuy(P);
        //anneling
        for(auto ii=1;ii<numOfCoolings+1;++ii){
            //spancir
            for(auto i=0;i<numOfSteps;++i){
                    newGuy.move(range/(double)(ii));
                    //prn(range/(double)ii);
                    if(P<newGuy) {
                        std::cout << "Replacing with: " << newGuy.getCriteria() << std::endl;
                        P=newGuy;
                    }
                    //out.push_back(P);	
            }
        }
        //outFile<<"P="<<out<<";";
        //outFile.close();
	}
};

/// dummy classes
class noLocalOptim{
public:
	noLocalOptim(){
		std::cout<<"using noLocalOptim class"<<std::endl;
	};
	template<class obj>
	void const minimize(const obj& P){}
};

#endif
