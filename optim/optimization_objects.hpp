#ifndef optimOBJ
#define optimOBJ

#include <cmath> 
#include "util/common.hpp"
#include "morana/io.hpp"

/// optimBase
/// ///////////////////////////////////////////////////////////////////////////
class optimObjBase{
protected:
	std::vector<double> params;
	std::vector<double> criteria;
	
	virtual void evaluate ()=0 ;		//evaluates optimObject :: computes cost functions
	virtual std::vector<double> updateParameters ()=0;
	///IO functions
public:
	optimObjBase(){};
	///evaluate operator
	void operator ()(){					//evaluation 
		std::fill(criteria.begin(), criteria.end(), 0);
		std::fill(params.begin(), params.end(), 0);
		//TODO::here we could supply randomization engine as a parameter of base class
		params=updateParameters();
		evaluate();
		dominant=true;
	}
	///move operator 
	//we need that for random walk local optimization :: again depends on randomization
	void move(double howFar){
        auto Z = updateParameters();
        const auto foo = (howFar*1.0*random_sign());
        for(size_t i=0;i<params.size();++i) params[i] += foo * Z[i];
	}
	///IO
	std::vector<double> getParams() const {return params;}
	std::vector<double> getCriteria() const {return criteria;}
	bool dominant;
};
//template<class T>
std::ostream& operator<<(std::ostream& xx,const optimObjBase& arr){
    std::stringstream str;
    // do it like the matlab does it.
    str << "[{";
	str << arr.getParams();
	str << "};{";
	str << arr.getCriteria();
	str << "};";
	str << arr.dominant;
	str << "]";
    xx << str.str();
	return xx;
}
/// ///////////////////////////////////////////////////////////////////////////
/// end of Base
/// ///////////////////////////////////////////////////////////////////////////
/// Simple test functions 
/// ///////////////////////////////////////////////////////////////////////////
class trivial2D : public optimObjBase{
	typedef trivial2D TT; 
public:
	///PARAMETERs AND CRITERIA definition
	enum CriteriaName{R1,endCrit};
	enum ParamName{X,Y,endParam};
	trivial2D(){
		params.resize(endParam);	//define number of parameters, in simple case we use only two;
		criteria.resize(endCrit);
	}
	/// CRITERIA COMPUTATION
	void evaluate(){
		criteria[R1]=(params[X]-0.5)*(params[X] -0.5) + (params[Y]-0.5)*(params[Y] -0.5) ; 
	}
	/// RANDOMIZATION POLICY
	std::vector<double> updateParameters(){
		std::vector<double> ret;
		ret.resize(params.size());
		make_stochastic_vector(ret); 
		return ret;
	}
	///IS BETTER OPERATOR
	//is better operator A<B = true means B is dominant regarding A 
	bool operator <(const TT& B){
		if(criteria[R1]>B.criteria[R1]) return true; //minimzation example, we minimize R1
		return false;
	}
};

//standard optimization test : http://en.wikipedia.org/wiki/Test_functions_for_optimization
class CeXp : public optimObjBase{
	typedef CeXp TT; 
public:
	///PARAMETERs AND CRITERIA definition
	enum CriteriaName{R1,R2,endCrit};
	enum ParamName{X,Y,endParam};
	CeXp(){
		params.resize(endParam);	//define number of parameters, in simple case we use only two;
		criteria.resize(endCrit);
	}
	/// CRITERIA COMPUTATION
	void evaluate(){
		criteria[R1]=params[X];
		//criteria[R2]=(1+params[Y])*exp(-params[X]/(1+params[Y]));
		criteria[R2]=(1+params[Y])/(params[X]);
	}
	/// RANDOMIZATION POLICY :: including ranges
	bool parameterConstrainCheck(std::vector<double>& I){
		if((I[Y]+9*I[X])>=6 && (-I[Y]+9*I[X])>=1) return true;
		return false;
	}
	std::vector<double> updateParameters(){
		std::vector<double> ret;
		ret.resize(params.size());
		//make_stochastic_vector(ret,0.4,0.5);
		while(!parameterConstrainCheck(ret)){
			make_stochastic_scalar(ret[X],0.1,1);
			make_stochastic_scalar(ret[Y],0.0,5);
		}
		return ret;
	}
	///IS BETTER OPERATOR
	//is better operator A<B = true means B is dominant regarding A 
	bool operator <(const TT& B){
		//> minimization //< maximization
		if(criteria[R1]>B.criteria[R1] && criteria[R2]>B.criteria[R2]) return true; //minimzation example, we minimize R1
		return false;
	}
};




#endif
