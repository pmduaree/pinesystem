#include <iostream>

#include "hurst.hpp"

#include "sql/sql_rawdata.hpp"
#include "util/prog_opts.hpp"


int main(int argc, char** argv)
{
    prog_opts parser;
    parser.add_option("-i", "--instrument", OptionType::Integer, "instrument_id", 0);
    parser.add_option("-d", "--date", OptionType::String, "date", "now");
    if (!parser.parse_args(argc, argv) || parser.option<bool>("help")) {
        std::cout << parser.help() << std::endl;
        return 0;
    }

    int instrument_id = parser.option<int>("instrument_id");
    tiempo::datetime_type on_date(parser.option<>("date"));

    // get all the data we have.
    
    sql_db db;
    sql_conn conn(db, "tix");

    sql_rawdata rawdata(conn);

    tiempo::datetime_type date1 = on_date - std::chrono::hours(24)*7;

    auto tix = rawdata.get_series<double>(instrument_id, 1, "close", date1, on_date);
    if (tix.empty() ) {
        std::cout << "NO data.\n";
        return 0;
    }
    std::cout << "tix range["<<tix.size() << "]: " << tix.begin()->first << " to " << tix.rbegin()->first << std::endl;

    on_date = tix.rbegin()->first;

    Hurst hurst;
    double hex = hurst(tix, on_date);
    std::cout << "hex: " << on_date << " : " << hex << std::endl;




    return 0;
}
