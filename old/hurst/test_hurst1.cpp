#include <iostream>
#include <map>
#include <array>

#include <boost/lexical_cast.hpp>

#include "util/date_time.hpp"
#include "util/read_data.hpp"
#include "util/data_series.hpp"
#include "util/time_series.hpp"


#include "hurst.hpp"



int main()
{
    std::string instrumentname = "/BRN";
    std::string exchange = "IPE";
    int freq = 1;
    tiempo::duration quant(freq*60);
    tiempo::datetime_type date_to("now");
    tiempo::datetime_type date_from = date_to - 10000 * quant;
    time_series<double> s_C;
    if (true) {
        std::cout << "Getting data..." << std::endl;
        // read data from teh internet
        data_series data;
        read_data_from_net(instrumentname, exchange, date_from, date_to, 
                           freq, data);
        std::cout << "items: " << data.items.size() << std::endl;
        int idx_D = data.sub_index("TRADE_DATETIME");
        int idx_C = data.sub_index("CLOSE");
        for(const auto& x : data.items) {
            tiempo::datetime_type date(x[idx_D]);
            s_C[date] = boost::lexical_cast<double>(x[idx_C]);
        }
        std::cout << "HLC: " << s_C.size() << std::endl;
    }
    if (s_C.empty()) { return 0; }

    Hurst hurst;
    auto last_date = s_C.rbegin()->first;
    std::cout << "Calculating Hurst exponent on date " << last_date << std::endl;
    double hurst_exp = hurst(s_C, last_date);
    std::cout << "hurst exponent: " << hurst_exp << std::endl;
    return 0;
}
