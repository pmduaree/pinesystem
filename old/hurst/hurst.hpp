#ifndef __hurst_module__
#define __hurst_module__
#include <map>
#include <vector>
#include <cmath>
#include "util/tiempo.hpp"
#include "util/time_series.hpp"
#include "util/ticks.hpp"

class Hurst
{
  public:
    Hurst() { }
    
    // input:
    //  closing price for 1 minute interval
    // output:
    //  hurst exponent for the 'last date' (assumed to be in the dataset)
    double operator()(const time_series<double>& P, 
                      const tiempo::datetime_type& date) const
    {
        assert( P.get_period() == tiempo::duration(60) );
        if (P.empty()) return 0;
        
        tiempo::datetime_type date2 = date - tiempo::duration(60);
        tiempo::datetime_type date1 = date2 - tiempo::duration(6*24*3600);
        std::vector<double> prices;
        auto it = P.rbegin();
        while(it != P.rend() && it->first > date2) ++it;
        if (it == P.rend()) return 0;
        while(it != P.rend()) {
            if (it->first < date1) break;
            prices.push_back(it->second);
            ++it;
        }
        size_t Nn = prices.size();
        size_t n = Nn;
        
        std::map<double, double> Rser;
        while( n > 30) {
            //std::cout << "for n = " << n << std::endl;
            double rr = 0;
            size_t cc = 0;
            for(size_t i=0;i<(Nn/n);++i) {
                auto iz1 = prices.begin() + i * n;
                auto iz2 = prices.begin() + (i+1)*n;
                double rescaled_range = analyze_partial_series(iz1,iz2);
                rr += rescaled_range;
                ++cc;
            }
            rr *= 1.0 / cc;
            Rser[n*1.0] = rr;
            n  = static_cast<size_t>(n / 1.3);
        }
        if (Rser.size() < 3) return 0.0;
        
        // the range_series corresponds to 
        // E[ R(n) / S(n) ] vs n.
        // and we want to have
        // E[ R(n) / S(n) ] = C n^H.
        // so we fit log(E[..]) = log(C) + H log(n) with a straight line
        // which gives us the hurst exponent
        double C, hurst;
        std::tie(C, hurst) = hurst_fit(Rser);
        return hurst;
    }
    
  private:
    std::tuple<double,double>
    hurst_fit(const std::map<double,double>& data) const
    {
        if (data.size() <= 1) return std::make_tuple(0.0,0.0);
        double mx=0, my=0;
        size_t c=0;
        // mx = E[ log(X) ]
        // my = E[ log(Y) ]
        for(const auto& r : data) {
            double x = log(r.first);
            double y = log(r.second);
            mx += x;
            my += y;
            ++c;
        }
        assert( c > 1);
        mx *= 1.0 / c;
        my *= 1.0 / c;
        // Var(X) = E[ (log(X) - mx)^2 ]
        // Cov(X,Y) = E[ (log(X) - mx) * (log(Y) - my) ]
        double varx = 0.0;
        double cov = 0.0;
        for(const auto& r : data)
        {
            double x = log(r.first);
            double y = log(r.second);
            varx += (x-mx) * (x-mx);
            cov += (x-mx) * (y-my);
        }
        // beta = Cov(X,Y) / Var(X)
        varx *= 1.0 / (c-1);
        cov *= 1.0 / (c-1);
        double beta = cov / varx;
        // alpha = E[Y] - beta * E[x]
        double alpha = my - beta * mx;
        return std::make_tuple(alpha, beta);
    }
    
    template<class Iter>
    double analyze_partial_series(Iter beg, Iter end) const
    {
        double m = 0.0;
        size_t c = 0;
        for(auto it = beg; it != end; ++it) {
            m += *it;
            ++c;
        }
        if (c == 0) return 0;
        m *= 1.0 / c;
        double var = 0.0;
        double Zmax = -1e100;
        double Zmin =  1e100;
        double Zt=0;
        for(auto it = beg; it != end ; ++it) {
            double v = *it;
            double Yt = v - m;
            var += Yt * Yt;
            Zt += Yt;
            Zmax = std::max(Zmax, Zt);
            Zmin = std::min(Zmin, Zt);
        }
        double stdev = sqrt(var / c);
        if (stdev == 0.0) return 0.0;
        double Rn = Zmax - Zmin;
        return Rn / stdev;
    }
};

#endif
