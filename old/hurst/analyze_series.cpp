#include <iostream>
#include "hurst.hpp"
#include "util/time_series.hpp"
#include "util/textfile_io.hpp"
#include "util/prog_opts.hpp"

int main(int argc, char** argv)
{
    prog_opts opts;
    opts.add_option("-i", "--input", OptionType::String, "fname", "");
    opts.add_option("-f", "--format", OptionType::String, "format", "");
    if (!opts.parse_args(argc, argv) || opts.option<bool>("help")) {
        std::cout << opts.help() << std::endl;
        return 0;
    }
    std::string fname = opts.option<>("fname");
    std::string format = opts.option<>("format");
    if (fname.empty() || format.empty()) {
        std::cout << "analyze_series.exe -i {fname} -f {format}\n";
        return 0;
    }
    time_series<double> P;
    read_data_from_file(fname, format, P);
    Hurst hurst;
    auto it = P.begin();
    while(it != P.end() && 
          it->first < P.begin()->first + std::chrono::hours(24*6) ) ++it;
    while(it != P.end() ) {
        const auto& date = it->first;
        double hex = hurst(P, date);
        std::cout << date.to_string("%Y-%m-%d %H:%M:%S") << " " << hex << std::endl;
        ++it;
    }
    return 0;
}
