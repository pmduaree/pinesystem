#include <iostream>
#include "date_time.hpp"


int main()
{
    tiempo::duration freq(60*10);
    tiempo::datetime_type date1("2014-03-04 15:00:00");
    tiempo::datetime_type date2("2014-03-04 15:10:00");
    tiempo::datetime_type date3("2014-03-04 15:11:00");
    tiempo::datetime_type date4("2014-03-04 15:10:20");
    tiempo::datetime_type date5("2014-03-04 15:11:20");

    std::cout << ((date1.time_of_day().seconds() % freq.seconds()) == 0) << std::endl;
    std::cout << ((date2.time_of_day().seconds() % freq.seconds()) == 0) << std::endl;
    std::cout << ((date3.time_of_day().seconds() % freq.seconds()) == 0) << std::endl;
    std::cout << ((date4.time_of_day().seconds() % freq.seconds()) == 0) << std::endl;
    std::cout << ((date5.time_of_day().seconds() % freq.seconds()) == 0) << std::endl;

    std::cout << "---------\n";
    std::cout << date1.is_div(freq) << std::endl;
    std::cout << date2.is_div(freq) << std::endl;
    std::cout << date3.is_div(freq) << std::endl;
    std::cout << date4.is_div(freq) << std::endl;
    std::cout << date5.is_div(freq) << std::endl;

    return 0;
}
