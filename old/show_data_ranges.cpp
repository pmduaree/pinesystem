#include <iostream>
#include "sql_connection.hpp"
#include "sql_rawdata.hpp"
#include "sql_instruments.hpp"
#include "util/prog_opts.hpp"
#include "util/date_time.hpp"

class Process
{
    std::shared_ptr<sql_connection> conn;
    sql_rawdata rawdata;
    sql_instruments instruments;
    
  public:
    Process()
        : conn(std::make_shared<sql_connection>() ),
          rawdata(conn->connection()), 
          instruments(conn->connection())
    {
    }
    
    void print_range(int instrument_id, int freq)
    {
        raw_data_item first, last;
        bool has_first = rawdata.get_first(instrument_id, freq, first);
        //bool has_last = rawdata.get_last(instrument_id, freq, last);

        if (!has_first) return;

        std::cout << boost::format("Instrument [id=%2d] @ freq=%2d: %s --> %s")
                    % instrument_id % freq 
                    % first.date.to_string()
                    % last.date.to_string() << std::endl;
    }


    void run(int instrument_id, int freq)
    {
        if (instrument_id)
            print_range(instrument_id, freq);
        else {
            std::vector<int> IDs; 
            instruments.get_IDs(IDs);
            for(int iid: IDs) {
                print_range(iid, freq);
            }
        }
    }

};

int main(int argc, char** argv)
{
    prog_opts parser;
    parser.add_option("-i", "--instrument", OptionType::Integer, "instrument_id", 0);
    parser.add_option("-q", "--freq", OptionType::Integer, "freq", 1);
    if (!parser.parse_args(argc, argv) || parser.option<bool>("help")) {
        std::cerr << parser.help() << std::endl;
        return 0;
    }

    int instrument_id = parser.option<int>("instrument_id");
    int freq = parser.option<int>("freq");
    
    Process process;
    process.run(instrument_id, freq);



    return 0;
}
