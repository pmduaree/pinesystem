#include <iostream>
#include "prices.hpp"


int main()
{
    std::cout << "We are in.\n";

    tix_series tix;


    tiempo::datetime_type date1("2014-03-10");
    tiempo::datetime_type date2("2014-03-20");

    size_t i = 1;
    for(tiempo::datetime_type date = date1 ; date < date2 ; date += std::chrono::hours(1) ) {
        tix[date] = TickType(i *1.0, i);
        ++i;
        std::cout << date << ": " << tix[date].p << std::endl;
    }

    tiempo::datetime_type date_1("2014-03-15 14:59:00");
    tiempo::datetime_type date_2("2014-03-15 15:00:00");
    tiempo::datetime_type date_3("2014-03-15 15:01:00");

    std::cout << "exact price: " << tix.find(date_2)->second.p << std::endl;
    std::cout << "price 1: " << tix.get_price(date_1) << std::endl;
    std::cout << "price 2: " << tix.get_price(date_2) << std::endl;
    std::cout << "price 3: " << tix.get_price(date_3) << std::endl;
    
    return 0;
}

