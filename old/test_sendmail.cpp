#include <iostream>
#include <boost/format.hpp>
#include "mail_sender.hpp"

#include "date_time.hpp"

int main()
{
    MailSender mailer;
    std::string message = (boost::format("Test at %s: UJust some test really") % tiempo::now().to_string() ).str();
    mailer.send_message("pizorn@gmail.com","Just a test", message);
    return 0;
}
