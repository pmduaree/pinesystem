#include <iostream>
#include "date_time.hpp"


int main()
{
    tiempo::datetime_type d("2014-04-21 04:38:12");
    std::cout << "d: " << d << std::endl;
    int of = 3;
    std::cout << "x[60]: " << offset_date(d, 60, of) << std::endl;
    std::cout << "x[20]: " << offset_date(d, 20, of) << std::endl;
    std::cout << "x[10]: " << offset_date(d, 10, of) << std::endl;
    std::cout << "x[5]: " << offset_date(d, 5, of) << std::endl;
    return 0;

}
