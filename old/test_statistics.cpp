#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

#include "statistics.hpp"


int main()
{
    std::vector<double> a, b;
    size_t n = 300;
    double mu = 0.0;
    double sigma = 1.0;
    
    std::default_random_engine gen;
    std::normal_distribution<double> dist(mu,sigma);
    auto gen_normal = [&dist,&gen]() { return dist(gen);};
    
    a.resize(n);
    b.resize(n);

    for(auto i=0;i<n;++i) {
        a[i] = 1.0 * i + 0.5 * gen_normal();
        b[i] = 2.0 * i + 1.0 * gen_normal()+ 3.0;
    }

    
    double m_a = mean(a);
    std::cout << "mean(a) = " << m_a << std::endl;
    double v_a = variance(a);
    double s_a = sqrt(v_a);
    double c_aa = covariance(a,a);
    std::cout << "var(a) = " << v_a << std::endl;
    std::cout << "stdev(a) = " << s_a << std::endl;
    std::cout << "cov(a,a) = " << c_aa << std::endl;
    
    double c_ab = covariance(a,b);
    double v_b = variance(b);
    
    std::cout << "var(b) = " << v_b << std::endl;
    std::cout << "cov(a,b) = " << c_ab << std::endl;
    std::cout << "rho(a,b) = " << c_ab / sqrt(v_a*v_b) << std::endl;
    
    double z0,z1;
    std::tie(z0,z1) = OLS(b, a);
    std::cout << "z0,z1: " << z0 <<","<<z1<<std::endl;
    std::tie(z0,z1) = OLS(b, a, 0.0);
    std::cout << "z0,z1: " << z0 <<","<<z1<<std::endl;
    return 0;
}
